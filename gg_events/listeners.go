package gg_events

import (
	"reflect"
	"sync"
)

//----------------------------------------------------------------------------------------------------------------------
//	EmitterListeners
//----------------------------------------------------------------------------------------------------------------------

type EmitterListeners struct {
	emitter   *Emitter
	mux       *sync.RWMutex
	listeners map[string][]EventCallback
}

func NewListeners(emitter *Emitter) (instance *EmitterListeners) {
	instance = new(EmitterListeners)
	instance.emitter = emitter
	instance.mux = new(sync.RWMutex)
	instance.listeners = make(map[string][]EventCallback)
	return
}

func (instance *EmitterListeners) GetListeners() (response map[string][]EventCallback) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		response = make(map[string][]EventCallback)
		for k, v := range instance.listeners {
			response[k] = v
		}
	}
	return
}

func (instance *EmitterListeners) Has(eventName string) bool {
	if nil != instance {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		uid := eventName
		if _, b := instance.listeners[uid]; b {
			return len(instance.listeners[uid]) > 0
		}
	}
	return false
}

func (instance *EmitterListeners) On(eventName string, callback EventCallback) (response map[string][]EventCallback) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		uid := eventName // Events.EventId(instance.emitter, eventName)
		instance.listeners[uid] = append(instance.listeners[uid], callback)
	}
	return
}

func (instance *EmitterListeners) Off(eventName string, callback ...func(event *Event)) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		uid := eventName // Events.EventId(instance.emitter, eventName)
		if _, ok := instance.listeners[uid]; ok {
			if len(callback) == 0 {
				instance.listeners[uid] = make([]EventCallback, 0)
			} else {
				handlers := instance.listeners[uid]
				toRemove := make([]int, 0)
				// loop starting from end
				for i := 0; i < len(handlers); i++ {
					f := handlers[i]
					for _, h := range callback {
						v1 := reflect.ValueOf(f)
						v2 := reflect.ValueOf(h)
						if v1.Pointer() == v2.Pointer() {
							toRemove = append(toRemove, i)
						}
					}

				}
				if len(toRemove) > 0 {
					handlers = removeIndexes(handlers, toRemove)
					instance.listeners[uid] = handlers
				}
			}
		}
	}
}

func (instance *EmitterListeners) Clear() {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		// reset listeners
		instance.listeners = make(map[string][]EventCallback)
	}
}
