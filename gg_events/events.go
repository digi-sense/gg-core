package gg_events

import (
	"fmt"
	"time"
)

type EventsHelper struct {
}

var Events *EventsHelper

func init() {
	Events = new(EventsHelper)
}

func (instance *EventsHelper) NewEmitter(payload ...interface{}) *Emitter {
	emitter := NewEmitterInstance(0, payload...)
	return emitter
}

func (instance *EventsHelper) NewDebounceEmitter(waitTime time.Duration, payload ...interface{}) *Emitter {
	emitter := NewEmitterInstance(waitTime, payload...)
	return emitter
}

func (instance *EventsHelper) TopicId(sender interface{}, eventName string) string {
	if nil == sender {
		return eventName
	}
	return fmt.Sprintf("%p.%s", sender, eventName)
}
