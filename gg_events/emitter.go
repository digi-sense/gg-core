package gg_events

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"sync"
	"time"
)

type EventCallback func(event *Event)

type stackItem struct {
	event    *Event
	callback EventCallback
}

//----------------------------------------------------------------------------------------------------------------------
//	Emitter
//----------------------------------------------------------------------------------------------------------------------

type Emitter struct {
	waitTime  time.Duration
	debounces map[string]*Debouncer
	listeners *EmitterListeners
	mux       *sync.RWMutex
	payload   interface{}
}

func NewEmitterInstance(waitTime time.Duration, payload ...interface{}) (instance *Emitter) {
	instance = new(Emitter)
	instance.mux = &sync.RWMutex{}
	instance.waitTime = waitTime
	instance.listeners = NewListeners(instance)
	if waitTime > 0 {
		instance.debounces = make(map[string]*Debouncer)
	}
	if len(payload) == 1 {
		instance.payload = payload[0]
	} else {
		instance.payload = payload
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Emitter) Clear() {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if nil != instance.listeners {
			instance.listeners.Clear()
		}
		instance.debounces = make(map[string]*Debouncer)
	}
}

// Debounce transform a standard emitter into a debounced one
func (instance *Emitter) Debounce(waitTime time.Duration) *Emitter {
	if nil != instance {
		instance.waitTime = waitTime
		if nil == instance.debounces {
			instance.debounces = make(map[string]*Debouncer)
		}
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	e v e n t s
//----------------------------------------------------------------------------------------------------------------------

func (instance *Emitter) Has(eventName string) bool {
	if nil != instance && nil != instance.listeners {
		return instance.listeners.Has(eventName)
	}
	return false
}

func (instance *Emitter) On(eventName string, callback func(event *Event)) *Emitter {
	if nil != instance && nil != instance.listeners {
		instance.listeners.On(eventName, callback)
	}
	return instance
}

func (instance *Emitter) Off(eventName string, callback ...func(event *Event)) *Emitter {
	if nil != instance && nil != instance.listeners {
		instance.listeners.Off(eventName, callback...)
	}
	return instance
}

// Emit @Deprecated use Trigger method instead
func (instance *Emitter) Emit(eventName string, args ...interface{}) *Emitter {
	return instance.Trigger(false, eventName, args...)
}

func (instance *Emitter) EmitOnce(eventName string, args ...interface{}) *Emitter {
	return instance.TriggerOnce(false, eventName, args...)
}

// EmitAsync @Deprecated use Trigger method instead
func (instance *Emitter) EmitAsync(eventName string, args ...interface{}) *Emitter {
	return instance.Trigger(true, eventName, args...)
}

func (instance *Emitter) EmitAsyncOnce(eventName string, args ...interface{}) *Emitter {
	return instance.TriggerOnce(true, eventName, args...)
}

// Trigger emit an event synch or async
func (instance *Emitter) Trigger(async bool, eventName string, args ...interface{}) *Emitter {
	if nil != instance {
		instance.trigger(async, false, eventName, args...)
	}
	return instance
}

func (instance *Emitter) TriggerOnce(async bool, eventName string, args ...interface{}) *Emitter {
	if nil != instance {
		instance.trigger(async, true, eventName, args...)
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	q u e u e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Emitter) Subscribe(channel interface{}, topic string, callback func(event *Event)) *Emitter {
	if nil != instance && nil != instance.listeners {
		uid := Events.TopicId(channel, topic)
		instance.listeners.On(uid, callback)
	}
	return instance
}

func (instance *Emitter) Unsubscribe(channel interface{}, topic string, callback ...func(event *Event)) *Emitter {
	if nil != instance && nil != instance.listeners {
		uid := Events.TopicId(channel, topic)
		instance.listeners.Off(uid, callback...)
	}
	return instance
}

func (instance *Emitter) Publish(channel interface{}, async, once bool, topic string, args ...interface{}) *Emitter {
	if nil != instance {
		uid := Events.TopicId(channel, topic)
		instance.trigger(async, once, uid, args...)
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Emitter) trigger(async, remove bool, eventName string, args ...interface{}) {
	if nil != instance {
		if nil != instance.debounces {
			instance.debounce(eventName, async, remove, args...)
		} else {
			instance.emit(eventName, async, remove, args...)
		}
	}
}

func (instance *Emitter) removeDebounce(item *Debouncer) {
	if nil != instance && nil != instance.debounces && nil != item {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		delete(instance.debounces, item.key)
	}
}

func (instance *Emitter) debounce(eventName string, async, remove bool, args ...interface{}) *Emitter {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		key := buildKey(eventName, async)

		if _, ok := instance.debounces[key]; !ok {
			instance.debounces[key] = new(Debouncer)
		}
		instance.debounces[key].Init(instance, instance.waitTime, eventName, async, remove, args...)
	}
	return instance
}

func (instance *Emitter) emit(eventName string, async, remove bool, args ...interface{}) {
	if nil != instance {
		defer func() {
			if r := recover(); r != nil {
				// recovered from panic
				message := gg_utils.Strings.Format("Emit '%s' ERROR: %s", eventName, r)
				fmt.Println(message)
			}
		}()

		instance.mux.RLock()
		defer instance.mux.RUnlock()

		// creates internal execution stack
		listeners := instance.listeners.GetListeners()
		stack := make([]*stackItem, 0)
		for k, handlers := range listeners {
			if match(eventName, k) {
				// if k == eventName || k == "*" || k == "" {
				for _, handler := range handlers {
					if nil != handler {
						event := NewEvent(async, eventName, instance.payload, args...)
						item := &stackItem{
							event:    event,
							callback: handler,
						}
						stack = append(stack, item)
					}
				}
			}
		}

		if remove {
			offAll(instance.listeners, stack)
		}

		if len(stack) > 0 {
			if async {
				go rawEmit(stack)
			} else {
				rawEmit(stack)
			}
		}
	}
}

func match(event string, listener string) bool {
	if listener == event || listener == "*" || listener == "" {
		return true
	}
	var eChannel, eTopic string
	tokens := gg_utils.Strings.SplitFirst(event, '.')
	if len(tokens) == 2 {
		eChannel = tokens[0]
		eTopic = tokens[1]
	} else {
		eChannel = ""
		eTopic = tokens[0]
	}
	var lChannel, lTopic string
	tokens = gg_utils.Strings.SplitFirst(listener, '.')
	if len(tokens) == 2 {
		lChannel = tokens[0]
		lTopic = tokens[1]
	} else {
		lChannel = ""
		lTopic = tokens[0]
	}
	if len(eChannel) == 0 {
		// EMITTED for all channels
		return eTopic == lTopic || lTopic == "*"
	} else {
		if len(lChannel) == 0 {
			// LISTEN to channels on the same topic
			return eTopic == lTopic || lTopic == "*"
		}
		return eChannel == lChannel && (eTopic == lTopic || lTopic == "*")
	}
}

func offAll(listeners *EmitterListeners, stack []*stackItem) {
	if nil != listeners && nil != stack {
		for _, item := range stack {
			if nil != item && nil != item.event && nil != item.callback {
				listeners.Off(item.event.Name, item.callback)
			}
		}
	}
}

func rawEmit(stack []*stackItem) {
	if nil != stack {
		for _, item := range stack {
			if nil != item && nil != item.event && nil != item.callback {
				item.callback(item.event)
			}
		}
	}
}

func removeIndexes(a []EventCallback, indexes []int) []EventCallback {
	response := make([]EventCallback, 0)
	for i, handler := range a {
		if gg_utils.Arrays.IndexOf(i, indexes) == -1 {
			response = append(response, handler)
		}
	}
	return response
}

func buildKey(eventName string, async bool) string {
	return fmt.Sprintf("%s-%v", eventName, async)
}
