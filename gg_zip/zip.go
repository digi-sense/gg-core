package gg_zip

import (
	"archive/zip"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

type ZipHelper struct {
}

var Zip *ZipHelper

func init() {
	Zip = new(ZipHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// Unzip extracts files from a zip archive.
// Param 1: src is the source zip file's path.
// Param 2: dest is the destination directory to extract files to.
// Returns: a slice of filenames that were extracted and any encountered error.
func (instance *ZipHelper) Unzip(src string, dest string) (filenames []string, err error) {
	r, err := zip.OpenReader(src)
	if err != nil {
		return
	}
	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fPath := filepath.Join(dest, f.Name)

		if !strings.HasPrefix(fPath, filepath.Clean(dest)+string(os.PathSeparator)) {
			err = fmt.Errorf("%s: illegal file path", fPath)
			return
		}

		filenames = append(filenames, fPath)

		if f.FileInfo().IsDir() {
			// Make Folder
			_ = os.MkdirAll(fPath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fPath), os.ModePerm); err != nil {
			return
		}

		var outFile *os.File
		outFile, err = os.OpenFile(fPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return
		}

		var rc io.ReadCloser
		rc, err = f.Open()
		if err != nil {
			return
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		_ = outFile.Close()
		_ = rc.Close()

		if err != nil {
			return
		}
	} // for file
	return
}

// ZipFiles compresses one or many files into a single zip archive file.
// Param 1: filename is the output zip file's name.
// Param 2: files is a list of files to add to the zip.
func (instance *ZipHelper) ZipFiles(filename string, files []string) (err error) {
	return zipFilesFromRoot(filename, files, "")
}

// ZipFilesFromRoot compresses multiple files into a single zip archive file from a specified root directory.
// Root path is not preserved.
// Param 1: filename is the name of the output zip archive file.
// Param 2: files is a slice of file paths to include in the zip archive.
// Param 3: root denotes the root directory to be removed from the zip archive.
func (instance *ZipHelper) ZipFilesFromRoot(filename string, files []string, root string) (err error) {
	return zipFilesFromRoot(filename, files, root)
}

// AddFileToZip adds a file to an existing zip archive.
// Param 1: zipWriter is the writer for the zip archive.
// Param 2: filename is the name of the file to be added to the zip.
func (instance *ZipHelper) AddFileToZip(zipWriter *zip.Writer, filename string) error {
	return addFileToZip(zipWriter, filename, "")
}

func (instance *ZipHelper) GZip(data []byte) (response []byte, err error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	if _, err = gz.Write(data); err != nil {
		return
	}
	if err = gz.Close(); err != nil {
		return
	}
	response = b.Bytes()
	return
}

func (instance *ZipHelper) GUnzip(data []byte) (response []byte, err error) {
	reader := bytes.NewReader(data)
	var gzreader *gzip.Reader
	gzreader, err = gzip.NewReader(reader)
	if nil == err {
		response, err = io.ReadAll(gzreader)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func zipFilesFromRoot(filename string, files []string, root string) (err error) {

	var newZipFile *os.File
	newZipFile, err = os.Create(filename)
	if err != nil {
		return err
	}
	defer newZipFile.Close()

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

	// Add files to zip
	for _, file := range files {
		if err = addFileToZip(zipWriter, file, root); err != nil {
			return
		}
	}
	return
}

func addFileToZip(zipWriter *zip.Writer, filename string, removeRoot string) error {

	fileToZip, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer fileToZip.Close()

	// Get the file information
	info, err := fileToZip.Stat()
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}

	// Using FileInfoHeader() above only uses the basename of the file. If we want
	// to preserve the folder structure we can overwrite this with the full path.
	if len(removeRoot) == 0 {
		header.Name = filename
	} else {
		header.Name = strings.Replace(filename, removeRoot, "", 1)
	}

	// Change to deflate to gain better compression
	// see http://golang.org/pkg/archive/zip/#pkg-constants
	header.Method = zip.Deflate

	writer, err := zipWriter.CreateHeader(header)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, fileToZip)
	return err
}
