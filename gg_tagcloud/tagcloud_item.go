package gg_tagcloud

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
)

//----------------------------------------------------------------------------------------------------------------------
//	TagCloudItem
//----------------------------------------------------------------------------------------------------------------------

type TagCloudItem struct {
	Name  string     `json:"name"`
	Count int        `json:"count"`
	Score float64    `json:"score"`
	Tags  []*TagItem `json:"tags"`
}

func (instance *TagCloudItem) String() string {
	return gg_json.JSON.Stringify(instance)
}

func (instance *TagCloudItem) Map() map[string]interface{} {
	if nil != instance {
		return gg_utils.Convert.ToMap(instance.String())
	}
	return map[string]interface{}{}
}

//----------------------------------------------------------------------------------------------------------------------
//	TagItem
//----------------------------------------------------------------------------------------------------------------------

type TagItem struct {
	Name    string                 `json:"name"`
	Payload map[string]interface{} `json:"payload"`
}
