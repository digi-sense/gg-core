package gg_tagcloud

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_math"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"math"
	"sort"
	"strings"
	"sync"
)

var TagCloud *TagCloudHelper

func init() {
	TagCloud = NewTagCloudHelper()
}

//----------------------------------------------------------------------------------------------------------------------
//	const
//----------------------------------------------------------------------------------------------------------------------

const (
	OutPatternNameScore       = "{{ .name }} [{{ .score }}%]"
	OutPatternScoreName       = "[{{ .score }}%] {{ .name }}"
	OutPatternNameColumnScore = "{{ .name }}: {{ .score }}%"
)

//----------------------------------------------------------------------------------------------------------------------
//	TagCloudHelper
//----------------------------------------------------------------------------------------------------------------------

type TagCloudHelper struct {
}

func NewTagCloudHelper() (instance *TagCloudHelper) {
	instance = new(TagCloudHelper)
	return
}

func (instance *TagCloudHelper) New(args ...interface{}) (response *TagCloudList) {
	response = NewTagCloudList(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	TagCloudList
//----------------------------------------------------------------------------------------------------------------------

type TagCloudList struct {
	mux  *sync.RWMutex
	list []*TagCloudItem
}

func NewTagCloudList(args ...interface{}) (instance *TagCloudList) {
	instance = new(TagCloudList)
	instance.list = make([]*TagCloudItem, 0)
	instance.mux = new(sync.RWMutex)
	instance.init(args...)
	return
}

func (instance *TagCloudList) String() string {
	if nil != instance {
		return gg_.Stringify(instance.list)
	}
	return ""
}

func (instance *TagCloudList) Clone() (response *TagCloudList) {
	if nil != instance {
		response = NewTagCloudList()
		response.list = instance.Items()
	}
	return
}

func (instance *TagCloudList) Add(tags ...interface{}) *TagCloudList {
	if nil != instance && len(tags) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.add(tags...)
		instance.sortDesc()
	}
	return instance
}

func (instance *TagCloudList) Recalculate() *TagCloudList {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.recalculate()
	}
	return instance
}

func (instance *TagCloudList) SortAsc() *TagCloudList {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.sortAsc()
	}
	return instance
}

func (instance *TagCloudList) SortDesc() *TagCloudList {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.sortDesc()
	}
	return instance
}

func (instance *TagCloudList) MaxScore() float64 {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		return maxScore(instance.list)
	}
	return 0.0
}

func (instance *TagCloudList) MinScore() float64 {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		return minScore(instance.list)
	}
	return 0.0
}

func (instance *TagCloudList) MaxCount() int {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		return maxCount(instance.list)
	}
	return 0.0
}

func (instance *TagCloudList) MinCount() int {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		return minCount(instance.list)
	}
	return 0.0
}

func (instance *TagCloudList) Items() (response []*TagCloudItem) {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		response = make([]*TagCloudItem, 0)
		_ = gg_json.JSON.Read(instance.String(), &response)
	}
	return
}

func (instance *TagCloudList) GroupItems(groups int, force bool) (response [][]*TagCloudItem) {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		response = instance.groupItems(groups, force)
	}
	return
}

func (instance *TagCloudList) GroupItemsNoEmpty(desiredNumberOfClusters int) (response [][]*TagCloudItem) {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		groups := instance.groupItems(desiredNumberOfClusters, false)
		return instance.Shrink(groups)
	}
	return
}

func (instance *TagCloudList) GroupItemsAsc(groups int, force bool) (response [][]*TagCloudItem) {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		response = instance.groupItems(groups, force)
		sortAscGroups(response)
	}
	return
}

func (instance *TagCloudList) GroupItemsDesc(groups int, force bool) (response [][]*TagCloudItem) {
	if nil != instance && len(instance.list) > 0 {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		response = instance.groupItems(groups, force)
		sortDescGroups(response)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	u t i l i t y
//----------------------------------------------------------------------------------------------------------------------

// SortDescGroups sorts the given list of tag cloud item groups in descending order.
// It first checks if the TagCloudList instance is not nil and if there are non-empty groups in the list.
// Then it calls the sortDescGroups function to perform the sorting operation.
// Signature: func (instance *TagCloudList) SortDescGroups(groups [][]*TagCloudItem)
func (instance *TagCloudList) SortDescGroups(groups [][]*TagCloudItem) {
	if nil != instance && len(groups) > 0 {
		sortDescGroups(groups)
	}
}

// SortAscGroups sorts the given list of tag cloud item groups in ascending order.
// It first checks if the TagCloudList instance is not nil and if there are non-empty groups in the list.
// Then it calls the sortAscGroups function to perform the sorting operation.
// Signature: func (instance *TagCloudList) SortAscGroups(groups [][]*TagCloudItem)
func (instance *TagCloudList) SortAscGroups(groups [][]*TagCloudItem) {
	if nil != instance && len(groups) > 0 {
		sortAscGroups(groups)
	}
}

// Shrink removes empty groups from the given list of groups and returns the updated list.
// It checks if the TagCloudList is not nil and if there are non-empty groups in the list.
// It appends non-empty groups to the `response` list and returns it.
// Signature: func (instance *TagCloudList) Shrink(groups [][]*TagCloudItem) (response [][]*TagCloudItem)
func (instance *TagCloudList) Shrink(groups [][]*TagCloudItem) (response [][]*TagCloudItem) {
	if nil != instance && len(groups) > 0 {
		for _, group := range groups {
			if len(group) > 0 {
				response = append(response, group)
			}
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n v e r s i o n s
//----------------------------------------------------------------------------------------------------------------------

// ItemsAsStrings converts the list of TagCloudItems into a list of strings.
// It takes an optional optOutPattern string as a parameter, which specifies the format of the converted strings.
// If the optOutPattern is empty, OutPatternNameScore will be used as the default pattern.
// The method creates a slice of formatted values using the optOutPattern.
// Finally, the method returns the response slice.
// Signature: func (instance *TagCloudList) ItemsAsStrings(optOutPattern string) (response []string)
func (instance *TagCloudList) ItemsAsStrings(optOutPattern string) (response []string) {
	if nil != instance {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		response = make([]string, 0)
		if len(optOutPattern) == 0 {
			optOutPattern = OutPatternNameScore
		}
		instance.sortDesc()
		payload := make(map[string]interface{})
		for _, tag := range instance.list {
			payload["name"] = tag.Name
			payload["score"] = tag.Score
			text, err := gg_utils.Formatter.MergeText(optOutPattern, payload)
			if nil == err {
				response = append(response, text)
			} else {
				response = append(response, err.Error())
			}
		}
	}
	return
}

// ItemsAsMap returns a map representation of the tag cloud items in the TagCloudList instance.
// It iterates over each tag in the instance list and adds the tag's Name as the key and Score as the value to the map.
// Finally, it returns the map 'response'.
// Signature: func (instance *TagCloudList) ItemsAsMap() (response map[string]interface{})
func (instance *TagCloudList) ItemsAsMap() (response map[string]interface{}) {
	if nil != instance {
		response = make(map[string]interface{})
		for _, tag := range instance.list {
			response[tag.Name] = tag.Score
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	f o r m a t s
//----------------------------------------------------------------------------------------------------------------------

// FormatText returns a formatted string with each item's text in the TagCloudList instance.
// The method calls the ItemsAsStrings method
// passing the optOutPattern parameter to get the list of strings for each item in the TagCloudList.
// Signature: func (instance *TagCloudList) FormatText(optOutPattern string) (response string)
func (instance *TagCloudList) FormatText(optOutPattern string) (response string) {
	if nil != instance {
		rows := instance.ItemsAsStrings(optOutPattern)
		sb := new(strings.Builder)
		for _, row := range rows {
			sb.WriteString(row + "\n")
		}
		response = sb.String()
	}
	return
}

// FormatJSON returns a string representation of the JSON format for the list of tag cloud items.
// It calls the ItemsAsMap function to retrieve the items as a map.
// Signature: func (instance *TagCloudList) FormatJSON() (response string)
func (instance *TagCloudList) FormatJSON() (response string) {
	if nil != instance {
		response = gg_.Stringify(instance.ItemsAsMap())
	}
	return
}

func (instance *TagCloudList) FormatMarkDown(optTitles ...string) (response string) {
	if nil != instance {
		title := "You have '%v' cluster of tags."
		subTitle := "Tags: '%v'"
		if len(optTitles) == 1 {
			title = optTitles[0]
		}
		if len(optTitles) == 2 {
			title = optTitles[0]
			subTitle = optTitles[1]
		}
		// create a group of tags
		groups := instance.Shrink(instance.GroupItems(5, false))
		if len(groups) > 0 {
			sb := new(strings.Builder)
			sb.WriteString(fmt.Sprintf("# "+title+" \n", len(groups)))
			for _, group := range groups {
				sb.WriteString(fmt.Sprintf("## "+subTitle+" \n", len(group)))
				for _, tag := range group {
					sb.WriteString(fmt.Sprintf(" - %s (%v%%) \n", tag.Name, tag.Score))
				}
			}
			response = sb.String()
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TagCloudList) init(args ...interface{}) {
	if nil != instance {
		for _, arg := range args {
			if s, ok := arg.(string); ok {
				instance.add(s)
				continue
			}
		}
		instance.sortDesc()
	}
}

func (instance *TagCloudList) add(tags ...interface{}) {
	if nil != instance && len(tags) > 0 {
		for _, item := range tags {
			tag := toTag(item)
			if nil == tag || len(tag.Name) == 0 {
				continue
			}

			added := false
			// search existing
			for _, tagCloudItem := range instance.list {
				if tagCloudItem.Name == tag.Name {
					tagCloudItem.Tags = append(tagCloudItem.Tags, tag)
					tagCloudItem.Count++
					added = true
					break
				}
			}
			if added {
				continue
			}
			// new tag
			instance.list = append(instance.list, &TagCloudItem{
				Name:  tag.Name,
				Count: 1,
				Score: 0,
				Tags:  []*TagItem{tag},
			})
		}
		instance.recalculate()
	}
}

func (instance *TagCloudList) recalculate() {
	if nil != instance && len(instance.list) > 0 {
		t := total(instance.list)
		for _, tagCloudItem := range instance.list {
			tagCloudItem.Score = gg_math.Math.Round(float64(tagCloudItem.Count)/t*100, 2)
		}
	}
}

func (instance *TagCloudList) sortAsc() {
	if nil != instance {
		sortAsc(instance.list)
	}
}

func (instance *TagCloudList) sortDesc() {
	if nil != instance {
		sortDesc(instance.list)
	}
}

func (instance *TagCloudList) groupItems(groups int, force bool) (response [][]*TagCloudItem) {
	if nil != instance && len(instance.list) > 0 {
		response = groupItems(groups, instance.list, force)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func groupItems(numClusters int, list []*TagCloudItem, force bool) (response [][]*TagCloudItem) {
	if len(list) > 0 {
		response = make([][]*TagCloudItem, numClusters)

		// sort internal values from smaller to grater
		sortAsc(list)

		// ensure we do not have more clusters that items in list
		if numClusters > len(list) {
			numClusters = len(list)
		}

		if force {
			// calculate ideal size of each list in cluster
			idealSize := float64(len(list)) / float64(numClusters)
			start := 0
			for i := 0; i < numClusters; i++ {
				end := int(math.Round(float64(i+1) * idealSize))

				// be sure the last cluster contains all remaining numbers
				if i == numClusters-1 {
					end = len(list)
				}

				// ensure each cluster has at least one element
				if end <= start {
					end = start + 1
				}

				response[i] = list[start:end]
				start = end
			}
		} else {
			mx := float64(maxCount(list))
			mi := float64(minCount(list))
			clusterRange := (mx - mi) / float64(numClusters)
			// distribute tags in clusters
			for _, tag := range list {
				clusterIndex := int(math.Floor(float64(tag.Count)-mi) / clusterRange)
				// handle limit of the array
				if clusterIndex == numClusters {
					clusterIndex--
				}
				response[clusterIndex] = append(response[clusterIndex], tag)
			}
		}

		// sort from greater to smaller
		sortDescGroups(response)
	}
	return
}

func enumEmpty(array [][]*TagCloudItem) []int {
	response := make([]int, 0)
	if len(array) > 0 {
		for i := 0; i < len(array); i++ {
			if len(array[i]) == 0 {
				response = append(response, i)
			}
		}
	}
	return response
}

func maxScore(list []*TagCloudItem) (response float64) {
	response = 0.0
	for _, tag := range list {
		if tag.Score > response {
			response = tag.Score
		}
	}
	return
}

func minScore(list []*TagCloudItem) (response float64) {
	response = 200.0
	for _, tag := range list {
		if tag.Score < response {
			response = tag.Score
		}
	}
	return
}

func maxCount(list []*TagCloudItem) (response int) {
	response = 0
	for _, tag := range list {
		if tag.Count > response {
			response = tag.Count
		}
	}
	return
}

func minCount(list []*TagCloudItem) (response int) {
	response = math.MaxInt
	for _, tag := range list {
		if tag.Count < response {
			response = tag.Count
		}
	}
	return
}

func total(list []*TagCloudItem) (response float64) {
	response = 0.0
	for _, tag := range list {
		response += float64(tag.Count)
	}
	return
}

func sortAsc(list []*TagCloudItem) {
	if len(list) > 0 {
		sort.Slice(list, func(i, j int) bool {
			return list[i].Score < list[j].Score
		})
	}
}

func sortDesc(list []*TagCloudItem) {
	if len(list) > 0 {
		sort.Slice(list, func(i, j int) bool {
			return list[i].Score > list[j].Score
		})
	}
}

func sortDescGroups(groups [][]*TagCloudItem) {
	if len(groups) > 0 {
		sort.Slice(groups, func(i, j int) bool {
			if len(groups[i]) > 0 && len(groups[j]) > 0 {
				return groups[i][0].Score > groups[j][0].Score
			}
			if len(groups[i]) > 0 {
				return true
			}
			return false
		})
		for i := 0; i < len(groups); i++ {
			sortDesc(groups[i])
		}
	}
}

func sortAscGroups(groups [][]*TagCloudItem) {
	if len(groups) > 0 {
		sort.Slice(groups, func(i, j int) bool {
			if len(groups[i]) > 0 && len(groups[j]) > 0 {
				return groups[i][0].Score < groups[j][0].Score
			}
			if len(groups[i]) > 0 {
				return false
			}
			return true
		})
		for i := 0; i < len(groups); i++ {
			sortAsc(groups[i])
		}
	}
}

func toTag(item interface{}) (response *TagItem) {
	if nil != item {
		if t, ok := item.(*TagItem); ok {
			response = t
			return
		}
		if s, ok := item.(string); ok {
			response = &TagItem{
				Name: s,
			}
			return
		}
		if m, ok := item.(map[string]interface{}); ok {
			response = &TagItem{}
			response.Name = gg_utils.Maps.GetString(m, "name")
			if payload, ok := m["payload"]; ok {
				response.Payload = gg_utils.Convert.ToMap(payload)
			} else {
				response.Payload = make(map[string]interface{})
				for key, value := range m {
					if key != "name" {
						response.Payload[key] = value
					}
				}
			}
		}
	}
	return
}
