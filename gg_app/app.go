package gg_app

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_observable"
	"bitbucket.org/digi-sense/gg-core/gg_stoppable"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"time"
)

var App *AppCtrlHelper

func init() {
	App = new(AppCtrlHelper)
}

type AppCtrlHelper struct {
}

func (helper *AppCtrlHelper) New(mode, dirWork string) (instance *AppCtrl, err error) {
	instance, err = NewAppCtrl(mode, dirWork)
	return
}

func (helper *AppCtrlHelper) Main() (ctrl *AppCtrl, err error) {
	ctrl, err = Main()
	return
}

const (
	EventOnStartApp = gg_.EventOnStartApp
	EventOnCloseApp = gg_.EventOnCloseApp
)

//----------------------------------------------------------------------------------------------------------------------
//	IAppLaunchCtrl, IAppCtrl
//----------------------------------------------------------------------------------------------------------------------

type IAppLaunchCtrl interface {
	Start() (err error)
	Stop()
	Join()
}

type IAppCtrl interface {
	IAppLaunchCtrl
	Events() *gg_events.Emitter
	Logger() gg_.ILogger
}

//----------------------------------------------------------------------------------------------------------------------
//	AppCtrl
//----------------------------------------------------------------------------------------------------------------------

type AppCtrl struct {
	mode    string
	dirWork string

	state        *gg_observable.ObservableObj
	info         *ApplicationInfo
	stoppable    *gg_stoppable.Stoppable
	events       *gg_events.Emitter
	loggerRotate bool
	loggerLevel  string
	loggerFile   string

	handled bool // flag on initHandlers

	__logger gg_.ILogger
}

func NewAppCtrl(mode, dirWork string) (instance *AppCtrl, err error) {
	instance = new(AppCtrl)
	instance.mode = mode
	instance.dirWork = dirWork
	instance.loggerRotate = false
	if mode == "debug" {
		instance.loggerLevel = mode
	} else {
		instance.loggerLevel = "info"
	}

	err = instance.init()

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *AppCtrl) String() string {
	if nil != instance {
		return instance.info.String()
	}
	return ""
}

func (instance *AppCtrl) SetAppName(value string) {
	if nil != instance {
		gg_.AppName = value
		if nil != instance.info {
			instance.info.AppName = value
		}
		if nil != instance.stoppable {
			instance.stoppable.SetName(value)
		}
	}
}

func (instance *AppCtrl) SetAppVersion(value string) {
	if nil != instance {
		gg_.AppVersion = value
		if nil != instance.info {
			instance.info.AppVersion = value
		}
	}
}

func (instance *AppCtrl) SetAppSalt(value string) {
	if nil != instance {
		gg_.AppSalt = value
		if nil != instance.info {
			instance.info.AppSalt = value
		}
	}
}

func (instance *AppCtrl) SetEvents(value *gg_events.Emitter) {
	if nil != instance {
		instance.events = value
	}
}

func (instance *AppCtrl) SetLogger(value gg_.ILogger) {
	if nil != instance {
		instance.__logger = value
	}
}

func (instance *AppCtrl) SetLogRotation(value bool) {
	if nil != instance {
		if logger, ok := instance.__logger.(*gg_log.Logger); ok {
			logger.RotateEnable(value)
		} else {
			instance.loggerRotate = value
		}
	}
}

func (instance *AppCtrl) SetLogLevel(value string) {
	if nil != instance {
		if logger, ok := instance.__logger.(*gg_log.Logger); ok {
			logger.SetLevel(value)
		} else {
			instance.loggerLevel = value
		}
	}
}

func (instance *AppCtrl) SetLogFilename(value string) {
	if nil != instance {
		instance.loggerFile = value
		instance.__logger = nil // remove logger
	}
}

func (instance *AppCtrl) Info() *ApplicationInfo {
	if nil != instance {
		return instance.info
	}
	return nil
}

func (instance *AppCtrl) WorkspaceDir() string {
	if nil != instance {
		return instance.dirWork
	}
	return ""
}

func (instance *AppCtrl) WorkspacePath(path string) string {
	if nil != instance {
		return gg_utils.Paths.Absolutize(path, instance.dirWork)
	}
	return path
}

func (instance *AppCtrl) Events() *gg_events.Emitter {
	if nil != instance {
		return instance.events
	}
	return nil
}

func (instance *AppCtrl) Logger() gg_.ILogger {
	if nil != instance {
		return instance.logger()
	}
	return nil
}

// State returns the App "observable" state.
func (instance *AppCtrl) State() *gg_observable.ObservableObj {
	if nil != instance {
		return instance.state
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	s t o p p a b l e
//----------------------------------------------------------------------------------------------------------------------

func (instance *AppCtrl) SetStopTimeout(value time.Duration) {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.SetTimeout(value)
	}
}

func (instance *AppCtrl) SetStopSleep(value time.Duration) {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.SetSleep(value)
	}
}

func (instance *AppCtrl) AddStopOperation(name string, callback gg_stoppable.ShutdownCallback) *AppCtrl {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.AddStopOperation(name, callback)
	}
	return instance
}

func (instance *AppCtrl) OnStart(f func()) {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.OnStart(f)
	}
}

func (instance *AppCtrl) OnStop(f func()) {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.OnStop(f)
	}
}

func (instance *AppCtrl) Start() (err error) {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Start()
	}
	return
}

func (instance *AppCtrl) Stop() {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Stop()
	}
}

func (instance *AppCtrl) Join() {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Join()
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	d e p l o y
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppCtrl) DeployIntoWorkspace(path string, tpl string, overwrite bool) (err error) {
	filename := instance.WorkspacePath(path)
	err = instance.Deploy(filename, tpl, overwrite)
	return
}

func (instance *AppCtrl) Deploy(filename string, text string, overwrite bool) (err error) {
	err = gg_utils.Paths.Mkdir(filename)
	if nil == err {
		var exists bool
		exists, err = gg_utils.Paths.Exists(filename)
		if overwrite || !exists {
			_, err = gg_utils.IO.WriteTextToFile(text, filename)
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	dir watchdog
//----------------------------------------------------------------------------------------------------------------------

// AddPathWatchdog activate an observer over a path directory
func (instance *AppCtrl) AddPathWatchdog(path string, callback func()) {
	if nil != instance {
		path = instance.WorkspacePath(path)

	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *AppCtrl) init() (err error) {
	if nil != instance {
		instance.state = gg_observable.NewObservable()
		instance.state.SetAsync(true)

		// set the path of workspace
		instance.dirWork = gg_utils.Paths.Absolutize(instance.dirWork, gg_utils.Paths.Absolute("."))
		err = gg_utils.Paths.Mkdir(instance.dirWork)
		if nil != err {
			return
		}

		instance.info = NewApplicationInfo(instance.dirWork)
		instance.info.Mode = instance.mode

		// stoppable
		instance.stoppable = gg_stoppable.NewStoppable()
		instance.stoppable.SetName(instance.info.AppName)
		instance.stoppable.SetLogger(instance.Logger)
		instance.stoppable.OnStart(instance.onStart)
		instance.stoppable.OnStop(instance.onStop)

		// events
		instance.events = gg_events.NewEmitterInstance(0)
	}
	return
}

func (instance *AppCtrl) logger() (response gg_.ILogger) {
	if nil != instance {
		if nil == instance.__logger {
			// create new logger
			if len(instance.loggerFile) == 0 {
				instance.loggerFile = instance.WorkspacePath("./logging/logging.log")
			} else {
				instance.loggerFile = instance.WorkspacePath(instance.loggerFile)
			}
			if instance.loggerRotate {
				instance.__logger = gg_log.Log.New(instance.loggerLevel, instance.loggerFile)
			} else {
				instance.__logger = gg_log.Log.NewNoRotate(instance.loggerLevel, instance.loggerFile)
			}
		}
		response = instance.__logger
	}
	return
}

func (instance *AppCtrl) onStart() {
	if nil != instance {
		logger := instance.Logger()
		if nil != logger {
			info := instance.info.PrintInfo()
			logger.Info(info)
		}
		if nil != instance.events {
			instance.events.Trigger(true, EventOnStartApp, instance)
		}
	}
}

func (instance *AppCtrl) onStop() {
	if nil != instance {
		if nil != instance.events {
			instance.events.Trigger(true, EventOnCloseApp, instance)
		}
	}
}
