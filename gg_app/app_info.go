package gg_app

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_license/license_commons"
	"bitbucket.org/digi-sense/gg-core/gg_sys"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"mime"
	"strings"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ApplicationInfo
// ---------------------------------------------------------------------------------------------------------------------

type ApplicationInfoError struct {
	Context string `json:"context"`
	Error   string `json:"error"`
}

type ApplicationInfo struct {
	MachineId      string                   `json:"machine-id"`
	AppName        string                   `json:"app-name"`
	AppVersion     string                   `json:"app-version"`
	AppSalt        string                   `json:"app-salt"`
	MimeJS         string                   `json:"mime-js"`
	MimeCSS        string                   `json:"mime-css"`
	MimeHTML       string                   `json:"mime-html"`
	MimeJSON       string                   `json:"mime-json"`
	StartTime      time.Time                `json:"start-time"`
	Mode           string                   `json:"running-mode"`
	UIMode         string                   `json:"ui-mode"`
	Root           string                   `json:"root"`
	LaunchDir      string                   `json:"launch-dir"`
	SettingsFile   string                   `json:"settings-file"`
	SettingsLoaded bool                     `json:"settings-loaded"`
	ProcessPID     int                      `json:"process-pid"`
	UserHome       string                   `json:"user-home"`
	UserName       string                   `json:"user-name"`
	SysHostName    string                   `json:"sys-hostname"`
	SysKernel      string                   `json:"sys-kernel"`
	SysPlatform    string                   `json:"sys-platform"`
	SysCPUs        int                      `json:"sys-cpu"`
	SysMemoryUsage string                   `json:"sys-memoryusage"`
	Errors         []*ApplicationInfoError  `json:"errors"`
	License        *license_commons.License `json:"license"`
	LibVersions    map[string]string        `json:"lib-versions"`
}

func NewApplicationInfo(dirWork string) *ApplicationInfo {
	instance := new(ApplicationInfo)
	instance.StartTime = time.Now()
	instance.Errors = make([]*ApplicationInfoError, 0)
	instance.Root = gg_utils.Paths.Absolute(dirWork)
	instance.LaunchDir = gg_utils.Paths.Absolute(".")
	instance.MimeJS = mime.TypeByExtension(".js")
	instance.MimeCSS = mime.TypeByExtension(".css")
	instance.MimeHTML = mime.TypeByExtension(".html")
	instance.MimeJSON = mime.TypeByExtension(".json")
	instance.UserHome, _ = gg_utils.Paths.UserHomeDir()
	instance.UserName = gg_utils.Paths.FileName(instance.UserHome, false)
	instance.LibVersions = map[string]string{"gg-core": gg_.LibVersion}

	_ = instance.Refresh() // load system variables

	return instance
}

func (instance *ApplicationInfo) String() string {
	return gg_.Stringify(instance)
}

func (instance *ApplicationInfo) PrintInfo() string {
	response := strings.Builder{}
	response.WriteString(fmt.Sprintf("RUN '%s' v%s\n", instance.AppName, instance.AppVersion))
	response.WriteString(fmt.Sprintf("\t* Mode='%s'\n", instance.Mode))
	response.WriteString(fmt.Sprintf("\t* LauchDir='%s'\n", instance.LaunchDir))
	response.WriteString(fmt.Sprintf("\t* Workspace='%s'\n", instance.Root))
	response.WriteString("=================================\n")

	response.WriteString(fmt.Sprintf(" USER INFO: home=%s, name=%s\n", instance.UserHome, instance.UserName))
	response.WriteString(fmt.Sprintf(" SYSTEM INFO: name=%v, kernel=%v, platform=%v, cpu=%v\n",
		instance.SysHostName, instance.SysKernel, instance.SysPlatform, instance.SysCPUs))
	response.WriteString(fmt.Sprintf(" MEMORY INFO: %v\n", instance.SysMemoryUsage))
	response.WriteString(fmt.Sprintf(" CURRENT PROCESS PID: %v\n", instance.ProcessPID))

	// LICENSE info
	if nil != instance.License {
		response.WriteString(instance.PrintLicense() + "\n")
	}

	// MIME info
	response.WriteString(" ---------------------------------\n")
	response.WriteString(" MIME TEST:\n")
	response.WriteString(fmt.Sprintf("   js:   %s\n", instance.MimeJS))
	response.WriteString(fmt.Sprintf("   css:  %s\n", instance.MimeCSS))
	response.WriteString(fmt.Sprintf("   html: %s\n", instance.MimeHTML))
	response.WriteString(fmt.Sprintf("   json: %s\n", instance.MimeJSON))
	response.WriteString(" ---------------------------------\n")

	// libs info
	for k, v := range instance.LibVersions {
		response.WriteString(fmt.Sprintf(" %s version: %v\n", k, v))
	}

	response.WriteString("=================================\n")

	return response.String()
}

func (instance *ApplicationInfo) PrintLicense() string {
	if nil != instance {
		response := strings.Builder{}
		response.WriteString(" ---------------------------------\n")
		response.WriteString(" LICENSE INFO:\n")
		if nil != instance.License {
			response.WriteString(fmt.Sprintf("\tLicense UID: \t\t\t%s\n", instance.License.Uid))
			response.WriteString(fmt.Sprintf("\tOwner Name: \t\t\t%s\n", instance.License.Name))
			response.WriteString(fmt.Sprintf("\tOwner Email: \t\t\t%s\n", instance.License.Email))
			response.WriteString(fmt.Sprintf("\tCreation Date: \t\t\t%v\n", instance.License.CreationTime))
			response.WriteString(fmt.Sprintf("\tExpiration Date: \t\t%v\n", instance.License.GetExpireDate()))
			response.WriteString(fmt.Sprintf("\tRemaining Days \t\t\t%v\n", instance.License.RemainingDays()))
		} else {
			response.WriteString("\tUNLICENSED SOFTWARE. PLEASE, ASK FOR A VALID LICENSE.\n")
		}

		return response.String()
	}
	return ""
}

func (instance *ApplicationInfo) Refresh() *ApplicationInfo {
	if nil != instance {
		instance.MachineId, _ = gg_sys.Sys.ProtectedID("")
		instance.AppName = gg_.AppName
		instance.AppVersion = gg_.AppVersion
		instance.AppSalt = gg_.AppSalt
		process, e := gg_sys.Sys.FindCurrentProcess()
		if nil == e {
			instance.ProcessPID = process.Pid
		} else {
			instance.ProcessPID = -1
		}
		// machine info
		info := gg_sys.Sys.GetInfo()
		instance.SysHostName = info.Hostname
		instance.SysKernel = info.Kernel
		instance.SysPlatform = info.Platform
		instance.SysCPUs = info.CPUs
		instance.SysMemoryUsage = info.MemoryUsage

	}
	return instance
}

func (instance *ApplicationInfo) AddLibVersion(name, version string) *ApplicationInfo {
	if nil != instance {
		instance.LibVersions[name] = version
	}
	return instance
}

func (instance *ApplicationInfo) PathRoot(value ...string) string {
	if len(value) == 1 {
		instance.Root = gg_utils.Paths.Absolute(value[0])
	}
	return instance.Root
}

func (instance *ApplicationInfo) PathSettings(value ...string) string {
	if len(value) == 1 {
		instance.SettingsFile = strings.Replace(value[0], instance.Root, ".", 1)
	}
	return instance.SettingsFile
}

func (instance *ApplicationInfo) AddError(context string, err error) {
	if nil != instance {
		instance.Errors = append(instance.Errors, &ApplicationInfoError{
			Context: context,
			Error:   err.Error(),
		})
	}
}

func (instance *ApplicationInfo) StartedAgo() (response string) {
	if nil != instance {
		d := time.Now().Sub(instance.StartTime)
		if d > time.Hour {
			if d > time.Hour*24 {
				// DAYS
				response = fmt.Sprintf("Started %s day/s ago.", format(d.Hours()/24))
			} else {
				// HOURS
				response = fmt.Sprintf("Started %s hour/s ago.", format(d.Hours()))
			}
		} else if d > time.Minute {
			if d > time.Minute*60 {
				// HOURS
				response = fmt.Sprintf("Started %s hour/s ago.", format(d.Minutes()/60))
			} else {
				// MINUTES
				response = fmt.Sprintf("Started %s minute/s ago.", format(d.Minutes()))
			}
		} else {
			// SECONDS
			response = fmt.Sprintf("Started %s second/s ago.", format(d.Seconds()))
		}
	}
	return
}

func (instance *ApplicationInfo) SetLicense(license *license_commons.License) {
	if nil != instance {
		instance.License = license
	}
}

func (instance *ApplicationInfo) GetLicense() *license_commons.License {
	if nil != instance {
		return instance.License
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	private
// ---------------------------------------------------------------------------------------------------------------------

func format(i float64) string {
	return gg_utils.Formatter.FormatFloat(i, "")
}
