package gg_app

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_net"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"flag"
	"fmt"
	"os"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//	l a u n c h e r s
// ---------------------------------------------------------------------------------------------------------------------

func Main() (ctrl *AppCtrl, err error) {

	defDir := gg_utils.Paths.Absolute("./_workspace")
	defMode := gg_.ModeProduction

	//-- command flags --//
	// run -dir_work=./_test/_workspace -m=debug
	// run
	cmdRun := flag.NewFlagSet("run", flag.ExitOnError)
	dirWork := cmdRun.String("dir_work", defDir, "Set a particular folder as main workspace")
	mode := cmdRun.String("m", defMode, "Mode allowed: 'debug' or 'production'")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "run":
			_ = cmdRun.Parse(os.Args[2:])
		default:
			// panic("Command not supported: " + cmd)
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, fmt.Sprintf("CommandLine not supported '%s': ", cmd))
		}
	} else {
		// app launched directly from executable and from con command line
		*mode = defMode
		*dirWork = gg_utils.Paths.UserHomePath(gg_.AppName + "Workspace")
		// err = gg_utils.Errors.Prefix(gg_.PanicSystemError, "Missing command. i.e. 'run': ")
	}
	if nil != err {
		return
	}

	ctrl, err = NewAppCtrl(*mode, *dirWork)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	n e t w o r k
// ---------------------------------------------------------------------------------------------------------------------

func Download(url string) (data []byte, err error) {
	if gg_utils.Paths.IsUrl(url) {
		client := gg_net.Net.NewHttpClient()
		resp, e := client.Get(url)

		if nil != e {
			err = e
		} else {
			data = resp.Body
		}
	} else {
		data, err = gg_utils.IO.Download(url)
	}
	return
}

func StatusCodeToError(statusCode int) (err error) {
	if statusCode != 200 {
		switch statusCode {
		case 400:
			err = errors.New("bad request")
		case 401:
			err = errors.New("unauthorized")
		case 403:
			err = errors.New("forbidden")
		case 404:
			err = errors.New("not found")
		case 429:
			err = errors.New("too many requests")
		case 500:
			err = errors.New("internal server error")
		case 502:
			err = errors.New("bad gateway")
		case 503:
			err = errors.New("service unavailable")
		case 504:
			err = errors.New("gateway timeout")
		default:
			err = errors.New("unknown error")
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	e n c r y p t i o n
// ---------------------------------------------------------------------------------------------------------------------

func Encrypt(value string) string {
	salt := gg_.AppSalt
	if len(salt) > 0 && len(value) > 0 {
		value = gg_utils.Coding.UnwrapBase64(value)
		enc, err := gg_utils.Coding.EncryptTextWithPrefix(value, []byte(salt))
		if nil == err {
			enc = gg_utils.Coding.WrapBase64(enc)
			return enc
		}
	}
	return value
}

func Decrypt(value string) string {
	salt := gg_.AppSalt
	if len(salt) > 0 && len(value) > 0 {
		value = gg_utils.Coding.UnwrapBase64(value)
		response, err := gg_utils.Coding.DecryptTextWithPrefix(value, []byte(salt))
		if nil == err {
			return response
		}
	}
	return value
}

func MD5(value string) string {
	return gg_utils.Coding.MD5(strings.ToLower(value))
}

func MD5Wrap(value string) string {
	salt := gg_.AppSalt
	if len(value) > 0 {
		if len(salt) > 0 {
			value = salt + value
		}
		return "md5-" + MD5(value)
	}
	return value
}
