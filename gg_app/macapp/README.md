# DMG

https://medium.com/@mattholt/packaging-a-go-application-for-macos-f7084b00f6b5

## Making a template DMG

The template need only be made once, which is good because it’s hard to automate this and get nice results.

Open Disk Utility. Press ⌘N to create a new disk image. Give it a name and a size that is large enough to fit your app bundle:

![](./docs/img_screen_1.jpeg)

Go to your mounted image in Finder. 

Customize the view settings for this folder so that it looks just the way you want when users mount their DMG 
to install your application. Consider setting a background image, hiding the toolbar and sidebar, and increasing the icon size.

NOTE: A background image must be contained within the DMG itself. 
This is usually done in a folder called .background. 
Because it starts with a dot, it will be hidden from view. 
Put the background image in there, and set it by dragging it into the View Options:

![](./docs/img_screen_2.png)

You’ll also need a shortcut (“alias”) to the /Applications folder in your DMG 
for convenience. 
You can right-click your own /Applications folder and choose “Make alias” then move it into your mounted image.

### Finish customize the view

Now we just have to add our app bundle into the DMG and finish customizing the view:

![](./docs/img_screen_3.png)

Great! Our template DMG is all set up. Now we can get it ready to distribute.

## Converting the DMG for distribution

The current DMG is not compressed and is writeable. That’s not ideal for distributing an application, so we’ll fix that by converting it.

Open Disk Utility and choose Images → Convert. 

Name the file something presentable, and leave the rest of the settings the same. 
(Image format should be “compressed”.)