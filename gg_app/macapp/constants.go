package macapp

import _ "embed"

//go:embed templates/Info.plist
var infoPlistTemplate string

type infoPListData struct {
	Name               string
	Executable         string
	Identifier         string
	Version            string
	InfoString         string
	ShortVersionString string
	IconFile           string
	IsAgent            bool
}

//go:embed templates/version.plist
var versionPlistTemplate string

type versionPListData struct {
	Name               string // AppNameJet
	BundleVersion      string // 2024.10.2.1
	SourceVersion      string // 1000.000.000
	BuildVersion       string // 6
	BundleShortVersion string // 3.0
}

// readme goes into a README file inside the package for
// future reference.
const readme = `Made with MacApp by G&G Technologies Srl
https://ggtechnologies.sm
`
const pkgInfoTemplate = `APPL????`
