package macapp

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	MacAppOptions
//----------------------------------------------------------------------------------------------------------------------

type MacAppOptions struct {
	Name        string `json:"name"`       // App name
	Author      string `json:"author"`     // App Author name
	Version     string `json:"version"`    // App Version
	Identifier  string `json:"identifier"` // bundle identifier
	Icon        string `json:"icon"`       // icon image file (.icns|.png|.jpg|.jpeg)
	Executable  string `json:"executable"` // path to exec file
	IsAgent     bool   `json:"is-agent"`   // only if app is an agent service not to store in dock
	TemplateDMG string `json:"template-dmg"`
}

func NewMacAppOptions(args ...interface{}) (instance *MacAppOptions, err error) {
	for _, arg := range args {
		if s, ok := arg.(string); ok {
			if gg_utils.Regex.IsValidJsonObject(s) {
				err = gg_json.JSON.Read(s, &instance)
			} else {
				err = gg_json.JSON.ReadFromFile(s, &instance)
			}
			break
		}
		if o, ok := arg.(*MacAppOptions); ok {
			instance = o
			break
		}
		if o, ok := arg.(MacAppOptions); ok {
			instance = &o
			break
		}
	}
	if nil == instance {
		instance = new(MacAppOptions)
	}
	if len(instance.Executable) == 0 {
		instance.Executable = gg_utils.Paths.Absolute(fmt.Sprintf("./build/%s", strings.ToLower(instance.Name)))
	}
	return
}
