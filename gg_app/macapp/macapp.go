package macapp

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_img"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	_ "embed"
	"errors"
	"fmt"
	"image"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"
)

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func CreateMacAppBundle(options *MacAppOptions, outputDir string) (appBundle string, err error) {
	var builder *MacAppBuilder
	builder, err = NewMacAppBuilder(options)
	if nil != err {
		return
	}
	appBundle, err = builder.Build(outputDir)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	MacAppBuilder
//----------------------------------------------------------------------------------------------------------------------

type MacAppBuilder struct {
	options *MacAppOptions
}

func NewMacAppBuilder(args ...interface{}) (instance *MacAppBuilder, err error) {
	instance = new(MacAppBuilder)
	err = instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *MacAppBuilder) Build(outputDir string) (appBundle string, err error) {
	if nil != instance {
		// validate options
		err = gg_utils.Paths.Mkdir(outputDir + gg_utils.OS_PATH_SEPARATOR)
		if nil != err {
			err = gg_errors.Errors.Prefix(err, "Creating output directory: ")
			return
		}
		if nil == instance.options {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Missing required options: ")
			return
		}
		if len(instance.options.Executable) == 0 {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Missing required executable path: ")
			return
		}

		// ready to build package
		appBundle, err = build(outputDir, instance.options)
		if nil != err {
			return
		}

		// check if we have a DMG template
		if len(instance.options.TemplateDMG) > 0 {
			err = makeDMGFromTemplate(instance.options.TemplateDMG, appBundle)
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *MacAppBuilder) init(args ...interface{}) (err error) {
	if nil != instance {
		instance.options, err = NewMacAppOptions(args...)
	}
	return
}

func build(outputDir string, options *MacAppOptions) (appBundle string, err error) {
	outputDir = gg_utils.Paths.Absolute(outputDir)
	appRoot := filepath.Join(outputDir, options.Name+".app")
	contentsPath := filepath.Join(appRoot, "Contents")
	appPath := filepath.Join(contentsPath, "MacOS")
	resouresPath := filepath.Join(contentsPath, "Resources")
	pluginsPath := filepath.Join(contentsPath, "PlugIns")
	binName := gg_utils.Paths.Absolute(options.Executable)
	binPath := filepath.Join(appPath, gg_utils.Paths.FileName(binName, true))

	// remove old
	_ = gg_utils.IO.RemoveAll(appRoot)

	// file and dir creation
	{
		if err = os.MkdirAll(appPath, 0777); err != nil {
			err = gg_errors.Errors.Prefix(err, "Creating app directory: ")
			return
		}
		if err = os.MkdirAll(pluginsPath, 0777); err != nil {
			err = gg_errors.Errors.Prefix(err, "Creating plugins directory: ")
			return
		}
		var fdst *os.File
		fdst, err = os.Create(binPath)
		if err != nil {
			err = gg_errors.Errors.Prefix(err, "Creating bin: ")
			return
		}
		defer fdst.Close()

		var fsrc *os.File
		fsrc, err = os.Open(binName)
		if err != nil {
			if os.IsNotExist(err) {
				err = errors.New(binName + " not found")
				return
			}
			err = gg_errors.Errors.Prefix(err, "Opening bin: ")
			return
		}
		defer fsrc.Close()
		if _, err = io.Copy(fdst, fsrc); err != nil {
			err = gg_errors.Errors.Prefix(err, "Copying bin: ")
			return
		}
		if err = exec.Command("chmod", "+x", appPath).Run(); err != nil {
			err = gg_errors.Errors.Prefix(err, fmt.Sprintf("Chmod '%s': ", appPath))
			return
		}
		if err = exec.Command("chmod", "+x", binPath).Run(); err != nil {
			err = gg_errors.Errors.Prefix(err, fmt.Sprintf("Chmod '%s': ", binPath))
			return
		}
	}

	id := options.Identifier
	author := options.Author
	name := options.Name
	version := options.Version
	icon := options.Icon
	if id == "" {
		id = author + "." + name
	}

	iconFile := ""
	iconFile, err = writeIcns(icon, resouresPath)
	if nil != err {
		return
	}

	// write Info.Plist
	info := infoPListData{
		Name:               name,
		Executable:         gg_utils.Paths.FileName(binName, true), // filepath.Join("MacOS", appRoot),
		Identifier:         id,
		Version:            version,
		InfoString:         name + " by " + author,
		ShortVersionString: version,
		IconFile:           iconFile,
	}
	err = writeInfoPlist(contentsPath, info)
	if nil != err {
		return
	}

	// write version.plist
	versionData := versionPListData{
		Name:               name,
		BundleVersion:      version,
		SourceVersion:      version,
		BuildVersion:       "1",
		BundleShortVersion: version,
	}
	err = writeVersionPlist(contentsPath, versionData)
	if nil != err {
		return
	}

	// write PkgInfo
	err = writePkgInfo(contentsPath)
	if nil != err {
		return
	}

	// write README
	err = writeREADME(contentsPath)
	if nil != err {
		return
	}

	appBundle = appRoot

	return
}

func writeInfoPlist(contentsPath string, info infoPListData) (err error) {
	var tpl *template.Template
	tpl, err = template.New("template").Parse(infoPlistTemplate)
	if err != nil {
		err = gg_errors.Errors.Prefix(err, "Parsing infoPlistTemplate: ")
		return
	}
	var fplist *os.File
	fplist, err = os.Create(filepath.Join(contentsPath, "Info.plist"))
	if err != nil {
		err = gg_errors.Errors.Prefix(err, "Create Info.plist: ")
		return
	}
	defer fplist.Close()
	if err = tpl.Execute(fplist, info); err != nil {
		err = gg_errors.Errors.Prefix(err, "Execute Info.plist template: ")
		return
	}
	return
}

func writeVersionPlist(contentsPath string, info versionPListData) (err error) {
	var tpl *template.Template
	tpl, err = template.New("template").Parse(versionPlistTemplate)
	if err != nil {
		err = gg_errors.Errors.Prefix(err, "Parsing versionPlistTemplate: ")
		return
	}
	var fplist *os.File
	fplist, err = os.Create(filepath.Join(contentsPath, "version.plist"))
	if err != nil {
		err = gg_errors.Errors.Prefix(err, "Create version.plist: ")
		return
	}
	defer fplist.Close()
	if err = tpl.Execute(fplist, info); err != nil {
		err = gg_errors.Errors.Prefix(err, "Execute version.plist template: ")
		return
	}
	return
}

func writeREADME(contentsPath string) (err error) {
	if err = os.WriteFile(filepath.Join(contentsPath, "README"), []byte(readme), 0666); err != nil {
		err = gg_errors.Errors.Prefix(err, "Write README: ")
		return
	}
	return
}

func writePkgInfo(contentsPath string) (err error) {
	if err = os.WriteFile(filepath.Join(contentsPath, "PkgInfo"), []byte(pkgInfoTemplate), 0666); err != nil {
		err = gg_errors.Errors.Prefix(err, "Write PkgInfo: ")
		return
	}
	return
}

func writeIcns(icon, resouresPath string) (response string, err error) {
	response = ""
	if icon != "" {
		var iconPath string
		iconPath, err = prepareIcons(icon, resouresPath)
		if err != nil {
			err = gg_errors.Errors.Prefix(err, fmt.Sprintf("Icon '%s': ", icon))
			return
		}
		response = filepath.Base(iconPath)
	}
	return
}

func prepareIcons(iconPath, resourcesPath string) (destFile string, err error) {
	ext := filepath.Ext(strings.ToLower(iconPath))
	var fsrc *os.File
	fsrc, err = os.Open(iconPath)
	if err != nil {
		err = gg_errors.Errors.Prefix(err, "open icon file: ")
		return
	}
	defer fsrc.Close()

	if err = os.MkdirAll(resourcesPath, 0777); err != nil {
		err = gg_errors.Errors.Prefix(err, "os.MkdirAll resourcesPath: ")
		return
	}
	destFile = filepath.Join(resourcesPath, "icon.icns")
	var fdst *os.File
	fdst, err = os.Create(destFile)
	if err != nil {
		err = gg_errors.Errors.Prefix(err, "Create icon.icns file: ")
		return
	}
	defer fdst.Close()

	switch ext {
	case ".icns": // just copy the .icns file
		_, err = io.Copy(fdst, fsrc)
		if err != nil {
			err = gg_errors.Errors.Prefix(err, fmt.Sprintf("Copying '%s': ", iconPath))
			return
		}
	case ".png", ".jpg", ".jpeg", ".gif": // process any images
		var srcImg image.Image
		srcImg, _, err = image.Decode(fsrc)
		if err != nil {
			err = gg_errors.Errors.Prefix(err, "Decode image: ")
			return
		}
		if err = gg_img.Images.Icns.Encode(fdst, srcImg); err != nil {
			err = gg_errors.Errors.Prefix(err, "Generate icns file: ")
			return
		}
	default:
		err = errors.New(ext + " icons not supported")
	}
	return
}
