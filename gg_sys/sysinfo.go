package gg_sys

import "bitbucket.org/digi-sense/gg-core/gg_"

type SysInfo struct {
	Machine     string      `json:"machine-id"`
	MachineHash string      `json:"machine-hash"`
	Info        *InfoObject `json:"info"`
	MemDetails  *MemObject  `json:"mem-details"`
}

func (instance *SysInfo) String() string {
	return gg_.Stringify(instance)
}

func (instance *SysInfo) Map() map[string]interface{} {
	return gg_.Map(gg_.Stringify(instance))
}

func (instance *SysHelper) NewSysInfo() (response *SysInfo) {
	response = new(SysInfo)
	response.Machine, _ = Sys.ID()
	response.MachineHash, _ = Sys.ProtectedID("")
	response.Info = Sys.GetInfo()
	response.MemDetails = NewMemoryUsageInfo()

	return
}
