package gg_fs

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"os"
	"path/filepath"
)

var Fs *FsHelper

func init() {
	Fs = new(FsHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	FileWrapper
//----------------------------------------------------------------------------------------------------------------------

// FileWrapper represents a wrapper structure for file system entities with metadata and utility methods.
// Works both for files and dirs
type FileWrapper struct {
	Uid     string `json:"uid"`  // unique ID based on full-path
	Tag     string `json:"tag"`  // file, dir
	Path    string `json:"path"` // full-path
	Name    string `json:"name"` // file name, file.txt
	Ext     string `json:"ext"`  // extension. .txt
	IsDir   bool   `json:"is_dir"`
	Size    int64  `json:"size"`
	Mode    string `json:"mode"`
	ModTime int64  `json:"mod_time"`
	Bytes   []byte `json:"bytes"`
	Hash    string `json:"hash"`
}

// NewFileWrapper creates a new FileWrapper instance for the specified filename without reading file contents.
func NewFileWrapper(filename string) (instance *FileWrapper) {
	instance, _ = wrap(filename, false)
	return
}

func (instance *FileWrapper) String() string {
	return string(gg_.Marshal(instance))
}

func (instance *FileWrapper) SetUid(uid string) *FileWrapper {
	if nil != instance {
		instance.Uid = uid
	}
	return instance
}

func (instance *FileWrapper) SetTag(tag string) *FileWrapper {
	if nil != instance {
		instance.Tag = tag
	}
	return instance
}

func (instance *FileWrapper) ReadDir() (response []*FileWrapper, err error) {
	response = make([]*FileWrapper, 0)
	if nil != instance {
		path := instance.Path
		if !instance.IsDir {
			path = filepath.Dir(instance.Path)
		}
		var dirs []os.DirEntry
		dirs, err = os.ReadDir(path)
		if nil == err {
			for _, dir := range dirs {
				response = append(response, NewFileWrapper(filepath.Join(path, dir.Name())))
			}
		}
	}
	return
}

func (instance *FileWrapper) Read() (response []byte, err error) {
	response = make([]byte, 0)
	if nil != instance {
		if len(instance.Bytes) == 0 {
			instance.Bytes, err = gg_.IOReadBytesFromFile(instance.Path)
			if nil == err {
				instance.Hash, _ = gg_.IOReadHashFromBytes(instance.Bytes)
			}
		}
		response = instance.Bytes
	}
	return
}

func (instance *FileWrapper) Write(bytes []byte) (size int, err error) {
	if nil != instance {
		if instance.IsDir {
			err = errors.New("cannot write to directory")
		} else {
			err = os.WriteFile(instance.Path, bytes, 0644)
			if nil == err {
				size = len(bytes)
				instance.Hash, _ = gg_.IOReadHashFromBytes(bytes)
			}
		}
	}
	return
}

func (instance *FileWrapper) Remove() error {
	return os.Remove(instance.Path)
}

func (instance *FileWrapper) MkDir() error {
	return os.Mkdir(instance.Path, 0755)
}

//----------------------------------------------------------------------------------------------------------------------
//	FsHelper
//----------------------------------------------------------------------------------------------------------------------

type FsHelper struct {
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *FsHelper) Wrap(filename string) *FileWrapper {
	return NewFileWrapper(filename)
}

func (instance *FsHelper) Read(filename string) (response []byte, err error) {
	return NewFileWrapper(filename).Read()
}

func (instance *FsHelper) ReadDir(path string) (response []*FileWrapper, err error) {
	return NewFileWrapper(path).ReadDir()
}

func (instance *FsHelper) Write(data []byte, target string) (int, error) {
	return NewFileWrapper(target).Write(data)
}

func (instance *FsHelper) Remove(source string) error {
	return NewFileWrapper(source).Remove()
}

func (instance *FsHelper) MkDir(path string) error {
	return NewFileWrapper(path).MkDir()
}

func (instance *FsHelper) Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	return nil == err, err
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func wrap(filename string, read bool) (response *FileWrapper, err error) {
	var fullpath string
	fullpath, err = filepath.Abs(filename)
	var stats os.FileInfo
	stats, err = os.Stat(fullpath)
	if nil == err {

		uid := encode(fullpath)
		name := filepath.Base(fullpath)
		ext := filepath.Ext(fullpath)
		isDir := stats.IsDir()
		size := stats.Size()
		mode := stats.Mode().String()
		hash := ""
		var bytes []byte
		if read {
			bytes, err = gg_.IOReadBytesFromFile(filename)
			if nil != err {
				return
			}
			hash, _ = gg_.IOReadHashFromBytes(bytes)
		}
		tag := "file"
		if isDir {
			tag = "dir"
		}
		response = &FileWrapper{
			Uid:     uid,
			Tag:     tag,
			Path:    fullpath,
			Name:    name,
			Ext:     ext,
			IsDir:   isDir,
			Size:    size,
			Mode:    mode,
			ModTime: stats.ModTime().Unix(),
			Bytes:   bytes,
			Hash:    hash,
		}
	}
	return
}

func encode(text string) string {
	h := md5.New()
	h.Write([]byte(text))
	return hex.EncodeToString(h.Sum(nil))
}
