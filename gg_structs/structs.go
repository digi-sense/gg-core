package gg_structs

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"reflect"
	"strings"
	"time"
)

type StructHelper struct {
}

var Structs *StructHelper

func init() {
	Structs = new(StructHelper)
}

func (instance *StructHelper) NewBuilder() *StructBuilder {
	return NewBuilder()
}

//----------------------------------------------------------------------------------------------------------------------
//	structs
//----------------------------------------------------------------------------------------------------------------------

type StructBuilder struct {
	autoTagJson bool
	fields      []reflect.StructField
	values      map[string]interface{}
}

func NewBuilder() *StructBuilder {
	instance := new(StructBuilder)
	instance.values = make(map[string]interface{})
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *StructBuilder) EnableAutoTagJson(value bool) *StructBuilder {
	instance.autoTagJson = value
	return instance
}

func (instance *StructBuilder) Set(name string, value interface{}) *StructBuilder {
	return instance.AddFieldByValue(name, "", value)
}

func (instance *StructBuilder) AddFieldByValue(name, tag string, value interface{}) *StructBuilder {
	name = gg_utils.Strings.CapitalizeFirst(name)
	instance.addField(name, tag, reflect.TypeOf(value))
	instance.values[name] = value
	return instance
}

// AddFieldByStringType add a field using its string type name with a default value
func (instance *StructBuilder) AddFieldByStringType(name, tag string, sType string) *StructBuilder {
	name = gg_utils.Strings.CapitalizeFirst(name)
	t, v := TypeFromText(sType)
	instance.addField(name, tag, t)
	instance.values[name] = v
	return instance
}

// Elem return element you can use to add fields: ex. elem.Field(0).SetInt(1234)
func (instance *StructBuilder) Elem() reflect.Value {
	t := reflect.StructOf(instance.fields)
	return reflect.New(t).Elem()
}

func (instance *StructBuilder) Interface() interface{} {
	elem := instance.Elem()
	for k, v := range instance.values {
		value := reflect.ValueOf(v)
		field := elem.FieldByName(k)
		field.Set(value)
	}
	return elem.Addr().Interface()
}

func (instance *StructBuilder) Descriptor() (response map[string]string) {
	if nil != instance {
		response = make(map[string]string)
		for name, v := range instance.values {
			value := reflect.ValueOf(v)
			response[name] = value.Type().Name()
		}
	}
	return
}

func (instance *StructBuilder) Json() string {
	return gg_json.JSON.Stringify(instance.Interface())
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *StructBuilder) addField(name, tag string, typeOf reflect.Type) *StructBuilder {
	if len(tag) == 0 && instance.autoTagJson {
		tag = fmt.Sprintf(`json:"%s"`, gg_utils.Strings.Underscore(name))
	}
	instance.fields = append(instance.fields, reflect.StructField{
		Name: name,
		Tag:  reflect.StructTag(tag),
		Type: typeOf,
	})
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func TypeFromText(sType string) (t reflect.Type, v interface{}) {
	sType = strings.ToLower(sType)
	switch sType {
	case "int", "integer", "number":
		v = 0
	case "int8":
		v = int8(0)
	case "int16":
		v = int16(0)
	case "int32":
		v = int32(0)
	case "int64":
		v = int64(0)
	case "uint":
		v = uint(0)
	case "uint8":
		v = uint8(0)
	case "uint16":
		v = uint16(0)
	case "uint32":
		v = uint32(0)
	case "uint64":
		v = uint64(0)
	case "float", "float32":
		v = float32(0.0)
	case "float64", "long":
		v = float64(0.0)
	case "time", "date", "datetime":
		v = time.Now()
	case "bool", "boolean":
		v = false
	case "byte":
		v = byte(0)
	case "rune":
		v = rune(0)
	case "array-string":
		v = make([]string, 0)
	case "array-byte":
		v = make([]byte, 0)
	case "array-int":
		v = make([]int, 0)
	case "array-int8":
		v = make([]int8, 0)
	case "array-int16":
		v = make([]int16, 0)
	case "array-int32":
		v = make([]int32, 0)
	case "array-int64":
		v = make([]int64, 0)
	case "array-float", "array-float32":
		v = make([]float32, 0)
	case "array-float64":
		v = make([]float64, 0)
	default:
		v = ""
	}
	t = reflect.TypeOf(v)
	return
}
