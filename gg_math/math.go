package gg_math

import (
	"math"
)

type MathHelper struct {
}

var Math *MathHelper

func init() {
	Math = new(MathHelper)
}

func (*MathHelper) Round(num float64, decimals int) float64 {
	factor := math.Pow(10, float64(decimals))
	return math.Floor(num*factor+.5) / factor
}

func (*MathHelper) MaxMinArray(a []float64) (ma, mi float64) {
	mi, ma = a[0], a[0]
	for _, num := range a {
		if num < mi {
			mi = num
		}
		if num > ma {
			ma = num
		}
	}
	return
}
