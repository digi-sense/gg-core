package gg_sys_buildinfo

import (
	"flag"
	"fmt"
	"os"
	"runtime/debug"
	"strconv"
	"strings"
	"time"
)

var (
	// Version will be the version tag if the binary is built with "go install url/tool@version".
	// If the binary is built some other way, it will be "(devel)".
	Version = "unknown"
	// Revision is taken from the vcs.revision tag in Go 1.18+.
	Revision = "unknown"
	// LastCommit is taken from the vcs.time tag in Go 1.18+.
	LastCommit time.Time
	// DirtyBuild is taken from the vcs.modified tag in Go 1.18+.
	DirtyBuild = true
)

//----------------------------------------------------------------------------------------------------------------------
//	SysBuildInfoHelper
//----------------------------------------------------------------------------------------------------------------------

type SysBuildInfoHelper struct {
}

var SysBuildInfo *SysBuildInfoHelper

func init() {
	SysBuildInfo = new(SysBuildInfoHelper)

	// get build info
	info, ok := debug.ReadBuildInfo()
	if !ok {
		return
	}

	// legacy build info
	if info.Main.Version != "" {
		Version = info.Main.Version
	}

	// modern build info
	for _, kv := range info.Settings {
		if kv.Value == "" {
			continue
		}
		switch kv.Key {
		case "vcs.revision":
			Revision = kv.Value
		case "vcs.time":
			LastCommit, _ = time.Parse(time.RFC3339, kv.Value)
		case "vcs.modified":
			DirtyBuild = kv.Value == "true"
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *SysBuildInfoHelper) Version() string {
	return Version
}

func (instance *SysBuildInfoHelper) Revision() string {
	return Revision
}

func (instance *SysBuildInfoHelper) LastCommit() time.Time {
	return LastCommit
}

func (instance *SysBuildInfoHelper) IsDirtyBuild() bool {
	return DirtyBuild
}

func (instance *SysBuildInfoHelper) Short() string {
	parts := make([]string, 0, 3)
	if Version != "unknown" && Version != "(devel)" {
		parts = append(parts, Version)
	}
	if Revision != "unknown" && Revision != "" {
		parts = append(parts, "rev")
		commit := Revision
		if len(commit) > 7 {
			commit = commit[:7]
		}
		parts = append(parts, commit)
		if DirtyBuild {
			parts = append(parts, "dirty")
		}
	}
	if len(parts) == 0 {
		return "devel"
	}
	return strings.Join(parts, "-")
}

//----------------------------------------------------------------------------------------------------------------------
//	f l a g
//----------------------------------------------------------------------------------------------------------------------

// AddFlag adds -v and -version flags to the FlagSet.
// If triggered, the flags print version information and call os.Exit(0).
// If FlagSet is nil, it adds the flags to flag.CommandLine.
func (instance *SysBuildInfoHelper) AddFlag(f *flag.FlagSet) {
	if f == nil {
		f = flag.CommandLine
	}
	f.Var(boolFunc(printVersion), "v", "short alias for -version")
	f.Var(boolFunc(printVersion), "version", "print version information and exit")
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func printVersion(b bool) error {
	if !b {
		return nil
	}
	fmt.Println("Version:", Version)
	fmt.Println("Revision:", Revision)
	if Revision != "unknown" {
		fmt.Println("Committed:", LastCommit.Format(time.RFC1123))
		if DirtyBuild {
			fmt.Println("Dirty Build")
		}
	}
	os.Exit(0)
	panic("unreachable")
}

type boolFunc func(bool) error

func (f boolFunc) IsBoolFlag() bool {
	return true
}

func (f boolFunc) String() string {
	return ""
}

func (f boolFunc) Set(s string) error {
	b, err := strconv.ParseBool(s)
	if err != nil {
		return err
	}
	return f(b)
}
