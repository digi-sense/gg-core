package gg_deployer

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_vfs"
)

// ---------------------------------------------------------------------------------------------------------------------
//  DeployerFileSystemOptions
// ---------------------------------------------------------------------------------------------------------------------

type DeployerFileSystemOptions struct {
	DriverName string                 `json:"driver-name"`
	Payload    map[string]interface{} `json:"payload"`
}

type DeployerFileWriteOptions struct {
	FileSystem *DeployerFileSystemOptions `json:"file-system"`
	DirDeploy  string                     `json:"dir-deploy"`
	Overwrite  bool                       `json:"overwrite"` // default overwrite
	Minify     bool                       `json:"minify"`
}

// ---------------------------------------------------------------------------------------------------------------------
//  DeployerOptions
// ---------------------------------------------------------------------------------------------------------------------

type DeployerOptions struct {
	DeployerFileWriteOptions
	DirLoad               []string `json:"dir-load"`
	DirLoadFileFilter     string   `json:"dir-load-file-filter"`
	EmptyDirDeployOnClose bool     `json:"empty-dir-deploy-on-close"`
}

func NewDeployerOptions(args ...interface{}) (instance *DeployerOptions, err error) {
	instance = new(DeployerOptions)
	err = instance.init(args...)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DeployerOptions) String() string {
	return gg_.Stringify(instance)
}

func (instance *DeployerOptions) Map() (response map[string]interface{}) {
	if nil != instance {
		_ = gg_json.JSON.Read(instance.String(), &response)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DeployerOptions) init(args ...interface{}) (err error) {
	if nil != instance {
		for _, arg := range args {
			if o, ok := arg.(*DeployerOptions); ok {
				err = instance.init(o.String())
				if nil != err {
					return
				}
				continue
			}
			if s, ok := arg.(string); ok {
				err = gg_.Unmarshal(s, &instance)
				if nil != err {
					return
				}
				continue
			}
		}

		if nil == instance.FileSystem {
			instance.FileSystem = new(DeployerFileSystemOptions)
			instance.FileSystem.DriverName = gg_vfs.DriverOS
		}
	}
	return
}
