package gg_deployer

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
)

type DeployerFile struct {
	Filename  string `json:"filename"` // name of the file. i.e. "index.html"
	Content   []byte `json:"content"`  // content in bytes
	Path      string `json:"path"`     // Output path. For example, "/files/out/". If empty is used the default value
	Overwrite bool   `json:"overwrite"`
	Minify    bool   `json:"minify"`
}

func NewDeployerFile(filename string, content []byte, dirDeploy string, overwrite, minify bool) (response *DeployerFile) {
	response = &DeployerFile{
		Filename:  gg_utils.Paths.FileName(filename, true),
		Content:   content,
		Overwrite: overwrite,
		Minify:    minify,
		Path:      dirDeploy,
	}
	return
}

func (instance *DeployerFile) String() string {
	return gg_.Stringify(instance)
}

func (instance *DeployerFile) GetAbsolutePath() string {
	if nil != instance {
		path := gg_utils.Paths.Absolutize(instance.Filename, instance.Path)
		return gg_utils.Paths.Absolutize(path, gg_utils.Paths.GetWorkspacePath())
	}
	return ""
}
