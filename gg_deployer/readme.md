# Deployer

Deploy files into output directory.

Use a Virtual File System approach, so you can write your own file writer (FTP, Google Drive, etc...).

Sample usage:

```go
package main

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_deployer"
)

func main(){
	// init workspace
	gg.Paths.SetWorkspacePath("./")

	// load options for deployer
	options, err := gg_deployer.NewDeployerOptions("./deployer.json")
	if nil!=err{
		panic(err)
    }

	// create a deployer
	deployer, err := gg_deployer.NewDeployer(options)
	if nil!=err{
		panic(err)
	}
	
	// open and load from "DirLoad" option 
	err = deployer.Open()
	if nil!=err{
		panic(err)
	}

	// load a file to deploy later
	err = deployer.LoadFilename("./sample.txt")
	if nil!=err{
		panic(err)
	}
	
	// deploy anything loaded from "DirLoad" option and LoadFile method
	err = deployer.DeployAll()
	if nil!=err{
		panic(err)
	}
	
	// close and remove all deployed files if "empty-dir-deploy-on-close" options is true
    err = deployer.Close()
	if nil!=err{
		panic(err)
	}
}

```