package gg_deployer

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_mini"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-core/gg_vfs"
	"errors"
	"fmt"
	"log"
	"path/filepath"
	"sync"
)

// ---------------------------------------------------------------------------------------------------------------------
//  DeployerHelper
// ---------------------------------------------------------------------------------------------------------------------

type DeployerHelper struct {
}

var Deployer *DeployerHelper

func init() {
	Deployer = new(DeployerHelper)
}

func (instance *DeployerHelper) New(args ...interface{}) (response *DeployCtrl, err error) {
	response, err = NewDeployer(args...)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  DeployCtrl
// ---------------------------------------------------------------------------------------------------------------------

type DeployCtrl struct {
	uid     string
	options *DeployerOptions
	mux     *sync.Mutex

	opened   bool
	deployed []string
	loaded   []*DeployerFile
	logger   gg_.ILogger

	_vfs map[string]gg_vfs.IVFSController
}

func NewDeployer(args ...interface{}) (instance *DeployCtrl, err error) {
	instance = new(DeployCtrl)
	instance.uid = gg_rnd.Rnd.Uuid()
	err = instance.init(args...)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DeployCtrl) String() string {
	if nil != instance {
		return gg_.Stringify(instance.Map())
	}
	return ""
}

func (instance *DeployCtrl) Map() (response map[string]interface{}) {
	if nil != instance {
		response = make(map[string]interface{})
		response["options"] = instance.options.Map()
		response["deployed"] = instance.deployed
		response["loaded"] = instance.loaded
	}
	return
}

func (instance *DeployCtrl) Uid() string {
	return instance.uid
}

func (instance *DeployCtrl) CountLoaded() int {
	return len(instance.loaded)
}

func (instance *DeployCtrl) Open() (err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		err = instance.open()
	}
	return
}

func (instance *DeployCtrl) Close() (err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		err = instance.close()
	}
	return
}

func (instance *DeployCtrl) Deployed() []string {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		return instance.deployed
	}
	return make([]string, 0)
}

func (instance *DeployCtrl) Loaded() []*DeployerFile {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		return instance.loaded
	}
	return make([]*DeployerFile, 0)
}

// WalkLoaded pass all loaded files to a callback function for changes
func (instance *DeployCtrl) WalkLoaded(callback func(file *DeployerFile)) *DeployCtrl {
	if nil != instance && nil != callback {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		for _, file := range instance.loaded {
			callback(file)
		}
	}
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  l o a d
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DeployCtrl) LoadFilename(filename string) (err error) {
	if nil != instance {
		var file *DeployerFile
		file, err = instance.newDeployerFileFromPath(filename)
		if nil != err {
			return
		}
		err = instance.LoadFile(file)
	}
	return
}

func (instance *DeployCtrl) LoadFile(file *DeployerFile) (err error) {
	if nil != instance && nil != file {
		if instance.opened {
			err = instance.DeployFile(file)
		} else {
			instance.mux.Lock()
			defer instance.mux.Unlock()

			instance.loaded = append(instance.loaded, file)
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  d e p l o y
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DeployCtrl) Deploy(content string, fileName string, overwrite bool) (err error) {
	if nil != instance {
		file := &DeployerFile{
			Filename:  fileName,
			Content:   []byte(content),
			Overwrite: overwrite,
		}

		err = instance.DeployFile(file)
	}
	return
}

func (instance *DeployCtrl) DeployFilename(filename string) (err error) {
	if nil != instance {
		var file *DeployerFile
		file, err = instance.newDeployerFileFromPath(filename)
		if nil != err {
			return
		}
		err = instance.DeployFile(file)
	}
	return
}

func (instance *DeployCtrl) DeployFile(file *DeployerFile) (err error) {
	if nil != instance && nil != file {
		err = instance.DeployFileWithOptions(file, &instance.options.DeployerFileWriteOptions)
	}
	return
}

func (instance *DeployCtrl) DeployAll() (count int, err error) {
	if nil != instance {
		errs := make([]error, 0)
		for _, file := range instance.loaded {
			e := instance.DeployFile(file)
			if nil != e {
				errs = append(errs, e)
			} else {
				count++
			}
		}

		if len(errs) > 0 {
			err = errors.New("deploy_errors")
			err = gg_errors.Errors.Append(err, errs...)
			return
		}
	}
	return
}

func (instance *DeployCtrl) DeployFileWithOptions(file *DeployerFile, options *DeployerFileWriteOptions) (err error) {
	if nil != instance && nil != file {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		err = instance.open()
		if nil != err {
			return
		}

		if nil == options {
			options = &DeployerFileWriteOptions{
				FileSystem: instance.options.FileSystem,
				DirDeploy:  instance.options.DirDeploy,
				Overwrite:  instance.options.Overwrite,
			}
		}

		fileName := gg_utils.Paths.Absolutize(file.Filename, options.DirDeploy)
		if len(file.Path) > 0 {
			file.Path = gg_utils.Paths.Absolutize(file.Path, gg_utils.Paths.GetWorkspacePath())
			fileName = gg_utils.Paths.Absolutize(file.Filename, file.Path)
		} else {
			file.Path = options.DirDeploy
		}

		var vfsCtrl gg_vfs.IVFSController
		vfsCtrl, err = instance.vfs(options.FileSystem)
		if nil != err {
			return
		}

		err = write(file.Content, fileName, vfsCtrl,
			file.Minify || options.Minify,
			file.Overwrite || options.Overwrite,
			instance.logger)
		if nil == err {
			instance.deployed = append(instance.deployed, fileName)
			instance.removeFileFromLoaded(file)
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DeployCtrl) init(args ...interface{}) (err error) {
	if nil != instance {
		instance.mux = new(sync.Mutex)
		instance.deployed = make([]string, 0)
		instance.loaded = make([]*DeployerFile, 0)
		instance._vfs = make(map[string]gg_vfs.IVFSController)

		for _, arg := range args {
			if o, ok := arg.(*DeployerOptions); ok {
				instance.options = o
				continue
			}
			if o, ok := arg.(DeployerOptions); ok {
				instance.options = &o
				continue
			}
			if l, ok := arg.(gg_.ILogger); ok {
				instance.logger = l
				continue
			}
			if s, ok := arg.(string); ok {
				err = gg_.Unmarshal(s, &instance.options)
				if nil != err {
					return
				}
				continue
			}
		}

		if nil == instance.options {
			instance.options = new(DeployerOptions)
			instance.options.DirDeploy = gg_utils.Paths.WorkspacePath("./deploy-out")
		}
		if nil == instance.options.DirLoad {
			instance.options.DirLoad = make([]string, 0)
		}
		if len(instance.options.DirLoadFileFilter) == 0 {
			instance.options.DirLoadFileFilter = "*.*"
		}
		if nil == instance.options.FileSystem {
			instance.options.FileSystem = new(DeployerFileSystemOptions)
			instance.options.FileSystem.DriverName = gg_vfs.DriverOS
		}

	}
	return
}

func (instance *DeployCtrl) open() (err error) {
	if nil != instance && !instance.opened {
		var vfsCtrl gg_vfs.IVFSController
		vfsCtrl, err = instance.vfs(instance.options.FileSystem)
		if nil != err {
			return
		}
		instance.opened = true

		// load from dirs
		errs := make([]error, 0)
		for _, dir := range instance.options.DirLoad {
			dir = gg_utils.Paths.Absolutize(dir, gg_utils.Paths.GetWorkspacePath())
			files, _ := vfsCtrl.ListAll(dir)
			for _, file := range files {
				if !file.IsDir && file.MatchOne(instance.options.DirLoadFileFilter) {
					dFile, e := instance.newDeployerFileFromPath(file.AbsolutePath)
					if nil != e {
						errs = append(errs, e)
					} else {
						instance.loaded = append(instance.loaded, dFile)
					}
				}
			}
		}

		if len(errs) > 0 {
			err = errors.New("loading_errors")
			err = gg_errors.Errors.Append(err, errs...)
			instance.opened = false
			return
		}

	}
	return
}

func (instance *DeployCtrl) close() (err error) {
	if nil != instance && instance.opened {
		instance.opened = false

		errs := make([]error, 0)

		// remove deployed files
		if instance.options.EmptyDirDeployOnClose {
			for _, file := range instance.deployed {
				e := gg_utils.IO.Remove(file)
				if nil != e {
					errs = append(errs, e)
				}
			}
		}

		if len(errs) > 0 {
			err = errors.New("closing_errors")
			err = gg_errors.Errors.Append(err, errs...)
		}

		// reset internal arrays
		instance.deployed = make([]string, 0)
		instance.loaded = make([]*DeployerFile, 0)
	}
	return
}

func (instance *DeployCtrl) newDeployerFileFromPath(path string) (response *DeployerFile, err error) {
	var vfsCtrl gg_vfs.IVFSController
	vfsCtrl, err = instance.vfs(instance.options.FileSystem)
	if nil != err {
		return
	}

	content, e := vfsCtrl.Read(path)
	if nil != e {
		err = e
		return
	}

	response = NewDeployerFile(path, content, instance.options.DirDeploy,
		instance.options.Overwrite, instance.options.Minify)

	return
}

func (instance *DeployCtrl) removeFileFromLoaded(file *DeployerFile) {
	if nil != instance && len(instance.loaded) > 0 {
		index := -1
		for i, item := range instance.loaded {
			if item.GetAbsolutePath() == file.GetAbsolutePath() {
				index = i
				break
			}
		}
		if index > -1 {
			instance.loaded = gg_utils.Arrays.RemoveIndex(index, instance.loaded).([]*DeployerFile)
		}
	}
}

func (instance *DeployCtrl) vfs(options *DeployerFileSystemOptions) (response gg_vfs.IVFSController, err error) {
	if nil != instance && nil != instance._vfs {
		// lookup existing
		key := options.DriverName
		if _, ok := instance._vfs[key]; !ok {
			instance._vfs[key], err = gg_vfs.VFS.New(options.DriverName, options.Payload)
			if nil != err {
				return
			}
		}
		response = instance._vfs[key]
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func minify(data []byte, filename string, logger gg_.ILogger) (response []byte, err error) {
	ext := filepath.Ext(filename)
	switch ext {
	case ".css":
		response, err = gg_mini.Minify.CSS(data)
	case ".js":
		response, err = gg_mini.Minify.JS(data)
	case ".html":
		response, err = gg_mini.Minify.HTML(data)
	case ".json":
		response, err = gg_mini.Minify.JSON(data)
	default:
		response = data
	}
	return
}

func write(data []byte, filename string, vfsCtrl gg_vfs.IVFSController, flagMinify, flagOverwrite bool, logger gg_.ILogger) (err error) {
	if nil != vfsCtrl {
		if flagMinify {
			var miniErr error
			data, miniErr = minify(data, filename, logger)
			if nil != miniErr {
				msg := fmt.Sprintf("WARNING, minifying %s failed: %s", filename, miniErr.Error())
				// warning, non blocking error
				if nil != logger {
					logger.Warn(msg)
				} else {
					log.Println(msg)
				}
			}
		}

		if exists, _ := vfsCtrl.Exists(filename); !exists || flagOverwrite {
			_ = vfsCtrl.MkDir(filename)
			_, err = vfsCtrl.Write(data, filename)
		}
	}
	return
}
