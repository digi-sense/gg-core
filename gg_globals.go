package gg

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
)

// Version library version
const Version = gg_.LibVersion

const (
	ModeProduction = gg_.ModeProduction
	ModeDebug      = gg_.ModeDebug
)

const (
	EventOnStartApp = gg_.EventOnStartApp
	EventOnCloseApp = gg_.EventOnCloseApp
)

var (
	// AppName Deprecated: use SetAppName
	AppName = ""
	// AppVersion Deprecated: use SetAppVersion
	AppVersion = ""

	PanicSystemError = gg_.PanicSystemError // errors.New("panic_system_error")
)

//-- variable names --//

const (
	VarDirEnv       = gg_.VarDirEnv       // Root workspace
	VarDirWorkspace = gg_.VarDirWorkspace // Root workspace
	VarDirFlows     = gg_.VarDirFlows     // Flows root
	VarDirLocal     = gg_.VarDirLocal     // block root path
	VarDirLocalRel  = gg_.VarDirLocalRel  // block root relative path
	VarDirOut       = gg_.VarOut          // block output path
	VarDirHome      = gg_.VarHome         // block root
	VarDirRoot      = gg_.VarRoot         // block root
	VarHome         = gg_.VarDirHome      // block root
	VarRoot         = gg_.VarDirRoot      // block root
	VarWorkspace    = gg_.VarWorkspace    // Root workspace
	VarOut          = gg_.VarOut          // block output path
	VarFlows        = gg_.VarFlows        // Flows root
)

//-- functions --//

func SetAppName(value string) {
	gg_.AppName = value
}

func GetAppName() string {
	return gg_.AppName
}

func SetAppVersion(value string) {
	gg_.AppVersion = value
}

func GetAppVersion() string {
	return gg_.AppVersion
}

func SetAppSalt(value string) {
	gg_.AppSalt = value
}

func GetAppSalt() string {
	return gg_.AppSalt
}

var Recover = gg_.Recover
