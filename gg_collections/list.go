package gg_collections

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_"
	"math/rand"
	"path/filepath"
	"sort"
	"sync"
	"time"
)

// List is a generic container that stores elements of any type T.
// It supports a maximum size and can operate as a FIFO r LIFO.
// Elements are stored in a slice, and access to the container is synchronized using a mutex to ensure thread safety.
// LIFO mode is default (Last In - First Out).
// Order of elements depends on "FIFO" or "LIFO" mode.
// If "LIFO", this object works as a normal array (stack) where items are appended at the end with Push() method.
// If "FIFO", this object works as a reversed array (queue) where items are inserted into first place with Push().
type List[T any] struct {
	Data    []T  `json:"data"`     // list data
	MaxSize int  `json:"max_size"` // max size of the list
	FIFO    bool `json:"fifo"`     // first in - first out

	mux *sync.Mutex
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewList[T any](maxSize int) (response *List[T]) {
	response = new(List[T])
	response.Data = make([]T, 0)
	response.MaxSize = maxSize
	response.mux = new(sync.Mutex)

	return
}

func NewListFIFO[T any](maxSize int) (response *List[T]) {
	response = NewList[T](maxSize).SetFIFO(true)
	return
}

func NewListLIFO[T any](maxSize int) (response *List[T]) {
	response = NewList[T](maxSize).SetFIFO(false)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	s e r i a l i z a t i o n
//----------------------------------------------------------------------------------------------------------------------

func (instance *List[T]) Encode() (response []byte, err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response, err = gg_.Serialize(instance)
	}
	return
}

func (instance *List[T]) Decode(byteData []byte) (err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		err = gg_.DeserializeBytes(byteData, &instance)
	}
	return
}

func (instance *List[T]) SaveToFile(filename string) (err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		err = instance.saveToFile(filename, instance.Data)
	}
	return
}

func (instance *List[T]) MapToFile(filename string, callback func(item T, index int, arr []T) (selected T, err error)) (err error) {
	if nil != instance && nil != callback {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response := instance.mapItems(callback)
		err = instance.saveToFile(filename, response)
	}
	return
}

func (instance *List[T]) LoadFromFile(filename string) (err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		ext := filepath.Ext(filename)
		if ext == ".json" {
			err = gg.JSON.ReadFromFile(filename, &instance)
		} else {
			var byteData []byte
			byteData, err = gg.IO.ReadBytesFromFile(filename)
			if nil != err {
				return
			}
			err = gg_.DeserializeBytes(byteData, &instance)
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r o p e r t i e s
//----------------------------------------------------------------------------------------------------------------------

func (instance *List[T]) SetFIFO(v bool) *List[T] {
	if nil != instance {
		instance.FIFO = v
	}
	return instance
}

func (instance *List[T]) GetFIFO() (response bool) {
	if nil != instance {
		return instance.FIFO
	}
	return
}

func (instance *List[T]) SetLIFO(v bool) *List[T] {
	if nil != instance {
		instance.FIFO = !v
	}
	return instance
}

func (instance *List[T]) GetLIFO() (response bool) {
	if nil != instance {
		return !instance.FIFO
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *List[T]) String() string {
	if nil != instance {
		return gg_.Stringify(instance.Data)
	}
	return ""
}

func (instance *List[T]) Clear()*List[T]{
	if nil!=instance{
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.Data = make([]T, 0)
	}
	return instance
}

func (instance *List[T]) Len() (response int) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = len(instance.Data)
	}
	return
}

func (instance *List[T]) Items() (response []T) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = make([]T, len(instance.Data))
		copy(response, instance.Data)

		return instance.Data
	}
	return
}

func (instance *List[T]) Push(item T) *List[T] {
	if nil != instance && !gg_.IsNil(item) {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		// remove the older element if the size exceed the max size
		if instance.MaxSize > 0 && len(instance.Data) >= instance.MaxSize {
			_ = instance.pop()
		}
		instance.push(item)
	}
	return instance
}

func (instance *List[T]) Pop() (response T) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = instance.pop()
	}
	return
}

func (instance *List[T]) Get(index int) (response T) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if index >= 0 && index < len(instance.Data) {
			return instance.Data[index]
		}
	}
	return
}

func (instance *List[T]) First() (response T) {
	if nil != instance && len(instance.Data) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		return instance.Data[0]
	}
	return
}

func (instance *List[T]) Last() (response T) {
	if nil != instance && len(instance.Data) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		return instance.Data[len(instance.Data)-1]
	}
	return
}

func (instance *List[T]) Remove(index int) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	instance.remove(index)
}

func (instance *List[T]) Insert(index int, items ...T) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	instance.insert(index, items...)
}



//----------------------------------------------------------------------------------------------------------------------
//	s p e c i a l s
//----------------------------------------------------------------------------------------------------------------------

func (instance *List[T]) For(callback func(item T, index int, arr []T) error) {
	if nil != instance && nil != callback {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		for i, item := range instance.Data {
			err := callback(item, i, instance.Data)
			if nil != err {
				break
			}
		}
	}
}

func (instance *List[T]) Map(callback func(item T, index int, arr []T) (selected T, err error)) (response []T) {
	response = make([]T, 0)

	if nil != instance && nil != callback {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = instance.mapItems(callback)
	}
	return
}

func (instance *List[T]) Shuffle() *List[T] {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.shuffle()
	}
	return instance
}

func (instance *List[T]) Reverse() *List[T] {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.reverse()
	}
	return instance
}

func (instance *List[T]) Sort(sortFunc func(a, b T) bool) *List[T] {
	if nil != instance && nil != sortFunc {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.sort(sortFunc)
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *List[T]) insert(index int, values ...T) {
	if nil != instance {
		// remove nil
		items := gg_.ShrinkArray(values)
		instance.Data = append(instance.Data[:index], append(items, instance.Data[index:]...)...)
	}
}

func (instance *List[T]) remove(index int) {
	if index >= 0 && index < len(instance.Data) {
		instance.Data = append(instance.Data[:index], instance.Data[index+1:]...)
	}
}

func (instance *List[T]) shuffle() {
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	rnd.Shuffle(len(instance.Data), func(i, j int) {
		instance.Data[i], instance.Data[j] = instance.Data[j], instance.Data[i]
	})
}

func (instance *List[T]) reverse() {
	for i := 0; i < len(instance.Data)/2; i++ {
		instance.Data[i], instance.Data[len(instance.Data)-1-i] = instance.Data[len(instance.Data)-1-i], instance.Data[i]
	}
}

func (instance *List[T]) sort(sortFunc func(a, b T) bool) {
	sort.Slice(instance.Data, func(ii, jj int) bool {
		i := instance.Data[ii]
		j := instance.Data[jj]
		b := sortFunc(i, j)
		return b
	})
}

func (instance *List[T]) pop() (response T) {
	if nil != instance {
		if len(instance.Data) == 0 {
			return
		}

		response = instance.Data[len(instance.Data)-1]
		instance.Data = instance.Data[:len(instance.Data)-1]
	}
	return
}

func (instance *List[T]) push(item T) {
	if nil != instance {
		if instance.FIFO {
			// First In, First Out = take from bottom
			// 3, 2, 1, 0
			if len(instance.Data) == 0 {
				instance.Data = append(instance.Data, item)
			} else {
				instance.insert(0, item)
			}
		} else {
			// Last In, First Out = take from top
			// 0, 1, 2, 3
			instance.Data = append(instance.Data, item)
		}
	}
}

func (instance *List[T]) saveToFile(filename string, data []T) (err error) {
	if nil != instance {

		item := NewList[T](instance.MaxSize)
		item.FIFO = instance.FIFO
		item.Data = gg_.ShrinkArray(data)

		ext := filepath.Ext(filename)
		if ext == ".json" {
			s := gg.JSON.Stringify(item)
			_, err = gg.IO.WriteTextToFile(s, filename)
		} else {
			var byteData []byte
			byteData, err = gg_.Serialize(item)
			if nil != err {
				return
			}
			_, err = gg.IO.WriteBytesToFile(byteData, filename)
		}
	}
	return
}

func (instance *List[T]) mapItems(callback func(item T, index int, arr []T) (selected T, err error)) (response []T) {
	response = make([]T, 0)
	for i, item := range instance.Data {
		v, e := callback(item, i, instance.Data)
		if nil == e && !gg_.IsNil(v) && !gg_.IsEmpty(v) {
			response = append(response, v)
		}
		if nil != e {
			break
		}
	}
	return
}
