##!/bin/sh
NAME="myapp"
BASE="0.1"

# silicon 64 bit
GOOS=darwin GOARCH=amd64 go build -o __build/darwin-amd64/$NAME _test/app/cmd/main.go
echo "Compiled $NAME darwin-amd64"
#intel 64 bit
CGO_ENABLED=1 GOOS=darwin GOARCH=arm64 go build -o __build/darwin-arm64/$NAME _test/app/cmd/main.go
echo "Compiled $NAME darwin-arm64"