package icns

import (
	"bytes"
	"image"
	"image/png"
	"io"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Icon
// ---------------------------------------------------------------------------------------------------------------------

// Icon encodes an icns icon.
type Icon struct {
	Type  OsType
	Image image.Image

	header    [8]byte
	headerSet bool
	data      []byte
}

// WriteTo encodes the icon into wr.
func (instance *Icon) WriteTo(wr io.Writer) (int64, error) {
	var written int64
	if err := instance.encodeImage(); err != nil {
		return written, err
	}
	size, err := instance.writeHeader(wr)
	written += size
	if err != nil {
		return written, err
	}
	size, err = instance.writeData(wr)
	written += size
	if err != nil {
		return written, err
	}
	return written, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	Icon  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Icon) encodeImage() error {
	if len(instance.data) > 0 {
		return nil
	}
	data, err := encodeImage(instance.Image)
	if err != nil {
		return err
	}
	instance.data = data
	return nil
}

func (instance *Icon) writeHeader(wr io.Writer) (int64, error) {
	if !instance.headerSet {
		defer func() { instance.headerSet = true }()
		instance.header[0] = instance.Type.ID[0]
		instance.header[1] = instance.Type.ID[1]
		instance.header[2] = instance.Type.ID[2]
		instance.header[3] = instance.Type.ID[3]
		length := uint32(len(instance.data) + 8)
		writeUint32(instance.header[4:8], length)
	}
	written, err := wr.Write(instance.header[:8])
	return int64(written), err
}

func (instance *Icon) writeData(wr io.Writer) (int64, error) {
	written, err := wr.Write(instance.data)
	return int64(written), err
}

// ---------------------------------------------------------------------------------------------------------------------
//	IconSet
// ---------------------------------------------------------------------------------------------------------------------

// IconSet encodes a set of icons into an ICNS file.
type IconSet struct {
	Icons []*Icon

	header    [8]byte
	headerSet bool
	data      []byte
}

// WriteTo writes the ICNS file to wr.
func (instance *IconSet) WriteTo(wr io.Writer) (int64, error) {
	var written int64
	if err := instance.encodeIcons(); err != nil {
		return written, err
	}
	size, err := instance.writeHeader(wr)
	written += size
	if err != nil {
		return written, err
	}
	size, err = instance.writeData(wr)
	written += size
	if err != nil {
		return written, err
	}
	return written, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	IconSet  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *IconSet) encodeIcons() error {
	if len(instance.data) > 0 {
		return nil
	}
	buf := bytes.NewBuffer(nil)
	for _, icon := range instance.Icons {
		if icon == nil {
			continue
		}
		if _, err := icon.WriteTo(buf); err != nil {
			return err
		}
	}
	instance.data = buf.Bytes()
	return nil
}

func (instance *IconSet) writeHeader(wr io.Writer) (int64, error) {
	if !instance.headerSet {
		defer func() { instance.headerSet = true }()
		instance.header[0] = 'i'
		instance.header[1] = 'c'
		instance.header[2] = 'n'
		instance.header[3] = 's'
		length := uint32(len(instance.data) + 8)
		writeUint32(instance.header[4:8], length)
	}
	written, err := wr.Write(instance.header[:8])
	return int64(written), err
}

func (instance *IconSet) writeData(wr io.Writer) (int64, error) {
	written, err := wr.Write(instance.data)
	return int64(written), err
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func encodeImage(img image.Image) ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if err := png.Encode(buf, img); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
