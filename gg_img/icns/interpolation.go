package icns

import (
	"bitbucket.org/digi-sense/gg-core/gg_img/resize"
)

// InterpolationFunction is the algorithm used to resize the image.
type InterpolationFunction = resize.InterpolationFunction

// InterpolationFunction constants.
const (
	// NearestNeighbor Nearest-neighbor interpolation
	NearestNeighbor InterpolationFunction = iota
	// Bilinear interpolation
	Bilinear
	// Bicubic interpolation (with cubic hermite spline)
	Bicubic
	// MitchellNetravali Mitchell-Netravali interpolation
	MitchellNetravali
	// Lanczos2 Lanczos interpolation (a=2)
	Lanczos2
	// Lanczos3 Lanczos interpolation (a=3)
	Lanczos3
)
