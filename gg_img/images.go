package gg_img

import (
	"bitbucket.org/digi-sense/gg-core/gg_img/icns"
	"bitbucket.org/digi-sense/gg-core/gg_img/resize"
	"image"
)

var Images *ImageHelper

func init() {
	Images = &ImageHelper{
		Icns: icns.Icns,
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	ImageHelper
// ---------------------------------------------------------------------------------------------------------------------

type ImageHelper struct {
	Icns *icns.IcnsHelper
}

// ---------------------------------------------------------------------------------------------------------------------
//	r e s i z e
// ---------------------------------------------------------------------------------------------------------------------

// Resize scales an image to new width and height using the interpolation function interp.
// A new image with the given dimensions will be returned.
// If one of the parameters width or height is set to 0, its size will be calculated so that
// the aspect ratio is that of the originating image.
// The resizing algorithm uses channels for parallel computation.
// If the input image has width or height of 0, it is returned unchanged.
func (instance *ImageHelper) Resize(width, height uint, img image.Image, interp resize.InterpolationFunction) image.Image {
	if nil != instance {
		return resize.Resize(width, height, img, interp)
	}
	return nil
}

func (instance *ImageHelper) Thumbnail(maxWidth, maxHeight uint, img image.Image, interp resize.InterpolationFunction) image.Image {
	if nil != instance {
		return resize.Thumbnail(maxWidth, maxHeight, img, interp)
	}
	return nil
}
