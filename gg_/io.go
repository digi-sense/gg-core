package gg_

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	I O
//----------------------------------------------------------------------------------------------------------------------

func IORemove(filenames ...string) (undeleted []string, errs []error) {
	for _, filename := range filenames {
		if PathIsFile(filename) {
			e := os.Remove(filename)
			if nil != e {
				errs = append(errs, e)
				undeleted = append(undeleted, filename)
				continue
			}
		} else {
			e := os.RemoveAll(filename)
			if nil != e {
				errs = append(errs, e)
				undeleted = append(undeleted, filename)
				continue
			}
		}

	}
	return
}

func IORemoveSilent(filenames ...string) {
	_, _ = IORemove(filenames...)
}

func IOReaderOf(input interface{}) (reader io.Reader, err error) {
	if nil != input {
		if s, ok := input.(string); ok {
			if IsValidJson(s) {
				reader = strings.NewReader(s)
			} else {
				// file
				f, e := os.Open(s)
				if nil != e {
					err = e
					return
				}
				reader = f
			}
			return
		}
		if b, ok := input.([]byte); ok {
			reader = bytes.NewReader(b)
			return
		}
		if f, ok := input.(*os.File); ok {
			reader = f
			return
		}
	} else {
		err = errors.New("missing_input")
	}
	return
}

func IOReadBytesFromFile(fileName string) ([]byte, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	b, err := io.ReadAll(file)
	return b, err
}

func IOReadTextFromFile(fileName string) (string, error) {
	b, err := IOReadBytesFromFile(fileName)
	return string(b), err
}

func IOReadHashFromFile(fileName string) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := sha256.New()
	if _, cpErr := io.Copy(hash, file); cpErr != nil {
		return "", cpErr
	}
	return hex.EncodeToString(hash.Sum(nil)), nil
}

func IOReadHashFromBytes(data []byte) (string, error) {
	reader := bytes.NewReader(data)
	hash := sha256.New()
	if _, err := io.Copy(hash, reader); err != nil {
		return "", err
	}
	return hex.EncodeToString(hash.Sum(nil)), nil
}

func IOReadHash(docs ...interface{}) (response string, err error) {
	if len(docs) > 0 {
		var buff bytes.Buffer
		for _, item := range docs {
			if nil != item {
				data := []byte(fmt.Sprintf("%v", item))
				if len(data) > 0 {
					buff.Write(data)
				}
			}
		}
		response, err = IOReadHashFromBytes(buff.Bytes())
	}
	return
}

func IOWriteTextToFile(text, file string, append bool) (bytes int, err error) {
	var f *os.File
	if !append {
		f, err = os.Create(file)
		if nil != err {
			return
		}
	} else {
		if b, _ := PathExists(file); b {
			f, err = os.OpenFile(file,
				os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		} else {
			f, err = os.Create(file)
		}
		if nil != err {
			return
		}
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	bytes, err = w.WriteString(text)
	_ = w.Flush()

	return
}

func IOWriteBytesToFile(data []byte, file string, append bool) (bytes int, err error) {
	var f *os.File
	if !append {
		f, err = os.Create(file)
		if nil != err {
			return
		}
	} else {
		if b, _ := PathExists(file); b {
			f, err = os.OpenFile(file,
				os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		} else {
			f, err = os.Create(file)
		}
		if nil != err {
			return
		}
	}

	defer f.Close()
	w := bufio.NewWriter(f)
	bytes, err = w.Write(data)
	_ = w.Flush()

	return
}
