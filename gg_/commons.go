package gg_

import (
	"errors"
	"os"
)

const LibVersion = "0.3.41"

var AppName = "New Application"
var AppVersion = "0.1.0" // change this for a global AppName variable
var AppSalt = "this-is-token-to-encrypt"

const DEF_TEMP = "./_temp"
const OS_PATH_SEPARATOR = string(os.PathSeparator)

var PanicSystemError = errors.New("panic_system_error")

const (
	EventOnStartApp = "on_start_app"
	EventOnCloseApp = "on_close_app"
)

const (
	ModeProduction = "production"
	ModeDebug      = "debug"
)

//-- variable names --//

const (
	VarDirEnv       = "dir_env"       // Root workspace
	VarDirWorkspace = "dir_workspace" // Root workspace
	VarDirFlows     = "dir_flows"     // Flows root
	VarDirLocal     = "dir_local"     // block root path
	VarDirLocalRel  = "dir_local_rel" // block root relative path
	VarDirOut       = "dir_out"       // block output path
	VarDirHome      = "dir_home"      // block root
	VarDirRoot      = "dir_root"      // block root
	VarHome         = "home"          // block root
	VarRoot         = "root"          // block root
	VarWorkspace    = "workspace"     // Root workspace
	VarOut          = "out"           // block output path
	VarFlows        = "flows"         // Flows root
)
