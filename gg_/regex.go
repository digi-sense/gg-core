package gg_

import (
	"regexp"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	r e g e x   u t i l s
//----------------------------------------------------------------------------------------------------------------------

func RegexpMatchBetween(text string, offset int, patternStart *regexp.Regexp, patternEnd *regexp.Regexp, cutset string) []string {
	text = Sub(text, offset, len(text))
	response := make([]string, 0)

	indexesStart := RegexpMatchIndex(text, patternStart) // [][]int
	for _, indexStart := range indexesStart {
		is := indexStart[0]
		ie := indexStart[1] // end of first pattern
		if is < ie {
			sub := Sub(text, ie, len(text))
			indexesEnd := RegexpMatchIndex(sub, patternEnd) // [][]int
			if len(indexesEnd) > 0 {
				indexEnd := indexesEnd[0][0]
				sub = Sub(sub, 0, indexEnd)
				if len(cutset) > 0 {
					sub = strings.Trim(sub, cutset)
				}
				response = append(response, sub)
			} else {
				if len(cutset) > 0 {
					sub = strings.Trim(sub, cutset)
				}
				response = append(response, sub)
			}
		}
	}

	return response
}

func RegexpMatchIndex(text string, regex *regexp.Regexp) [][]int {
	return regex.FindAllStringIndex(text, -1)
}
