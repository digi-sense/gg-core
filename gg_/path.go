package gg_

import (
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"unicode"
)

//----------------------------------------------------------------------------------------------------------------------
//	f i l e p a t h
//----------------------------------------------------------------------------------------------------------------------

func PathExtensionName(path string) string {
	return strings.Replace(filepath.Ext(path), ".", "", 1)
}

func PathBase(path string) string {
	if PathIsUrl(path) {
		uri, err := url.Parse(path)
		if nil != err {
			return ""
		}
		path := uri.Path
		if len(path) > 1 {
			ext := PathExtensionName(path)
			if len(ext) > 0 {
				return PathBase(path)
			}
			return filepath.Base(uri.Path) // i.e. http://file-no-ext
		}
		return ""
	} else {
		base := filepath.Base(path)
		return base
	}
}

// PathDir returns the directory part of a given path, ensuring it ends without a trailing slash.
// It handles both local file paths and URLs.
func PathDir(path string) (response string) {
	if PathIsUrl(path) {
		parsedURL, err := url.Parse(path)
		if err == nil {
			dir := parsedURL.JoinPath("../")
			response = dir.String()
		} else {
			response = filepath.Dir(path)
		}
	} else {
		response = filepath.Dir(path)
	}

	if len(response) > 0 && strings.HasSuffix(response, "/") {
		response = response[:len(response)-1]
	}
	return
}

func PathIsUrl(path string) bool {
	return strings.Index(path, "http") == 0
}

func PathIsAbs(path string) bool {
	if PathIsUrl(path) {
		return true
	}
	return filepath.IsAbs(path)
}

func PathConcat(paths ...string) (result string) {
	if len(paths) > 0 {
		root := paths[0]
		isUrl := PathIsUrl(root)
		if len(paths) > 1 {
			if isUrl {
				var err error
				result, err = url.JoinPath(root, paths[1:]...)
				if nil != err {
					result = filepath.Join(paths...)
					if PathIsUrl(result) && strings.Index(result, "://") == -1 {
						result = strings.Replace(result, ":/", "://", 1)
					}
				}
			} else {
				result = filepath.Join(paths...)
				if PathIsUrl(result) && strings.Index(result, "://") == -1 {
					result = strings.Replace(result, ":/", "://", 1)
				}
			}
		} else {
			if isUrl {
				result, _ = url.JoinPath(root, "")
			} else {
				result = filepath.Join(root, "")
			}
		}
	}
	return
}

func PathAbsolute(path string) string {
	abs, err := filepath.Abs(path)
	if nil == err {
		return abs
	}
	return path
}

func PathAbsolutize(path, root string) string {
	if PathIsAbs(path) {
		return path
	}
	if len(root) == 0 {
		return PathAbsolute(path)
	}
	return PathConcat(root, path)
}

// PathExists Check if a path exists and returns a boolean value or an error if access is denied
// @param "path" the Path to check
func PathExists(path string) (bool, error) {
	if !strings.Contains(path, "\n") && IsPath(path) {
		_, err := os.Stat(path)
		if err == nil {
			return true, nil
		}
		if os.IsNotExist(err) {
			return false, nil
		}
		return true, err
	}
	return false, nil // not a valid path
}

func PathIsFile(path string) bool {
	path = PathAbsolute(path)
	fi, err := os.Lstat(path)
	if nil == err {
		mode := fi.Mode()
		isDir := mode.IsDir()
		return !isDir
	} else {
		if strings.HasSuffix(path, OS_PATH_SEPARATOR) {
			// is a directory
			return false
		}

		// path or file does not exists
		// just check if has extension
		ext := filepath.Ext(path)
		return PathIsFileExt(ext)
	}
}

func PathIsFileExt(s string) bool {
	s = strings.TrimSpace(s)
	minLen := 0
	if strings.HasPrefix(s, ".") {
		minLen = 1
	}
	if len(s) > minLen && len(s) < 20 {
		for _, r := range s {
			if !unicode.IsNumber(r) && !unicode.IsLetter(r) && '.' != r {
				return false
			}
		}
		return true
	}
	return false
}

// PathMkdir Creates a directory and all subdirectories if does not exists
func PathMkdir(path string) (err error) {
	// ensure we have a directory
	var abs string
	if filepath.IsAbs(path) {
		abs = path
	} else {
		abs, err = filepath.Abs(path)
	}

	if nil == err {
		if PathIsFile(abs) {
			abs = filepath.Dir(abs)
		}

		if !strings.HasSuffix(abs, OS_PATH_SEPARATOR) {
			path = abs + string(os.PathSeparator)
		} else {
			path = abs
		}

		var b bool
		if b, err = PathExists(path); !b && nil == err {
			err = os.MkdirAll(path, os.ModePerm)
		}

	}

	return err
}

func PathChangeFileNameExtension(fromPath, toFileExtension string) string {
	parent := filepath.Dir(fromPath)
	base := filepath.Base(fromPath)
	ext := filepath.Ext(base)
	name := strings.Replace(base, ext, "", 1)
	return filepath.Join(parent, name+ensureDot(toFileExtension))
}

func ensureDot(extension string) string {
	if strings.Index(extension, ".") == -1 {
		extension = "." + extension
	}
	return extension
}
