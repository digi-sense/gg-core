package gg_

import (
	"regexp"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

//----------------------------------------------------------------------------------------------------------------------
//	s t r i n g s
//----------------------------------------------------------------------------------------------------------------------

func Quote(v interface{}) string {
	return strconv.Quote(String(v))
}

// Sub get a substring
// @param s string The string
// @param start int Start index
// @param end int End index
func Sub(s string, start int, end int) string {
	runes := []rune(s) // convert in rune to handle all characters.
	if start < 0 || start > end {
		start = 0
	}
	if end > len(runes) {
		end = len(runes)
	}

	return string(runes[start:end])
}

func MatchBetween(text string, offset int, patternStart string, patternEnd string, cutset string) []string {
	expStart := regexp.MustCompile(patternStart)
	expEnd := regexp.MustCompile(patternEnd)

	return RegexpMatchBetween(text, offset, expStart, expEnd, cutset)
}

// Split using all runes in a string of separators
func Split(s string, seps string) []string {
	return strings.FieldsFunc(s, func(r rune) bool {
		for _, sep := range seps {
			if r == sep {
				return true
			}
		}
		return false
	})
}

// SnakeCase from "ThisIsAName" to "this_is_a_name".
// Used to format json field names
func SnakeCase(s string) string {
	b := buffer{
		r: make([]byte, 0, len(s)),
	}
	var m rune
	var w bool
	for _, ch := range s {
		if unicode.IsUpper(ch) {
			if m != 0 {
				if !w {
					b.indent()
					w = true
				}
				b.write(m)
			}
			m = unicode.ToLower(ch)
		} else {
			if m != 0 {
				b.indent()
				b.write(m)
				m = 0
				w = false
			}
			b.write(ch)
		}
	}
	if m != 0 {
		if !w {
			b.indent()
		}
		b.write(m)
	}
	return string(b.r)
}

func Title(s string) string {
	if len(s) > 0 {
		// from ThisIsATitle into this_is_a_title
		underscored := SnakeCase(s)
		// from this_is_a_title into "this is a title"
		text := strings.Join(Split(underscored, "_- "), " ")
		// from "this is a title" to "This Is A Title"
		return strings.Title(text)
	}
	return ""
}

func CapitalizeFirst(text string) string {
	if len(text) > 0 {
		words := Split(text, " ")
		if len(words) > 0 {
			words[0] = strings.Title(words[0])
			return strings.Join(words, " ")
		}
	}
	return text
}

//----------------------------------------------------------------------------------------------------------------------
//	buffer
//----------------------------------------------------------------------------------------------------------------------

type buffer struct {
	r         []byte
	runeBytes [utf8.UTFMax]byte
}

func (b *buffer) write(r rune) {
	if r < utf8.RuneSelf {
		b.r = append(b.r, byte(r))
		return
	}
	n := utf8.EncodeRune(b.runeBytes[0:], r)
	b.r = append(b.r, b.runeBytes[0:n]...)
}

func (b *buffer) indent() {
	if len(b.r) > 0 {
		b.r = append(b.r, '_')
	}
}
