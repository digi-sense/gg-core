package gg_

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n v e r t
//----------------------------------------------------------------------------------------------------------------------

func Bytes(val interface{}) (response []byte) {
	response = make([]byte, 0)
	if nil != val {
		if data, b := val.([]byte); b {
			response = data
			return
		}
		if s, b := val.(string); b {
			response = []byte(s)
			return
		}

		// convert to string
		return []byte(String(val))
	}
	return
}

func String(val interface{}) string {
	if nil == val {
		return ""
	}
	// string
	s, ss := val.(string)
	if ss {
		return s
	}
	// integer
	i, ii := val.(int)
	if ii {
		return strconv.Itoa(i)
	}
	// float32
	f, ff := val.(float32)
	if ff {
		return fmt.Sprintf("%g", f) // Exponent as needed, necessary digits only
	}
	// float 64
	F, FF := val.(float64)
	if FF {
		return fmt.Sprintf("%g", F) // Exponent as needed, necessary digits only
		// return strconv.FormatFloat(F, 'E', -1, 64)
	}

	// boolean
	b, bb := val.(bool)
	if bb {
		return strconv.FormatBool(b)
	}

	// array
	if aa, _ := IsArrayValue(val); aa {
		// byte array??
		if ba, b := val.([]byte); b {
			return string(ba)
		} else {
			data, err := json.Marshal(val)
			if nil == err {
				return string(data)
			}
		}
	}

	// map
	if b, _ := IsMapValue(val); b {
		data, err := json.Marshal(val)
		if nil == err {
			return string(data)
		}
	}

	// struct
	if b, _ := IsStructValue(val); b {
		data, err := json.Marshal(val)
		if nil == err {
			return string(data)
		}
	}

	// undefined value
	return fmt.Sprintf("%v", val)
}

func Bool(val interface{}, def bool) bool {
	if nil == val {
		return def
	}
	if v, b := val.(bool); b {
		return v
	}
	if s, b := IsString(val); b {
		v, err := strconv.ParseBool(s)
		if nil == err {
			return v
		}
	}
	if a, b := val.([]byte); b {
		if len(a) > 1 {
			v, err := strconv.ParseBool(string(a))
			if nil == err {
				return v
			}
		} else {
			if a[0] == 0 {
				return false
			} else {
				return true
			}
		}
	}
	return def
}

func Map(val interface{}) (response map[string]interface{}) {
	var err error
	response = map[string]interface{}{}
	if nil != val {
		// string
		if s, b := IsString(val); b {
			response, err = stringToMap(s)
			if nil == err {
				return
			}
		}
		// map
		if m, b := IsMap[interface{}](val); b {
			return Map(Stringify(m))
		}
		// object
		if response, err = stringToMap(String(val)); nil == err {
			return
		}
	}
	return
}

func Number(s string, def interface{}) interface{} {
	s = strings.TrimSpace(s)
	v, err := strconv.Atoi(s)
	if nil == err {
		return v
	} else {
		pi, pe := strconv.ParseInt(s, 10, 64)
		if nil == pe {
			return pi
		} else {
			pi, pe := strconv.ParseFloat(s, 64)
			if nil == pe {
				return pi
			}
		}
	}
	return def
}

func Int(val interface{}, def int) int {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Int(n, def)
	}
	switch i := val.(type) {
	case float32:
		return int(i)
	case float64:
		return int(i)
	case int:
		return i
	case int8:
		return int(i)
	case int16:
		return int(i)
	case int32:
		return int(i)
	case int64:
		return int(i)
	case uint:
		return int(i)
	case uint8:
		return int(i)
	case uint16:
		return int(i)
	case uint32:
		return int(i)
	case uint64:
		return int(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Int(s, def)
		}
	}

	return def
}

func Int8(val interface{}, def int8) int8 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Int8(n, def)
	}
	switch i := val.(type) {
	case float32:
		return int8(i)
	case float64:
		return int8(i)
	case int:
		return int8(i)
	case int8:
		return i
	case int16:
		return int8(i)
	case int32:
		return int8(i)
	case int64:
		return int8(i)
	case uint:
		return int8(i)
	case uint8:
		return int8(i)
	case uint16:
		return int8(i)
	case uint32:
		return int8(i)
	case uint64:
		return int8(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Int8(s, def)
		}
	}

	return def
}

func Int16(val interface{}, def int16) int16 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Int16(n, def)
	}
	switch i := val.(type) {
	case float32:
		return int16(i)
	case float64:
		return int16(i)
	case int:
		return int16(i)
	case int8:
		return int16(i)
	case int16:
		return i
	case int32:
		return int16(i)
	case int64:
		return int16(i)
	case uint:
		return int16(i)
	case uint8:
		return int16(i)
	case uint16:
		return int16(i)
	case uint32:
		return int16(i)
	case uint64:
		return int16(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Int16(s, def)
		}
	}

	return def
}

func Int32(val interface{}, def int32) int32 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Int32(n, def)
	}
	switch i := val.(type) {
	case float32:
		return int32(i)
	case float64:
		return int32(i)
	case int:
		return int32(i)
	case int8:
		return int32(i)
	case int16:
		return int32(i)
	case int32:
		return i
	case int64:
		return int32(i)
	case uint:
		return int32(i)
	case uint8:
		return int32(i)
	case uint16:
		return int32(i)
	case uint32:
		return int32(i)
	case uint64:
		return int32(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Int32(s, def)
		}
	}

	return def
}

func Int64(val interface{}, def int64) int64 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Int64(n, def)
	}
	switch i := val.(type) {
	case float32:
		return int64(i)
	case float64:
		return int64(i)
	case int:
		return int64(i)
	case int8:
		return int64(i)
	case int16:
		return int64(i)
	case int32:
		return int64(i)
	case int64:
		return i
	case uint:
		return int64(i)
	case uint8:
		return int64(i)
	case uint16:
		return int64(i)
	case uint32:
		return int64(i)
	case uint64:
		return int64(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Int64(s, def)
		}
	}

	return def
}

func Uint(val interface{}, def uint) uint {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Uint(n, def)
	}
	switch i := val.(type) {
	case float32:
		return uint(i)
	case float64:
		return uint(i)
	case int:
		return uint(i)
	case int8:
		return uint(i)
	case int16:
		return uint(i)
	case int32:
		return uint(i)
	case int64:
		return uint(i)
	case uint:
		return i
	case uint8:
		return uint(i)
	case uint16:
		return uint(i)
	case uint32:
		return uint(i)
	case uint64:
		return uint(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Uint(s, def)
		}
	}

	return def
}

func Uint8(val interface{}, def uint8) uint8 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Uint8(n, def)
	}
	switch i := val.(type) {
	case float32:
		return uint8(i)
	case float64:
		return uint8(i)
	case int:
		return uint8(i)
	case int8:
		return uint8(i)
	case int16:
		return uint8(i)
	case int32:
		return uint8(i)
	case int64:
		return uint8(i)
	case uint:
		return uint8(i)
	case uint8:
		return i
	case uint16:
		return uint8(i)
	case uint32:
		return uint8(i)
	case uint64:
		return uint8(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Uint8(s, def)
		}
	}

	return def
}

func Uint16(val interface{}, def uint16) uint16 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Uint16(n, def)
	}
	switch i := val.(type) {
	case float32:
		return uint16(i)
	case float64:
		return uint16(i)
	case int:
		return uint16(i)
	case int8:
		return uint16(i)
	case int16:
		return uint16(i)
	case int32:
		return uint16(i)
	case int64:
		return uint16(i)
	case uint:
		return uint16(i)
	case uint8:
		return uint16(i)
	case uint16:
		return i
	case uint32:
		return uint16(i)
	case uint64:
		return uint16(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Uint16(s, def)
		}
	}

	return def
}

func Uint32(val interface{}, def uint32) uint32 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Uint32(n, def)
	}
	switch i := val.(type) {
	case float32:
		return uint32(i)
	case float64:
		return uint32(i)
	case int:
		return uint32(i)
	case int8:
		return uint32(i)
	case int16:
		return uint32(i)
	case int32:
		return uint32(i)
	case int64:
		return uint32(i)
	case uint:
		return uint32(i)
	case uint8:
		return uint32(i)
	case uint16:
		return uint32(i)
	case uint32:
		return i
	case uint64:
		return uint32(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Uint32(s, def)
		}
	}

	return def
}

func Uint64(val interface{}, def uint64) uint64 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Uint64(n, def)
	}
	switch i := val.(type) {
	case float32:
		return uint64(i)
	case float64:
		return uint64(i)
	case int:
		return uint64(i)
	case int8:
		return uint64(i)
	case int16:
		return uint64(i)
	case int32:
		return uint64(i)
	case int64:
		return uint64(i)
	case uint:
		return uint64(i)
	case uint8:
		return uint64(i)
	case uint16:
		return uint64(i)
	case uint32:
		return uint64(i)
	case uint64:
		return i
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Uint64(s, def)
		}
	}

	return def
}

func Float32(val interface{}, def float32) float32 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Float32(n, def)
	}
	switch i := val.(type) {
	case float32:
		return i
	case float64:
		return float32(i)
	case int:
		return float32(i)
	case int8:
		return float32(i)
	case int16:
		return float32(i)
	case int32:
		return float32(i)
	case int64:
		return float32(i)
	case uint:
		return float32(i)
	case uint8:
		return float32(i)
	case uint16:
		return float32(i)
	case uint32:
		return float32(i)
	case uint64:
		return float32(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Float32(s, def)
		}
	}

	return def
}

func Float64(val interface{}, def float64) float64 {
	if s, b := IsString(val); b {
		n := Number(s, def)
		return Float64(n, def)
	}
	switch i := val.(type) {
	case float32:
		return float64(i)
	case float64:
		return i
	case int:
		return float64(i)
	case int8:
		return float64(i)
	case int16:
		return float64(i)
	case int32:
		return float64(i)
	case int64:
		return float64(i)
	case uint:
		return float64(i)
	case uint8:
		return float64(i)
	case uint16:
		return float64(i)
	case uint32:
		return float64(i)
	case uint64:
		return float64(i)
	default:
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.Ptr {
			s := fmt.Sprintf("%v", ValueOf(i))
			return Float64(s, def)
		}
	}

	return def
}
