package gg_

import (
	"encoding/json"
	"fmt"
	"net/url"
	"reflect"
	"sort"
	"strings"
	"unicode"
)

func ShrinkArray[T any](items []T) (response []T) {
	response = make([]T, 0)
	for _, item := range items {
		if !IsNil(item) {
			response = append(response, item)
		}
	}
	return
}

func ShrinkMap[T any](items map[string]T) (response map[string]T) {
	response = make(map[string]T)
	for k, v := range items {
		if !IsEmpty(v) {
			response[k] = v
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	compare
//----------------------------------------------------------------------------------------------------------------------

func Compare(item1, item2 interface{}) int {
	v1 := ValueOf(item1)
	v2 := ValueOf(item2)
	if v1.Kind() == v2.Kind() {

		// native
		switch v1.Kind() {
		case reflect.Struct, reflect.Map, reflect.Slice:
			c1 := v1.Interface()
			c2 := v2.Interface()
			if reflect.DeepEqual(c1, c2) {
				return 0
			} else {
				// string check
				s1 := fmt.Sprintf("%v", c1)
				s2 := fmt.Sprintf("%v", c2)
				if s1 > s2 {
					return 1
				} else {
					return -1
				}
			}
		case reflect.Bool:
			c1 := v1.Interface().(bool)
			c2 := v2.Interface().(bool)
			if c1 == c2 {
				return 0
			} else if c1 {
				return 1
			} else {
				return -1
			}
		case reflect.Int:
			c1 := v1.Interface().(int)
			c2 := v2.Interface().(int)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Int8:
			c1 := v1.Interface().(int8)
			c2 := v2.Interface().(int8)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Int16:
			c1 := v1.Interface().(int16)
			c2 := v2.Interface().(int16)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Int32:
			c1 := v1.Interface().(int32)
			c2 := v2.Interface().(int32)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Int64:
			c1 := v1.Interface().(int64)
			c2 := v2.Interface().(int64)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Uint:
			c1 := v1.Interface().(uint)
			c2 := v2.Interface().(uint)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Uint8:
			c1 := v1.Interface().(uint8)
			c2 := v2.Interface().(uint8)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Uint16:
			c1 := v1.Interface().(uint16)
			c2 := v2.Interface().(uint16)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Uint32:
			c1 := v1.Interface().(uint32)
			c2 := v2.Interface().(uint32)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Uint64:
			c1 := v1.Interface().(uint64)
			c2 := v2.Interface().(uint64)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Float32:
			c1 := v1.Interface().(float32)
			c2 := v2.Interface().(float32)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.Float64:
			c1 := v1.Interface().(float64)
			c2 := v2.Interface().(float64)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		case reflect.String:
			c1 := v1.Interface().(string)
			c2 := v2.Interface().(string)
			if c1 == c2 {
				return 0
			} else if c1 > c2 {
				return 1
			} else {
				return -1
			}
		default:
			return -1
		}
	}
	return -1
}

func IsGreater(item1, item2 interface{}) bool {
	return Compare(item1, item2) > 0
}

func IsLower(item1, item2 interface{}) bool {
	return Compare(item1, item2) < 0
}

func IsNil[T any](t T) bool {
	v := reflect.ValueOf(t)
	kind := v.Kind()
	if kind == reflect.Invalid {
		return true
	}
	// Must be one of these types to be nillable
	return (kind == reflect.Ptr ||
		kind == reflect.Interface ||
		kind == reflect.Slice ||
		kind == reflect.Array ||
		kind == reflect.Map ||
		kind == reflect.Chan ||
		kind == reflect.Func ||
		kind == reflect.Struct) &&
		v.IsNil()
}

func IsEmpty[T any](t T) bool {
	if IsNil(t) {
		return true
	}
	v := reflect.ValueOf(t)
	k := v.Kind()
	switch k {
	case reflect.Array, reflect.Map, reflect.Slice, reflect.String:
		return v.Len() == 0
	case reflect.Bool:
		return !v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return v.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return v.Float() == 0
	case reflect.Interface, reflect.Ptr:
		return v.IsNil()
	default:
		return false
	}
}

func IsMapType(value any) bool {
	v := reflect.ValueOf(value)
	k := v.Kind()
	return k == reflect.Map
}

func IsArrayType(value any) bool {
	v := reflect.ValueOf(value)
	k := v.Kind()
	return k == reflect.Slice || k == reflect.Array
}

func IsStringType(value any) bool {
	v := reflect.ValueOf(value)
	k := v.Kind()
	return k == reflect.String
}

func IsNumType(value any) bool {
	v := reflect.ValueOf(value)
	k := v.Kind()
	return k == reflect.Uint || k == reflect.Uint8 || k == reflect.Uint16 || k == reflect.Uint32 ||
		k == reflect.Uint64 || k == reflect.Float32 || k == reflect.Float64 ||
		k == reflect.Int || k == reflect.Int8 || k == reflect.Int16 || k == reflect.Int32 || k == reflect.Int64
}

func IsString(val interface{}) (response string, ok bool) {
	if b, s := IsStringValue(val); b {
		response, ok = s.Interface().(string)
	}
	return
}

func IsMap[T any](val interface{}) (response map[string]T, ok bool) {
	if b, m := IsMapValue(val); b {
		response, ok = m.Interface().(map[string]T)
	}
	return
}

func IsArray[T any](val interface{}) (response []T, ok bool) {
	if b, m := IsArrayValue(val); b {
		response, ok = m.Interface().([]T)
	}
	return
}

func IsStringValue(val interface{}) (bool, reflect.Value) {
	rt := ValueOf(val)
	switch rt.Kind() {
	case reflect.String:
		return true, rt
	case reflect.Ptr:
		return IsStringValue(rt.Elem().Interface())
	default:
		return false, rt
	}
}

func IsStructValue(val interface{}) (bool, reflect.Value) {
	rt := ValueOf(val)
	switch rt.Kind() {
	case reflect.Struct:
		return true, rt
	case reflect.Ptr:
		return IsStructValue(rt.Elem().Interface())
	default:
		return false, rt
	}
}

func IsMapValue(val interface{}) (bool, reflect.Value) {
	rt := ValueOf(val)
	kind := rt.Kind()
	// fmt.Println(fmt.Sprintf("%v", kind))
	switch kind {
	case reflect.Map:
		return true, rt
	case reflect.Ptr:
		return IsMapValue(rt.Elem().Interface())
	default:
		return false, rt
	}
}

func IsArrayValue(val interface{}) (bool, reflect.Value) {
	rt := ValueOf(val)
	switch rt.Kind() {
	case reflect.Slice, reflect.Array:
		return true, rt
	default:
		return false, rt
	}
}

func IsArrayValueNotEmpty(array interface{}) (bool, reflect.Value) {
	s := ValueOf(array)
	b := (s.Kind() == reflect.Slice || s.Kind() == reflect.Array) && s.Len() > 0
	if b {
		return true, s
	}
	return false, s
}

//----------------------------------------------------------------------------------------------------------------------
//	compare text
//----------------------------------------------------------------------------------------------------------------------

// IsPath get a string and check if is a valid path
func IsPath(path string) bool {
	clean := strings.Trim(path, " ")
	if strings.Contains(clean, OS_PATH_SEPARATOR) {
		return true
	}
	return false
}

func IsValidJson(text string) bool {
	return IsValidJsonObject(text) || IsValidJsonArray(text)
}

func IsValidJsonObject(text string) bool {
	var js map[string]interface{}
	err := json.Unmarshal([]byte(text), &js)
	return nil == err
}

func IsValidJsonArray(text string) bool {
	var js []map[string]interface{}
	return json.Unmarshal([]byte(text), &js) == nil
}

func IsHTML(text string) bool {
	if strings.Index(text, "<") > -1 && (strings.Index(text, "</") > -1 || strings.Index(text, "/>") > -1) {
		return true
	}
	return false
}

func IsASCII(s string) bool {
	for i := 0; i < len(s); i++ {
		if s[i] > unicode.MaxASCII {
			return false
		}
	}
	return true
}

func IsLetter(s string) bool {
	for _, r := range s {
		if !unicode.IsLetter(r) {
			return false
		}
	}
	return true
}

func IsMark(s string) bool {
	for _, r := range s {
		if !unicode.IsLetter(r) {
			return false
		}
	}
	return true
}

func IsLetterOrPunct(s string) bool {
	for _, r := range s {
		l := unicode.IsLetter(r)
		m := unicode.IsPunct(r)
		if !l && !m {
			return false
		}
	}
	return true
}

//----------------------------------------------------------------------------------------------------------------------
//	SORT
//----------------------------------------------------------------------------------------------------------------------

func SortAscMap(data []map[string]interface{}, field string) {
	sort.Slice(data, func(i, j int) bool {
		m1 := data[i]
		m2 := data[j]
		if v1, ok := m1[field]; ok {
			if v2, ok := m2[field]; ok {
				return IsLower(v1, v2)
			}
		}
		return true
	})
}

func SortDescMap(data []map[string]interface{}, field string) {
	sort.Slice(data, func(i, j int) bool {
		m1 := data[i]
		m2 := data[j]
		if v1, ok := m1[field]; ok {
			if v2, ok := m2[field]; ok {
				return IsGreater(v1, v2)
			}
		}
		return true
	})
}

func SortAscArray(array interface{}) {
	if a, b := array.([]interface{}); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]string); b {
		sort.Strings(a)
	} else if a, b := array.([]byte); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]int); b {
		sort.Ints(a)
	} else if a, b := array.([]int8); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]int16); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]int32); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]int64); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]uint); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]uint8); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]uint16); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]uint32); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]uint64); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]uintptr); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]float32); b {
		sort.Slice(array, func(i, j int) bool {
			return IsLower(a[i], a[j])
		})
	} else if a, b := array.([]float64); b {
		sort.Float64s(a)
	}
}

func SortDescArray(array interface{}) {
	SortAscArray(array)
	ReverseArray(array)
}

func ReverseArray(array interface{}) {
	if a, b := array.([]interface{}); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]string); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]byte); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]int); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]int8); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]int16); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]int32); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]int64); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]uint); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]uint8); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]uint16); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]uint32); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]uint64); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]uintptr); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]float32); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	} else if a, b := array.([]float64); b {
		for left, right := 0, len(a)-1; left < right; left, right = left+1, right-1 {
			a[left], a[right] = a[right], a[left]
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	reflect
//----------------------------------------------------------------------------------------------------------------------

func ValueOf(item interface{}) reflect.Value {
	v := reflect.ValueOf(item)
	if nil != item {
		if value, ok := item.(reflect.Value); ok {
			return value
		}
		k := v.Kind()
		switch k {
		case reflect.Ptr:
			return ValueOf(v.Elem().Interface())
		}
	}
	return v
}

func GetAt(array interface{}, index int, defValue interface{}) (response interface{}) {
	response = defValue
	v := ValueOf(array)
	k := v.Kind()
	switch k {
	case reflect.Slice, reflect.Array:
		if v.Len() > index {
			response = v.Index(index).Interface()
		}
	default:
		response = defValue
	}
	return
}

func Get(object interface{}, path string) (response interface{}) {
	if len(path) == 0 {
		return nil
	}

	// map
	if b, value := IsMapValue(object); b {
		response = searchField(value, path)
		return
	}

	// struct
	if b, value := IsStructValue(object); b {
		response = searchField(value, path)
		return
	}

	// array
	if b, _ := IsArrayValue(object); b {
		i := Int(path, -1)
		if i > -1 {
			response = GetAt(object, i, nil)
		}
		return
	}

	// try with a struct
	v := reflect.ValueOf(object)
	if v.IsValid() {
		response = getFieldValue(v, path)
	}

	return
}

func GetString(object interface{}, path string) string {
	return String(Get(object, path))
}

func GetBytes(object interface{}, path string) []byte {
	return Bytes(Get(object, path))
}

func GetMap(object interface{}, path string) map[string]interface{} {
	return Map(Get(object, path))
}

func GetInt(object interface{}, path string) int {
	return Int(Get(object, path), -1)
}

func GetInt8(object interface{}, path string) int8 {
	return Int8(Get(object, path), -1)
}

func GetInt16(object interface{}, path string) int16 {
	return Int16(Get(object, path), -1)
}

func GetInt32(object interface{}, path string) int32 {
	return Int32(Get(object, path), -1)
}

func GetInt64(object interface{}, path string) int64 {
	return Int64(Get(object, path), -1)
}

func GetUint(object interface{}, path string) uint {
	return Uint(Get(object, path), 0)
}

func GetUint8(object interface{}, path string) uint8 {
	return Uint8(Get(object, path), 0)
}

func GetUint16(object interface{}, path string) uint16 {
	return Uint16(Get(object, path), 0)
}

func GetUint32(object interface{}, path string) uint32 {
	return Uint32(Get(object, path), 0)
}

func GetUint64(object interface{}, path string) uint64 {
	return Uint64(Get(object, path), 0)
}

func GetFloat32(object interface{}, path string) float32 {
	return Float32(Get(object, path), -1)
}

func GetFloat64(object interface{}, path string) float64 {
	return Float64(Get(object, path), -1)
}

func GetBool(object interface{}, path string) bool {
	return Bool(Get(object, path), false)
}

func Merge(override bool, context ...interface{}) (response map[string]interface{}) {
	response = map[string]interface{}{}
	if len(context) > 0 {
		for _, v := range context {
			m := Map(v)
			if m != nil {
				for k, vv := range m {
					if _, ok := response[k]; !ok || override {
						response[k] = vv
					}
				}
			}
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func stringToMap(text string) (response map[string]interface{}, err error) {
	response = map[string]interface{}{}
	if len(text) > 0 {

		// JSON object
		if IsValidJsonObject(text) {
			err = Unmarshal(text, &response)
			return
		}

		// JSON array
		if IsValidJsonArray(text) {
			var arr []interface{}
			err = Unmarshal(text, &arr)
			if err == nil {
				for i, item := range arr {
					response[fmt.Sprintf("%v", i)] = item
				}
			}
			return
		}

		// url like
		if strings.Index(text, "=") > 0 {
			var uri *url.URL
			uri, err = url.Parse("?" + text)
			if nil == err {
				query := uri.Query()
				if nil != query && len(query) > 0 {
					response = make(map[string]interface{})
					for k, v := range query {
						if len(v) == 1 {
							value := v[0]
							if IsValidJsonObject(value) {
								item := map[string]interface{}{}
								err = json.Unmarshal([]byte(value), &item)
								if nil == err {
									response[k] = item
								}
							} else {
								response[k] = value
							}
						}
					}
				}
			} else {
				return nil, err
			}
		}
	}
	return
}

func searchField(target reflect.Value, path string) (response interface{}) {
	// path = this.is.a.path
	tokens := strings.Split(path, ".")
	length := len(tokens)
	for i := 0; i < length; i++ {
		token := tokens[i]
		// fmt.Println("OBJECT: ", fmt.Sprintf("%v", mapValue))
		tv := ValueOf(getFieldValue(target, token)) // mapValue.MapIndex(reflect.ValueOf(token))
		if !tv.IsValid() {
			break // not found
		}
		if i == length-1 {
			// found value
			response = tv.Interface()
			return
		}
		if ok, subTarget := IsMapValue(tv); ok {
			target = subTarget // replace the point to map
			continue
		}
		if ok, subTarget := IsStructValue(tv); ok {
			target = subTarget // replace the point to struct
			continue
		}
	}
	return
}

func getFieldValue(e reflect.Value, field string) interface{} {
	switch e.Kind() {
	case reflect.Ptr:
		elem := e.Elem()
		return getFieldValue(elem, field)
	case reflect.Struct:
		f := e.FieldByName(strings.Title(field))
		if f.IsValid() {
			return f.Interface()
		}
	case reflect.Map:
		f := e.MapIndex(reflect.ValueOf(field))
		if f.IsValid() {
			return f.Interface()
		}
	default:
		return nil
	}
	return nil
}
