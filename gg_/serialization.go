package gg_

import (
	"bytes"
	"encoding/gob"
	"io"
)

//----------------------------------------------------------------------------------------------------------------------
//	s e r i a l i z a t i o n
//----------------------------------------------------------------------------------------------------------------------

func Serialize(obj interface{}) (response []byte, err error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(obj)
	if nil != err {
		return
	}
	response = buf.Bytes()
	return
}

func DeserializeBytes(data []byte, e any) (err error) {
	r := bytes.NewReader(data)
	err = Deserialize(r, e)

	return
}

func Deserialize(source io.Reader, e any) (err error) {
	dec := gob.NewDecoder(source)
	err = dec.Decode(e)

	return
}
