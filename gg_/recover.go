package gg_

import (
	"fmt"
	"log"
)

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func Recover(args ...interface{}) {
	if r := recover(); r != nil {
		// recovered from panic
		message := getMessage(r, args)
		logger := getLogger(args)
		if nil != logger {
			logger.Error(message)
		} else {
			log.Println(message)
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func getMessage(r interface{}, args ...interface{}) (response string) {
	method := ""
	for _, item := range args {
		if s, ok := item.(string); ok {
			method = s
			break
		}
	}
	if len(method) > 0 {
		response = fmt.Sprintf("Error in application '%s' on '%s': %s", AppName, method, r)
	} else {
		response = fmt.Sprintf("[panic] Generic error in application '%s': %s", AppName, r)
	}
	return
}

func getLogger(args ...interface{}) (response ILogger) {
	for _, item := range args {
		if logger, ok := item.(ILogger); ok {
			response = logger
			break
		}
	}
	return
}
