package gg_

import (
	"golang.org/x/text/language"
	"golang.org/x/text/language/display"
)

const DefLang = "en"

// Lang extracts the ISO 639-1 language code (e.g., "en", "it") from the provided language or locale code string.
func Lang(code string) string {
	tag := language.Make(code)
	base, _ := tag.Base()
	return base.String() // ISO2
}

// LangName return ISO language name (i.e. "Italian") from a lang code like "it"
func LangName(codeIn string, out ...string) string {
	if len(codeIn) > 0 {
		codeIn = Lang(codeIn)
		codeOut := codeIn
		if len(out) > 0 {
			codeOut = out[0]
		}

		tagIn, tagInErr := language.Parse(codeIn)
		if tagInErr != nil {
			tagIn, _ = language.Parse(DefLang)
		}
		tagOut, tagOutErr := language.Parse(codeOut)
		if tagOutErr != nil {
			tagOut, _ = language.Parse(DefLang)
		}

		namer := display.Languages(tagOut)
		name := namer.Name(tagIn)
		return CapitalizeFirst(name)
	}
	return DefLang
}
