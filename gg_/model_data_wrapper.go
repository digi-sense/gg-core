package gg_

import (
	"errors"
	"fmt"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ModelDataWrapper
// ---------------------------------------------------------------------------------------------------------------------

// ModelDataWrapper is a serializable object useful for data transport
type ModelDataWrapper struct {
	Uid         string                 `json:"uid"`
	Group       string                 `json:"group"`
	Name        string                 `json:"name"`
	Description string                 `json:"description"`
	Payload     map[string]interface{} `json:"payload"`

	source []interface{}
	err    error
}

func NewDataWrapper(data ...interface{}) (instance *ModelDataWrapper) {
	instance = new(ModelDataWrapper)
	instance.err = instance.reload(data...)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ModelDataWrapper) String() string {
	if nil != instance {
		return Stringify(instance.Data())
	}
	return ""
}

func (instance *ModelDataWrapper) Map() map[string]interface{} {
	m := Map(instance.String())
	return m
}

func (instance *ModelDataWrapper) HasError() bool {
	if nil != instance {
		return instance.err != nil
	}
	return false
}

func (instance *ModelDataWrapper) Error() error {
	if nil != instance {
		return instance.err
	}
	return nil
}

func (instance *ModelDataWrapper) Data(values ...interface{}) (response map[string]interface{}) {
	if nil != instance {
		instance.err = instance.reload(values...)
		response = instance.data()
		// add different versions of wrapped data
		response["dataObj"] = instance.DataObjects()
		response["dataString"] = instance.DataString()
		response["dataJSON"] = instance.DataJSON()
		response["dataNative"] = instance.source
	}
	return
}

// Source return original data
func (instance *ModelDataWrapper) Source() (response []interface{}) {
	if nil != instance {
		response = instance.source
	}
	if nil == response {
		response = make([]interface{}, 0)
	}
	return
}

// DataString return wrapped data as an array of JSON "quoted string" objects
func (instance *ModelDataWrapper) DataString() (response []string) {
	if nil != instance {
		response = make([]string, 0)
		for _, item := range instance.source {
			response = append(response, Quote(String(item)))
		}
	}
	return
}

// DataJSON return wrapped data as an array of JSON "stringify" objects
func (instance *ModelDataWrapper) DataJSON() (response []string) {
	if nil != instance {
		response = make([]string, 0)
		for _, item := range instance.source {
			response = append(response, Stringify(item))
		}
	}
	return
}

// DataObjects return wrapped data as an array of Map objects
func (instance *ModelDataWrapper) DataObjects() (response []map[string]interface{}) {
	if nil != instance {
		response = make([]map[string]interface{}, 0)
		for _, item := range instance.source {
			response = append(response, Map(item))
		}
	}
	return
}

func (instance *ModelDataWrapper) Reload(values ...interface{}) *ModelDataWrapper {
	if nil != instance {
		instance.err = instance.reload(values...)
	}
	return instance
}

func (instance *ModelDataWrapper) SaveToFile(filename string) (err error) {
	if nil != instance {
		_, err = IOWriteTextToFile(instance.String(), filename, false)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ModelDataWrapper) reload(data ...interface{}) (err error) {
	if nil != instance {
		if nil == instance.Payload {
			instance.Payload = make(map[string]interface{})
		}
		if nil == instance.source {
			instance.source = make([]interface{}, 0)
		}

		if len(data) > 0 {
			// try to assign to master data node
			if len(data) == 1 {
				d := data[0]
				if model, ok := d.(*ModelDataWrapper); ok {
					instance.Uid = model.Uid
					instance.Group = model.Group
					instance.Name = model.Name
					instance.Description = model.Description
					instance.Payload = model.Payload
					// reload source data
					err = instance.reload(model.Source()...)
					return
				}
				if m, ok := d.(map[string]interface{}); ok {
					if _, isWrapper := m["dataNative"]; isWrapper {
						// clone data
						clone := Map(Stringify(m))
						if source, exists := clone["dataNative"].([]interface{}); exists {
							instance.source = source
						}
						instance.Uid = GetString(clone, "uid")
						instance.Name = GetString(clone, "name")
						instance.Description = GetString(clone, "description")
						instance.Group = GetString(clone, "group")
						instance.Payload = GetMap(clone, "payload")
						return
					}
				}
				if s, ok := d.(string); ok {
					if IsValidJsonArray(s) {
						a := make([]interface{}, 0)
						err = Unmarshal(s, &a)
						if nil != err {
							return
						}
						err = instance.reload(a...)
						return
					} else if IsValidJsonObject(s) {
						m := Map(s)
						err = instance.reload(m)
						return
					} else if len(s) > 0 {
						s, err = IOReadTextFromFile(s)
						if nil != err {
							return
						}
						err = instance.reload(s)
						return
					}
					err = errors.New(fmt.Sprintf("Unable to parse data: %v", d))
					return
				}
			}

			instance.source = make([]interface{}, 0)
			for _, item := range data {
				instance.source = append(instance.source, item)
			}
		}
	}
	return
}

func (instance *ModelDataWrapper) data() (response map[string]interface{}) {
	if nil != instance {
		response = make(map[string]interface{})
		// header
		response["uid"] = instance.Uid
		response["name"] = instance.Name
		response["description"] = instance.Description
		response["group"] = instance.Group
		response["payload"] = instance.Payload
	}
	return
}
