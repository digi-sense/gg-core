package gg_

import (
	"encoding/json"
	"errors"
	"io"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	J S O N
//----------------------------------------------------------------------------------------------------------------------

func Marshal(entity interface{}) []byte {
	if nil != entity {
		if s, b := entity.(string); b {
			return []byte(s)
		}
		b, err := json.Marshal(&entity)
		if nil == err {
			return b
		}
	}

	return []byte{}
}

func Unmarshal(input interface{}, entity interface{}) (err error) {
	if s, b := input.(string); b {
		if IsValidJson(s) {
			err = json.Unmarshal([]byte(s), &entity)
		} else {
			err = UnmarshalFromFile(s, entity)
		}
		return
	}
	if s, b := input.([]byte); b {
		err = json.Unmarshal(s, &entity)
		return
	}
	if m, b := input.(map[string]interface{}); b {
		err = json.Unmarshal(Marshal(m), &entity)
		return
	}
	return
}

func UnmarshalFromFile(fileName string, entity interface{}) error {
	b, err := IOReadBytesFromFile(fileName)
	if nil != err {
		return err
	}
	err = json.Unmarshal(b, &entity)
	if nil != err {
		return err
	}
	return nil
}

func UnmarshalFromString(s string, entity interface{}) (err error) {
	if IsValidJsonObject(s) {
		err = Unmarshal(s, entity)
	} else {
		err = UnmarshalFromFile(s, entity)
	}
	return
}

func Stringify(entity interface{}) string {
	if nil != entity {
		if s, b := entity.(string); b {
			s = strings.TrimSpace(s)

			if IsValidJson(s) {
				return s
			}

			if strings.Index(s, "\"") != 0 {
				// quote to not quoted string
				// return string(instance.Bytes(Strings.Quote(s)))
				return Quote(s)
			}
		}
		return string(Marshal(entity))
	}
	return ""
}

func Decode(input interface{}, callback func(index int, item map[string]interface{})) (err error) {
	if nil != callback {
		reader, e := IOReaderOf(input)
		if nil != e {
			err = e
			return
		}
		if c, ok := reader.(io.ReadCloser); ok {
			defer c.Close()
		}

		isArray := isJSONArray(input)
		d := json.NewDecoder(reader)
		// start reading lines
		for i := 1; ; i++ {
			a := make([]map[string]interface{}, 0)
			m := make(map[string]interface{})
			if isArray {
				err = d.Decode(&a)
				if err != nil {
					if err == io.EOF {
						err = nil
						return // reached the end of file
					} else {
						return
					}
				}
				if len(a) > 0 {
					for ii := 0; ii < len(a); ii++ {
						// invoke callback
						index := (ii + 1) * i
						callback(index, a[ii])
					}
				}
			} else {
				err = d.Decode(&m)
				if err != nil {
					if err == io.EOF {
						err = nil
						return // reached the end of file
					} else {
						return
					}
				}
				callback(i, m)
			}
		}
	} else {
		err = errors.New("missing_callback")
	}

	return
}

func isJSONArray(input interface{}) bool {
	reader, err := IOReaderOf(input)
	if nil != err {
		return false
	}
	if c, ok := reader.(io.ReadCloser); ok {
		defer c.Close()
	}

	d := json.NewDecoder(reader)
	a := make([]map[string]interface{}, 0)
	err = d.Decode(&a)
	return err == nil
}
