package gg_

import (
	"mime"
	"path/filepath"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	m i m e   t y p e s
//----------------------------------------------------------------------------------------------------------------------

func MimeTypeIsAudio(mimeType string) (response bool) {
	if len(mimeType) > 0 {
		response = strings.HasPrefix(mimeType, "audio/")
	}
	return
}

func MimeTypeIsImage(mimeType string) (response bool) {
	if len(mimeType) > 0 {
		response = strings.HasPrefix(mimeType, "image/")
	}
	return
}

func MimeTypeIsVideo(mimeType string) (response bool) {
	if len(mimeType) > 0 {
		response = strings.HasPrefix(mimeType, "video/")
	}
	return
}

func MimeTypeGetExtensions(mimeType string) (response []string, err error) {
	if len(mimeType) > 0 {
		response, err = mime.ExtensionsByType(mimeType)
	}
	return
}

func MimeTypeGet(filename string) (response string) {
	if len(filename) > 0 {
		response = mime.TypeByExtension(filepath.Ext(filename))
	}
	return
}

func MimeTypeIsAudioFile(filename string) (response bool) {
	if len(filename) > 0 {
		response = MimeTypeIsAudio(MimeTypeGet(filename))
	}
	return
}

func MimeTypeIsVideoFile(filename string) (response bool) {
	if len(filename) > 0 {
		response = MimeTypeIsVideo(MimeTypeGet(filename))
	}
	return
}

func MimeTypeIsImageFile(filename string) (response bool) {
	if len(filename) > 0 {
		response = MimeTypeIsImage(MimeTypeGet(filename))
	}
	return
}
