package main

import "bitbucket.org/digi-sense/gg-core/gg_stoppable"

func main() {
	stoppable := gg_stoppable.NewStoppable()
	stoppable.Stop()
}
