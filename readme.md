# G&G Core #

![](icon.png)

Utility library.

This folder contains some utility packages.

Mose of them are pure Go, only some uses external modules but always in pure Go.

No dependencies from other external binaries suing C bindings.

## Special Modules ##

- [Scheduler](./gg_scheduler/readme.md): Schedule a task.
- [Updater](./gg_updater/readme.md): Update and launch a program.

## LICENSE

[3-Clause BSD License](./LICENSE.txt)

## How to Use

To use just call:

```
go get bitbucket.org/digi-sense/gg-core@v0.3.41
```

### Versioning ###

Sources are versioned using git tags:


```
git tag v0.3.41
git push origin v0.3.41
```