package license_commons

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type License struct {
	Uid          string                 `json:"uid"`           // license product SKU or UID
	CreationTime time.Time              `json:"creation_time"` // date of license creation
	DurationDays int64                  `json:"duration_days"` // license duration
	Id           string                 `json:"owner_id"`      // user id
	Name         string                 `json:"owner_name"`    // user name
	Email        string                 `json:"owner_email"`   // user email
	Lang         string                 `json:"owner_lang"`    // user lang
	Enabled      bool                   `json:"enabled"`       // license status
	Payload      map[string]interface{} `json:"payload"`       // additional payload
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewLicense(uid string) *License {
	instance := new(License)
	instance.CreationTime = time.Now()
	instance.Uid = uid
	instance.Payload = make(map[string]interface{})

	instance.init()
	return instance
}

func NewLicenseWithDuration(uid string, duration time.Duration) *License {
	instance := NewLicense(uid)
	instance.DurationDays = int64(duration.Hours() / 24)

	return instance
}

func NewLicenseWithExpiration(uid string, dateLayout, dateValue string) *License {
	instance := NewLicense(uid)
	_ = instance.ParseExpireDate(dateLayout, dateValue)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	License
//----------------------------------------------------------------------------------------------------------------------

func (instance *License) String() string {
	if nil != instance {
		msg := strings.Builder{}
		msg.WriteString("//-- START LICENSE INFO\t--//\n")
		if instance.IsValid() {
			msg.WriteString(fmt.Sprintf("\tLicense UID: \t\t\t%s\n", instance.Uid))
			msg.WriteString(fmt.Sprintf("\tOwner ID: \t\t\t%s\n", instance.Id))
			msg.WriteString(fmt.Sprintf("\tOwner Name: \t\t\t%s\n", instance.Name))
			msg.WriteString(fmt.Sprintf("\tOwner Email: \t\t\t%s\n", instance.Email))
			msg.WriteString(fmt.Sprintf("\tCreation Date: \t\t\t%v\n", instance.CreationTime))
			msg.WriteString(fmt.Sprintf("\tExpiration Date: \t\t%v\n", instance.GetExpireDate()))
			msg.WriteString(fmt.Sprintf("\tRemaining Days \t\t\t%v\n", instance.RemainingDays()))
		} else {
			msg.WriteString("\tUNLICENSED SOFTWARE. PLEASE, ASK FOR A VALID LICENSE.\n")
		}
		msg.WriteString("//-- END LICENSE INFO\t--//")

		return msg.String()
	}
	return ""
}

func (instance *License) Map() (response map[string]interface{}) {
	if nil != instance {
		remaining := instance.RemainingDays()
		response = map[string]interface{}{
			"uid":            instance.Uid,
			"owner_id":       instance.Id,
			"owner_name":     instance.Name,
			"owner_email":    instance.Email,
			"owner_lang":     instance.Lang,
			"creation_time":  instance.CreationTime,
			"duration_days":  instance.DurationDays,
			"expire_time":    instance.GetExpireDate(),
			"remaining_days": remaining,
			"expired":        remaining < 1,
			"payload":        instance.Payload,
			"enabled":        instance.Enabled,
		}
	}
	return
}

func (instance *License) Stringify() string {
	if nil != instance {
		return gg_.Stringify(instance.Map())
	}
	return ""
}

func (instance *License) Parse(text string) error {
	return instance.ParseBytes([]byte(text))
}

func (instance *License) ParseBytes(bytes []byte) error {
	return json.Unmarshal(bytes, &instance)
}

func (instance *License) GetDataAsString() string {
	return gg_utils.Strings.Format("\tId: %s\n\tOwner: %s\n\tCreation Date: %s\n\tExpire Date: %s\n\tExpired from days: %s",
		instance.Uid,
		instance.Name,
		instance.CreationTime,
		instance.GetExpireDate(),
		instance.RemainingDays()*-1,
	)
}

func (instance *License) IsValid() bool {
	if instance.Enabled && len(instance.Uid) > 0 {
		created := instance.CreationTime
		duration := instance.DurationDays
		now := time.Now()
		days := int64(now.Sub(created).Hours() / 24)

		return days <= duration
	}
	return false
}

func (instance *License) RemainingDays() int64 {
	if instance.Enabled && len(instance.Uid) > 0 {
		created := instance.CreationTime
		duration := instance.DurationDays
		now := time.Now()
		days := int64(now.Sub(created).Hours() / 24)

		return duration - days
	}
	return 0
}

func (instance *License) GetExpireDate() time.Time {
	created := instance.CreationTime
	duration := instance.DurationDays
	durationHours := duration * 24
	return created.Add(time.Duration(durationHours) * time.Hour)
}

func (instance *License) SetExpireDate(date time.Time) {
	created := instance.CreationTime
	days := int64(math.Round(date.Sub(created).Hours() / 24))
	instance.DurationDays = days
}

func (instance *License) ParseExpireDate(layout string, value string) error {
	if len(layout) > 0 {
		layout = gg_utils.Dates.ToGoDateLayout(layout)
		date, err := time.Parse(layout, value)
		if nil == err {
			instance.SetExpireDate(date)
		}
		return err
	} else {
		return errors.New("missing layout")
	}
}

func (instance *License) Add(days int64) {
	instance.Enabled = true
	instance.DurationDays = instance.DurationDays + days
}

func (instance *License) Encode() (text string, err error) {
	text, err = EncodeText(instance.String())
	return
}

func (instance *License) SaveToFile(filename string) (err error) {
	text := gg_.Stringify(instance.Map())
	encoded, e := EncodeText(text)
	if nil != e {
		err = e
	} else {
		_, err = gg_utils.IO.WriteTextToFile(encoded, filename)
	}
	return
}

func (instance *License) ReadFromFile(filename string) (err error) {
	encoded, e := gg_utils.IO.ReadBytesFromFile(filename)
	if nil != e {
		err = e
	} else {
		decoded, e := Decode(encoded)
		if nil != e {
			err = e
		} else {
			err = instance.ParseBytes(decoded)
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	private
//----------------------------------------------------------------------------------------------------------------------

func (instance *License) init() {
	if nil != instance {
		instance.Enabled = true
		if nil == instance.Payload {
			instance.Payload = make(map[string]interface{})
		}
		if len(instance.Uid) == 0 {
			instance.Uid = gg_rnd.Rnd.Uuid()
		}
		if instance.CreationTime.IsZero() {
			instance.CreationTime = time.Now()
		}
		if instance.DurationDays == 0 {
			instance.DurationDays = 1
		}
		if len(instance.Lang) == 0 {
			instance.Lang = "en"
		}
		if len(instance.Name) == 0 {
			instance.Name = "Anonymous"
		}
	}
}
