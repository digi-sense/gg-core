package nio_client

import (
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_nio/nio_commons"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_sys"
	"bufio"
	"crypto/rsa"
	"encoding/gob"
	"errors"
	"fmt"
	"net"
	"sync"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type NioClient struct {
	Timeout    time.Duration
	Secure     bool
	EnablePing bool

	//-- private --//
	uuid       string
	conn       net.Conn
	host       string
	port       int
	commMux    sync.Mutex
	connMux    sync.Mutex
	stopChan   chan bool
	statusChan chan bool
	events     *gg_events.Emitter
	connected  bool
	closed     bool
	pingTimer  *time.Ticker
	// RSA
	doneHandshake bool
	ready         bool
	publicKey     *rsa.PublicKey
	privateKey    *rsa.PrivateKey
	serverKey     *rsa.PublicKey // server signature (got on handshake)
	sessionKey    []byte
}

func NewNioClient(host string, port int) (instance *NioClient) {
	instance = new(NioClient)
	instance.host = host
	instance.port = port
	instance.Timeout = 10 * time.Second
	instance.events = gg_events.Events.NewEmitter()
	instance.connected = false
	instance.closed = true
	instance.pingTimer = time.NewTicker(1 * time.Second)
	instance.EnablePing = false // ping disabled (avoid continuous connect/disconnect)

	sysid, err := gg_sys.Sys.ID()
	if nil != err {
		sysid = gg_rnd.Rnd.Uuid()
	} else {
		// add counter for NioConnection machine
		sysid += fmt.Sprintf("-%d", gg_rnd.Rnd.Inc("nio-cli"))
	}
	instance.uuid = fmt.Sprintf("[%v]:%v", sysid, port)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *NioClient) GetUUID() string {
	if nil != instance {
		return instance.uuid
	}
	return ""
}

func (instance *NioClient) IsOpen() bool {
	if nil != instance {
		if !instance.closed {
			err := instance.test() // test for real feedback
			return nil == err
		}
	}
	return false
}

func (instance *NioClient) Open() (err error) {
	if nil != instance {
		if instance.closed {
			instance.closed = false
			instance.stopChan = make(chan bool, 1)
			instance.statusChan = make(chan bool, 1)

			err = instance.initRSA()
			if nil != err {
				return err
			}

			// start pinging remote server every 1 second
			go instance.startLoop()
			go instance.checkStatus()

			instance.ready = true // ready for connections

			// test first connection
			_, err = instance.connect()
		}
	}
	return
}

func (instance *NioClient) Close() (err error) {
	if nil != instance {
		if nil != instance.conn {
			instance.statusChan <- false
			instance.closed = true
			instance.ready = false

			if nil != instance.conn {
				err = instance.conn.Close()
				instance.conn = nil
			}

		}
		instance.stopChan <- true
	}
	return
}

// Join Wait is stopped
func (instance *NioClient) Join() {
	// locks and wait for exit response
	<-instance.stopChan
}

func (instance *NioClient) Send(data interface{}) (*nio_commons.NioMessage, error) {
	if nil != instance {

		// creates NIO message
		message := new(nio_commons.NioMessage)
		message.Body = data

		return instance.send(message, false)
	}
	return nil, nil
}

func (instance *NioClient) SendId(name, description string) (*nio_commons.NioMessage, error) {
	if nil != instance {
		return instance.sendIdentityDescriptor(name, description)
	}
	return nil, nil
}

func (instance *NioClient) OnConnect(callback func(e *gg_events.Event)) {
	if nil != instance {
		instance.events.On("connect", callback)
	}
}

func (instance *NioClient) OffConnect() {
	if nil != instance {
		instance.events.Off("connect")
	}
}

func (instance *NioClient) OnDisconnect(callback func(e *gg_events.Event)) {
	if nil != instance {
		instance.events.On("disconnect", callback)
	}
}

func (instance *NioClient) OffDisconnect() {
	if nil != instance {
		instance.events.Off("disconnect")
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *NioClient) initRSA() error {
	if nil != instance && instance.Secure && nil == instance.privateKey {
		// TODO: implement loading from file

		// auto-generates
		pri, pub, err := nio_commons.KeysGenerateStandard()
		if nil != err {
			return err
		}
		instance.privateKey = pri
		instance.publicKey = pub
	}
	return nil
}

func (instance *NioClient) ping() (err error) {
	if nil != instance {
		// log.Println("ping()")
		err = instance.test()
		if nil == err {
			// instance.setConnected(true)
			instance.statusChan <- true
		} else {
			// instance.setConnected(false)
			instance.statusChan <- false
		}
	}
	return
}

func (instance *NioClient) test() error {
	if nil != instance {
		instance.commMux.Lock()
		defer instance.commMux.Unlock()

		// log.Println("test()")

		conn, err := net.DialTimeout("tcp", fmt.Sprintf("%v:%v", instance.host, instance.port), instance.Timeout)
		if nil != conn {
			err = conn.Close()
		}
		return err
	}
	return nil
}

func (instance *NioClient) connect() (net.Conn, error) {
	if nil != instance {

		// log.Println("connect()")

		if nil == instance.conn {
			conn, err := net.DialTimeout("tcp", fmt.Sprintf("%v:%v", instance.host, instance.port), instance.Timeout)
			if nil == err {
				err = conn.SetReadDeadline(time.Now().Add(instance.Timeout))
				if nil == err {
					instance.conn = conn
					// trigger connect
					instance.statusChan <- true
				} else {
					instance.statusChan <- false
				}
			} else {
				// trigger disconnect
				// instance.setConnected(false)
				instance.statusChan <- false
			}
			return conn, err
		}
		return instance.conn, nil
	}
	return nil, nil
}

func (instance *NioClient) sendHandshake() error {
	if nil != instance && !instance.doneHandshake && instance.ready && instance.connected {
		// log.Println("handshake()")
		message := nio_commons.HANDSHAKE.Clone()
		message.PublicKey = instance.publicKey
		response, err := instance.send(message, true)
		if nil != err {
			return err
		}
		instance.serverKey = response.PublicKey
		instance.doneHandshake = true
	}
	return nil
}

func (instance *NioClient) sendIdentityDescriptor(name, description string) (response *nio_commons.NioMessage, err error) {
	if nil != instance && instance.ready && instance.connected {
		message := nio_commons.NewMessage()
		message.PublicKey = instance.publicKey
		message.Body = []byte(fmt.Sprintf("|%s|%s|", name, description))
		if message.IsIdentityDescriptor() {
			response, err = instance.send(message, false)
		}
	}
	return
}

func (instance *NioClient) startLoop() {
	if nil != instance {
		for {
			if instance.closed {
				goto exit
			}
			if nil != instance && nil != instance.pingTimer {
				select {
				case <-instance.stopChan:
					goto exit
				case <-instance.pingTimer.C:
					// log.Println("pingTimer()", instance.EnablePing)
					if nil != instance && !instance.closed && instance.EnablePing {
						_ = instance.ping()
					}
				}
			}
		}
	}

exit:
	// log.Println("QUITTING CLIENT LOOP", instance.uuid)
}

func (instance *NioClient) checkStatus() {
	if nil != instance {
		for {
			if nil != instance {
				if instance.closed {
					goto exit
				} else {
					// lock and wait for new status signal
					select {
					case connected := <-instance.statusChan:
						instance.statusChan = make(chan bool, 1) // reset channel
						instance.setConnectionStatus(connected)
					}
				}
			}
		}
	exit:
	}
}

// setConnectionStatus check connection integrity and perform HANDSHAKE if required
func (instance *NioClient) setConnectionStatus(connected bool) {
	// log.Println("setConnected()", connected, instance.connected)
	if nil != instance && connected != instance.connected {
		instance.connMux.Lock()
		defer instance.connMux.Unlock()

		instance.connected = connected

		if connected {
			instance.events.EmitAsync("connect")

			_ = instance.sendHandshake()
		} else {
			instance.events.EmitAsync("disconnect")
			instance.doneHandshake = false // handshake at new connection
			// reset connection for next call to regenerate
			if nil != instance.conn {
				_ = instance.conn.Close()
				instance.conn = nil
			}
		}
	}
}

func (instance *NioClient) send(message *nio_commons.NioMessage, handshake bool) (*nio_commons.NioMessage, error) {
	if nil != instance {
		instance.commMux.Lock()
		defer instance.commMux.Unlock()

		// log.Println("send()")

		conn, err := instance.connect()
		if nil != err {
			_ = instance.Close() // reset connection
			return nil, err
		}

		if handshake {
			message.PublicKey = instance.publicKey
		}

		// ENCRYPT BODY
		if !handshake && nil != instance.publicKey && len(instance.sessionKey) > 0 {
			v := nio_commons.Serialize(message.Body)
			data, err := nio_commons.Encrypt(v, instance.sessionKey)
			if nil == err {
				message.Body = data
			} else {
				return nil, errors.New("Client Encryption error")
			}
		} else {
			message.Body = nio_commons.Serialize(message.Body)
		}

		rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
		enc := gob.NewEncoder(rw)
		err = enc.Encode(message)
		if err != nil {
			return nil, errors.New(fmt.Sprintf("Encode failed for message: %#v", message))
		}
		err = rw.Flush()
		if err != nil {
			return nil, errors.New("Flush failed.")
		}

		// read NIO response
		// log.Println("READ RESPONSE")
		var response nio_commons.NioMessage
		dec := gob.NewDecoder(rw)
		err = dec.Decode(&response) // maybe go in timeout
		// log.Println("READ RESPONSE DONE", response)
		if err != nil {
			// log.Println(err, "Client failed to read response")
			return nil, errors.New("Client failed to read response")
		} else {
			// RESPONSE FROM SERVER
			if !handshake {
				if len(instance.sessionKey) > 0 {
					// DECRYPT BODY
					if v, b := response.Body.([]byte); b {
						data, err := nio_commons.Decrypt(v, instance.sessionKey)
						if nil == err {
							response.Body = data
						}
					}
				}
			} else {
				// handshake
				if len(response.SessionKey) > 0 {
					data, err := nio_commons.DecryptKey(response.SessionKey, instance.privateKey)
					if nil == err {
						instance.sessionKey = data
					}
				}
			}
			return &response, nil
		}
	}
	return nil, nil
}
