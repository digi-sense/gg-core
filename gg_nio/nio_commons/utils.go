package nio_commons

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"crypto/rsa"
)

func Serialize(data interface{}) []byte {
	if nil != data {
		if v, b := data.([]byte); b {
			return v
		} else if v, b := data.(string); b {
			return []byte(v)
		} else if v, b := data.(error); b {
			data = map[string]interface{}{
				"error": v.Error(),
			}
		}
		return gg_.Marshal(data)
	}
	return []byte{}
}

func KeysGenerate(bits int) (*rsa.PrivateKey, *rsa.PublicKey, error) {
	return gg_utils.Coding.GenerateKeyPair(bits)
}

func KeysGenerateStandard() (*rsa.PrivateKey, *rsa.PublicKey, error) {
	return gg_utils.Coding.GenerateKeyPair(KEY_LEN)
}

func NewSessionKey() [32]byte {
	return gg_utils.Coding.GenerateSessionKey()
}

func NewSessionKeyBytes() []byte {
	key := NewSessionKey()
	return key[:]
}

func EncryptKey(data []byte, key *rsa.PublicKey) ([]byte, error) {
	if nil != data && len(data) > 0 {
		response, err := gg_utils.Coding.EncryptWithPublicKey(data, key)
		return response, err
	}
	return []byte{}, nil
}

func Encrypt(data []byte, key []byte) ([]byte, error) {
	if nil != data && len(data) > 0 {
		response, err := gg_utils.Coding.EncryptBytesAES(data, key)
		return response, err
	}
	return []byte{}, nil
}

func DecryptKey(data []byte, privateKey *rsa.PrivateKey) ([]byte, error) {
	if nil != data && len(data) > 0 {
		response, err := gg_utils.Coding.DecryptWithPrivateKey(data, privateKey)
		return response, err
	}
	return []byte{}, nil
}

func Decrypt(data []byte, key []byte) ([]byte, error) {
	if nil != data && len(data) > 0 {
		response, err := gg_utils.Coding.DecryptBytesAES(data, key)
		return response, err
	}
	return []byte{}, nil
}
