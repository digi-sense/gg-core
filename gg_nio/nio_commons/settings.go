package nio_commons

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"encoding/json"
	"strings"
)

type NioSettings struct {
	Address string `json:"address"`
	host    string
	port    int
}

func (instance *NioSettings) Parse(text string) error {
	err := json.Unmarshal([]byte(text), &instance)
	instance.parseAddress(instance.Address)
	return err
}

func (instance *NioSettings) Host() string {
	if instance.port == 0 && len(instance.host) == 0 {
		instance.parseAddress(instance.Address)
	}
	return instance.host
}
func (instance *NioSettings) Port() int {
	if instance.port == 0 && len(instance.host) == 0 {
		instance.parseAddress(instance.Address)
	}
	return instance.port
}
func (instance *NioSettings) parseAddress(address string) {
	tokens := strings.Split(address, ":")
	switch len(tokens) {
	case 1:
		instance.port = gg_utils.Convert.ToInt(tokens[0])
	case 2:
		instance.host = tokens[0]
		instance.port = gg_utils.Convert.ToInt(tokens[1])
	}
}
