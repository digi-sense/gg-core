package nio_commons

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"crypto/rsa"
)

type NioMessage struct {
	PublicKey  *rsa.PublicKey // public key for response
	SessionKey []byte         // session key
	Body       interface{}    // message object
}

func NewMessage() (instance *NioMessage) {
	instance = new(NioMessage)

	return
}

func NewResponseToHandshakeMessage(sessionKey []byte, clientKey, serverKey *rsa.PublicKey) (instance *NioMessage) {
	instance = new(NioMessage)

	// public key is passed only with handshake
	instance.PublicKey = serverKey
	if nil != clientKey {
		instance.SessionKey, _ = EncryptKey(sessionKey, clientKey)
	}
	return
}

func (instance *NioMessage) Clone() (response *NioMessage) {
	response = new(NioMessage)
	response.Body = instance.Body
	response.PublicKey = instance.PublicKey
	response.SessionKey = instance.SessionKey

	return
}

func (instance *NioMessage) IsHandshake() bool {
	if nil != instance {
		if v, b := instance.Body.([]byte); b {
			body := string(v)
			handshakeBody := string(HANDSHAKE.Body.([]byte))
			return body == handshakeBody
		}
	}
	return false
}

func (instance *NioMessage) IsIdentityDescriptor() bool {
	if nil != instance {
		if v, b := instance.Body.([]byte); b {
			s := string(v)
			if len(s) > 3 {
				return string(s[0]) == "|" && string(s[len(s)-1]) == "|"
			}
		}
	}
	return false
}

func (instance *NioMessage) SetBody(body interface{}) *NioMessage {
	if nil != instance {
		instance.Body = Serialize(body)
	}
	return instance
}

func (instance *NioMessage) GetBodyAsString() string {
	if nil != instance {
		return gg_utils.Convert.ToString(instance.Body)
	}
	return ""
}

func (instance *NioMessage) GetBodyAsBool() bool {
	if nil != instance {
		return gg_utils.Convert.ToBool(instance.Body)
	}
	return false
}
