package nio_commons

const KEY_LEN = 1024 * 3

var (
	HANDSHAKE = &NioMessage{
		PublicKey: nil,
		Body:      []byte("ACK"),
	}
)
