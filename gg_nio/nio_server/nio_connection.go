package nio_server

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_nio/nio_commons"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bufio"
	"crypto/rsa"
	"encoding/gob"
	"fmt"
	"io"
	"net"
)

type NioConnectionDescriptor struct {
	Id          string `json:"id"`
	Addr        string `json:"addr"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (instance *NioConnectionDescriptor) String() string {
	if nil != instance {
		return gg_json.JSON.Stringify(instance)
	}
	return ""
}

type NioConnection struct {
	Descriptor *NioConnectionDescriptor

	id         string
	addr       string
	publicKey  *rsa.PublicKey // public key of the NioConnection
	sessionKey []byte         // session key for the NioConnection

	serverKey *rsa.PublicKey
	conn      net.Conn
	rw        *bufio.ReadWriter
}

func WrapConnection(conn net.Conn, serverKey *rsa.PublicKey) (instance *NioConnection) {
	if nil != conn {
		instance = new(NioConnection)
		instance.conn = conn
		instance.rw = bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
		instance.serverKey = serverKey

		instance.id = gg_utils.Coding.MD5(conn.RemoteAddr().String())
		instance.addr = conn.RemoteAddr().String()
		instance.sessionKey = nio_commons.NewSessionKeyBytes()

		// name and description can be changed for better description
		instance.Descriptor = new(NioConnectionDescriptor)
		instance.Descriptor.Id = instance.id
		instance.Descriptor.Addr = instance.addr
		instance.Descriptor.Name = instance.id
		instance.Descriptor.Description = fmt.Sprintf("A connection with Name='%s' and ID='%s'",
			instance.Descriptor.Name, instance.id)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *NioConnection) String() string {
	m := make(map[string]interface{})
	if nil != instance {
		if nil != instance.conn {
			m["id"] = instance.id
			m["addr"] = instance.addr
			m["session-key"] = instance.sessionKey
			m["public-key"] = nil != instance.publicKey
			m["server-key"] = nil != instance.serverKey
		} else {
			m["error"] = "Invalid connection!"
		}
	}
	return gg_json.JSON.Stringify(m)
}

func (instance *NioConnection) Close() {
	if nil != instance && nil != instance.conn {
		_ = instance.conn.Close()
	}
}

func (instance *NioConnection) GetId() (response string) {
	if nil != instance {
		response = instance.id
	}
	return
}

func (instance *NioConnection) GetAddr() (response string) {
	if nil != instance {
		response = instance.addr
	}
	return
}

func (instance *NioConnection) GetPublicKey() (response *rsa.PublicKey) {
	if nil != instance {
		response = instance.publicKey
	}
	return
}

func (instance *NioConnection) ParseDescriptor(descriptor string) {
	if nil != instance {
		tokens := gg_utils.Strings.SplitTrimSpace(descriptor, "|")
		switch len(tokens) {
		case 1:
			instance.Descriptor.Name = tokens[0]
		case 2:
			instance.Descriptor.Name = tokens[0]
			instance.Descriptor.Description = tokens[1]
		}
	}
}

func (instance *NioConnection) Conn() (conn net.Conn) {
	if nil != instance {
		conn = instance.conn
	}
	return
}

func (instance *NioConnection) ReadMessage() (response *nio_commons.NioMessage, err error) {
	if nil != instance && nil != instance.conn {
		response = new(nio_commons.NioMessage)
		dec := gob.NewDecoder(instance.rw)
		err = dec.Decode(&response)
	}
	return
}

func (instance *NioConnection) WriteHandshake(body interface{}) (err error) {
	if nil != instance {
		message := nio_commons.NewResponseToHandshakeMessage(instance.sessionKey,
			instance.publicKey, instance.serverKey).
			SetBody(body)

		err = write(message, instance.rw)
	}
	return
}

func (instance *NioConnection) WriteMessage(body interface{}, isHandshake bool) error {
	message := new(nio_commons.NioMessage)

	sessionKey := instance.sessionKey
	clientKey := instance.publicKey
	serverKey := instance.serverKey

	// public key is passed only with handshake
	if isHandshake {
		message.PublicKey = serverKey
		if nil != clientKey {
			message.SessionKey, _ = nio_commons.EncryptKey(sessionKey, clientKey)
		}
	}

	s := nio_commons.Serialize(body)

	// encode server message body
	if nil != clientKey && !isHandshake {
		data, err := nio_commons.Encrypt(s, sessionKey)
		if nil == err {
			s = data
		} else {
			fmt.Println("Http error encrypting data", err)
		}
	}

	message.Body = s

	err := write(message, instance.rw)

	return err
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func write(message *nio_commons.NioMessage, writer io.Writer) (err error) {
	enc := gob.NewEncoder(writer)
	err = enc.Encode(message)
	if err != nil {
		return
	}

	if buffWriter, ok := writer.(*bufio.ReadWriter); ok {
		err = buffWriter.Flush()
	}

	return
}
