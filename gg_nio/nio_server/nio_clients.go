package nio_server

import (
	"crypto/rsa"
	"net"
	"sync"
)

type NioClientsStore struct {
	clientsMap map[string]*NioConnection
	mux        *sync.RWMutex
	publicKey  func() *rsa.PublicKey
}

func NewNioClientsStore(publicKey func() *rsa.PublicKey) (instance *NioClientsStore) {
	instance = new(NioClientsStore)
	instance.publicKey = publicKey
	instance.clientsMap = make(map[string]*NioConnection)
	instance.mux = &sync.RWMutex{}

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *NioClientsStore) Count() int {
	if nil != instance {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		return len(instance.clientsMap)
	}
	return 0
}

func (instance *NioClientsStore) Ids() []string {
	response := make([]string, 0)
	if nil != instance {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		for k, _ := range instance.clientsMap {
			response = append(response, k)
		}
	}
	return response
}

func (instance *NioClientsStore) Descriptors() []*NioConnectionDescriptor {
	response := make([]*NioConnectionDescriptor, 0)
	if nil != instance {
		instance.mux.RLock()
		defer instance.mux.RUnlock()

		for _, v := range instance.clientsMap {
			response = append(response, v.Descriptor)
		}
	}
	return response
}

func (instance *NioClientsStore) Get(connId string) (response *NioConnection) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if item, ok := instance.clientsMap[connId]; ok {
			response = item
		}
	}
	return
}

func (instance *NioClientsStore) Push(conn net.Conn) (response *NioConnection) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		var pk *rsa.PublicKey
		if nil != instance.publicKey {
			pk = instance.publicKey()
		}

		response = WrapConnection(conn, pk)
		instance.clientsMap[response.GetId()] = response
	}
	return
}

func (instance *NioClientsStore) Pop(connId string) (response *NioConnection) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if item, ok := instance.clientsMap[connId]; ok {
			response = item
			delete(instance.clientsMap, connId)
		}
	}
	return
}
