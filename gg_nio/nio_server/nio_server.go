package nio_server

import (
	"bitbucket.org/digi-sense/gg-core/gg_nio/nio_commons"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_sys"
	"crypto/rsa"
	"fmt"
	"net"
	"sync"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type NioServer struct {

	//-- private --//
	uuid           string
	port           int
	listener       net.Listener
	clients        *NioClientsStore
	mux            sync.RWMutex
	handlerMessage NioMessageHandler
	stopChan       chan bool
	chanConn       chan net.Conn
	active         bool
	// RSA
	publicKey  *rsa.PublicKey
	privateKey *rsa.PrivateKey
	// callback
	onNewClient func(conn *NioConnection)
}

func NewNioServer(port int) *NioServer {
	instance := new(NioServer)
	instance.clients = NewNioClientsStore(instance.GetPublicKey)
	instance.port = port
	instance.active = false

	sysid, err := gg_sys.Sys.ID()
	if nil != err {
		sysid = gg_rnd.Rnd.Uuid()
	}
	instance.uuid = fmt.Sprintf("[%v]:%v", sysid, port)

	return instance
}

type NioMessageHandler func(message *nio_commons.NioMessage) interface{}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *NioServer) GetUUID() string {
	if nil != instance {
		return instance.uuid
	}
	return ""
}

func (instance *NioServer) GetPublicKey() (response *rsa.PublicKey) {
	if nil != instance {
		response = instance.publicKey
	}
	return
}

func (instance *NioServer) IsOpen() bool {
	if nil != instance {
		return instance.active
	}
	return false
}

func (instance *NioServer) Port() int {
	if nil != instance {
		return instance.port
	}
	return 0
}

func (instance *NioServer) Open() (err error) {
	if nil != instance && !instance.active {

		instance.active = true
		instance.stopChan = make(chan bool, 1)
		instance.chanConn = make(chan net.Conn, 1)

		err = instance.initRSA()
		if nil != err {
			return err
		}

		// create listener
		var listener net.Listener
		listener, err = net.Listen("tcp", fmt.Sprintf(":%v", instance.port))
		if nil != err {
			return err
		}
		instance.listener = listener

		// main listener loop
		go instance.open()
		go instance.connLoop()
	}

	return
}

func (instance *NioServer) Close() error {
	if nil != instance && instance.active {
		instance.active = false

		var err error
		if nil != instance.listener {
			err = instance.listener.Close()
			instance.listener = nil
		}
		instance.stopChan <- true
		close(instance.stopChan)
		instance.stopChan = nil
		// close(instance.chanConnWait)
		// instance.chanConnWait = nil
		return err
	}
	return nil
}

// Join Wait is stopped
func (instance *NioServer) Join() {
	// locks and wait for exit response
	<-instance.stopChan
}

func (instance *NioServer) ClientsCount() int {
	if nil != instance && nil != instance.clients {
		return instance.clients.Count()
	}
	return 0
}

func (instance *NioServer) ClientsId() []string {
	if nil != instance && nil != instance.clients {
		return instance.clients.Ids()
	}
	return []string{}
}

func (instance *NioServer) ClientDescriptors() []*NioConnectionDescriptor {
	if nil != instance && nil != instance.clients {
		return instance.clients.Descriptors()
	}
	return []*NioConnectionDescriptor{}
}

func (instance *NioServer) OnMessage(callback NioMessageHandler) {
	if nil != instance {
		instance.handlerMessage = callback
	}
}

func (instance *NioServer) OnNewConn(callback func(conn *NioConnection)) {
	if nil != instance {
		instance.onNewClient = callback
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *NioServer) initRSA() error {
	if nil != instance && nil == instance.privateKey {
		// TODO: implement loading from file

		// auto-generates
		pri, pub, err := nio_commons.KeysGenerateStandard()
		if nil != err {
			return err
		}
		instance.privateKey = pri
		instance.publicKey = pub
	}
	return nil
}

func (instance *NioServer) open() {
	for {
		if nil != instance && nil != instance.listener && instance.active {
			// accept connections
			conn, err := instance.listener.Accept()
			if err != nil {
				// error accepting connection
				continue
			} else {
				instance.chanConn <- conn
			}
		}
	}
}

func (instance *NioServer) connLoop() {
	for {
		if nil == instance || !instance.active || nil == instance.listener {
			return
		}
		// wait new connection
		if conn, ok := <-instance.chanConn; ok {
			instance.chanConn = make(chan net.Conn, 1) // regenerate the channel
			go instance.handleConnection(conn)
		}
	}
}

func (instance *NioServer) handleConnection(conn net.Conn) {
	if nil != instance {
		// log.Println("CONNECTING...", conn.RemoteAddr().String())
		// new connection wrapper
		nioConn := instance.clients.Push(conn)
		if nil == nioConn {
			return
		}
		defer nioConn.Close()

		// connection loop
		for {
			if !instance.active {
				break
			}
			if nil == instance.listener {
				break
			}
			message, err := nioConn.ReadMessage()
			if nil != err {
				if err.Error() == "EOF" {
					// NioConnection disconnected
				}
				// exit
				// log.Println("Error decoding message: ", err, message)
				break
			}

			// log.Println("Message read from server: ", message)
			isHandshake := message.IsHandshake()
			if isHandshake {
				// set public key for a NioConnection
				nioConn.publicKey = message.PublicKey
				if nil != instance.onNewClient {
					instance.onNewClient(nioConn)
				}
			}

			// check for custom handler
			if !isHandshake {

				// check for a  message to decode
				if nil != nioConn.publicKey && nil != nioConn.sessionKey {
					// decode NioConnection message body
					if v, b := message.Body.([]byte); b {
						data, err := nio_commons.Decrypt(v, nioConn.sessionKey)
						if nil == err {
							message.Body = data
						} else {
							// encryption error
							fmt.Println("Http error decrypting data:", err)
						}
					}
				}

				if message.IsIdentityDescriptor() {
					// log.Println("IDENTITY MESSAGE:", message.GetBodyAsString())
					nioConn.ParseDescriptor(message.GetBodyAsString())
				}

				// check for custom handler
				var customResponse interface{}
				if nil != instance.handlerMessage {
					customResponse = instance.handlerMessage(message)
				}
				if nil == customResponse {
					customResponse = true
				}
				err = nioConn.WriteMessage(customResponse, isHandshake)
				if err != nil {
					break
				}
			} else {
				// response OK (default)
				err = nioConn.WriteMessage(true, isHandshake)
				if err != nil {
					break
				}
			}
		}

		// NioConnection removed
		_ = instance.clients.Pop(nioConn.id)
	}
}
