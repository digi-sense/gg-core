package gg_nio

import (
	"bitbucket.org/digi-sense/gg-core/gg_nio/nio_client"
	"bitbucket.org/digi-sense/gg-core/gg_nio/nio_server"
)

type NIOHelper struct {
}

var NIO *NIOHelper

func init() {
	NIO = new(NIOHelper)
}

func (*NIOHelper) NewClient(host string, port int) *nio_client.NioClient {
	return nio_client.NewNioClient(host, port)
}

func (*NIOHelper) NewServer(port int) *nio_server.NioServer {
	return nio_server.NewNioServer(port)
}
