package gg_templating

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"fmt"
	"strings"
)

var Templating *TemplatingHelper

func init() {
	Templating = NewTemplatingHelper()
}

//----------------------------------------------------------------------------------------------------------------------
//	TemplatingHelper
//----------------------------------------------------------------------------------------------------------------------

type TemplatingHelper struct {
}

func NewTemplatingHelper() (instance *TemplatingHelper) {
	instance = new(TemplatingHelper)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// Render processes a template string by replacing placeholders with corresponding values from the provided context.
// If allowEmpty is true, placeholders without matching values are replaced with empty strings;
// otherwise, they remain unchanged.
// Additional context values can be passed as variadic arguments, merged into a single context map for processing.
func (instance *TemplatingHelper) Render(data string, allowEmpty bool, context ...interface{}) (response string) {
	if nil != instance {
		m := gg_.Merge(false, context...)
		response = instance.render(data, allowEmpty, m, "", "")
	}
	return
}

// RenderExt processes a template string with customizable prefix and suffix for placeholders,
// replacing them with values from context.
// If allowEmpty is true, placeholders without matching values are replaced with empty strings;
// otherwise, they remain unchanged.
// Additional context is merged from variadic arguments into a single context map for processing.
func (instance *TemplatingHelper) RenderExt(data string, allowEmpty bool, prefix, suffix string, context ...interface{}) (response string) {
	if nil != instance {
		m := gg_.Merge(false, context...)
		response = instance.render(data, allowEmpty, m, prefix, suffix)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TemplatingHelper) render(data string, allowEmpty bool, context map[string]interface{}, prefix, suffix string) string {
	response := data
	if len(prefix) == 0 {
		prefix = "{{"
	}
	if len(suffix) == 0 {
		suffix = "}}"
	}
	fields := gg_.MatchBetween(data, 0, prefix, suffix, "")
	for _, field := range fields {
		fieldName := strings.Trim(field, " \n.")
		value := gg_.GetString(context, fieldName)
		if allowEmpty || len(value) > 0 {
			format := prefix + "%s" + suffix
			pattern := fmt.Sprintf(format, field)
			response = strings.ReplaceAll(response, pattern, value)
		}
	}
	return response
}
