package gg_updater

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"strings"
)

func NeedUpdate(currentVersion, remoteVersion string, versionIsRequired bool) bool {
	if versionIsRequired && len(currentVersion) == 0 && len(remoteVersion) > 0 {
		// missing a current version, but have a remote
		return true
	}
	if len(currentVersion) > 0 && len(remoteVersion) > 0 && currentVersion != remoteVersion {
		v1 := strings.Split(strings.Trim(currentVersion, " \n"), ".")
		v2 := strings.Split(strings.Trim(remoteVersion, " \n"), ".")
		for i := 0; i < 3; i++ {
			a := gg_utils.Convert.ToInt(gg_utils.Arrays.GetAt(v1, i, 0))
			b := gg_utils.Convert.ToInt(gg_utils.Arrays.GetAt(v2, i, 0))
			if b == a {
				continue
			} else if b < a {
				return false
			} else if b > a {
				return true
			}
		}
	}
	return false
}
