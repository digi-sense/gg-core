package gg_exec_shell

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_sys"
	"bitbucket.org/digi-sense/gg-core/gg_templating"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	_ "embed"
	"errors"
	"fmt"
	"path/filepath"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	e m b e d
//----------------------------------------------------------------------------------------------------------------------

//go:embed tpl.env.sh
var TplEnvSh string

//go:embed tpl.autorun.sh
var TplAutorunSh string

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func ToError(command string, err error) (response error) {
	if nil != err {
		response = err
		if strings.Index(err.Error(), "127") > -1 {
			// command not found
			response = errors.New(fmt.Sprintf("CommandLine '%s' not found.", command))
		}
	}
	return
}

func ToCommand(item *ShellScriptItem, args ...string) (response []string) {
	response = make([]string, 0)
	if nil != item {
		response = append(response, item.Name)
		response = append(response, item.PrefixArguments...)
		response = append(response, ToArguments(args...)...)
		response = append(response, item.SuffixArguments...)
	}
	return
}

func ToArguments(args ...string) (result []string) {
	result = make([]string, 0)
	for _, arg := range args {
		tokens := Split(arg)
		for _, token := range tokens {
			token = strings.TrimSpace(token)
			if len(token) > 0 {
				result = append(result, token)
			}
		}
	}
	return result
}

func Split(arg string) (response []string) {
	tokens := gg_utils.Strings.SplitQuoted(arg, ' ', '"')
	for _, token := range tokens {
		token = strings.TrimSpace(token)
		if len(token) > 0 {
			token = Unquote(token) // ensure the param has no quote
			response = append(response, token)
		}
	}
	return response
}

func Unquote(s string) string {
	if len(s) > 0 {
		ss, err := gg_utils.Strings.Unquote(s)
		if nil == err && len(ss) > 0 {
			return ss
		}
	}
	return s
}

func IsAutorun(command string) bool {
	if strings.Index(command, ".autorun") > -1 {
		return true
	}
	return false
}

func IsEnv(command string) bool {
	if strings.Index(command, ".env") > -1 {
		return true
	}
	return false
}

func IsValidEnvValue(text string) bool {
	if len(text) > 0 {
		tokens := strings.Split(text, "=")
		return len(tokens) == 2
	}
	return false
}

// IsShellCmd determines if a given command string is a shell script or batch file based on its file extension.
func IsShellCmd(command string) bool {
	if len(command) > 0 {
		tokens := strings.Split(command, " ")
		if len(tokens) > 0 {
			ext := filepath.Ext(strings.ToLower(tokens[0]))
			return ext == ".sh" || ext == ".bat"
		}
	}
	return false
}

// GetShellCmd returns the default shell command depending on the operating system.
// For Windows, it returns "cmd", otherwise it defaults to "/bin/sh".
func GetShellCmd() string {
	cmd := BashCommand // "/bin/bash"
	if gg_sys.Sys.IsWindows() {
		cmd = "cmd"
	}
	return cmd
}

// GetShellCmdFrom determines the shell command to use based on the provided file or defaults to the system shell command.
func GetShellCmdFrom(command string) (response string) {
	response = GetShellCmd()
	if filepath.Ext(command) == ".sh" {
		// try to read the file
		text, err := gg_.IOReadTextFromFile(command)
		if err == nil && len(text) > 0 {
			text = strings.TrimSpace(text)
			rows := strings.Split(text, "\n")
			if len(rows) > 0 {
				row := rows[0]
				if strings.HasPrefix(row, "#!") {
					cmd := strings.TrimSpace(row[2:])
					if strings.HasPrefix(cmd, "/") {
						response = cmd
					}
				}
			}
		}
	}
	return
}

// GetShellTempFile generates an absolute path for a temporary shell script file, with a platform-specific extension.
func GetShellTempFile() string {
	ext := ".sh"
	if gg_sys.Sys.IsWindows() {
		ext = ".bat"
	}
	filename := fmt.Sprintf("%s%s", gg_rnd.Rnd.Uuid(), ext)
	return gg_utils.Paths.TempPath(filename)
}

func GetShellEnvFilename(command string) string {
	name := filepath.Base(command)
	ext := filepath.Ext(command)
	if len(ext) == 0 {
		if gg_sys.Sys.IsWindows() {
			ext = ".bat"
		} else {
			ext = ".sh"
		}
	} else {
		name = name[:len(name)-len(ext)]
	}
	return name + ".env" + ext
}

func GetShellAutorunFilename(command string) string {
	name := filepath.Base(command)
	ext := filepath.Ext(command)
	if len(ext) == 0 {
		if gg_sys.Sys.IsWindows() {
			ext = ".bat"
		} else {
			ext = ".sh"
		}
	} else {
		name = name[:len(name)-len(ext)]
	}
	return name + ".autorun" + ext
}

func Quote(s string) string {
	isQuoted := strings.HasPrefix(s, "\"") && strings.HasSuffix(s, "\"")
	if !isQuoted {
		return gg_utils.Strings.Quote(s)
	}
	return s
}

func WriteShellEnvFile(dirTarget, dirToExport string, command string, exports map[string]interface{}, rows []any) (response string, err error) {
	if len(dirTarget) == 0 {
		dirTarget = gg_utils.Paths.Absolute(DirRoot)
	}
	if len(dirToExport) == 0 {
		dirToExport = gg_utils.Paths.Absolute(DirRoot)
	}
	// get template and precompile
	context := map[string]interface{}{
		fieldBinBash:      BashCommand,                         // usually is #!/bin/bash
		fieldDirWorkspace: gg_utils.Strings.Quote(dirToExport), // directory to export in env file
	}
	tpl := gg_templating.Templating.RenderExt(TplEnvSh, true, "<", ">", context)
	if len(tpl) > 0 {
		// build exports
		exportsBuff := new(strings.Builder)
		for k, v := range exports {
			value := fmt.Sprintf("%v", v)
			if len(value) > 0 {
				if exportsBuff.Len() > 0 {
					exportsBuff.WriteString("\n")
				}
				if s, ok := v.(string); ok {
					value = Quote(s)
				}
				exportsBuff.WriteString(fmt.Sprintf("export %s=%v", k, value))
				exportsBuff.WriteString("\n")
				exportsBuff.WriteString(fmt.Sprintf("echo %s=$%s", k, k))
			}
		}

		rowsBuff := new(strings.Builder)
		for _, row := range rows {
			if nil != row {
				if rowsBuff.Len() > 0 {
					rowsBuff.WriteString("\n")
				}
				value := fmt.Sprintf("%v", row)
				rowsBuff.WriteString(value)
			}
		}

		// render content
		content := gg_templating.Templating.Render(tpl, true, map[string]interface{}{
			"exports": exportsBuff.String(),
			"rows":    rowsBuff.String(),
		})

		response = gg_.PathConcat(dirTarget, GetShellEnvFilename(command))
		_, err = gg_.IOWriteTextToFile(content, response, false)
		if nil != err {
			return
		}

		// make executable
		err = gg_sys.Sys.ChmodExecutable(response)
	}
	return
}

func WriteShellAutorunFile(dirTarget string, command string, rows []any) (response string, err error) {
	if len(dirTarget) == 0 {
		dirTarget = gg_utils.Paths.Absolute(DirRoot)
	}

	tpl := TplAutorunSh
	if len(tpl) > 0 {
		// build exports
		exportsBuff := new(strings.Builder)
		for _, row := range rows {
			if nil != row {
				if exportsBuff.Len() > 0 {
					exportsBuff.WriteString("\n")
				}
				value := fmt.Sprintf("%v", row)
				exportsBuff.WriteString(value)
			}
		}

		// render content
		content := gg_templating.Templating.Render(tpl, true, map[string]interface{}{
			"rows": exportsBuff.String(),
		})

		response = gg_.PathConcat(dirTarget, GetShellAutorunFilename(command))
		_, err = gg_.IOWriteTextToFile(content, response, false)
		if nil != err {
			return
		}

		// make executable
		err = gg_sys.Sys.ChmodExecutable(response)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
