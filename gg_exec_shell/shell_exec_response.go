package gg_exec_shell

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"errors"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	ShellExecResponse
//----------------------------------------------------------------------------------------------------------------------

type ShellExecResponse struct {
	CommandLine string   `json:"command-line"` // executed command
	Command     string   `json:"command"`      // executed command
	Arguments   []string `json:"arguments"`    //
	Env         []string `json:"environment"`  // executed command
	Response    string   `json:"response"`     // execution response
	Error       string   `json:"error"`        // execution error
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ShellExecResponse) String() string {
	return gg_json.JSON.Stringify(instance)
}

func (instance *ShellExecResponse) Map() map[string]interface{} {
	return gg_.Map(gg_json.JSON.Stringify(instance))
}

func (instance *ShellExecResponse) HasError() bool {
	if nil != instance {
		return len(instance.Error) > 0
	}
	return false
}

func (instance *ShellExecResponse) SetError(err error) *ShellExecResponse {
	if nil != instance {
		instance.Error = err.Error()
	}
	return instance
}

func (instance *ShellExecResponse) GetError() error {
	if nil != instance {
		if len(instance.Error) > 0 {
			return errors.New(instance.Error)
		}
	}
	return nil
}

func (instance *ShellExecResponse) SetCommand(args ...string) *ShellExecResponse {
	if nil != instance && len(args) > 0 {
		instance.CommandLine = strings.Join(args, " ")
		if len(args) > 1 {
			instance.Command = args[0]
			instance.Arguments = args[1:]
		} else {
			instance.Command = args[0]
		}
	}
	return instance
}

func (instance *ShellExecResponse) SetResponse(response string, env []string, err error) *ShellExecResponse {
	if nil != instance {
		instance.Env = env
		instance.Response = response
		if nil != err {
			err = ToError(instance.CommandLine, err)
			if nil != err {
				instance.Error = err.Error()
			}
		}
	}
	return instance
}
