package gg_exec_shell

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_json"
)

//----------------------------------------------------------------------------------------------------------------------
//	ShellScriptItem
//----------------------------------------------------------------------------------------------------------------------

type ShellScriptItem struct {
	Uid             string   `json:"uid"`              // unique identifier
	Name            string   `json:"name"`             // name of the script or command. i.e. "myshell.sh", "pandoc", "ls", ...
	PrefixArguments []string `json:"prefix-arguments"` // default arguments to be passed to script at every call
	SuffixArguments []string `json:"suffix-arguments"` // default arguments
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ShellScriptItem) String() string {
	return gg_json.JSON.Stringify(instance)
}

func (instance *ShellScriptItem) Map() map[string]interface{} {
	return gg_.Map(gg_json.JSON.Stringify(instance))
}

func (instance *ShellScriptItem) IsShellCmd() bool {
	if nil != instance {
		return IsShellCmd(instance.Name)
	}
	return false
}
