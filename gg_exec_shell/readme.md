# Shell Scripts

This package contains utility to invoke .sh or .bat (or executables commands) files starting from a root directory.

The package also save in data files some default parameters (like credentials) to pass to scripts.

## ENV Files

The shell controller can generate .env files that are executables that are launched
automatically before the execution of a command or a shell script.

