package gg_exec_shell

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_sys"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bytes"
	"errors"
	"os/exec"
	"strings"
	"sync"
)

const indexFilename = "scripts.dat"
const indexSeed = "this-is-key-for-scripts.dat"

//----------------------------------------------------------------------------------------------------------------------
//	ShellScriptCtrl
//----------------------------------------------------------------------------------------------------------------------

// ShellScriptCtrl is a controller for managing and executing shell scripts or commands with customizable arguments.
type ShellScriptCtrl struct {
	dirRoot      string
	dirWorkspace string
	scripts      []*ShellScriptItem

	mux *sync.Mutex
}

func NewShellScriptCtrl() (instance *ShellScriptCtrl) {
	instance = new(ShellScriptCtrl)
	instance.mux = new(sync.Mutex)
	instance.scripts = make([]*ShellScriptItem, 0)

	instance.SetRoot(DirRoot)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// SetRoot sets the root directory for the ShellScriptCtrl instance and updates its state by re-loading data.
func (instance *ShellScriptCtrl) SetRoot(value string) *ShellScriptCtrl {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.dirRoot = gg_.PathAbsolute(value)
		if len(instance.dirWorkspace) == 0 {
			instance.dirWorkspace = instance.dirRoot
		}
		_ = instance.load()
		_ = instance.save()
	}
	return instance
}

func (instance *ShellScriptCtrl) SetWorkspace(value string) *ShellScriptCtrl {
	if nil != instance {
		instance.dirWorkspace = gg_.PathAbsolute(value)
	}
	return instance
}

func (instance *ShellScriptCtrl) GetRoot() string {
	return instance.dirRoot
}

func (instance *ShellScriptCtrl) GetWorkspace() string {
	return instance.dirWorkspace
}

// SaveAll force to save all shell items into index.dat file.
func (instance *ShellScriptCtrl) SaveAll() *ShellScriptCtrl {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		_ = instance.save()
	}
	return instance
}

// ReadAll retrieves all ShellScriptItem objects managed by the ShellScriptCtrl instance, after ensuring they are loaded.
func (instance *ShellScriptCtrl) ReadAll() (response []*ShellScriptItem) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		_ = instance.load()
		response = instance.scripts
	}
	return
}

// SetDefaults sets default prefix or suffix arguments for a specific command in the ShellScriptCtrl instance.
func (instance *ShellScriptCtrl) SetDefaults(command string, prefix bool, args ...string) *ShellScriptCtrl {
	if nil != instance && len(args) > 0 {
		item := instance.get(command)
		if nil != item {
			if prefix {
				item.PrefixArguments = ToArguments(args...)
			} else {
				item.SuffixArguments = ToArguments(args...)
			}
			_ = instance.save()
		}
	}
	return instance
}

// GetShellItem retrieves a ShellScriptItem for the given command, creating it if it does not exist.
func (instance *ShellScriptCtrl) GetShellItem(command string) (response *ShellScriptItem) {
	if nil != instance && len(command) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = instance.get(command)
	}
	return
}

// ResetShellItem resets the prefix and suffix arguments for the specified command.
func (instance *ShellScriptCtrl) ResetShellItem(command string) *ShellScriptCtrl {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		item := instance.get(command)
		if nil != item {
			item.SuffixArguments = make([]string, 0)
			item.PrefixArguments = make([]string, 0)

			// update local history
			_ = instance.save()

			// remove .env file
			instance.removeEnvFile(command)
			// remove .autorun file
			instance.removeAutorunFiles(command)
		}
	}
	return instance
}

// ListAutorunFiles retrieves a list of autorun files based on the provided command.
// It ensures thread safety with a mutex lock.
func (instance *ShellScriptCtrl) ListAutorunFiles(command string) (response []string) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		response = instance.listAutorunFiles(command)
	}
	return
}

func (instance *ShellScriptCtrl) CreateShellItemEnvironment(command string, exports map[string]interface{}, rows []any) (filename string, err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		dirTarget := instance.dirRoot
		dirWorkspace := gg_.PathAbsolutize("./workspace", dirTarget) // exposed DIR_WORKSPACE variable
		filename, err = WriteShellEnvFile(dirTarget, dirWorkspace, command, exports, rows)
	}
	return
}

func (instance *ShellScriptCtrl) CreateShellItemAutorun(command string, rows []interface{}) (filename string, err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		dirTarget := instance.dirRoot
		filename, err = WriteShellAutorunFile(dirTarget, command, rows)
	}
	return
}

// Autorun Run all autorun files for a specific command
func (instance *ShellScriptCtrl) Autorun(command string) (response []*ShellExecResponse) {
	response = make([]*ShellExecResponse, 0)
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		files := instance.listAutorunFiles(command)
		for _, file := range files {
			response = append(response, instance.run(file))
		}
	}
	return
}

// AutorunAll Run all autorun files
func (instance *ShellScriptCtrl) AutorunAll() (response []*ShellExecResponse) {
	response = make([]*ShellExecResponse, 0)
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		files := instance.listAutorunFiles("")
		for _, file := range files {
			response = append(response, instance.run(file))
		}
	}
	return
}

// Split divides the provided arguments into a processed list of strings for further handling or execution.
// The response is a list of arguments ready for execution as command or as shell script.
// This method is useful to see which arguments will be run as a command or shell command like a sort of "Run" simulation.
func (instance *ShellScriptCtrl) Split(args ...string) (response []string) {
	if nil != instance && len(args) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		_, response = instance.prepare(args...)
	}
	return
}

// Run executes a shell script or command based on the arguments provided and returns the response or an error.
func (instance *ShellScriptCtrl) Run(args ...string) (response *ShellExecResponse) {
	if nil != instance && len(args) > 0 {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		// split all arguments in trimmed tokens
		response = instance.run(args...)
		if !response.HasError() {
			_ = instance.save()
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *ShellScriptCtrl) uid(cmd string) (response string) {
	if nil != instance {
		response = gg_utils.Coding.MD5(strings.TrimSpace(strings.ToLower(cmd)))
	}
	return
}

func (instance *ShellScriptCtrl) get(cmd string) (response *ShellScriptItem) {
	if nil != instance {
		uid := instance.uid(cmd)
		for _, script := range instance.scripts {
			if nil != script {
				if script.Uid == uid {
					response = script
					return
				}
			}
		}

		// not found
		response = new(ShellScriptItem)
		response.Uid = uid
		response.Name = cmd
		response.PrefixArguments = make([]string, 0)
		response.SuffixArguments = make([]string, 0)

		_ = instance.add(response)
	}
	return
}

func (instance *ShellScriptCtrl) add(item *ShellScriptItem) (err error) {
	if nil != instance {
		instance.scripts = append(instance.scripts, item)
		err = instance.save()
	}
	return
}

func (instance *ShellScriptCtrl) load() (err error) {
	if nil != instance {
		filename := gg_.PathAbsolutize(indexFilename, instance.dirRoot)
		var enc string
		enc, err = gg_.IOReadTextFromFile(filename)
		if nil != err {
			return
		}
		var dec string
		dec, err = gg_utils.Coding.DecryptTextWithPrefix(enc, []byte(indexSeed))
		if nil != err {
			return
		}
		if len(dec) > 0 {
			_ = gg_json.JSON.Read(dec, &instance.scripts)
		}
	}
	return
}

func (instance *ShellScriptCtrl) save() (err error) {
	if nil != instance {
		filename := gg_.PathAbsolutize(indexFilename, instance.dirRoot)
		text := gg_.Stringify(instance.scripts)
		var enc string
		enc, err = gg_utils.Coding.EncryptTextWithPrefix(text, []byte(indexSeed))
		if nil != err {
			return
		}
		_, err = gg_.IOWriteTextToFile(enc, filename, false)
	}
	return
}

func (instance *ShellScriptCtrl) removeEnvFile(command string) {
	envName := GetShellEnvFilename(command)
	filename := gg_.PathAbsolutize(envName, instance.dirRoot)
	gg_.IORemoveSilent(filename)
}

func (instance *ShellScriptCtrl) removeAutorunFiles(command string) {
	gg_.IORemoveSilent(instance.listAutorunFiles(command)...)
}

func (instance *ShellScriptCtrl) listAutorunFiles(command string) (response []string) {
	files, err := gg_utils.Paths.ListDirFiles(instance.dirRoot, "*.*")
	if nil != err {
		return
	}

	if len(command) > 0 {
		suffix := GetShellAutorunFilename(command)
		for _, file := range files {
			filename := gg_.PathAbsolutize(file, instance.dirRoot)
			if strings.HasSuffix(filename, suffix) {
				response = append(response, filename)
			}
		}
	} else {
		for _, file := range files {
			filename := gg_.PathAbsolutize(file, instance.dirRoot)
			if IsAutorun(filename) {
				response = append(response, filename)
			}
		}
	}

	return
}

func (instance *ShellScriptCtrl) prepare(args ...string) (item *ShellScriptItem, arguments []string) {
	if nil != instance && len(args) > 0 {
		cmd := args[0]
		item = instance.get(cmd)
		arguments = ToCommand(item, args[1:]...)
	}
	return
}

func (instance *ShellScriptCtrl) run(args ...string) (response *ShellExecResponse) {
	if nil != instance && len(args) > 0 {
		item, arguments := instance.prepare(args...)
		if nil != item && len(arguments) > 0 {

			// execute .env if any
			_, env, _ := instance.runEnv(item.Name)

			// execute the command
			command := make([]string, 0)
			if item.IsShellCmd() {
				filename := gg_.PathAbsolutize(arguments[0], instance.dirRoot)
				// run shell
				command = append(command, GetShellCmdFrom(filename))
				arguments[0] = filename
				_ = gg_sys.Sys.ChmodExecutable(arguments[0])
				command = append(command, arguments...)

				response = instance.exec(env, command...)
			} else {
				command = append(command, arguments...)

				response = instance.exec(env, command...)
			}
		}
	}
	return
}

func (instance *ShellScriptCtrl) runEnv(command string) (response string, env []string, err error) {
	if nil != instance {
		if IsAutorun(command) || IsEnv(command) {
			return
		}
		command = GetShellEnvFilename(command)
		command = gg_.PathConcat(instance.dirRoot, command)
		execResp := instance.exec(nil, GetShellCmdFrom(command), command)
		if len(execResp.Error) > 0 {
			err = ToError(command, errors.New(execResp.Error))
			return
		} else {
			response = execResp.Response
			env = execResp.Env
		}

		// parse response to build environment for next command
		if len(response) > 0 {
			rows := strings.Split(response, "\n")
			for _, row := range rows {
				if IsValidEnvValue(row) {
					if gg_utils.Arrays.IndexOf(row, env) == -1 {
						env = append(env, row)
					}
				}
			}
		}
	}
	return
}

func (instance *ShellScriptCtrl) exec(environment []string, args ...string) (response *ShellExecResponse) {
	if nil != instance && len(args) > 0 {
		response = new(ShellExecResponse)
		response.SetCommand(args...)

		command := args[0]
		params := args[1:]
		c := exec.Command(command, params...)
		c.Dir = instance.dirWorkspace
		_ = gg_.PathMkdir(c.Dir + gg_.OS_PATH_SEPARATOR)
		// combined output
		var b bytes.Buffer
		var berr bytes.Buffer
		c.Stdout = &b
		c.Stderr = &berr

		// env
		if len(environment) > 0 {
			c.Env = environment
		}

		err := c.Run()
		if len(berr.String()) > 0 {
			err = errors.New(berr.String())
		}
		if nil == err {
			response.Response = strings.TrimSpace(b.String())
			response.Env = c.Environ()
		} else {
			response.Error = err.Error()
		}
	}
	return
}
