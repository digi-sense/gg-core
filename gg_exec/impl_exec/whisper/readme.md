# Whisper

Whisper is a LLM that requires Python 3, PIP and FFMPEG.

For further help on how to install:
https://www.gpu-mart.com/blog/install-whisper-in-ubuntu

Sample usage:
whisper panettone.mp3 --language Italian