package whisper

import "strings"

var (
	Whisper *WhisperHelper

	whisperCommand = "whisper"
)

const fsName = "./.ffmpeg" // dir name

type WhisperHelper struct {
}

func init() {
	Whisper = new(WhisperHelper)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WhisperHelper) IsInstalled() bool {
	if nil != instance {
		return instance.NewExec().IsInstalled()
	}
	return false
}

func (instance *WhisperHelper) Version() (version string, err error) {
	program := instance.NewExec()
	version, err = program.Version()
	if nil == err {
		tokens := strings.Split(version, " ")
		if len(tokens) > 2 {
			version = tokens[2]
		}
	}
	return
}

// NewExec
// Creates new Git command
func (instance *WhisperHelper) NewExec() *WhisperExec {
	if nil != instance {
		return NewExec(whisperCommand)
	}
	return nil
}
