package whisper

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_exec/executor"
	"bitbucket.org/digi-sense/gg-core/gg_exec/impl_exec/ffmpeg"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

type WhisperExec struct {
	uid      string
	execPath string
	dirRoot  string
	dirWork  string // temporary working folder for outputs

	initialized bool
}

func NewExec(execPath string) *WhisperExec {
	instance := new(WhisperExec)
	instance.uid = gg_rnd.Rnd.RndId()
	instance.execPath = execPath

	instance.SetRoot(".")

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WhisperExec) SetRoot(dir string) *WhisperExec {
	instance.dirRoot = gg_utils.Paths.Absolute(dir)

	if instance.initialized {
		instance.initialized = false
		instance.init()
	}

	return instance
}

func (instance *WhisperExec) Root() string {
	return instance.dirRoot
}

func (instance *WhisperExec) GetPath(path string) (response string) {
	response = gg_utils.Paths.Absolutize(path, instance.dirRoot)
	_ = gg_utils.Paths.Mkdir(response)
	return
}

func (instance *WhisperExec) ExecuteCommand(arguments ...string) (out string, err error) {
	program := instance.program()
	session, execErr := program.Run(arguments...)
	if nil != execErr {
		err = execErr
		return
	}
	defer session.Close()

	stdErr := session.StdErr()
	if isError(stdErr) {
		err = errors.New(stdErr)
	} else {
		out = strings.TrimSpace(session.StdOut())
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o m m a n d s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WhisperExec) IsInstalled() bool {
	version, err := instance.Version()
	if nil != err {
		return false
	}
	return len(version) > 0
}

func (instance *WhisperExec) Version() (response string, err error) {
	args := []string{"-h"}
	response, err = instance.ExecuteCommand(args...)
	return
}

func (instance *WhisperExec) Help() (response string, err error) {
	args := []string{"-h"}
	response, err = instance.ExecuteCommand(args...)
	return
}

// Transcribe processes an audio file, optionally converts it to MP3 if not already in audio format, and generates a transcription.
// It returns a map containing transcription data and potential errors that occurred during the process.
func (instance *WhisperExec) Transcribe(source string, params ...string) (response map[string]interface{}, err error) {
	if nil != instance {
		if len(source) > 0 {
			filesToRemove := make([]string, 0)

			// reset output
			instance.initWorkDir()

			source = gg_.PathAbsolutize(source, instance.dirRoot)
			if ok, _ := gg_.PathExists(source); !ok {
				err = gg_errors.Errors.Prefix(gg_.PanicSystemError, fmt.Sprintf("File '%s' not found: ", source))
				return
			}

			// check file is audio file
			if !gg_.MimeTypeIsAudioFile(source) {
				convertDir := "./convertions"
				convertDir = gg_.PathAbsolutize(convertDir, instance.dirWork)
				_ = gg_.PathMkdir(convertDir)
				// convert
				converter := ffmpeg.Ffmpeg.NewExec()
				converter.SetRoot(instance.dirWork)
				target := gg_.PathConcat(convertDir, gg_.PathChangeFileNameExtension(filepath.Base(source), ".mp3"))
				_, err = converter.Convert(source, target)
				if nil != err {
					err = gg_errors.Errors.Prefix(err, fmt.Sprintf("Error converting '%s' to '%s': ",
						filepath.Base(source), filepath.Base(target)))
					return
				} else {
					filesToRemove = append(filesToRemove, target)
					source = target // replace with converted file
				}
			}

			// whisper panettone.mp3 --language Italian
			args := []string{source}
			args = append(args, params...)
			execResp, execErr := instance.ExecuteCommand(args...)
			if nil != execErr {
				err = execErr
				return
			}

			if len(execResp) > 0 {
				// do something with whisper response
			}

			response = instance.readResponse()

			// reset output
			gg_.IORemoveSilent(instance.dirWork)
		} else {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Source File and Target Dir files are required: ")
		}
	}
	return
}

func (instance *WhisperExec) GetTranscriptionText(source string) (response string, err error) {
	if nil != instance {
		var data map[string]interface{}
		data, err = instance.Transcribe(source)
		if nil == err {
			response = gg_utils.Maps.GetString(data, "txt")
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WhisperExec) program(args ...string) *executor.ConsoleProgram {
	instance.init()
	return executor.NewConsoleProgramWithDir(instance.execPath, instance.dirWork, args...)
}

func (instance *WhisperExec) init() {
	if nil != instance && !instance.initialized {
		instance.initialized = true

		instance.dirRoot = gg_.PathAbsolute(instance.dirRoot)
		instance.dirWork = gg_.PathConcat(instance.dirRoot, "tmp_"+instance.uid)

		_ = gg_.PathMkdir(instance.dirRoot)
	}
}

func (instance *WhisperExec) initWorkDir() {
	if nil != instance {
		instance.dirWork = gg_.PathConcat(instance.dirRoot, "tmp_"+instance.uid)
		gg_.IORemoveSilent(instance.dirWork)
		_ = gg_.PathMkdir(instance.dirWork)
	}
}

func (instance *WhisperExec) readResponse() (response map[string]interface{}) {
	response = make(map[string]interface{})
	files, _ := os.ReadDir(instance.dirWork)
	for _, file := range files {
		if !file.IsDir() {
			key := gg_utils.Paths.ExtensionName(file.Name())
			value, _ := gg_.IOReadTextFromFile(gg_.PathConcat(instance.dirWork, file.Name()))
			if len(value) > 0 {
				response[key] = value
			}
		}
	}
	return
}

func isError(text string) bool {
	return len(text) > 0 && !strings.Contains(text, "FutureWarning: ")
}
