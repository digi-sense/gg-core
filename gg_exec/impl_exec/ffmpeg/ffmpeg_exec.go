package ffmpeg

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_exec/executor"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"strings"
)

type FfmpegExec struct {
	execPath string
	dirRoot  string

	initialized bool
}

func NewExec(execPath string) *FfmpegExec {
	instance := new(FfmpegExec)
	instance.execPath = execPath

	instance.SetRoot(".")

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *FfmpegExec) SetRoot(dir string) *FfmpegExec {
	instance.dirRoot = gg_utils.Paths.Absolute(dir)

	if instance.initialized {
		instance.initialized = false
		instance.init()
	}

	return instance
}

func (instance *FfmpegExec) Root() string {
	return instance.dirRoot
}

func (instance *FfmpegExec) GetPath(path string) (response string) {
	response = gg_utils.Paths.Absolutize(path, instance.dirRoot)
	_ = gg_utils.Paths.Mkdir(response)
	return
}

func (instance *FfmpegExec) ExecuteCommand(arguments ...string) (out string, err error) {
	program := instance.program()
	session, execErr := program.Run(arguments...)
	if nil != execErr {
		err = execErr
		return
	}
	defer session.Close()

	stdErr := session.StdErr()
	if isError(stdErr) {
		err = errors.New(stdErr)
	} else {
		out = strings.TrimSpace(session.StdOut())
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o m m a n d s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *FfmpegExec) IsInstalled() bool {
	version, err := instance.Version()
	if nil != err {
		return false
	}
	return len(version) > 0
}

func (instance *FfmpegExec) Version() (response string, err error) {
	args := []string{"-version"}
	response, err = instance.ExecuteCommand(args...)
	return
}

func (instance *FfmpegExec) Help() (response string, err error) {
	args := []string{"-h"}
	response, err = instance.ExecuteCommand(args...)
	return
}

func (instance *FfmpegExec) Convert(source, target string) (response string, err error) {
	if nil != instance {
		if len(source) > 0 && len(target) > 0 {
			source = gg_utils.Paths.Absolutize(source, instance.dirRoot)
			target = gg_utils.Paths.Absolutize(target, instance.dirRoot)
			args := []string{"-i", source, target}
			response, err = instance.ExecuteCommand(args...)
		} else {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Source and Target files are required: ")
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *FfmpegExec) program(args ...string) *executor.ConsoleProgram {
	instance.init()
	return executor.NewConsoleProgramWithDir(instance.execPath, instance.dirRoot, args...)
}

func (instance *FfmpegExec) init() {
	if nil != instance && !instance.initialized {
		instance.initialized = true

	}
}

func isError(text string) bool {
	return len(text) > 0 && !strings.HasPrefix(text, "ffmpeg version")
}
