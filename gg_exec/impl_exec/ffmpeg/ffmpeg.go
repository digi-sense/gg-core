package ffmpeg

import "strings"

var (
	Ffmpeg *FfmpegHelper

	ffmpegCommand = "ffmpeg"
)

const fsName = "./.ffmpeg" // dir name

type FfmpegHelper struct {
}

func init() {
	Ffmpeg = new(FfmpegHelper)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *FfmpegHelper) IsInstalled() bool {
	if nil != instance {
		return instance.NewExec().IsInstalled()
	}
	return false
}

func (instance *FfmpegHelper) Version() (version string, err error) {
	program := instance.NewExec()
	version, err = program.Version()
	if nil == err {
		// ffmpeg version 7.0.1 Copyright (c) 2000-2024 the FFmpeg developers
		tokens := strings.Split(version, " ")
		if len(tokens) > 2 {
			version = tokens[2]
		}
	}
	return
}

// NewExec
// Creates new Git command
func (instance *FfmpegHelper) NewExec() *FfmpegExec {
	if nil != instance {
		return NewExec(ffmpegCommand)
	}
	return nil
}
