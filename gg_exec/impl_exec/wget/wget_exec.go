package wget

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_exec/executor"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"fmt"
	"os"
	"strings"
)

type WgetExec struct {
	uid      string
	execPath string
	dirRoot  string
	dirWork  string // temporary working folder for outputs

	initialized bool
}

func NewExec(execPath string) *WgetExec {
	instance := new(WgetExec)
	instance.uid = gg_rnd.Rnd.RndId()
	instance.execPath = execPath

	instance.SetRoot(".")

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WgetExec) SetRoot(dir string) *WgetExec {
	instance.dirRoot = gg_utils.Paths.Absolute(dir)

	if instance.initialized {
		instance.initialized = false
		instance.init()
	}

	return instance
}

func (instance *WgetExec) Root() string {
	return instance.dirRoot
}

func (instance *WgetExec) GetPath(path string) (response string) {
	response = gg_utils.Paths.Absolutize(path, instance.dirRoot)
	_ = gg_utils.Paths.Mkdir(response)
	return
}

func (instance *WgetExec) ExecuteCommand(arguments ...string) (out string, err error) {
	program := instance.program()
	session, execErr := program.Run(arguments...)
	if nil != execErr {
		err = execErr
		return
	}
	defer session.Close()

	stdErr := session.StdErr()
	if isError(stdErr) {
		err = errors.New(stdErr)
	} else {
		out = strings.TrimSpace(session.StdOut())
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o m m a n d s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WgetExec) IsInstalled() bool {
	version, err := instance.Version()
	if nil != err {
		return false
	}
	return len(version) > 0
}

func (instance *WgetExec) Version() (response string, err error) {
	args := []string{"-V"}
	response, err = instance.ExecuteCommand(args...)
	if nil == err {
		response = strings.Split(response, "\n")[0]
	}
	return
}

func (instance *WgetExec) Help() (response string, err error) {
	args := []string{"--help"}
	response, err = instance.ExecuteCommand(args...)
	return
}

func (instance *WgetExec) Download(sourceURL string, target string) (response string, err error) {
	if nil != instance {
		if len(sourceURL) > 0 {
			var args []string
			if len(target) > 0 {
				isDir := strings.HasSuffix(target, gg_.OS_PATH_SEPARATOR)
				if isDir {
					target = gg_.PathAbsolute(target)
					instance.dirWork = gg_.PathDir(target)
					args = append(args, fmt.Sprintf("-P%s", target))
				} else {
					instance.dirWork = instance.dirRoot
				}
			}
			args = append(args, sourceURL)
			response, err = instance.ExecuteCommand(args...)
			if nil == err {
				response = getFilename(response)
			}
		} else {
			err = errors.New("source_is_required")
		}
	}
	return
}

func (instance *WgetExec) DownloadBytes(sourceURL string) (response []byte, err error) {
	temp := gg_.PathAbsolute("./tmp") + gg_.OS_PATH_SEPARATOR
	defer os.Remove(temp)
	var filename string
	filename, err = instance.Download(sourceURL, temp)
	if nil == err {
		response, err = gg_.IOReadBytesFromFile(filename)
		_ = os.Remove(filename)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WgetExec) program(args ...string) *executor.ConsoleProgram {
	instance.init()
	return executor.NewConsoleProgramWithDir(instance.execPath, instance.dirWork, args...)
}

func (instance *WgetExec) init() {
	if nil != instance && !instance.initialized {
		instance.initialized = true

		instance.dirRoot = gg_.PathAbsolute(instance.dirRoot)
		instance.dirWork = instance.dirRoot

		_ = gg_.PathMkdir(instance.dirRoot)
	}
}

func (instance *WgetExec) initWorkDir() {
	if nil != instance {
		instance.dirWork = gg_.PathConcat(instance.dirRoot, "tmp_"+instance.uid)
		gg_.IORemoveSilent(instance.dirWork)
		_ = gg_.PathMkdir(instance.dirWork)
	}
}

func isError(text string) bool {
	if len(text) > 0 {
		return !strings.Contains(text, "100%")
	}
	return false
}

func getFilename(text string) (response string) {
	if len(text) > 0 {
		rows := strings.Split(text, "\n")
		for _, row := range rows {
			if strings.Contains(row, "saved [") {
				start := strings.Index(row, "'")
				end := strings.LastIndex(row, "'")
				response = row[start+1 : end]
			}
		}
	}
	return
}
