package wget

import "strings"

var (
	Wget *WgetHelper

	wgetCommand = "wget"
)

type WgetHelper struct {
}

func init() {
	Wget = new(WgetHelper)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WgetHelper) IsInstalled() bool {
	if nil != instance {
		return instance.NewExec().IsInstalled()
	}
	return false
}

func (instance *WgetHelper) Version() (version string, err error) {
	program := instance.NewExec()
	version, err = program.Version()
	if nil == err {
		tokens := strings.Split(version, " ")
		if len(tokens) > 2 {
			version = tokens[2]
		}
	}
	return
}

// NewExec
// Creates new Git command
func (instance *WgetHelper) NewExec() *WgetExec {
	if nil != instance {
		return NewExec(wgetCommand)
	}
	return nil
}
