# WGET

Uses WGET command line tool

https://linuxize.com/post/wget-command-examples/

### Saving the Downloaded File Under Different Name
To save the downloaded file under a different name, pass the -O option followed by the chosen name:

```
wget -O latest-hugo.zip https://github.com/gohugoio/hugo/archive/master.zip
```

