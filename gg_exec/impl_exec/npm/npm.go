package npm

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var (
	Npm *NpmHelper

	npmCommand = "npm"
)

const wpName = "npm"
const fsName = "./.npm"

type NpmHelper struct {
}

func init() {
	Npm = new(NpmHelper)
	npmCommand = findExecPath()
}

// findExecPath tries to find the Chrome browser somewhere in the current
// system. It performs a rather aggressive search, which is the same in all systems.
func findExecPath() string {
	for _, path := range [...]string{
		// Unix-like
		npmCommand,
		fmt.Sprintf("/usr/bin/%s", npmCommand),
		fmt.Sprintf("/usr/local/bin/%s", npmCommand),

		// Windows
		fmt.Sprintf("%s.exe", npmCommand), // in case PATHEXT is misconfigured
		fmt.Sprintf(`C:\Program Files (x86)\Node\program\%s.exe`, npmCommand),
		fmt.Sprintf(`C:\Program Files\Node\program\%s.exe`, npmCommand),
		filepath.Join(os.Getenv("USERPROFILE"), fmt.Sprintf(`AppData\Local\Node\Application\%s.exe`, npmCommand)),

		// Mac
		fmt.Sprintf("/Applications/LibreOffice.app/Contents/MacOS/%s", npmCommand),
	} {
		found, err := exec.LookPath(path)
		if err == nil {
			return found
		}
	}

	// Fall back to something simple and sensible, to give a useful error
	// message.
	return npmCommand
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *NpmHelper) IsInstalled() bool {
	if nil != instance {
		return instance.NewExec().IsInstalled()
	}
	return false
}

func (instance *NpmHelper) Version() (version string, err error) {
	program := instance.NewExec()
	version, err = program.Version()
	if nil == err {
		// git version 2.23.0
		tokens := strings.Split(version, " ")
		if len(tokens) == 3 {
			version = tokens[2]
		}
	}
	return
}

// NewExec
// Creates new npm command
func (instance *NpmHelper) NewExec() *NpmExec {
	if nil != instance {
		return NewExec(npmCommand, nil)
	}
	return nil
}
