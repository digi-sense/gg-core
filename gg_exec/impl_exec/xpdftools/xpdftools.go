package xpdftools

// ---------------------------------------------------------------------------------------------------------------------
//	i n i t
// ---------------------------------------------------------------------------------------------------------------------

var (
	XpdfTools *XpdfToolsHelper
)

func init() {
	XpdfTools = new(XpdfToolsHelper)
}

// ---------------------------------------------------------------------------------------------------------------------
//	XpdfToolsHelper
// ---------------------------------------------------------------------------------------------------------------------

type XpdfToolsHelper struct {
	dirBin       string // binaries directory
	dirWorkspace string // executable workspace
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *XpdfToolsHelper) IsInstalled() bool {
	if nil != instance && len(instance.dirBin) > 0 {
		return true
	}
	return false
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
