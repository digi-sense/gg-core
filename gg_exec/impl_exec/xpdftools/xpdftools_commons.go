package xpdftools

type XpdfToolsCommand string

const (
	PdfToText XpdfToolsCommand = "pdftotext" // Pdftotext converts Portable Document Format (PDF) files to plain text.
	PdfToPs   XpdfToolsCommand = "pdftops"   // Pdftops converts Portable Document Format (PDF) files to PostScript so they can be printed.
	PdfToPpm  XpdfToolsCommand = "pdftoppm"  // Pdftoppm converts Portable Document Format (PDF) files to color image files in Portable Pixmap (PPM) format, grayscale image files in Portable Graymap (PGM) format, or monochrome image files in Portable Bitmap (PBM) format.
	PdfToPng  XpdfToolsCommand = "pdftopng"  // Pdftopng converts Portable Document Format (PDF) files to color, grayscale, or monochrome image files in Portable Network Graphics (PNG) format.
	PdfToHtml XpdfToolsCommand = "pdftohtml" // Pdftohtml converts Portable Document Format (PDF) files to HTML.
	PdfInfo   XpdfToolsCommand = "pdfinfo"   // Pdfinfo prints the contents of the ´Info’ dictionary (plus some other useful information) from a Portable Document Format (PDF) file.
	PdfImages XpdfToolsCommand = "pdfimages" // Pdfimages saves images from a Portable Document Format (PDF) file as Portable Pixmap (PPM), Portable Graymap (PGM), Portable Bitmap (PBM), or JPEG files.
	PdfFonts  XpdfToolsCommand = "pdffonts"  // Pdffonts lists the fonts used in a Portable Document Format (PDF) file along with various information for each font.
	PdfDetach XpdfToolsCommand = "pdfdetach" // Pdfdetach lists or extracts embedded files (attachments) from a Portable Document Format (PDF) file.
)

var XpdfCommands = []XpdfToolsCommand{PdfToText, PdfToPs, PdfToPpm, PdfToPng, PdfToHtml, PdfInfo, PdfImages, PdfFonts, PdfDetach}
