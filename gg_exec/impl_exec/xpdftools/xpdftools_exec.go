package xpdftools

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_exec/executor"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"os"
	"strings"
)

type XpdfToolsExec struct {
	uid      string
	execPath string
	dirRoot  string
	dirWork  string // temporary working folder for outputs

	initialized bool
}

func NewExec(execPath string) *XpdfToolsExec {
	instance := new(XpdfToolsExec)
	instance.uid = gg_rnd.Rnd.RndId()
	instance.execPath = execPath

	instance.SetRoot(".")

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *XpdfToolsExec) SetRoot(dir string) *XpdfToolsExec {
	instance.dirRoot = gg_utils.Paths.Absolute(dir)

	if instance.initialized {
		instance.initialized = false
		instance.init()
	}

	return instance
}

func (instance *XpdfToolsExec) Root() string {
	return instance.dirRoot
}

func (instance *XpdfToolsExec) GetPath(path string) (response string) {
	response = gg_utils.Paths.Absolutize(path, instance.dirRoot)
	_ = gg_utils.Paths.Mkdir(response)
	return
}

func (instance *XpdfToolsExec) Run(arguments ...string) (out string, err error) {
	program := instance.program()
	session, execErr := program.Run(arguments...)
	if nil != execErr {
		err = execErr
		return
	}
	defer session.Close()

	stdErr := session.StdErr()
	if isError(stdErr) {
		err = errors.New(stdErr)
	} else {
		out = strings.TrimSpace(session.StdOut())
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *XpdfToolsExec) program(args ...string) *executor.ConsoleProgram {
	instance.init()
	return executor.NewConsoleProgramWithDir(instance.execPath, instance.dirWork, args...)
}

func (instance *XpdfToolsExec) init() {
	if nil != instance && !instance.initialized {
		instance.initialized = true

		instance.dirRoot = gg_.PathAbsolute(instance.dirRoot)
		instance.dirWork = gg_.PathConcat(instance.dirRoot, "tmp_"+instance.uid)

		_ = gg_.PathMkdir(instance.dirRoot)
	}
}

func (instance *XpdfToolsExec) initWorkDir() {
	if nil != instance {
		instance.dirWork = gg_.PathConcat(instance.dirRoot, "tmp_"+instance.uid)
		gg_.IORemoveSilent(instance.dirWork)
		_ = gg_.PathMkdir(instance.dirWork)
	}
}

func (instance *XpdfToolsExec) readResponse() (response map[string]interface{}) {
	response = make(map[string]interface{})
	files, _ := os.ReadDir(instance.dirWork)
	for _, file := range files {
		if !file.IsDir() {
			key := gg_utils.Paths.ExtensionName(file.Name())
			value, _ := gg_.IOReadTextFromFile(gg_.PathConcat(instance.dirWork, file.Name()))
			if len(value) > 0 {
				response[key] = value
			}
		}
	}
	return
}

func isError(text string) bool {
	return len(text) > 0 && !strings.Contains(text, "FutureWarning: ")
}
