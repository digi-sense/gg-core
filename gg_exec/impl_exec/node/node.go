package node

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var (
	Node *NodeHelper

	nodeCommand = "node"
)

const wpName = "node"
const fsName = "./.node"

type NodeHelper struct {
}

func init() {
	Node = new(NodeHelper)
	nodeCommand = findExecPath()
}

// findExecPath tries to find the Chrome browser somewhere in the current
// system. It performs a rather aggressive search, which is the same in all systems.
func findExecPath() string {
	for _, path := range [...]string{
		// Unix-like
		nodeCommand,
		fmt.Sprintf("/usr/bin/%s", nodeCommand),
		fmt.Sprintf("/usr/local/bin/%s", nodeCommand),

		// Windows
		fmt.Sprintf("%s.exe", nodeCommand), // in case PATHEXT is misconfigured
		fmt.Sprintf(`C:\Program Files (x86)\Node\program\%s.exe`, nodeCommand),
		fmt.Sprintf(`C:\Program Files\Node\program\%s.exe`, nodeCommand),
		filepath.Join(os.Getenv("USERPROFILE"), fmt.Sprintf(`AppData\Local\Node\Application\%s.exe`, nodeCommand)),

		// Mac
		fmt.Sprintf("/Applications/LibreOffice.app/Contents/MacOS/%s", nodeCommand),
	} {
		found, err := exec.LookPath(path)
		if err == nil {
			return found
		}
	}

	// Fall back to something simple and sensible, to give a useful error
	// message.
	return nodeCommand
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *NodeHelper) IsInstalled() bool {
	if nil != instance {
		return instance.NewExec().IsInstalled()
	}
	return false
}

func (instance *NodeHelper) Version() (version string, err error) {
	program := instance.NewExec()
	version, err = program.Version()
	if nil == err {
		// version 2.23.0
		tokens := strings.Split(version, " ")
		if len(tokens) == 3 {
			version = tokens[2]
		}
	}
	return
}

// NewExec
// Creates new Solana command with default password
func (instance *NodeHelper) NewExec(args ...string) *NodeExec {
	if nil != instance {
		filename := "" // js file to execute
		if len(args) == 1 {
			filename = gg_utils.Convert.ToString(args[0])
		}
		return NewNodeExec(nodeCommand, nil, filename)
	}
	return nil
}
