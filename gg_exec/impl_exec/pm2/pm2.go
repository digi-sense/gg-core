package pm2

import (
	"strings"
)

var (
	Pm2 *Pm2Helper

	pm2Command = "pm2"
)

const wpName = "pm2"
const fsName = "./.pm2"

type Pm2Helper struct {
}

func init() {
	Pm2 = new(Pm2Helper)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Pm2Helper) IsInstalled() bool {
	if nil != instance {
		return instance.NewExec().IsInstalled()
	}
	return false
}

func (instance *Pm2Helper) Version() (version string, err error) {
	program := instance.NewExec()
	version, err = program.Version()
	if nil == err {
		tokens := strings.Split(version, "\n")
		if len(tokens) > 1 {
			version = strings.TrimSpace(tokens[len(tokens)-1])
		}
	}
	return
}

// NewExec
// Creates new pm2 command
func (instance *Pm2Helper) NewExec() *Pm2Exec {
	if nil != instance {
		return NewExec(pm2Command, nil)
	}
	return nil
}
