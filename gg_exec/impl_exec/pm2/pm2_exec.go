package pm2

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_exec/executor"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"fmt"
	"path/filepath"
	"strings"
)

type Pm2Exec struct {
	execPath      string
	dirController *gg_utils.DirCentral
	logger        gg_log.ILogger
	initialized   bool
}

func NewExec(execPath string, logger gg_log.ILogger) *Pm2Exec {
	instance := new(Pm2Exec)
	instance.execPath = execPath
	instance.logger = logger
	instance.dirController = gg_utils.Dir.NewCentral(fsName, ".tmp", true)

	instance.SetRoot(".")

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Pm2Exec) SetLogger(logger gg_log.ILogger) *Pm2Exec {
	instance.logger = logger
	return instance
}

func (instance *Pm2Exec) SetRoot(dir string) *Pm2Exec {
	instance.dirController.SetRoot(dir)
	return instance
}

func (instance *Pm2Exec) SetWorkPath(dir string) *Pm2Exec {
	instance.dirController.SetWorkPath(dir)
	return instance
}

func (instance *Pm2Exec) SetTemp(dir string) *Pm2Exec {
	instance.dirController.SetTemp(dir)
	return instance
}

func (instance *Pm2Exec) SetSubTemp(enabled bool) *Pm2Exec {
	instance.dirController.SetSubTemp(enabled)
	return instance
}

func (instance *Pm2Exec) Root() string {
	return instance.dirController.DirRoot()
}

func (instance *Pm2Exec) Temp() string {
	return instance.dirController.DirTemp()
}

func (instance *Pm2Exec) Work() string {
	return instance.dirController.DirWork()
}

func (instance *Pm2Exec) GetPath(path string) (response string) {
	response = instance.dirController.GetPath(path)
	return
}

func (instance *Pm2Exec) GetWorkPath(subPath string) (response string) {
	response = instance.dirController.GetWorkPath(subPath)
	return
}

func (instance *Pm2Exec) GetTempPath(subPath string) (response string) {
	response = instance.dirController.GetTempPath(subPath)
	return
}

func (instance *Pm2Exec) LogFlush() {
	if l, ok := instance.logger.(*gg_log.Logger); ok {
		l.Flush()
	}
}

func (instance *Pm2Exec) LogDisableRotation() {
	if l, ok := instance.logger.(*gg_log.Logger); ok {
		l.RotateEnable(false)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	c o m m a n d s
//----------------------------------------------------------------------------------------------------------------------

func (instance *Pm2Exec) Version() (string, error) {
	exec, err := instance.program().Run("--version")
	if nil != err {
		return "", err
	}
	response := strings.ToLower(exec.StdOut())
	if strings.Index(response, ".") > -1 {
		version := strings.TrimSpace(strings.ReplaceAll(response, "v", ""))
		return version, nil
	}
	return "", errors.New(exec.StdOut())
}

func (instance *Pm2Exec) Install() (response string, err error) {
	if nil != instance {
		if !instance.IsInstalled() {
			p := instance.programCmd("npm") // get npm

			// get npm version to test if installed
			session, e := p.Run("--version")
			if nil != e {
				err = gg_errors.Errors.Prefix(e, "Node npm command not found. Install Node https://nodejs.org/: ")
				return
			}
			if nil != session {
				if len(session.StdErr()) > 0 {
					err = gg_errors.Errors.Prefix(errors.New(session.StdErr()), "Node npm command not found. Install Node https://nodejs.org/: ")
					return
				}
			}

			// launch install command
			session, e = p.Run("install", "pm2", "-g")
			if nil != e {
				err = gg_errors.Errors.Prefix(e, "npm install command failed: ")
				return
			}
			if nil != session {
				if len(session.StdErr()) > 0 {
					err = gg_errors.Errors.Prefix(errors.New(session.StdErr()), "npm install command failed: ")
					return
				}
			}

			response, err = getResponse(session, e)
		} else {
			response = "Already Installed!"
		}
	}
	return
}

func (instance *Pm2Exec) IsInstalled() bool {
	version, err := instance.Version()
	if nil != err {
		return false
	}
	return len(version) > 0
}

func (instance *Pm2Exec) Help() (string, error) {
	if instance.IsInstalled() {
		exec, err := instance.program().Run("--help")
		if nil != err && err.Error() != "exit status 1" {
			return "", err
		}
		if nil != exec {
			return exec.StdOut(), nil
		}
		return "", err
	}
	return "", executor.NotInstalledError
}

func (instance *Pm2Exec) HelpOn(command string) (string, error) {
	if instance.IsInstalled() {
		exec, err := instance.program().Run(command, "-h")
		if nil != err {
			return "", err
		}
		return exec.StdOut(), nil
	}
	return "", executor.NotInstalledError
}

func (instance *Pm2Exec) List() (string, error) {
	if instance.IsInstalled() {
		exec, err := instance.program().Run("list")
		return getResponse(exec, err)
	}
	return "", executor.NotInstalledError
}

// Start initializes the NpmExec instance with the given indexPath, setting root and work paths, then verifies installation.
// If the required environments are not installed, it proceeds with installation and starts the node program using pm2.
func (instance *Pm2Exec) Start(indexPath string, watchEnabled bool) (response string, err error) {
	if nil != instance {
		if len(indexPath) == 0 {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Open indexPath is empty")
			return
		}

		// set root
		root := filepath.Dir(indexPath)
		instance.SetRoot(root)
		instance.SetWorkPath(root)

		// check installation
		if !instance.IsInstalled() {
			_, err = instance.Install()
			if nil != err {
				return
			}
		}

		// launch node program with pm2
		var exec *executor.ConsoleProgramSession
		if watchEnabled {
			exec, err = instance.program().Run("start", indexPath, "--watch")
		} else {
			exec, err = instance.program().Run("start", indexPath)
		}
		if nil != err {
			err = gg_errors.Errors.Prefix(err, fmt.Sprintf("Open '%s' failed: ", indexPath))
			return
		}
		response, err = getResponse(exec, err)
	}
	return
}

func (instance *Pm2Exec) Serve(indexPath string, options ...string) (response string, err error) {
	if nil != instance {
		if len(indexPath) == 0 {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Open indexPath is empty")
			return
		}

		// set root
		root := indexPath
		if gg_.PathIsFile(root) {
			root = filepath.Dir(indexPath)
		}
		instance.SetRoot(root)
		instance.SetWorkPath(root)

		// check installation
		if !instance.IsInstalled() {
			_, err = instance.Install()
			if nil != err {
				return
			}
		}

		args := make([]string, 0)
		args = append(args, "serve")
		args = append(args, indexPath)
		if len(options) > 0 {
			args = append(args, options...)
		}
		args = append(args, options...)
		// launch node program with pm2
		var exec *executor.ConsoleProgramSession
		exec, err = instance.program().Run(args...)

		if nil != err {
			err = gg_errors.Errors.Prefix(err, fmt.Sprintf("Open '%s' failed: ", indexPath))
			return
		}
		response, err = getResponse(exec, err)
	}
	return
}

// Stop stops the npm process for the given unique identifier (uid) or stops all if uid is empty.
func (instance *Pm2Exec) Stop(uid string) (response string, err error) {
	if nil != instance {
		if len(uid) == 0 {
			uid = "all"
		}

		// check installation
		if !instance.IsInstalled() {
			_, err = instance.Install()
			if nil != err {
				return
			}
		}

		exec, e := instance.program().Run("stop", uid)
		if nil != e {
			err = gg_errors.Errors.Prefix(e, fmt.Sprintf("Stop '%s' failed: ", uid))
			return
		}
		response, err = getResponse(exec, e)
	}
	return
}

// Delete deletes a process identified by the given uid or all processes if uid is empty.
// It ensures the necessary program is installed before attempting deletion.
func (instance *Pm2Exec) Delete(uid string) (response string, err error) {
	if nil != instance {
		if len(uid) == 0 {
			uid = "all"
		}

		// check installation
		if !instance.IsInstalled() {
			_, err = instance.Install()
			if nil != err {
				return
			}
		}

		exec, e := instance.program().Run("delete", uid)
		if nil != e {
			err = gg_errors.Errors.Prefix(e, fmt.Sprintf("Delete '%s' failed: ", uid))
			return
		}
		response, err = getResponse(exec, e)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	r u n n e r
//----------------------------------------------------------------------------------------------------------------------

func (instance *Pm2Exec) ExecuteCommand(arguments ...string) (out string, err error) {
	program := instance.program()
	session, execErr := program.Run(arguments...)
	if nil != execErr {
		err = execErr
		return
	}

	stdErr := session.StdErr()
	if len(stdErr) > 0 {
		err = errors.New(stdErr)
	} else {
		out = strings.TrimSpace(session.StdOut())
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Pm2Exec) program() *executor.ConsoleProgram {
	return instance.programCmd(pm2Command)
}

func (instance *Pm2Exec) programCmd(command string) *executor.ConsoleProgram {
	instance.init()
	dir := instance.dirController.DirRoot()

	return executor.NewConsoleProgramWithDir(command, dir)
}

func (instance *Pm2Exec) init() {
	if nil != instance && !instance.initialized {
		instance.initialized = true

		instance.dirController.Refresh()

		logPath := instance.dirController.PathLog()
		if nil != instance.logger {
			instance.logger.(*gg_log.Logger).SetFilename(logPath)
		} else {
			instance.logger = gg_log.Log.New("info", logPath)
			if l, ok := instance.logger.(*gg_log.Logger); ok {
				l.SetMessageFormat("* " + l.GetMessageFormat())
			}
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func getResponse(exec *executor.ConsoleProgramSession, err error) (string, error) {
	if nil != err {
		message := err.Error()
		if strings.Index(message, "exit status") == -1 {
			return "", err
		}
	}
	if nil != exec {
		stderr := exec.StdErr()
		if len(stderr) > 0 {
			return "", errors.New(stderr)
		}
		return strings.TrimSpace(exec.StdOut()), nil
	}
	return "", err
}
