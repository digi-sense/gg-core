# PANDOC

If you need to convert files from one markup format into another, pandoc is your swiss-army knife. Pandoc can convert between the following formats:

(← = conversion from; → = conversion to; ↔︎ = conversion from and to)

[Pandoc](https://pandoc.org/index.html) understands a number of useful markdown syntax extensions, including document metadata (title, author, date); footnotes; tables; definition lists; superscript and subscript; strikeout; enhanced ordered lists (start number and numbering style are significant); running example lists; delimited code blocks with syntax highlighting; smart quotes, dashes, and ellipses; markdown inside HTML blocks; and inline LaTeX. If strict markdown compatibility is desired, all of these extensions can be turned off.

## Install PanDoc

To [install PanDoc](https://pandoc.org/installing.html) you can refer to official PanDoc Documentation.

## Samples

### To HTML Slides

```
pandoc -s --mathml -i -t dzslides SLIDES -o example16a.html
pandoc -s --webtex -i -t slidy SLIDES -o example16b.html
pandoc -s --mathjax -i -t revealjs SLIDES -o example16d.html
```