package pandoc

import "strings"

var (
	PanDoc *PanDocHelper

	pandocCommand = "pandoc"
)

const wpName = "pandoc"
const fsName = "./.pandoc"

type PanDocHelper struct {
}

func init() {
	PanDoc = new(PanDocHelper)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *PanDocHelper) IsInstalled() bool {
	if nil != instance {
		return instance.NewExec().IsInstalled()
	}
	return false
}

func (instance *PanDocHelper) Version() (version string, err error) {
	program := instance.NewExec()
	version, err = program.Version()
	if nil == err {
		tokens := strings.Split(version, "\n")
		if len(tokens) > 0 {
			version = strings.TrimSpace(tokens[0])
		}
	}
	return
}

// NewExec
// Creates new pm2 command
func (instance *PanDocHelper) NewExec() *PanDocExec {
	if nil != instance {
		return NewExec(pandocCommand)
	}
	return nil
}
