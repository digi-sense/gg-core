package pandoc

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_exec/executor"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"path/filepath"
	"strings"
)

type PanDocExec struct {
	execPath      string
	dirController *gg_utils.DirCentral

	initialized bool
}

func NewExec(execPath string) *PanDocExec {
	instance := new(PanDocExec)
	instance.execPath = execPath
	instance.dirController = gg_utils.Dir.NewCentral(fsName, ".tmp", true)

	instance.SetRoot(".")

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *PanDocExec) SetRoot(dir string) *PanDocExec {
	instance.dirController.SetRoot(dir)
	return instance
}

func (instance *PanDocExec) SetWorkPath(dir string) *PanDocExec {
	instance.dirController.SetWorkPath(dir)
	return instance
}

func (instance *PanDocExec) SetTemp(dir string) *PanDocExec {
	instance.dirController.SetTemp(dir)
	return instance
}

func (instance *PanDocExec) SetSubTemp(enabled bool) *PanDocExec {
	instance.dirController.SetSubTemp(enabled)
	return instance
}

func (instance *PanDocExec) Root() string {
	return instance.dirController.DirRoot()
}

func (instance *PanDocExec) Temp() string {
	return instance.dirController.DirTemp()
}

func (instance *PanDocExec) Work() string {
	return instance.dirController.DirWork()
}

func (instance *PanDocExec) GetPath(path string) (response string) {
	response = instance.dirController.GetPath(path)
	return
}

func (instance *PanDocExec) GetWorkPath(subPath string) (response string) {
	response = instance.dirController.GetWorkPath(subPath)
	return
}

func (instance *PanDocExec) GetTempPath(subPath string) (response string) {
	response = instance.dirController.GetTempPath(subPath)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	r u n n e r
//----------------------------------------------------------------------------------------------------------------------

func (instance *PanDocExec) ExecuteCommand(arguments ...string) (out string, err error) {
	program := instance.program()
	session, execErr := program.Run(arguments...)
	if nil != execErr {
		err = execErr
		return
	}

	stdErr := session.StdErr()
	if len(stdErr) > 0 {
		err = errors.New(stdErr)
	} else {
		out = strings.TrimSpace(session.StdOut())
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	c o m m a n d s
//----------------------------------------------------------------------------------------------------------------------

func (instance *PanDocExec) Version() (string, error) {
	exec, err := instance.program().Run("--version")
	if nil != err {
		return "", err
	}
	response := strings.ToLower(exec.StdOut())
	if strings.Index(response, ".") > -1 {
		version := strings.TrimSpace(strings.ReplaceAll(response, "v", ""))
		return version, nil
	}
	return "", errors.New(exec.StdOut())
}

func (instance *PanDocExec) IsInstalled() bool {
	version, err := instance.Version()
	if nil != err {
		return false
	}
	return len(version) > 0
}

func (instance *PanDocExec) Help() (string, error) {
	if instance.IsInstalled() {
		exec, err := instance.program().Run("--help")
		if nil != err && err.Error() != "exit status 1" {
			return "", err
		}
		if nil != exec {
			return exec.StdOut(), nil
		}
		return "", err
	}
	return "", executor.NotInstalledError
}

func (instance *PanDocExec) ConvertToFile(source, target string, standAlone bool) (response string, err error) {
	if nil != instance {
		if len(source) > 0 && len(target) > 0 {
			source = gg_.PathAbsolutize(source, instance.dirController.DirWork())
			target = gg_.PathAbsolutize(target, instance.dirController.DirWork())
			args := make([]string, 0)
			if standAlone {
				args = append(args, "-s")
			}
			args = append(args, "-o", target)
			args = append(args, source)

			response, err = instance.ExecuteCommand(args...)
		} else {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Missing required parameters: ")
		}
	}
	return
}

func (instance *PanDocExec) Convert(source, sourceFormat, target, targetFormat string, standAlone bool, options ...string) (response string, err error) {
	if nil != instance {
		if len(source) > 0 {
			source = gg_.PathAbsolutize(source, instance.dirController.DirWork())
			args := make([]string, 0)
			if standAlone {
				args = append(args, "-s")
			}
			if len(options) > 0 {
				args = append(args, options...)
			}

			// source
			if len(sourceFormat) > 0 {
				args = append(args, "-f", sourceFormat)
			}
			args = append(args, source)

			// target
			if len(targetFormat) > 0 {
				args = append(args, "-t", targetFormat)
			}
			if len(target) > 0 {
				target = gg_.PathAbsolutize(target, instance.dirController.DirWork())
				args = append(args, "-o", target)
			} else {
				if len(targetFormat) == 0 {
					err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Missing target and target format: ")
				}
			}

			response, err = instance.ExecuteCommand(args...)
		} else {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Missing required parameters: ")
		}
	}
	return
}

func (instance *PanDocExec) ConvertToFileMarkdown(source, target string, standAlone bool) (response string, err error) {
	if nil != instance {
		response, err = instance.Convert(source, "markdown", target, "", standAlone)
	}
	return
}

func (instance *PanDocExec) ConvertMarkdown(source, targetFormat string, standAlone bool) (response string, err error) {
	if nil != instance {
		response, err = instance.Convert(source, "markdown", "", targetFormat, standAlone)
	}
	return
}

// ConvertSlides1 uses DzSlides
func (instance *PanDocExec) ConvertSlides1(source string, standAlone, interactive bool) (response string, err error) {
	if nil != instance {
		options := make([]string, 0)
		if interactive {
			options = append(options, "-i")
		}
		response, err = instance.Convert(source, "", "",
			"dzslides", standAlone, options...)
	}
	return
}

// ConvertSlides2 uses DzSlides
func (instance *PanDocExec) ConvertSlides2(source string, standAlone, interactive bool) (response string, err error) {
	if nil != instance {
		options := make([]string, 0)
		if interactive {
			options = append(options, "-i")
		}
		response, err = instance.Convert(source, "", "",
			"slidy", standAlone, options...)
	}
	return
}

// ConvertSlides3 uses RevealJs
func (instance *PanDocExec) ConvertSlides3(source string, standAlone, interactive bool) (response string, err error) {
	if nil != instance {
		options := make([]string, 0)
		if interactive {
			options = append(options, "-i")
		}
		response, err = instance.Convert(source, "", "",
			"revealjs", standAlone, options...)
	}
	return
}

// ConvertToFileSlides1 uses DzSlides
func (instance *PanDocExec) ConvertToFileSlides1(source, target string, standAlone, interactive bool) (response string, err error) {
	if nil != instance {
		options := make([]string, 0)
		if interactive {
			options = append(options, "-i")
		}
		target = ensureTargetPath(source, target, "html")
		response, err = instance.Convert(source, "", target,
			"dzslides", standAlone, options...)
	}
	return
}

// ConvertToFileSlides2 uses Slidy
func (instance *PanDocExec) ConvertToFileSlides2(source, target string, standAlone, interactive bool) (response string, err error) {
	if nil != instance {
		options := make([]string, 0)
		if interactive {
			options = append(options, "-i")
		}
		target = ensureTargetPath(source, target, "html")
		response, err = instance.Convert(source, "", target,
			"slidy", standAlone, options...)
	}
	return
}

// ConvertToFileSlides3 uses RevealJs
func (instance *PanDocExec) ConvertToFileSlides3(source, target string, standAlone, interactive bool) (response string, err error) {
	if nil != instance {
		options := make([]string, 0)
		if interactive {
			options = append(options, "-i")
		}
		target = ensureTargetPath(source, target, "html")
		response, err = instance.Convert(source, "", target,
			"revealjs", standAlone, options...)
	}
	return
}

// ConvertToFileSlides4 uses Slideous
func (instance *PanDocExec) ConvertToFileSlides4(source, target string, standAlone, interactive bool) (response string, err error) {
	if nil != instance {
		options := make([]string, 0)
		if interactive {
			options = append(options, "-i")
		}
		target = ensureTargetPath(source, target, "html")
		response, err = instance.Convert(source, "", target,
			"slideous", standAlone, options...)
	}
	return
}

// ConvertToFileSlides5 uses S5
func (instance *PanDocExec) ConvertToFileSlides5(source, target string, standAlone, interactive bool) (response string, err error) {
	if nil != instance {
		options := make([]string, 0)
		if interactive {
			options = append(options, "-i")
		}
		target = ensureTargetPath(source, target, "html")
		response, err = instance.Convert(source, "", target,
			"s5", standAlone, options...)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *PanDocExec) program() *executor.ConsoleProgram {
	return instance.programCmd(instance.execPath)
}

func (instance *PanDocExec) programCmd(command string) *executor.ConsoleProgram {
	instance.init()
	dir := instance.dirController.DirRoot()

	return executor.NewConsoleProgramWithDir(command, dir)
}

func (instance *PanDocExec) init() {
	if nil != instance && !instance.initialized {
		instance.initialized = true

		// instance.dirController.Refresh()

	}
}

func ensureTargetPath(source string, target, targetFormat string) string {
	if len(target) == 0 {
		if len(targetFormat) > 0 {
			target = gg_.PathChangeFileNameExtension(source, "."+targetFormat)
		} else {
			target = gg_.PathChangeFileNameExtension(source, ".html")
		}
	} else {
		base := filepath.Base(target)
		dir := filepath.Dir(target)
		if base == dir || len(dir) == 0 || dir == "." {
			target = gg_.PathConcat(filepath.Dir(source), target)
		}
	}
	return target
}
