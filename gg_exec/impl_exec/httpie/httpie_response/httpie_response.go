package httpie_response

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"strings"
)

// HttpieResponse
// HTTP/1.1 200 OK
// Date: Tue, 10 Oct 2023 16:09:27 GMT
// Content-Type: application/json
// Transfer-Encoding: chunked
// Connection: keep-alive
// Access-Control-Allow-Origin: *
// Access-Control-Allow-Credentials: true
// CF-Cache-Status: DYNAMIC
// Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=yYkZWLBEGyiCzs1Mm5G%2BnlHiK7QSR%2FJnYhPspJljdZpYYntKgsRSC%2BkcXfOGgenokNCOmrBIWnvyA%2FBG%2F2ffBtd3N%2BKNBNDgoKP1M0jihOS05IfLYhfhBuDl"}],"group":"cf-nel","max_age":604800}
// NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
// Server: cloudflare
// CF-RAY: 814020bceefdbb06-MXP
// Content-Encoding: gzip
// alt-svc: h3=":443"; ma=86400
//
//	{
//	 "args": {},
//	 "data": "{\"name\": \"John\", \"surname\": \"foo\"}",
//	 "files": {},
//	 "form": {},
//	 "headers": {
//	   "Accept": "application/json, */*;q=0.5",
//	   "Accept-Encoding": "gzip",
//	   "Cdn-Loop": "cloudflare",
//	   "Cf-Connecting-Ip": "185.86.61.233",
//	   "Cf-Ipcountry": "SM",
//	   "Cf-Ray": "813e28201be8bab8-FRA",
//	   "Cf-Visitor": "{\"scheme\":\"http\"}",
//	   "Connection": "Keep-Alive",
//	   "Content-Length": "34",
//	   "Content-Type": "application/json",
//	   "Host": "pie.dev",
//	   "User-Agent": "HTTPie/3.2.2",
//	   "X-Api-Token": "123"
//	 },
//	 "json": {
//	   "name": "John",
//	   "surname": "foo"
//	 },
//	 "origin": "185.86.61.233",
//	 "url": "http://pie.dev/put"
//	}
type HttpieResponse struct {
	Original  string                 `json:"original"` // original parsed response
	Status    string                 `json:"status"`   // response status
	Body      interface{}            `json:"body"`
	ElapsedMs int                    `json:"elapsed_ms"`
	Headers   map[string]interface{} `json:"headers"`
	Meta      map[string]interface{} `json:"meta"`
}

func Parse(s, printOptions string) (response *HttpieResponse, err error) {
	response = new(HttpieResponse)
	response.Original = strings.TrimSpace(s)

	err = parse(response, printOptions)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *HttpieResponse) String() string {
	return gg_json.JSON.Stringify(instance)
}

// ---------------------------------------------------------------------------------------------------------------------
//	STATIC
// ---------------------------------------------------------------------------------------------------------------------

func parse(instance *HttpieResponse, printOptions string) (err error) {
	s := strings.ReplaceAll(instance.Original, "\r\n", "|||")
	s = strings.ReplaceAll(s, "\n\n", "||||||")
	s = strings.ReplaceAll(s, "||||||", "___")
	rows := strings.Split(s, "___")
	var headers, body, meta string
	if len(rows) > 0 {
		switch len(rows) {
		case 1:
			if strings.Index(printOptions, "h") > -1 {
				headers = rows[0] // h
			}
			if strings.Index(printOptions, "b") > -1 {
				body = rows[0] // b
			}
			if strings.Index(printOptions, "m") > -1 {
				meta = rows[0] // m
			}
		case 2:
			if strings.Index(printOptions, "h") > -1 {
				headers = rows[0] // h
			}
			if strings.Index(printOptions, "b") > -1 {
				body = rows[1] // b
			}
			if strings.Index(printOptions, "m") > -1 {
				meta = rows[1] // m
			}
		case 3:
			headers = rows[0] // h
			body = rows[1]    // b
			meta = rows[2]    // m
		}

		if len(headers) > 0 {
			err = parseHeaders(instance, headers)
			if nil != err {
				return
			}
		}
		if len(body) > 0 {
			err = parseBody(instance, body)
			if nil != err {
				return
			}
		}
		if len(meta) > 0 {
			err = parseMeta(instance, meta)
			if nil != err {
				return
			}
		}
	}
	return
}

func parseBody(instance *HttpieResponse, s string) (err error) {
	err = gg_json.JSON.Read(s, &instance.Body)
	return
}

func parseHeaders(instance *HttpieResponse, headers string) (err error) {
	instance.Headers = make(map[string]interface{})
	rows := strings.Split(headers, "|||")
	for _, row := range rows {
		if len(row) > 0 {
			keyValue := gg_utils.Strings.SplitFirst(row, ':')
			switch len(keyValue) {
			case 1:
				instance.Status = strings.TrimSpace(keyValue[0])
			case 2:
				instance.Headers[strings.TrimSpace(keyValue[0])] = strings.TrimSpace(keyValue[1])
			}
		}
	}
	return
}

func parseMeta(instance *HttpieResponse, s string) (err error) {
	instance.Meta = make(map[string]interface{})
	rows := strings.Split(s, "|||")
	for _, row := range rows {
		if len(row) > 0 {
			keyValue := gg_utils.Strings.SplitFirst(row, ':')
			switch len(keyValue) {
			case 2:
				instance.Meta[strings.TrimSpace(keyValue[0])] = strings.TrimSpace(keyValue[1])
			}
		}
	}
	return
}
