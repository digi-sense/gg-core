package httpie

import (
	"bitbucket.org/digi-sense/gg-core/gg_exec/executor"
	"bitbucket.org/digi-sense/gg-core/gg_exec/impl_exec/httpie/httpie_response"
	"bitbucket.org/digi-sense/gg-core/gg_exec/impl_exec/shell"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_stopwatch"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"errors"
	"fmt"
	"strings"
)

const (
	AuthBasic  = "basic"
	AuthDigest = "digest"
	AuthBearer = "bearer"
)

// HttpieExec httpie wrapper.
// https://httpie.io/docs/cli
type HttpieExec struct {
	uid         string
	execPath    string
	dirRoot     string // &userdir/.httpie
	dirWork     string // start dir - workspace
	dirSessions string // dirWork/sessions

	useWorkspace bool
	logger       gg_log.ILogger
	initialized  bool

	// session
	useSession  bool
	sessionFile string
	// auth
	useAuth  bool
	authType string // basic, digest, bearer
	authUser string
	authPsw  string
	// redirect
	useFollow bool
	followMax int
	followAll bool
	// verify SSL certificate
	useVerify bool
	// SSL certificate --cert
	cert        string // path to cert. "client.pem", "client.crt"
	certKey     string // path to private key "client.key"
	certKeyPass string // private key password

	timeoutSec     float32 // --timeout
	useCheckStatus bool    // --check-status
	maxRedirects   int     // --max-redirects
	printHeaders   bool    // --print=h
	printBody      bool
	printMeta      bool
}

func NewExec(execPath string, logger gg_log.ILogger) *HttpieExec {
	instance := new(HttpieExec)
	instance.execPath = execPath
	instance.logger = logger
	instance.uid = gg_rnd.Rnd.Uuid()

	instance.useVerify = true
	instance.useWorkspace = false // works directly into &userdir/.httpie
	instance.useSession = false   // use sessions for requests
	instance.useAuth = false
	instance.authType = AuthBasic

	instance.timeoutSec = 2.5
	instance.useCheckStatus = true
	instance.maxRedirects = -1
	instance.printHeaders = true
	instance.printBody = true
	instance.printMeta = true

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

// Close remove session file if any
func (instance *HttpieExec) Close() *HttpieExec {
	if nil != instance {
		if len(instance.sessionFile) > 0 {
			_ = gg_utils.IO.Remove(instance.sessionFile)
		}
	}
	return instance
}

func (instance *HttpieExec) SetLogger(logger gg_log.ILogger) *HttpieExec {
	if nil != instance {
		instance.logger = logger
	}
	return instance
}

func (instance *HttpieExec) SetUseWorkspace(value bool) *HttpieExec {
	if nil != instance {
		instance.useWorkspace = value

		if instance.initialized {
			instance.initialized = false
			instance.init()
		}
	}

	return instance
}

func (instance *HttpieExec) GetUseWorkspace() bool {
	return instance.useWorkspace
}

func (instance *HttpieExec) SetUseVerify(value bool) *HttpieExec {
	instance.useVerify = value
	return instance
}

func (instance *HttpieExec) GetUseVerify() bool {
	return instance.useVerify
}

func (instance *HttpieExec) GetUseVerifyAsYesNo() string {
	if instance.useVerify {
		return "yes"
	}
	return "no"
}

// SetUseSession enable/disable session usage.
// Sessions are stored into ./session folder.
// You can create just 1 session per Exec that will be shared between all endpoints.
// If you need more session you have to create different Exec instances calling method httpie.NewExec().
// All session data, including credentials, prompted passwords, cookie data,
// and custom headers are stored in plain text. That means session files can also
// be created and edited manually in a text editor—they are regular JSON. It also means
// that they can be read by anyone who has access to the session file.
// https://httpie.io/docs/cli/named-sessions
func (instance *HttpieExec) SetUseSession(value bool) *HttpieExec {
	instance.useSession = value
	return instance
}

func (instance *HttpieExec) GetUseSession() bool {
	return instance.useSession
}

func (instance *HttpieExec) GetSessionData() map[string]interface{} {
	data := make(map[string]interface{})
	if nil != instance && len(instance.sessionFile) > 0 {
		text, _ := gg_utils.IO.ReadTextFromFile(instance.sessionFile)
		if len(text) > 0 {
			_ = gg_json.JSON.Read(text, &data)
		}
	}
	return data
}

func (instance *HttpieExec) SetSessionData(value map[string]interface{}) *HttpieExec {
	if nil != instance {
		if len(instance.sessionFile) > 0 && len(value) > 0 {
			text := gg_json.JSON.Stringify(value)
			_, _ = gg_utils.IO.WriteTextToFile(text, instance.sessionFile)
		}
	}
	return instance
}

// SetUseAuth enable/disable authentication.
func (instance *HttpieExec) SetUseAuth(value bool) *HttpieExec {
	instance.useAuth = value
	return instance
}

func (instance *HttpieExec) GetUseAuth() bool {
	return instance.useAuth
}

// SetAuthType set auth type. Supported types: basic, digest, bearer
func (instance *HttpieExec) SetAuthType(value string) *HttpieExec {
	instance.authType = value
	return instance
}

func (instance *HttpieExec) GetAuthType() string {
	return instance.authType
}

func (instance *HttpieExec) SetAuthUser(value string) *HttpieExec {
	instance.authUser = value
	return instance
}

func (instance *HttpieExec) GetAuthUser() string {
	return instance.authUser
}

func (instance *HttpieExec) SetAuthPsw(value string) *HttpieExec {
	instance.authPsw = value
	return instance
}

func (instance *HttpieExec) GetAuthPsw() string {
	return instance.authPsw
}

// SetUseFollow enable/disable the follow of redirects
func (instance *HttpieExec) SetUseFollow(value bool) *HttpieExec {
	instance.useFollow = value
	return instance
}

func (instance *HttpieExec) GetUseFollow() bool {
	return instance.useFollow
}

// SetFollowAll enable/disable the follow of all redirects showing in response
func (instance *HttpieExec) SetFollowAll(value bool) *HttpieExec {
	instance.followAll = value
	return instance
}

func (instance *HttpieExec) GetFollowAll() bool {
	return instance.followAll
}

func (instance *HttpieExec) SetFollowMax(value int) *HttpieExec {
	instance.followMax = value
	return instance
}

func (instance *HttpieExec) GetFollowMax() int {
	return instance.followMax
}

// SetRoot set the working root
func (instance *HttpieExec) SetRoot(dir string) *HttpieExec {
	if nil != instance {
		instance.dirRoot = gg_utils.Paths.Absolute(dir)

		if instance.initialized {
			instance.initialized = false
			instance.init()
		}
	}
	return instance
}

func (instance *HttpieExec) GetRoot() string {
	return instance.dirRoot
}

func (instance *HttpieExec) SetWorkspace(dir string) *HttpieExec {
	if nil != instance {
		instance.dirWork = dir

		if instance.initialized {
			instance.initialized = false
			instance.init()
		}
	}
	return instance
}

func (instance *HttpieExec) GetWorkspace() string {
	return instance.dirWork
}

func (instance *HttpieExec) SetTimeoutSec(value float32) *HttpieExec {
	instance.timeoutSec = value
	return instance
}

func (instance *HttpieExec) GetTimeoutSec() float32 {
	return instance.timeoutSec
}

func (instance *HttpieExec) SetUseCheckStatus(value bool) *HttpieExec {
	instance.useCheckStatus = value
	return instance
}

func (instance *HttpieExec) GetUseCheckStatus() bool {
	return instance.useCheckStatus
}

func (instance *HttpieExec) SetMaxRedirects(value int) *HttpieExec {
	instance.maxRedirects = value
	return instance
}

func (instance *HttpieExec) GetMaxRedirects() int {
	return instance.maxRedirects
}

func (instance *HttpieExec) SetPrintHeaders(value bool) *HttpieExec {
	instance.printHeaders = value
	return instance
}

func (instance *HttpieExec) GetPrintHeaders() bool {
	return instance.printHeaders
}

func (instance *HttpieExec) SetPrintBody(value bool) *HttpieExec {
	instance.printBody = value
	return instance
}

func (instance *HttpieExec) GetPrintBody() bool {
	return instance.printBody
}

func (instance *HttpieExec) SetPrintMeta(value bool) *HttpieExec {
	instance.printMeta = value
	return instance
}

func (instance *HttpieExec) GetPrintMeta() bool {
	return instance.printMeta
}

func (instance *HttpieExec) PrintOptions() string {
	var sb strings.Builder
	if instance.printHeaders {
		sb.WriteString("h")
	}
	if instance.printBody {
		sb.WriteString("b")
	}
	if instance.printMeta {
		sb.WriteString("m")
	}
	if sb.Len() == 0 {
		sb.WriteString("hbm")
	}
	return sb.String()
}

func (instance *HttpieExec) GetPath(path string) (response string) {
	response = gg_utils.Paths.Absolutize(path, instance.dirRoot)
	_ = gg_utils.Paths.Mkdir(response)
	return
}

func (instance *HttpieExec) GetWorkPath(subPath string) (response string) {
	response = gg_utils.Paths.Absolutize(subPath, instance.dirWork)
	_ = gg_utils.Paths.Mkdir(response)
	return
}

func (instance *HttpieExec) LogFlush() {
	if l, ok := instance.logger.(*gg_log.Logger); ok {
		l.Flush()
	}
}

func (instance *HttpieExec) LogDisableRotation() {
	if l, ok := instance.logger.(*gg_log.Logger); ok {
		l.RotateEnable(false)
	}
}

func (instance *HttpieExec) IsInstalled() bool {
	version, err := instance.CmdVersion()
	if nil != err {
		return false
	}
	return len(version) > 0
}

// ---------------------------------------------------------------------------------------------------------------------
//	s e t u p
// ---------------------------------------------------------------------------------------------------------------------

func (instance *HttpieExec) CmdVersion() (response string, err error) {
	response, err = instance.exec("--version")
	if nil != err {
		return
	}

	return
}

func (instance *HttpieExec) CmdTryToInstall() (response string, err error) {
	if nil != instance {
		if instance.isPythonInstalled() {
			if !instance.IsInstalled() {
				var sb strings.Builder
				var execOut string
				// ensure pip
				_, err = shell.Run("python3 -m ensurepip --upgrade")
				if nil != err {
					return
				}
				// update installer
				execOut, err = shell.Run("python3 -m pip install --upgrade pip wheel")
				if nil != err {
					return
				}
				sb.WriteString("UPGRADE PIP WHEEL:\n")
				sb.WriteString(execOut + "\n")
				// update httpie
				execOut, err = shell.Run("python3 -m pip install httpie")
				if nil != err {
					return
				}
				sb.WriteString("INSTALL HTTPIE:\n")
				sb.WriteString(execOut + "\n")

				response = sb.String()
			} else {
				err = errors.New("HTTPie already installed!. Please, try to update.")
			}
		} else {
			err = errors.New("Python 3 not installed yet!")
		}
	}
	return
}

func (instance *HttpieExec) CmdTryToUpdate() (response string, err error) {
	if nil != instance {
		if instance.isPythonInstalled() {
			if instance.IsInstalled() {
				var sb strings.Builder
				var execOut string
				// ensure pip
				_, err = shell.Run("python3 -m ensurepip --upgrade")
				if nil != err {
					return
				}
				// update installer
				execOut, err = shell.Run("python3 -m pip install --upgrade pip wheel")
				if nil != err {
					return
				}
				sb.WriteString("UPGRADE PIP WHEEL:\n")
				sb.WriteString(execOut + "\n")
				// update httpie
				execOut, err = shell.Run("python3 -m pip install --upgrade httpie")
				if nil != err {
					return
				}
				sb.WriteString("UPGRADE HTTPIE:\n")
				sb.WriteString(execOut + "\n")

				response = sb.String()
			} else {
				err = errors.New("HTTPie not installed yet!. Please, visit https://httpie.io/docs/cli/installation")
			}
		} else {
			err = errors.New("Python 3 not installed yet!")
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o m m a n d s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *HttpieExec) CmdRunFile(filename string) (response *httpie_response.HttpieResponse, err error) {
	if nil != instance {
		var text string
		text, err = gg_utils.IO.ReadTextFromFile(filename)
		if nil != err {
			return
		}

		response, err = instance.CmdRunText(text)
	}
	return
}

func (instance *HttpieExec) CmdRunText(command string) (response *httpie_response.HttpieResponse, err error) {
	if nil != instance {
		tokens := strings.Split(command, " ")
		response, err = instance.CmdRun(tokens...)
	}
	return
}

func (instance *HttpieExec) CmdRun(command ...string) (response *httpie_response.HttpieResponse, err error) {
	if nil != instance {
		response, err = instance.run(true, command...)
	}
	return
}

func (instance *HttpieExec) CmdRunTextRaw(command string) (response *httpie_response.HttpieResponse, err error) {
	if nil != instance {
		tokens := strings.Split(command, " ")
		response, err = instance.CmdRunRaw(tokens...)
	}
	return
}

func (instance *HttpieExec) CmdRunRaw(command ...string) (response *httpie_response.HttpieResponse, err error) {
	if nil != instance {
		response, err = instance.run(false, command...)
	}
	return
}

func (instance *HttpieExec) CmdSaveRequest(filename string, command ...string) (response *httpie_response.HttpieResponse, err error) {
	if nil != instance {
		filename = gg_utils.Paths.Absolutize(filename, instance.dirWork)
		command = append(command, "--offline")
		response, err = instance.run(true, command...)
		if nil == err {
			_ = gg_utils.Paths.Mkdir(filename)
			_, err = gg_utils.IO.WriteTextToFile(response.String(), filename)
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *HttpieExec) init() {
	if nil != instance && !instance.initialized {
		instance.initialized = true

		if len(instance.dirRoot) == 0 {
			instance.dirRoot = gg_utils.Paths.UserHomePath(".httpie")
		}
		if instance.useWorkspace {
			if len(instance.dirWork) > 0 {
				instance.dirWork = gg_utils.Paths.Absolutize(instance.dirWork, instance.dirRoot)
			} else {
				instance.dirWork = gg_utils.Paths.Concat(instance.dirRoot, "_workspace")
			}
		} else {
			instance.dirWork = instance.dirRoot
		}

		instance.dirSessions = gg_utils.Paths.Concat(instance.dirWork, "sessions")

		// creates paths
		_ = gg_utils.Paths.Mkdir(instance.dirRoot + gg_utils.OS_PATH_SEPARATOR)
		_ = gg_utils.Paths.Mkdir(instance.dirWork + gg_utils.OS_PATH_SEPARATOR)
		_ = gg_utils.Paths.Mkdir(instance.dirSessions + gg_utils.OS_PATH_SEPARATOR)

		if instance.useSession {
			instance.sessionFile = gg_utils.Paths.Concat(instance.dirSessions, fmt.Sprintf("%s.json", instance.uid))
		}

		if nil == instance.logger {
			logPath := gg_utils.Paths.Concat(instance.dirWork, "logging.log")
			instance.logger = gg_log.Log.New("info", logPath)
			if l, ok := instance.logger.(*gg_log.Logger); ok {
				l.SetMessageFormat("* " + l.GetMessageFormat())
			}
		}
	}
}

func (instance *HttpieExec) exec(arguments ...string) (out string, err error) {
	program := instance.program()
	session, execErr := program.Run(arguments...)
	if nil != execErr {
		err = execErr
		return
	}
	defer session.Close()

	stdErr := session.StdErr()
	if len(stdErr) > 0 {
		err = errors.New(stdErr)
	} else {
		out = strings.TrimSpace(session.StdOut())
	}
	return
}

func (instance *HttpieExec) program(args ...string) *executor.ConsoleProgram {
	instance.init()
	return executor.NewConsoleProgramWithDir(instance.execPath, instance.dirWork, args...)
}

func (instance *HttpieExec) isPythonInstalled() bool {
	if nil != instance {
		python := executor.NewConsoleProgramWithDir("python", instance.dirWork)
		session, err := python.Run("--version")
		defer session.Close()
		if nil == err {
			if len(session.StdErr()) == 0 {
				out := strings.TrimSpace(strings.ReplaceAll(session.StdOut(), "Python ", ""))
				if len(out) > 0 {
					i := gg_utils.Convert.ToInt(fmt.Sprintf("%c", out[0]))
					return i > 2
				}
			}
		}
	}
	return false
}

func (instance *HttpieExec) purgeCommand(tokens ...string) (response []string, err error) {
	if len(tokens) > 0 {
		response = tokens
		// remove the command
		if strings.TrimSpace(response[0]) == httpieCommand {
			response = response[1:]
		}

		// check --verify
		verify := fmt.Sprintf("--verify=%v", instance.GetUseVerifyAsYesNo())
		if i := gg_utils.Arrays.IndexOf("--verify*", response); i == -1 {
			response = append(response, verify)
		} else {
			response[i] = verify
		}

		// check --cert
		// http --cert=client.pem --cert-key=client.key --cert-key-pass=my_password https://example.org
		if len(instance.cert) > 0 {
			cert := fmt.Sprintf("--cert=%s", gg_utils.Paths.Absolutize(instance.cert, instance.dirWork))
			if len(instance.certKey) > 0 {
				cert += fmt.Sprintf(" --cert-key=%s", gg_utils.Paths.Absolutize(instance.certKey, instance.dirWork))
			}
			if len(instance.certKeyPass) > 0 {
				cert += fmt.Sprintf(" --cert-key-pass=%s", instance.certKeyPass)
			}
			if i := gg_utils.Arrays.IndexOf("--cert*", response); i == -1 {
				response = append(response, cert)
			} else {
				response[i] = cert
			}
		}

		// check  --ignore-stdin
		if gg_utils.Arrays.IndexOf("--ignore-stdin", response) == -1 {
			response = append(response, "--ignore-stdin")
		}
		// check  --timeout
		if instance.timeoutSec > 0 && gg_utils.Arrays.IndexOf("--timeout*", response) == -1 {
			response = append(response, fmt.Sprintf("--timeout=%v", instance.timeoutSec))
		}
		// check  --check-status
		if instance.useCheckStatus && gg_utils.Arrays.IndexOf("--check-status", response) == -1 {
			response = append(response, "--check-status")
		}
		// check  --max-redirects
		if instance.maxRedirects > -1 && gg_utils.Arrays.IndexOf("--max-redirects*", response) == -1 {
			response = append(response, fmt.Sprintf("--max-redirects=%v", instance.maxRedirects))
		}
		// check --print
		if i := gg_utils.Arrays.IndexOf("--print*", response); i == -1 {
			p := instance.PrintOptions()
			response = append(response, fmt.Sprintf("--print=%s", p))
		} else {
			p := instance.PrintOptions()
			response[i] = fmt.Sprintf("--print=%s", p)
		}

		// check --auth -a, --auth-type -A
		if instance.useAuth {
			a := fmt.Sprintf("--auth-type %s", instance.authType)
			if i := gg_utils.Arrays.IndexOfOne([]interface{}{"--auth-type", "-A"}, response); i == -1 {

				response = append(response, a)
			} else {
				response[i] = a
			}
			auth := fmt.Sprintf("--auth %s:%s", instance.authUser, instance.authPsw)
			if instance.authType == AuthBearer {
				// https -A bearer -a token pie.dev/bearer
				token := instance.authUser
				if len(token) == 0 {
					token = instance.authPsw
				}
				auth = fmt.Sprintf("--auth %s", token)
			}
			if i := gg_utils.Arrays.IndexOfOne([]interface{}{"--auth", "-a"}, response); i == -1 {
				response = append(response, auth)
			} else {
				response[i] = auth
			}
		}

		// check --session
		if len(instance.sessionFile) > 0 && instance.useSession {
			session := fmt.Sprintf("--session=%s", instance.sessionFile)
			if i := gg_utils.Arrays.IndexOf("--session*", response); i == -1 {
				response = append(response, session)
			} else {
				response[i] = session
			}
		}

		// check --follow
		if instance.useFollow {
			follow := "--follow"
			if instance.followAll {
				follow += " --all"
			}
			if instance.followMax > 0 {
				follow = fmt.Sprintf("%s --max-redirects=%v", follow, instance.followMax)
			}
			if i := gg_utils.Arrays.IndexOfOne([]interface{}{"--follow", "-f"}, response); i == -1 {
				response = append(response, follow)
			} else {
				response[i] = follow
			}
		}

	} else {
		err = errors.New("CommandLine should be a valid string and not empty.")
	}
	return
}

func (instance *HttpieExec) errorCheck(text string) (err error) {
	text = strings.TrimSpace(text)
	if len(text) == 1 {
		errCode := gg_utils.Convert.ToInt(text)
		switch errCode {
		case 2:
			err = errors.New("Request timed out!")
		case 3:
			err = errors.New("Unexpected HTTP 3xx Redirection!")
		case 4:
			err = errors.New("HTTP 4xx Client Error!")
		case 5:
			err = errors.New("HTTP 5xx Server Error!")
		case 6:
			err = errors.New("Exceeded --max-redirects=<n> redirects!")
		default:
			err = errors.New("Generic Error")
		}
	}
	return
}

func (instance *HttpieExec) run(purge bool, command ...string) (response *httpie_response.HttpieResponse, err error) {
	if nil != instance {
		var tokens []string
		if purge {
			tokens, err = instance.purgeCommand(command...)
			if nil != err {
				return
			}
		} else {
			tokens = append(tokens, command...)
		}

		// start running
		watch := gg_stopwatch.New()
		watch.Start()
		var out string
		out, err = instance.exec(tokens...)
		if nil != err {
			return
		}
		watch.Stop()

		// check error
		err = instance.errorCheck(out)
		if nil != err {
			return
		}

		response, err = httpie_response.Parse(out, instance.PrintOptions())
		if nil == err {
			response.ElapsedMs = watch.Milliseconds()
		}
	}
	return
}
