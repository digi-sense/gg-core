# HTTPie 

![image](docs/httpie.png)

HTTPie is a Postman command line alternative.

For further information about HTTPie please navigate the [HTTPie website](https://httpie.io/).

This wrapper do not install HTTPie, to install the right version on your machine visit [https://httpie.io/docs/cli/installation](https://httpie.io/docs/cli/installation)
