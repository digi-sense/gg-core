package httpie

var (
	HTTPie        *HttpieHelper
	httpieCommand = "http"
)

type HttpieHelper struct {
	root    string
	rootTmp string
}

func init() {
	// httpieCommand = findExecPath()
	HTTPie = new(HttpieHelper)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

// NewExec
// Creates new HTTPie command
func (instance *HttpieHelper) NewExec() *HttpieExec {
	if nil != instance {
		return NewExec(httpieCommand, nil)
	}
	return nil
}
