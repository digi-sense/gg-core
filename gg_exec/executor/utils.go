package executor

import (
	"fmt"
	"os/exec"
)

func FindExecPath(name string) string {
	for _, path := range [...]string{
		// Unix-like
		name,
		fmt.Sprintf("/usr/bin/%s", name),
		fmt.Sprintf("/usr/local/bin/%s", name),

		// Windows
		fmt.Sprintf("%s.exe", name), // in case PATHEXT is misconfigured
		// `C:\Program Files (x86)\Microsoft VS Code\chrome.exe`,
		//filepath.Join(os.Getenv("USERPROFILE"), `AppData\Local\Programs\Microsoft VS Code\code.exe`),

		// Mac
		//"/Applications/Visual Studio Code.app/Contents/Resources/app/bin/code",
	} {
		found, err := exec.LookPath(path)
		if err == nil {
			return found
		}
	}
	// Fall back
	return name
}
