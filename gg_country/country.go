package gg_country

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	_ "embed"
	"golang.org/x/text/currency"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/number"
	"strings"
	"time"
)

//go:embed countries.json
var countries string

var countriesMap []map[string]string

var Country *CountryHelper

type CountryHelper struct {
	DefLang string
}

func init() {
	_ = gg_.Unmarshal(countries, &countriesMap)
	Country = new(CountryHelper)
	Country.DefLang = "en"
}

// ---------------------------------------------------------------------------------------------------------------------
//	l a n g
// ---------------------------------------------------------------------------------------------------------------------

// Lang extracts the ISO 639-1 language code (e.g., "en", "it") from the provided language or locale code string.
func (instance *CountryHelper) Lang(code string) string {
	return gg_.Lang(code) // ISO2
}

// LangName return ISO language name (i.e. "Italian") from a lang code like "it"
func (instance *CountryHelper) LangName(lang string) string {
	return gg_.LangName(lang)
}

func (instance *CountryHelper) CountryCode(langTag string) string {
	lang := instance.Lang(langTag)
	tag := language.Make(lang)
	region, _ := tag.Region()
	return region.String()
}

func (instance *CountryHelper) CountryName(langTag string) string {
	lang := instance.Lang(langTag)
	iso := strings.ToLower(instance.CountryCode(lang))
	if nil != countriesMap {
		for _, m := range countriesMap {
			if m["alpha2"] == iso {
				return m[lang]
			}
		}
	}
	tag := language.Make(lang)
	region, _ := tag.Region()
	return region.ISO3()
}

func (instance *CountryHelper) CountryNameOf(langTag, outTag string) string {
	lang := instance.Lang(langTag)
	iso := strings.ToLower(instance.CountryCode(lang))
	if nil != countriesMap {
		for _, m := range countriesMap {
			if m["alpha2"] == iso {
				return m[outTag]
			}
		}
	}
	tag := language.Make(lang)
	region, _ := tag.Region()
	return region.ISO3()
}

func (instance *CountryHelper) CurrencySymbol(langTag string) string {
	code := instance.Lang(langTag)
	lang := language.Make(code)
	cur, _ := currency.FromTag(lang)
	p := message.NewPrinter(lang)
	return p.Sprintf("%v", currency.Symbol(cur))
}

func (instance *CountryHelper) CurrencyName(langTag string) string {
	code := instance.Lang(langTag)
	lang := language.Make(code)
	cur, _ := currency.FromTag(lang)
	p := message.NewPrinter(lang)
	return p.Sprintf("%v", cur)
}

func (instance *CountryHelper) FormatCurrency(langTag string, value float64) string {
	code := instance.Lang(langTag)
	lang := language.Make(code)
	cur, _ := currency.FromTag(lang)
	scale, _ := currency.Cash.Rounding(cur) // fractional digits
	dec := number.Decimal(value, number.Scale(scale))
	p := message.NewPrinter(lang)
	return p.Sprintf("%v%v", currency.Symbol(cur), dec)
}

func (instance *CountryHelper) FormatDate(langTag string, date time.Time) string {
	code := instance.Lang(langTag)
	lang := language.Make(code)
	p := message.NewPrinter(lang)
	return p.Sprintf("%v", date.Format("Monday, 02 January 2006"))
}

func (instance *CountryHelper) FormatTime(langTag string, date time.Time) string {
	code := instance.Lang(langTag)
	lang := language.Make(code)
	p := message.NewPrinter(lang)
	return p.Sprintf("%v", date.Format("15:04:05"))
}

func (instance *CountryHelper) FormatTimeHM(langTag string, date time.Time) string {
	code := instance.Lang(langTag)
	lang := language.Make(code)
	p := message.NewPrinter(lang)
	return p.Sprintf("%v", date.Format("15:04"))
}
