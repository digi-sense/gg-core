# Country

Helper library for localization.

```
func TestCountry(t *testing.T) {
	code := "it"

	fmt.Println("Lang: ", code, gg.Country.Lang(code))
	fmt.Println("LangName: ", code, gg.Country.LangName(code))
	fmt.Println("CountryName: ", code, gg.Country.CountryName(code))
	fmt.Println("CurrencySymbol: ", code, gg.Country.CurrencySymbol(code))
	fmt.Println("CurrencyName: ", code, gg.Country.CurrencyName(code))
	fmt.Println("FormatDate: ", code, gg.Country.FormatDate(code, time.Now()))
	fmt.Println("FormatTime: ", code, gg.Country.FormatTimeHM(code, time.Now()))
	fmt.Println("FormatCurrency: ", code, gg.Country.FormatCurrency(code, 125.56888))

	code = "en"

	fmt.Println("Lang: ", code, gg.Country.Lang(code))
	fmt.Println("LangName: ", code, gg.Country.LangName(code))
	fmt.Println("CountryName: ", code, gg.Country.CountryName(code))
	fmt.Println("CurrencySymbol: ", code, gg.Country.CurrencySymbol(code))
	fmt.Println("CurrencyName: ", code, gg.Country.CurrencyName(code))
	fmt.Println("FormatDate: ", code, gg.Country.FormatDate(code, time.Now()))
	fmt.Println("FormatTime: ", code, gg.Country.FormatTimeHM(code, time.Now()))
	fmt.Println("FormatCurrency: ", code, gg.Country.FormatCurrency(code, 125.56888))

}

// OUTPUT:
=== RUN   TestCountry
Lang:  it it
LangName:  it Italiano
CountryName:  it Italia
CurrencySymbol:  it €
CurrencyName:  it EUR
FormatDate:  it Thursday, 28 November 2024
FormatTime:  it 09:24
FormatCurrency:  it €125,57
Lang:  en en
LangName:  en English
CountryName:  en USA
CurrencySymbol:  en $
CurrencyName:  en USD
FormatDate:  en Thursday, 28 November 2024
FormatTime:  en 09:24
FormatCurrency:  en $125.57
--- 
```