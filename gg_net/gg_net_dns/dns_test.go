package gg_net_dns

import (
	"fmt"
	"net"
	"testing"
)

const IP = "142.251.209.36" // "185.86.61.233" // https://myip.com

func TestDnsRaw(t *testing.T) {
	addrs, err := net.LookupAddr(IP)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("Addr: ", fmt.Sprintf("%v", addrs))

	for _, addr := range addrs {
		txt, err := net.LookupTXT(addr)
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
		fmt.Println("TXT: ", addr, fmt.Sprintf("%v", txt))
	}

}

func TestDns(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	fmt.Println("IP: ", IP, "VERSION: ", dns.GetIpVersion(IP))

	addrs, err := dns.LookupAddr(IP)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("Addr: ", fmt.Sprintf("%v", addrs))

	for _, addr := range addrs {

		cname, err := dns.LookupCNAME(addr)
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
		fmt.Println("CNAME: ", addr, fmt.Sprintf("%v", cname))

		addresses, err := dns.LookupHost(addr)
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
		fmt.Println("HOST: ", addr, fmt.Sprintf("%v", addresses))

		ips, err := dns.LookupIP(addr)
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
		fmt.Println("IPs: ", addr, fmt.Sprintf("%v", ips))

	}

}

func TestDnsLookupTXT(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	fmt.Println("IP: ", IP, "VERSION: ", dns.GetIpVersion(IP))

	txts, err := dns.LookupTXT("accyourate.com")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	for _, txt := range txts {
		fmt.Println(txt)
	}

}

func TestDnsLookupAddr(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	ip := "185.86.61.233" // "198.252.206.16"

	addrs, err := dns.LookupAddr(ip)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	for _, addr := range addrs {
		fmt.Println(ip, "->", addr)

		domain, _ := dns.GetDomainName(addr)
		fmt.Println(fmt.Sprintf("--- can send emails for '%s':", domain))

		txts, err := dns.LookupTXT(domain)
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
		for _, txt := range txts {
			fmt.Println("\t", txt)
		}
	}

}

func TestDnsLookupIP(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	ips, err := dns.LookupIP("telecomitaliasm.net")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	for _, ip := range ips {
		fmt.Println("\t", ip.String(), ip.Network())

		addrs, err := dns.LookupAddr(ip.String())
		if nil != err {
			fmt.Println("LookupAddr: ", ip.String(), err)
			continue
		}
		for _, addr := range addrs {
			fmt.Println(ip, "->", addr)

			fmt.Println(fmt.Sprintf("--- can send emails for '%s':", addr))
			txts, err := dns.LookupTXT(addr)
			if nil != err {
				t.Error(err)
				t.FailNow()
			}
			for _, txt := range txts {
				fmt.Println("\t", txt)
			}
		}
	}
}

func TestGetLocalAddress(t *testing.T) {
	dns := NewDnsHelper()

	local, err := dns.GetLocalAddress()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(local)
}

func TestGetPublicAddress(t *testing.T) {
	dns := NewDnsHelper()

	local, err := dns.GetPublicAddress()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(local)
}

func TestGetDomainName(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	ip := "198.252.206.16"
	host := "stackoverflow.com"

	domain, err := dns.GetDomainName(ip)
	if nil != err {
		t.Error(err)
		t.Fail()
	}
	fmt.Println(domain)

	domain, err = dns.GetDomainName(host)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(domain)

	canSendEmails, err := dns.CanSendEmails(domain)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(fmt.Sprintf("Domain '%s' is enabled to emails: %v", domain, canSendEmails))
}

func TestCanSendEmail(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	ip := "198.252.206.16"
	host := "telecomitaliasm.net" // "stackoverflow.com"

	canSendEmails, err := dns.CanSendEmails(host)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(fmt.Sprintf("Domain '%s' is enabled to emails: %v", host, canSendEmails))

	canSendEmails, err = dns.CanSendEmails(ip)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(fmt.Sprintf("Domain '%s' is enabled to emails: %v", ip, canSendEmails))
}

func TestDnsInfo(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	info, err := dns.GetInfo("accyourate.com")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(info)

}

func TestDnsInfo2(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	ip, err := dns.GetPublicAddress() // your ISP provider
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	info, err := dns.GetInfo(ip)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(info)

	domain := info.Domain
	info, err = dns.GetInfo(domain)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(info)
}

func TestDnsInfo3(t *testing.T) {
	dns := NewDnsHelper()
	dns.SetServerGoogle()

	info, err := dns.GetInfo("matteoflora.com")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(info)

}
