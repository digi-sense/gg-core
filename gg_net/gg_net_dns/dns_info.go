package gg_net_dns

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	ipisp "bitbucket.org/digi-sense/gg-core/gg_net/gg_net_dns/ipspn"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
)

type DnsIPInfo struct {
	IPAddress string `json:"ip-address"`
	IPVersion uint8  `json:"ip-version"`
}

func (instance *DnsIPInfo) String() string {
	return gg_json.JSON.Stringify(instance)
}

type DnsISPInfo struct {
	IP          string `json:"ip"`
	ASN         string `json:"asn"`
	ISPName     string `json:"name"`
	Country     string `json:"country"`
	Registry    string `json:"registry"`
	Range       string `json:"range"`
	AllocatedAt string `json:"time"`
}

func (instance *DnsISPInfo) String() string {
	return gg_json.JSON.Stringify(instance)
}

type DnsInfo struct {
	InitialRawAddress string        `json:"initial-raw-address"` // requested
	Domain            string        `json:"domain"`
	IPInfo            []*DnsIPInfo  `json:"ip-info"`
	ISPInfo           []*DnsISPInfo `json:"isp-info"`
	CanSendEmails     bool          `json:"can-send-email"`
	TXT               []string      `json:"txt"`
	ISPName           string        `json:"isp-name"`
	ISPCountry        string        `json:"isp-country"`
	Errors            []string      `json:"errors"`
}

func (instance *DnsInfo) String() string {
	return gg_json.JSON.Stringify(instance)
}

func (instance *DnsInfo) AddIP(ips ...string) *DnsInfo {
	if nil != instance {
		for _, ip := range ips {
			index := gg_utils.Arrays.IndexOfFunc(instance.IPInfo, func(item interface{}) bool {
				if i, ok := item.(*DnsIPInfo); ok {
					return i.IPAddress == ip
				}
				return false
			})
			if index == -1 {
				instance.IPInfo = append(instance.IPInfo, &DnsIPInfo{
					IPAddress: ip,
					IPVersion: GetIpVersion(ip),
				})
			}
		}
	}
	return instance
}

func (instance *DnsInfo) AddISP(isp *ipisp.Response) *DnsInfo {
	if nil != instance && nil != isp {
		item := new(DnsISPInfo)
		item.IP = isp.IP.String()
		item.Country = isp.Country
		item.ISPName = isp.ISPName
		item.Registry = isp.Registry
		item.AllocatedAt = isp.AllocatedAt.String()
		item.Range = isp.Range.String()
		item.ASN = isp.ASN.String()
		instance.ISPInfo = append(instance.ISPInfo, item)
		if len(instance.ISPName) == 0 && len(item.ISPName) > 0 {
			instance.ISPName = item.ISPName
			instance.ISPCountry = item.Country
		}
	}
	return instance
}

func (instance *DnsInfo) AddError(context string, err error) *DnsInfo {
	if nil != instance {
		instance.Errors = append(instance.Errors, fmt.Sprintf("Error in context '%s': %v", context, err))
	}
	return instance
}
