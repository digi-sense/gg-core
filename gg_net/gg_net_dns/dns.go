package gg_net_dns

import (
	ipisp "bitbucket.org/digi-sense/gg-core/gg_net/gg_net_dns/ipspn"
	"context"
	"io"
	"net"
	"net/http"
	"time"
)

// CNAME: is a Canonical Name Record or Alias Record.
// A type of resource record in the Domain Name System (DNS), that specifies that one domain name
// is an alias of another canonical domain name.
//
//A and AAAA Record: An A and AAAA record are actually primary DNS records.
// They associate a domain name with a specific IP address.
//
// TXT Record : A TXT record (short for text record) is a type of resource record
// in the Domain Name System (DNS) used to provide the ability to associate arbitrary text
// with a host or other name, such as human readable information about a server, network, data center,
// or other accounting information.
//
// MX(Mail Exchanger) Record: A mail exchanger record (MX record) specifies the mail server responsible
// for accepting email messages on behalf of a domain name.
// It is a resource record in the Domain Name System (DNS).
// It is possible to configure several MX records, typically pointing to an array of
// mail servers for load balancing and redundancy.
//
// NameServer: Nameserver is a server on the internet specialized in handling queries regarding
// the location of a domain name's various services.
// Nameservers are a fundamental part of the Domain Name System (DNS).
// They allow using domains instead of IP addresses.

type DnsHelper struct {
	server string

	_r *net.Resolver // https://stackoverflow.com/questions/59889882/specifying-dns-server-for-lookup-in-go
}

func NewDnsHelper() (instance *DnsHelper) {
	instance = new(DnsHelper)
	instance.server = "localhost:53" // 8.8.8.8:53

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DnsHelper) SetServer(value string) *DnsHelper {
	if nil != instance {
		instance.server = value
		instance._r = nil
	}
	return instance
}

func (instance *DnsHelper) SetServerGoogle() *DnsHelper {
	if nil != instance {
		instance.SetServer("8.8.8.8:53")
	}
	return instance
}

func (instance *DnsHelper) GetIpVersion(ip string) (version uint8) {
	if nil != instance {
		netIP := net.ParseIP(ip)
		if nil != netIP.To4() {
			version = 4
		} else {
			version = 6
		}
	}
	return
}

func (instance *DnsHelper) IsIPAddress(value string) (success bool) {
	ip := net.ParseIP(value)
	success = nil != ip
	return
}

// GetLocalAddress return local IP
func (instance *DnsHelper) GetLocalAddress() (address string, err error) {
	var conn net.Conn
	conn, err = net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return

	}

	defer conn.Close()

	if ipAddress, ok := conn.LocalAddr().(*net.UDPAddr); ok {
		address = ipAddress.String()
	}

	return
}

// GetPublicAddress return IP of current ISP provider using IPIFY.org
func (instance *DnsHelper) GetPublicAddress() (address string, err error) {
	url := "https://api.ipify.org?format=text"
	var resp *http.Response
	resp, err = http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	var ip []byte
	ip, err = io.ReadAll(resp.Body)
	if err != nil {
		return
	}
	address = string(ip)
	return
}

func (instance *DnsHelper) GetDomainName(addr string) (response string, err error) {
	if nil != instance {
		if instance.IsIPAddress(addr) {
			var names []string
			names, err = instance.LookupAddr(addr)
			if nil != err {
				return
			}
			for _, n := range names {
				n = GetDomain(n)
				if len(n) > 0 {
					response = n
					return
				}
			}
		} else {
			response = GetDomain(addr)
		}
	}
	return
}

func (instance *DnsHelper) CanSendEmails(addr string) (response bool, err error) {
	if nil != instance {
		var domainName string
		domainName, err = instance.GetDomainName(addr)
		if nil == err {
			var txts []string
			txts, err = instance.LookupTXT(domainName)
			if nil == err {
				response = len(txts) > 0
			}
		}
	}
	return
}

func (instance *DnsHelper) GetInfo(addr string) (response *DnsInfo, err error) {
	if nil != instance {
		response = new(DnsInfo)
		response.IPInfo = make([]*DnsIPInfo, 0)
		response.InitialRawAddress = addr

		// DOMAIN
		if instance.IsIPAddress(addr) {
			response.Domain, err = instance.GetDomainName(addr)
			if nil != err {
				response.AddError("GetDomainName()", err)
			}
		} else {
			response.Domain = GetDomain(addr)
		}

		// IPs
		var ips []string
		ips, err = instance.LookupHost(response.Domain)
		if nil != err {
			response.AddError("LookupHost()", err)
			if instance.IsIPAddress(response.InitialRawAddress) {
				response.AddIP(response.InitialRawAddress)
			}
		} else {
			response.AddIP(ips...)
		}

		// MX
		var txts []string
		txts, err = instance.LookupTXT(response.Domain)
		if nil != err {
			response.AddError("LookupTXT()", err)
		} else {
			if len(txts) > 0 {
				response.CanSendEmails = true
				response.TXT = make([]string, 0)
				response.TXT = append(response.TXT, txts...)
			}
		}

		// ISP
		for _, ipi := range response.IPInfo {
			ip := ipi.IPAddress
			var r *ipisp.Response
			r, err = ipisp.LookupIP(context.Background(), net.ParseIP(ip))
			if nil != err {
				response.AddError("ipisp.LookupIP()", err)
			} else {
				response.AddISP(r)
			}
		}

	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	lookup
// ---------------------------------------------------------------------------------------------------------------------

// LookupAddr does a reverse lookup for the address and returns a list of names that map to the address.
// The LookupAddr() function performs a reverse finding for the address and returns a list
// of names that map to the given address.
// USage: LookupAddr("6.8.8.8")
func (instance *DnsHelper) LookupAddr(ip string) (response []string, err error) {
	if nil != instance {
		resolver := instance.resolver()
		response, err = resolver.LookupAddr(instance.context(), ip)
	}
	return
}

// LookupCNAME returns the final name after following zero or more CNAME records.
// CNAME is the abbreviation for canonical name.
// CNAMEs are essentially domain and subdomain text aliases to bind traffic.
// The  LookupCNAME() function accepts a host-name(m.facebook.com) as a string and returns a single canonical domain name for the given host.
// Usage: LookupCNAME("m.facebook.com")
func (instance *DnsHelper) LookupCNAME(host string) (response string, err error) {
	if nil != instance {
		resolver := instance.resolver()
		response, err = resolver.LookupCNAME(instance.context(), host)
	}
	return
}

// LookupHost returns the host addresses using the local resolver.
// Usage: LookupHost("localhost")
func (instance *DnsHelper) LookupHost(host string) (addrs []string, err error) {
	if nil != instance {
		resolver := instance.resolver()
		addrs, err = resolver.LookupHost(instance.context(), host)
	}
	return
}

// LookupIP returns a hosts IP addresses. It is now preferred over the Hosts method to resolve a name to IP.
// The LookupIP() function accepts a string(domain-name) and returns a slice of net.IP objects
// that contains host's IPv4 and IPv6 addresses.
// Usage: LookupIP("google.com)
func (instance *DnsHelper) LookupIP(host string) (response []net.IPAddr, err error) {
	if nil != instance {
		resolver := instance.resolver()
		response, err = resolver.LookupIPAddr(instance.context(), host)
	}
	return
}

// LookupMX returns the DNS MX records for the domain name, sorted by preference.
// These records identify the servers that can exchange emails.
// The LookupMX() function takes a domain name as a string and returns a slice of MX structs sorted by preference.
// An MX struct is made up of a Host as a string and Pref as a uint16.
// Usage: LookupMX("google.com")
func (instance *DnsHelper) LookupMX(name string) (response []*net.MX, err error) {
	if nil != instance {
		resolver := instance.resolver()
		response, err = resolver.LookupMX(instance.context(), name)
	}
	return
}

// LookupNS returns DNS NS records for the domain name.
// The NS records describe the authorized name servers for the zone.
// The NS also delegates subdomains to other organizations on zone files.
// The  LookupNS() function takes a domain name(facebook.com) as a string and returns
// DNS-NS records as a slice of NS structs.
// Usage: LookupNS("gmail.com")
func (instance *DnsHelper) LookupNS(name string) (response []*net.NS, err error) {
	if nil != instance {
		resolver := instance.resolver()
		response, err = resolver.LookupNS(instance.context(), name)
	}
	return
}

// LookupSRVUdp attempts to resolve an SRV query of the service, protocol (udp), and domain name,
// sorted by priority and randomized by weight within a priority.
// Usage: LookupSRVUdp("xmpp-server", "google.com")
func (instance *DnsHelper) LookupSRVUdp(service, name string) (cname string, response []*net.SRV, err error) {
	if nil != instance {
		resolver := instance.resolver()
		cname, response, err = resolver.LookupSRV(instance.context(), service, "udp", name)
	}
	return
}

// LookupSRVTcp attempts to resolve an SRV query of the service, protocol (tcp), and domain name,
// sorted by priority and randomized by weight within a priority.
// Usage: LookupSRVTcp("xmpp-server", "google.com")
func (instance *DnsHelper) LookupSRVTcp(service, name string) (cname string, response []*net.SRV, err error) {
	if nil != instance {
		resolver := instance.resolver()
		cname, response, err = resolver.LookupSRV(instance.context(), service, "tcp", name)
	}
	return
}

// LookupTXT returns DNS TXT records for the domain name.
// This text record stores information about the SPF that can identify the authorized server
// to send email on behalf of your organization.
// The LookupTXT() function takes a domain name(facebook.com) as a string and returns a list of
// DNS TXT records as a slice of strings.
func (instance *DnsHelper) LookupTXT(name string) (response []string, err error) {
	if nil != instance {
		resolver := instance.resolver()
		response, err = resolver.LookupTXT(instance.context(), name)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DnsHelper) resolver() (resolver *net.Resolver) {
	if nil != instance {
		if nil == instance._r {
			instance._r = &net.Resolver{
				PreferGo: true,
				Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
					d := net.Dialer{
						Timeout: time.Millisecond * time.Duration(10000),
					}
					conn, e := d.DialContext(ctx, network, instance.server)
					return conn, e
				},
			}
		}
		resolver = instance._r
		instance._r = nil
	}
	return
}

func (instance *DnsHelper) context() (ctx context.Context) {
	if nil != instance {

	}
	if nil == ctx {
		ctx = context.Background()
	}
	return
}
