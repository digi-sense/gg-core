package gg_net_dns

import (
	"net"
	"strings"
)

func GetIpVersion(ip string) (version uint8) {
	netIP := net.ParseIP(ip)
	if nil != netIP.To4() {
		version = 4
	} else {
		version = 6
	}
	return
}

func GetDomain(name string) string {
	hostName := strings.Trim(name, " .")
	hostParts := strings.Split(hostName, ".")

	lengthOfHostParts := len(hostParts)

	if lengthOfHostParts == 3 {
		name = strings.Join([]string{hostParts[1], hostParts[2]}, ".")
	} else if lengthOfHostParts == 4 {
		name = strings.Join([]string{hostParts[2], hostParts[3]}, ".")
	} else if lengthOfHostParts == 5 {
		name = strings.Join([]string{hostParts[3], hostParts[4]}, ".")
	} else {
		name = hostName
	}

	return name
}
