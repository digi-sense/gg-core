package gg_net

import (
	"bitbucket.org/digi-sense/gg-core/gg_net/gg_net_dns"
	"bitbucket.org/digi-sense/gg-core/gg_net/gg_net_http"
	"bitbucket.org/digi-sense/gg-core/gg_net/gg_net_scan"
)

type NetHelper struct {
}

var Net *NetHelper

func init() {
	Net = new(NetHelper)
}

func (*NetHelper) NewHttpClient() *gg_net_http.HttpClient {
	return gg_net_http.NewHttpClient()
}

func (*NetHelper) NewDnsHelper() *gg_net_dns.DnsHelper {
	return gg_net_dns.NewDnsHelper()
}

func (*NetHelper) NetScan() *gg_net_scan.NetScanHelper {
	return gg_net_scan.NetScan
}
