package gg_net_scan

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	_ "embed"
)

//go:embed netscan_descriptor.json
var dataJSON string

//----------------------------------------------------------------------------------------------------------------------
//	NetScanDescriptorItem
//----------------------------------------------------------------------------------------------------------------------

type NetScanDescriptorItem struct {
	Port        []int    `json:"port"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Protocol    []string `json:"protocol"`
}

func (instance *NetScanDescriptorItem) String() string {
	return gg_json.JSON.Stringify(instance)
}

//----------------------------------------------------------------------------------------------------------------------
//	NetScanDescriptor
//----------------------------------------------------------------------------------------------------------------------

type NetScanDescriptor struct {
	data []*NetScanDescriptorItem
}

func NewNetScanDescriptor() (instance *NetScanDescriptor) {
	instance = &NetScanDescriptor{}
	instance.data = make([]*NetScanDescriptorItem, 0)
	_ = gg_json.JSON.Read(dataJSON, &instance.data)
	return
}

func (instance *NetScanDescriptor) Describe(port int) *NetScanDescriptorItem {
	for _, item := range instance.data {
		ports := item.Port
		for _, p := range ports {
			if p == port {
				return item
			}
		}
	}
	return nil
}
