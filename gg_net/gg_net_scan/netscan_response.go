package gg_net_scan

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"fmt"
)

//----------------------------------------------------------------------------------------------------------------------
//	NetScanResponse
//----------------------------------------------------------------------------------------------------------------------

type NetScanResponse struct {
	Uid      string `json:"uid"`
	Protocol string `json:"protocol"`
	Port     int    `json:"port"`
	IP       string `json:"ip"`
}

func NewNetScanResponse(protocol string, port int, ip string) *NetScanResponse {
	return &NetScanResponse{
		Uid:      uid(protocol, port, ip),
		Protocol: protocol,
		Port:     port,
		IP:       ip,
	}
}

func (instance *NetScanResponse) String() string {
	return gg_json.JSON.Stringify(instance)
}

func (instance *NetScanResponse) Describe() (response *NetScanDescriptorItem) {
	if nil != instance {
		descriptor := NewNetScanDescriptor()
		return descriptor.Describe(instance.Port)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	NetScanResponses
//----------------------------------------------------------------------------------------------------------------------

type NetScanResponses struct {
	Values []*NetScanResponse `json:"values"`
}

func NewNetScanResponses() *NetScanResponses {
	instance := new(NetScanResponses)
	instance.Values = make([]*NetScanResponse, 0)
	return instance
}

func (instance *NetScanResponses) String() string {
	return gg_json.JSON.Stringify(instance)
}

func (instance *NetScanResponses) Get(protocol string, port int, ip string) (response *NetScanResponse) {
	return instance.get(uid(protocol, port, ip))
}

func (instance *NetScanResponses) Append(item *NetScanResponse) *NetScanResponses {
	if nil == instance.get(item.Uid) {
		instance.Values = append(instance.Values, item)
	}
	return instance
}

func (instance *NetScanResponses) Add(protocol string, port int, ip string) *NetScanResponses {
	return instance.Append(NewNetScanResponse(protocol, port, ip))
}

func (instance *NetScanResponses) Filter(protocol string, port int) (response []*NetScanResponse) {
	response = make([]*NetScanResponse, 0)
	for _, item := range instance.Values {
		if (len(protocol) == 0 || item.Protocol == protocol) && (port == 0 || item.Port == port) {
			response = append(response, item)
		}
	}
	return response
}

func (instance *NetScanResponses) get(uuid string) (response *NetScanResponse) {
	for _, item := range instance.Values {
		if item.Uid == uuid {
			response = item
			return
		}
	}
	return response
}

func uid(protocol string, port int, ip string) string {
	return fmt.Sprintf("%s@%s:%v", protocol, ip, port)
}
