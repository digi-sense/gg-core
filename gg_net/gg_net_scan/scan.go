package gg_net_scan

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"runtime"
	"strings"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//	NetworkScanner
// ---------------------------------------------------------------------------------------------------------------------

// NetworkScanner is a struct that defines the configuration for scanning a network.
// Fields include network type, port number, number of threads, timeout duration, and an option to avoid scanning its own IP.
type NetworkScanner struct {
	network     string
	port        int
	threads     int
	timeout     time.Duration
	avoidThisIP bool // avoid scan IP of scanner machine
}

// NewNetworkScanner creates a new instance of NetworkScanner with default settings:
// network set to "tcp4", port set to 80, 20 threads, 5-second timeout, and avoidThisIP set to false.
func NewNetworkScanner() (instance *NetworkScanner) {
	instance = new(NetworkScanner)
	instance.network = "tcp4"
	instance.port = 80
	instance.threads = runtime.NumCPU() * 2
	instance.timeout = time.Second * 5
	instance.avoidThisIP = false

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r o p e r t i e s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *NetworkScanner) SetNetwork(network string) *NetworkScanner {
	instance.network = network
	return instance
}

func (instance *NetworkScanner) SetPort(port int) *NetworkScanner {
	instance.port = port
	return instance
}

func (instance *NetworkScanner) SetThreads(threads int) *NetworkScanner {
	instance.threads = threads
	return instance
}

func (instance *NetworkScanner) SetTimeout(timeout time.Duration) *NetworkScanner {
	instance.timeout = timeout
	return instance
}

func (instance *NetworkScanner) SetAvoidThisIP(b bool) *NetworkScanner {
	instance.avoidThisIP = b
	return instance
}

func (instance *NetworkScanner) GetNetwork() string {
	return instance.network
}

func (instance *NetworkScanner) GetPort() int {
	return instance.port
}

func (instance *NetworkScanner) GetThreads() int {
	return instance.threads
}

func (instance *NetworkScanner) GetTimeout() time.Duration {
	return instance.timeout
}

func (instance *NetworkScanner) GetAvoidThisIP() bool {
	return instance.avoidThisIP
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

// ScanHTTP scans the network for active HTTP servers using the configured network settings and returns a list of responses.
// The parameter onlyPrimaryLocal indicates whether to scan only the primary local subnet (e.g., "192.168.1.xxx").
// Returns a slice of response strings and an error, if any occurs during scanning.
func (instance *NetworkScanner) ScanHTTP(onlyPrimaryLocal bool) (response []string, err error) {
	if nil != instance {
		instance.SetNetwork("http")
		instance.SetTimeout(800 * time.Millisecond)
		instance.SetThreads(runtime.NumCPU() * 5)

		response, err = instance.scanIPs(onlyPrimaryLocal)
	}
	return
}

// ScanHTTPs scans the network for active HTTPS servers using the configured network settings and returns a list of responses.
// The parameter onlyPrimaryLocal indicates whether to scan only the primary local subnet (e.g., "192.168.1.xxx").
// Returns a slice of response strings and an error, if any occurs during scanning.
func (instance *NetworkScanner) ScanHTTPs(onlyPrimaryLocal bool) (response []string, err error) {
	if nil != instance {
		instance.SetNetwork("https")
		instance.SetTimeout(800 * time.Millisecond)
		instance.SetThreads(runtime.NumCPU() * 5)

		response, err = instance.scanIPs(onlyPrimaryLocal)
	}
	return
}

// ScanIPs scans the network for active IP addresses using the specified network, port, timeout, and thread settings.
// It returns a list of responsive IP addresses, or an error if the configuration is invalid or the scan fails.
func (instance *NetworkScanner) ScanIPs() (response []string, err error) {
	if nil != instance {
		response, err = instance.scanIPs(false)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *NetworkScanner) scanIPs(onlyPrimaryLocal bool) (response []string, err error) {
	if nil != instance {
		network := instance.network
		port := instance.port
		timeout := instance.timeout
		threads := instance.threads

		isHTTP := strings.HasPrefix(network, "http")

		// Validate settings
		if !isHTTP && !ValidateNetwork(network) {
			err = Errorf("Invalid network %s (Valid options: %v)", network, validNetworks)
			return
		}
		if port < 0 || port > 65535 {
			err = Errorf("Invalid port %d (Valid options: 0 - 65535)", port)
			return
		}

		hosts := make(chan string, 100)
		results := make(chan string, 10)
		done := make(chan bool, threads)

		// Start workers
		for worker := 0; worker < threads; worker++ {
			if isHTTP {
				go testHttp(hosts, port, network, timeout, results, done)
			} else {
				go testHosts(hosts, port, network, results, done)
			}
		}

		locals := make([]string, 0)
		// NET
		if !isHTTP {
			// list of hosts to test
			if onlyPrimaryLocal {
				locals = append(locals, LinkLocalAddress(network))
			} else {
				locals = append(locals, LinkLocalAddresses(network)...)
			}
			for _, current := range locals {
				allIPs := CalculateSubnetIPs(current, maxAddressesPerSubnet)

				startIndex := 0
				if instance.avoidThisIP {
					startIndex = IndexOf(current, allIPs)
				}
				for i := startIndex + 1; i < len(allIPs); i++ {
					hosts <- allIPs[i] // add all following hosts to channel
					if (startIndex - i) >= 0 {
						hosts <- allIPs[startIndex-i] // add all previous hosts to channel
					}
				}
			}
		} else {
			// HTTP
			// list of hosts to test
			if onlyPrimaryLocal {
				locals = append(locals, LinkLocalAddress("tcp4"))
			} else {
				locals = append(locals, LinkLocalAddresses("tcp4")...)
			}
			for _, current := range locals {
				allIPs := CalculateSubnetIPs(current, maxAddressesPerSubnet)

				startIndex := 0
				if instance.avoidThisIP {
					startIndex = IndexOf(current, allIPs)
				}
				for i := startIndex + 1; i < len(allIPs); i++ {
					hosts <- allIPs[i] // add all following hosts to channel
					if (startIndex - i) >= 0 {
						hosts <- allIPs[startIndex-i] // add all previous hosts to channel
					}
				}
			}
		}
		close(hosts)

		// collect responses
		for {
			select {
			case found := <-results:
				response = append(response, found)
			case <-done:
				threads--
				if threads == 0 {
					return response, nil
				}
			case <-time.After(timeout):
				return response, nil
			}
		}

	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

// testHosts tests a range of given hosts for connectivity on a specified port and protocol,
// adjusting timeout dynamically.
// Parameters:
// - hosts: A channel containing host IP addresses to be tested.
// - port: The port number to test connectivity on.
// - protocol: The protocol (e.g., "tcp", "udp") to use for the connection test.
// - respondingHosts: A channel to send back the IP addresses of hosts that responded.
// - done: A channel to signal when the function has finished processing.
func testHosts(hosts <-chan string, port int, protocol string, respondingHosts chan<- string, done chan<- bool) {
	adjustedTimeout := defaultTimeout
	for host := range hosts {
		start := time.Now()
		con, err := net.DialTimeout(protocol, fmt.Sprintf("%s:%d", host, port), adjustedTimeout)
		duration := time.Now().Sub(start)
		if err == nil {
			// WE HAVE A SERVICE
			// log.Println("found:", protocol, host, port)
			_ = con.Close()
			respondingHosts <- host
		}
		// Adjust timeout to current network speed
		if duration < adjustedTimeout {
			difference := adjustedTimeout - duration
			adjustedTimeout = adjustedTimeout - (difference / 2)
		}
	}
	done <- true
}

func testHttp(hosts <-chan string, port int, protocol string, timeout time.Duration, respondingHosts chan<- string, done chan<- bool) {
	for host := range hosts {
		requestURL := fmt.Sprintf("%s://%s:%d", protocol, host, port)
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		req, err := http.NewRequestWithContext(ctx, "GET", requestURL, nil)
		if err == nil {
			// Perform the HTTP request
			client := &http.Client{}
			start := time.Now()
			resp, respErr := client.Do(req)
			duration := time.Now().Sub(start)
			if nil == respErr {
				if duration < timeout {
					difference := timeout - duration
					timeout = timeout - (difference / 2)
				}
				_ = resp.Body.Close()
				respondingHosts <- host
			}

		}
		cancel()
	}
	done <- true
}
