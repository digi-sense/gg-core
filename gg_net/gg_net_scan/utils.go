package gg_net_scan

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"net"
	"strings"
	"time"
)

const maxAddressesPerSubnet = 1000
const defaultTimeout = 50 * time.Millisecond

var linkLocalCIDR = []string{
	"10.0.0.0/8",     // RFC ???
	"172.16.0.0/12",  // RFC ???
	"192.168.0.0/16", // RFC ???
	"169.254.0.0/16", // RFC ???
	"fc00::/7",       // RFC ???
	"fe80::/64"}      // RFC ???

var validNetworks = []string{"tcp", "tcp4", "tcp6", "udp", "udp4", "udp6", "ip", "ip4", "ip6", "unix", "unixgram", "unixpacket"}

// Errorf creates an error with the specified formatted message.
func Errorf(format string, args ...interface{}) error {
	return fmt.Errorf(format, args...)
}

func GetAllIPs(network string) (response []string) {
	response = make([]string, 0)
	nets := LinkLocalAddresses(network)
	for _, n := range nets {
		ips := CalculateSubnetIPs(n, maxAddressesPerSubnet)
		for _, ip := range ips {
			if !gg_utils.Arrays.Contains(ip, response) {
				response = append(response, ip)
			}
		}
	}
	return
}

func LinkLocalAddress(network string) string {
	all := LinkLocalAddresses(network)
	if len(all) > 0 {
		return all[0]
	}
	return ""
}

// LinkLocalAddresses retrieves a list of link-local IP addresses for the specified network type.
func LinkLocalAddresses(network string) []string {
	var addrs = []string{}
	interfaces, err := net.Interfaces()
	if err != nil {
		return addrs
	}

	for _, i := range interfaces {
		addresses, _ := i.Addrs()
		for _, address := range addresses {
			ip, _, err := net.ParseCIDR(address.String())
			if err != nil {
				continue
			}

			// should scan only v4 Hosts and current IP is v6
			if strings.Contains(network, "4") && ip.To4() == nil {
				continue
			}

			// should scan only v6 Hosts and current IP is v4
			if strings.Contains(network, "6") && ip.To16() == nil {
				continue
			}

			if IsLinkLocalAddress(ip) {
				addrs = append(addrs, address.String())
			}
		}
	}

	return addrs
}

// CalculateSubnetIPs calculates all valid IP addresses in a given subnet defined by the CIDR notation, excluding network and broadcast addresses.
// Parameters:
// - cidr: A string representing the CIDR notation of the subnet.
// - maxAddresses: The maximum number of IP addresses to calculate.
// Returns a slice of strings, each representing an IP address in the subnet.
func CalculateSubnetIPs(cidr string, maxAddresses int) (response []string) {
	response = make([]string, 0)

	ip, ipNet, err := net.ParseCIDR(cidr)
	if err != nil {
		return response
	}

	for ipAddress := ip.Mask(ipNet.Mask); ipNet.Contains(ipAddress); IncIP(ipAddress) {
		response = append(response, ipAddress.String())
		if len(response) > maxAddresses {
			return response[1:] // remove network address
		}
	}

	// remove network address and broadcast address
	if len(response) > 1 {
		return response[1 : len(response)-1]
	}
	return response
}

// IsLinkLocalAddress checks if the given IP address falls within link-local address ranges specified by RFCs.
func IsLinkLocalAddress(ip net.IP) bool {
	for _, curNet := range linkLocalCIDR {
		_, ipNet, err := net.ParseCIDR(curNet)
		if err != nil {
			return false
		}

		if !ip.IsLoopback() && ipNet.Contains(ip) {
			return true
		}
	}

	return false
}

// IncIP increments the given IP address by one. It works with both IPv4 and IPv6 addresses.
func IncIP(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

// ValidateNetwork checks if the provided network string is one of the valid network types.
// Returns true if the network is valid, false otherwise.
func ValidateNetwork(network string) bool {
	for _, n := range validNetworks {
		if network == n {
			return true
		}
	}
	return false
}

// IndexOf locates the index of a given IP address within a slice of host IPs.
// Returns the index if found, otherwise returns 0.
func IndexOf(address string, hosts []string) int {
	ip, _, err := net.ParseCIDR(address)
	if err != nil {
		return 0
	}
	for index, value := range hosts {
		if value == ip.String() {
			return index
		}
	}
	return 0
}
