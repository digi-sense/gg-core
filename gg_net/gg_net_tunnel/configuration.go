package gg_net_tunnel

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
)

type TunnelConfiguration struct {
	MaxConnectionAttempts int       `json:"max-connection-attempts"`
	Server                *Endpoint `json:"server"`
	Tunnel                *Endpoint `json:"tunnel"` // tunnel door
}

func NewTunnelConfiguration() (instance *TunnelConfiguration, err error) {
	instance = new(TunnelConfiguration)
	instance.Server = new(Endpoint) // ensure not nil
	instance.Tunnel = new(Endpoint) // ensure not nil

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *TunnelConfiguration) GoString() string {
	return gg_json.JSON.Stringify(instance)
}

func (instance *TunnelConfiguration) String() string {
	return gg_json.JSON.Stringify(instance)
}
