package gg_net_tunnel

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_nio/nio_client"
)

// Tunnel is a type that represents a tunnel in a network.
// A tunnel is a virtual connection that enables secure
// communication between two endpoints over an untrusted network.
// This tunnel creates a NIO connection to the server and forward traffic
// In the direction of a local service or server
type Tunnel struct {
	serverHost string
	serverPort int
	nio        *nio_client.NioClient
}

func NewTunnel(args ...interface{}) (instance *Tunnel) {
	instance = new(Tunnel)

	instance.init(args...)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Tunnel) Open() (err error) {
	if nil != instance {
		if nil != instance.nio {
			err = instance.nio.Open()
		} else {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Tunnel.Open() -> NIO connector is inconsistent: ")
		}
	}
	return
}

func (instance *Tunnel) Close() (err error) {
	if nil != instance {
		err = instance.nio.Close()
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Tunnel) init(args ...interface{}) {
	if nil != instance {

		// params autodetect
		for _, arg := range args {
			if host, ok := arg.(string); ok {
				instance.serverHost = host
				continue
			}
			if port, ok := arg.(int); ok {
				instance.serverPort = port
				continue
			}
			if endpoint, ok := arg.(*Endpoint); ok {
				instance.serverHost = endpoint.Host
				instance.serverPort = endpoint.Port
				continue
			}
		}

		if len(instance.serverHost) == 0 {
			instance.serverHost = "localhost"
		}
		if instance.serverPort == 0 {
			instance.serverPort = 10001
		}
		instance.nio = nio_client.NewNioClient(instance.serverHost, instance.serverPort)
	}
}
