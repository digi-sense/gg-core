package gg_net_tunnel

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_nio/nio_server"
	"bitbucket.org/digi-sense/gg-core/gg_stoppable"
	"fmt"
	"net"
	"strings"
	"time"
)

type TunnelServer struct {
	logger        gg_log.ILogger
	configuration *TunnelConfiguration
	stoppable     *gg_stoppable.Stoppable

	isOpen        bool
	clientTunnels map[string]*net.Conn  // clients that want to access to a tunnel to remote service
	clientService map[string]*net.Conn  // services that enable the tunnel and are reachable
	listener      net.Listener          // listen to clients that want to use a tunnel
	registrar     *nio_server.NioServer // listen to nio-clients that want to register them self as tunnel endpoints
}

func NewTunnelServer(args ...interface{}) (instance *TunnelServer, err error) {
	instance = new(TunnelServer)
	err = instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	stoppable
//----------------------------------------------------------------------------------------------------------------------

func (instance *TunnelServer) Start() (err error) {
	if nil != instance && nil != instance.stoppable && nil == instance.listener {
		instance.listener, err = instance.listen()
		if err != nil {
			instance.logger.Error(err)
			return err
		}
		err = instance.serve()
		if nil == err {
			err = instance.registrar.Open()
		}

		if nil != err {
			instance.handleStop()
		} else {
			instance.stoppable.Start()
		}
	}

	return
}

func (instance *TunnelServer) Stop() (err error) {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Stop()
	}
	return
}

func (instance *TunnelServer) Join() {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.Join()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *TunnelServer) String() string {
	if nil != instance {
		addr := instance.Addr()
		if nil != addr {
			return addr.String()
		} else {
			return "Not started!"
		}
	}
	return "Inconsistent status!"
}

func (instance *TunnelServer) Addr() *net.TCPAddr {
	if nil != instance && nil != instance.listener {
		if tcpAddr, ok := instance.listener.Addr().(*net.TCPAddr); ok {
			return tcpAddr
		}
	}
	return nil
}

func (instance *TunnelServer) Port() int {
	if nil != instance {
		addr := instance.Addr()
		if nil != addr {
			return addr.Port
		}
	}
	return 0
}

func (instance *TunnelServer) NetIP() net.IP {
	if nil != instance {
		addr := instance.Addr()
		if nil != addr {
			return addr.IP
		}
	}
	return nil
}

func (instance *TunnelServer) CountServices() int {
	if nil != instance && nil != instance.registrar {
		return instance.registrar.ClientsCount()
	}
	return 0
}

func (instance *TunnelServer) Services() []*nio_server.NioConnectionDescriptor {
	if nil != instance && nil != instance.registrar {
		return instance.registrar.ClientDescriptors()
	}
	return make([]*nio_server.NioConnectionDescriptor, 0)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TunnelServer) init(args ...interface{}) (err error) {
	if nil != instance {
		// check constructor params
		for _, arg := range args {
			if logger, ok := arg.(gg_log.ILogger); ok {
				instance.logger = logger
				continue
			}
			if config, ok := arg.(*TunnelConfiguration); ok {
				instance.configuration = config
			}
		}

		// logger
		{
			if nil == instance.logger {
				logFilename := gg.Paths.WorkspacePath("./logging/logging.log")
				instance.logger = gg_log.Log.New("info", logFilename)
			}
		}

		// config
		{
			if nil == instance.configuration {
				instance.configuration, _ = NewTunnelConfiguration()
			}
			if instance.configuration.MaxConnectionAttempts == 0 {
				instance.configuration.MaxConnectionAttempts = 1
			}
			if nil == instance.configuration.Server {
				instance.configuration.Server = new(Endpoint)
			}
			if nil == instance.configuration.Tunnel {
				instance.configuration.Tunnel = new(Endpoint)
			}
			if len(instance.configuration.Tunnel.Host) == 0 {
				instance.configuration.Tunnel.Host = "localhost" // not used
			}
			if instance.configuration.Tunnel.Port == 0 {
				instance.configuration.Tunnel.Port = 10001
			}
		}

		// registrar
		registrarPort := instance.configuration.Tunnel.Port
		instance.registrar = nio_server.NewNioServer(registrarPort)

		// stoppable
		{
			instance.stoppable = gg_stoppable.NewStoppable()
			instance.stoppable.SetName(fmt.Sprintf("Tunnel '%s'", instance.String()))
			instance.stoppable.OnStop(instance.handleStop)
		}

	}
	return
}

func (instance *TunnelServer) start() (err error) {
	if nil != instance {

		// Initialize the server
		instance.listener, err = instance.listen()
		if err != nil {
			instance.logger.Error(err)
			return err
		}
		err = instance.serve()
	}
	return
}

func (instance *TunnelServer) listen() (response net.Listener, err error) {
	if nil != instance {
		if nil != instance.configuration && nil != instance.configuration.Server {
			sConn := instance.configuration.Server.String()
			response, err = net.Listen("tcp", sConn)
		} else {
			err = gg_errors.Errors.Prefix(gg.PanicSystemError, "TunnelServer.listen() -> Mismatch configuration: ")
		}
	}
	return
}

func (instance *TunnelServer) serve() (err error) {
	if nil != instance && nil != instance.listener {
		instance.stoppable.SetName(fmt.Sprintf("Tunnel '%s'", instance.String()))
		instance.isOpen = true
		go instance.acceptConnections(instance.listener)
	} else {
		err = gg_errors.Errors.Prefix(gg.PanicSystemError, "TunnelServer.serve() -> Inconsistent status: ")
	}
	return
}

func (instance *TunnelServer) acceptConnections(listener net.Listener) {
	for {
		time.Sleep(100 * time.Millisecond)
		if !instance.isOpen {
			break
		}

		c := make(chan net.Conn)
		go newConnectionWaiter(listener, c)
		instance.logger.Info(fmt.Sprintf("Listening for new connections on %s:%d...",
			instance.configuration.Server.Host, instance.configuration.Server.Port))

		select {
		case conn := <-c:
			go instance.handleNewClient(conn)
		}
	}
}

func (instance *TunnelServer) handleStop() {
	if nil != instance {
		instance.isOpen = false
		// close server
		if nil != instance.listener {
			_ = instance.listener.Close()
			instance.listener = nil
		}
	}
}

func (instance *TunnelServer) handleNewClient(conn net.Conn) {
	if nil != instance && nil != conn {
		if tcpConn, ok := conn.(*net.TCPConn); ok {
			err := tcpConn.SetDeadline(time.Now().Add(10 * time.Second))
			if nil == err {
				// we have a valid connection
				instance.logger.Info("Accepted connection from %s",
					tcpConn.RemoteAddr().String())

				buff := make([]byte, 1024)
				i, err := tcpConn.Read(buff)
				if nil != err {
					fmt.Println(err)
				} else {
					fmt.Println(i, strings.TrimSpace(string(buff)))
				}

			}
			// sample timeout error handling
			if err != nil {
				if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
					fmt.Errorf("TCP timeout : %s", err.Error())
				} else {
					fmt.Errorf("Received error decoding message: %s", err.Error())
				}
			}
		}
	}
}

// handleNewRegistration handles a new registration by a client that needs a forward.
// It reads the incoming data from the connection and processes it.
// Parameters:
//
//	conn: the network connection with the client
func (instance *TunnelServer) handleNewRegistration(conn net.Conn) {

}

func newConnectionWaiter(listener net.Listener, c chan net.Conn) {
	conn, err := listener.Accept()
	if err != nil {
		return
	}
	c <- conn
}
