package gg_net_http

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"io/ioutil"
	"net/http"
)

type ResponseData struct {
	StatusCode int
	Body       []byte
	Header     map[string]string
}

func NewResponseDataEmpty() *ResponseData {
	instance := new(ResponseData)
	return instance
}

func NewResponseData(res *http.Response) (instance *ResponseData, err error) {
	instance = new(ResponseData)
	instance.StatusCode = res.StatusCode
	instance.Header = HttpHeaderToMap(res.Header)
	instance.Body, err = ioutil.ReadAll(res.Body)

	return
}

func (instance *ResponseData) String() string {
	m := map[string]interface{}{
		"status": instance.StatusCode,
		"header": instance.Header,
		"body":   string(instance.Body),
	}
	return gg_json.JSON.Stringify(m)
}

func (instance *ResponseData) BodyAsMap() map[string]interface{} {
	var m map[string]interface{}
	err := gg_json.JSON.Read(instance.Body, &m)
	if nil == err {
		return m
	}
	return map[string]interface{}{}
}
