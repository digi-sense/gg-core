package gg_net_http

import (
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"io"
	"net/http"
	"strings"
)

const MethodGet = "GET"
const MethodPost = "POST"
const MethodPut = "PUT"
const MethodDelete = "DELETE"

func HttpHeaderToMap(header http.Header) map[string]string {
	response := make(map[string]string)
	for k, v := range header {
		response[k] = strings.Join(v, ",")
	}
	return response
}

func ReadPayload(rawPayload interface{}) io.Reader {
	if nil != rawPayload {
		var payload string
		if v, b := rawPayload.(string); b {
			payload = v
		} else if v, b := rawPayload.([]byte); b {
			payload = string(v)
		} else if v, b := rawPayload.([]uint8); b {
			payload = string(v)
		} else if v, b := rawPayload.(map[string]interface{}); b {
			payload = gg_json.JSON.Stringify(v)
		} else if v, b := rawPayload.(map[string]string); b {
			payload = gg_json.JSON.Stringify(v)
		} else {
			payload = gg_utils.Convert.ToString(rawPayload)
		}
		return strings.NewReader(payload)
	}
	return nil
}

func ReadClientIP(r *http.Request) (response string) {
	response = r.Header.Get("X-Real-Ip")
	if len(response) == 0 {
		addrs := r.Header.Get("X-Forwarded-For")
		if len(addrs) > 0 {
			tokens := gg_utils.Strings.SplitTrimSpace(addrs, ",")
			if len(tokens) > 0 {
				response = tokens[0]
			}
		}
	}
	if len(response) == 0 {
		response = r.RemoteAddr
	}
	return
}

func ReadClientLang(r *http.Request) (response string) {
	response = r.Header.Get("X-Real-Lang")
	if len(response) == 0 {
		response = r.Header.Get("Accept-Language")
	}
	return
}

func ReadClientCharset(r *http.Request) (response string) {
	response = r.Header.Get("X-Device-Accept-Charset")
	if len(response) == 0 {
		response = r.Header.Get("Accept-Charset")
	}
	return
}
func ReadClientEncoding(r *http.Request) (response string) {
	response = r.Header.Get("X-Device-Accept-Encoding")
	if len(response) == 0 {
		response = r.Header.Get("Accept-Encoding")
	}
	return
}

func ReadClientUserAgent(r *http.Request) (response string) {
	response = r.Header.Get("X-Device-User-Agent")
	if len(response) == 0 {
		response = r.Header.Get("X-User-Agent")
	}
	if len(response) == 0 {
		response = r.Header.Get("User-Agent")
	}
	return
}

func ReadClientCookies(r *http.Request) (response []*http.Cookie) {
	response = r.Cookies()
	return
}
