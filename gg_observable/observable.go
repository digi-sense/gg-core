package gg_observable

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"sync"
)

var Observable *ObservableHelper

type ObservableHelper struct {
}

func init() {
	Observable = new(ObservableHelper)
}

func (instance ObservableHelper) New() *ObservableObj {
	return NewObservable()
}

type ObserverCallback func(sender *ObservableObj, key string, newValue, oldValue interface{})

type observableNotification struct {
	callback           ObserverCallback
	key                string
	newValue, oldValue interface{}
}

// ---------------------------------------------------------------------------------------------------------------------
//	ObservableObj
// ---------------------------------------------------------------------------------------------------------------------

// ObservableObj preserve values into a map and notify to observers any change
type ObservableObj struct {
	uid                  string
	async                bool
	keepOrder            bool
	pendingNotifications *sync.WaitGroup
	mux                  *sync.RWMutex
	data                 map[string]interface{}
	observers            map[string][]ObserverCallback
	notifications        []*observableNotification
}

func NewObservable() *ObservableObj {
	instance := new(ObservableObj)
	instance.uid = gg_rnd.Rnd.Uuid()
	instance.data = make(map[string]interface{})
	instance.observers = make(map[string][]ObserverCallback)
	instance.pendingNotifications = new(sync.WaitGroup)
	instance.mux = new(sync.RWMutex)
	instance.async = false
	instance.keepOrder = false
	instance.notifications = make([]*observableNotification, 0)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ObservableObj) String() string {
	if nil != instance {
		return gg_json.JSON.Stringify(instance.data)
	}
	return ""
}

func (instance *ObservableObj) Map() map[string]interface{} {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		return gg_utils.Maps.Clone(instance.data)
	}
	return map[string]interface{}{}
}

// Fork clone data but not observers
func (instance *ObservableObj) Fork() *ObservableObj {
	if nil != instance {
		response := NewObservable()
		response.data = instance.Map()
	}
	return nil
}

func (instance *ObservableObj) IsAsync() bool {
	if nil != instance {
		return instance.async
	}
	return false
}

// SetAsync enable async notifications, but do not give any warranty about execution order
func (instance *ObservableObj) SetAsync(value bool) *ObservableObj {
	if nil != instance {
		instance.async = value
	}
	return instance
}

// SetKeepOrder ensure FIFO execution order in notification
func (instance *ObservableObj) SetKeepOrder(value bool) *ObservableObj {
	if nil != instance {
		instance.keepOrder = value
	}
	return instance
}

func (instance *ObservableObj) Observe(key string, callback ObserverCallback) *ObservableObj {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if _, ok := instance.observers[key]; !ok {
			instance.observers[key] = make([]ObserverCallback, 0)
		}
		pull := instance.observers[key]
		pull = append(pull, callback)
		instance.observers[key] = pull
	}
	return instance
}

func (instance *ObservableObj) Get(key string) (value interface{}) {
	if nil != instance {
		instance.mux.RLock()
		defer instance.mux.RUnlock()
		if v, ok := instance.data[key]; ok {
			value = v
		}
	}
	return
}

func (instance *ObservableObj) Set(key string, value interface{}) *ObservableObj {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		var existing interface{}
		if v, ok := instance.data[key]; ok {
			existing = v
		}
		instance.data[key] = value

		// NOTIFY OBSERVERS
		instance.notify(key, value, existing)
	}
	return instance
}

func (instance *ObservableObj) Wait() {
	if nil != instance {
		instance.pendingNotifications.Wait()
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ObservableObj) notify(key string, newValue, oldValue interface{}) {
	for k, observer := range instance.observers {
		if nil != observer && (k == key || k == "" || k == "*") {
			for _, f := range observer {
				instance.pendingNotifications.Add(1)
				if instance.async {
					go invoke(instance.pendingNotifications, f, instance, key, newValue, oldValue)
				} else {
					invoke(instance.pendingNotifications, f, instance, key, newValue, oldValue)
				}
			}
		}
	}
	if instance.async && instance.keepOrder {
		instance.Wait()
	}
}

func invoke(wait *sync.WaitGroup, f ObserverCallback, sender *ObservableObj, key string, newValue, oldValue interface{}) {
	if nil != sender {
		defer wait.Done()

		gg_.Recover(fmt.Sprintf("Observable %s", sender.uid))

		f(sender, key, newValue, oldValue)
	}
}
