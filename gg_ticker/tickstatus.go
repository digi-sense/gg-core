package gg_ticker

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"sync"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	TickStatusRegistry
//----------------------------------------------------------------------------------------------------------------------

var TickStatusRegistry *tickStatusRegistry

func init() {
	TickStatusRegistry = &tickStatusRegistry{
		registry: make(map[string]*TickStatus),
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	tickStatusRegistry
//----------------------------------------------------------------------------------------------------------------------

type tickStatusRegistry struct {
	mux      sync.Mutex
	registry map[string]*TickStatus
}

func (instance *tickStatusRegistry) Count() (response int) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		response = len(instance.registry)
	}
	return
}

func (instance *tickStatusRegistry) Add(item *TickStatus) string {
	if nil != instance && nil != item {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		key := gg_utils.Coding.MD5(item.options.FileName)
		instance.registry[key] = item
		item.RegistryKey = key
		return key
	}
	return ""
}

func (instance *tickStatusRegistry) Remove(i interface{}) {
	if nil != instance && nil != i {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		var key string
		if s, ok := i.(string); ok {
			key = s
		} else if item, ok := i.(*TickStatus); ok {
			key = gg_utils.Coding.MD5(item.options.FileName)
		}

		if _, ok := instance.registry[key]; ok {
			delete(instance.registry, key)
		}
	}
}

func (instance *tickStatusRegistry) Get(key string) *TickStatus {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	if item, ok := instance.registry[key]; ok {
		return item
	}
	return nil
}

func (instance *tickStatusRegistry) Keys() []string {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	var keys []string
	for k := range instance.registry {
		keys = append(keys, k)
	}
	return keys
}

func (instance *tickStatusRegistry) Values() []*TickStatus {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	var values []*TickStatus
	for _, v := range instance.registry {
		values = append(values, v)
	}
	return values
}

func (instance *tickStatusRegistry) Start() {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	for _, v := range instance.registry {
		v.Start()
	}
}

func (instance *tickStatusRegistry) Stop() {
	instance.mux.Lock()
	defer instance.mux.Unlock()
	for _, v := range instance.registry {
		v.stop()
	}
	instance.registry = make(map[string]*TickStatus)
}

//----------------------------------------------------------------------------------------------------------------------
//	TickStatusOptions
//----------------------------------------------------------------------------------------------------------------------

type TickStatusOptions struct {
	Uid             string `json:"uid"`
	Enabled         bool   `json:"enabled"`
	Dir             string `json:"dir-root"`
	FileName        string `json:"filename"` // name of info file
	IntervalSeconds int    `json:"interval-seconds"`
	AddToRegistry   bool   `json:"add-to-registry"`
	CleanOnStart    bool   `json:"clean-on-start"`
}

func (instance *TickStatusOptions) String() string {
	return gg_json.JSON.Stringify(instance)
}

//----------------------------------------------------------------------------------------------------------------------
//	TickStatus
//----------------------------------------------------------------------------------------------------------------------

// TickStatus write info file every x time
// Useful to write a file to notify that application is alive
type TickStatus struct {
	RegistryKey string

	mux     *sync.Mutex
	options *TickStatusOptions
	ticker  *Ticker
	logger  gg_.ILogger
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewTickStatus(args ...interface{}) (instance *TickStatus, err error) {
	instance = new(TickStatus)
	err = instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	o p t i o n s
//----------------------------------------------------------------------------------------------------------------------

func (instance *TickStatus) Options() *TickStatusOptions {
	if nil != instance {
		return instance.options
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	s t o p p a b l e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TickStatus) Start() {
	if nil != instance {
		instance.start()
	}
	return
}

func (instance *TickStatus) Stop() {
	if nil != instance && nil != instance.ticker {
		instance.stop()
		if instance.options.AddToRegistry {
			TickStatusRegistry.Remove(instance)
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

// Write a string or a map into file
func (instance *TickStatus) Write(args ...interface{}) {
	if nil != instance && instance.options.Enabled {
		for _, arg := range args {
			instance.writeInfoLive(arg)
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *TickStatus) init(args ...interface{}) (err error) {
	if nil != instance {
		instance.mux = new(sync.Mutex)

		for _, arg := range args {
			if nil == arg {
				continue
			}
			if l, ok := arg.(gg_.ILogger); ok {
				instance.logger = l
				continue
			}
			if o, ok := arg.(*TickStatusOptions); ok {
				instance.options = o
				continue
			}
			if s, ok := arg.(string); ok {
				if gg_utils.Regex.IsValidJsonObject(s) {
					// from string
					err = gg_json.JSON.Read(s, &instance.options)
					if nil != err {
						return
					}
				} else {
					// from file
					err = gg_json.JSON.ReadFromFile(s, &instance.options)
					if nil != err {
						return
					}
				}
				continue
			}
			if m, ok := arg.(map[string]interface{}); ok {
				err = gg_json.JSON.Read(m, &instance.options)
				if nil != err {
					return
				}
				continue
			}
		}

		if nil == instance.options {
			instance.options = new(TickStatusOptions)
			instance.options.Enabled = true
			instance.options.Dir = "./out"
		} else {
			if len(instance.options.Dir) == 0 {
				instance.options.Dir = "./out"
			}
		}
	}
	return
}

func (instance *TickStatus) start() {
	if nil != instance {
		instance.stop()
		if instance.options.Enabled {
			// settings
			if len(instance.options.Uid) == 0 {
				instance.options.Uid = fmt.Sprintf("info-live-%v", gg_rnd.Rnd.Inc("TickStatus"))
			}
			instance.options.Dir = gg_utils.Paths.Absolutize(instance.options.Dir, gg_utils.Paths.WorkspacePath("."))
			if len(instance.options.FileName) == 0 {
				instance.options.FileName = fmt.Sprintf("%s.json", instance.options.Uid)
			}
			instance.options.FileName = gg_utils.Paths.Absolutize(instance.options.FileName, instance.options.Dir)
			_ = gg_utils.Paths.Mkdir(instance.options.FileName)
			// logger
			if nil == instance.logger {
				instance.logger = gg_log.Log.NewNoRotate("info", gg_utils.Paths.ChangeFileNameExtension(instance.options.FileName, ".log"))
			}
			// ticker
			if instance.options.IntervalSeconds > 0 {
				instance.ticker = NewTicker(time.Duration(instance.options.IntervalSeconds)*time.Second,
					instance.tick)
				if instance.options.AddToRegistry {
					_ = TickStatusRegistry.Add(instance)
				}
				if instance.options.CleanOnStart {
					// remove existing file
					_ = gg_utils.IO.Remove(instance.options.FileName)
				}
				instance.ticker.Start()
			}
		}
	}
	return
}

func (instance *TickStatus) stop() {
	if nil != instance && nil != instance.ticker {
		// log.Println("STOPPING...")
		instance.ticker.Stop()
		instance.ticker = nil
		// log.Println("STOPPED!")
	}
	return
}

func (instance *TickStatus) tick(_ *Ticker) {
	if nil != instance {
		instance.writeInfoLive(time.Now())
	}
}

func (instance *TickStatus) loadInfoLiveFile() (response map[string]interface{}) {
	response = make(map[string]interface{})
	if nil != instance {
		filename := instance.options.FileName
		if ok, _ := gg_utils.Paths.Exists(filename); ok {
			_ = gg_json.JSON.ReadFromFile(filename, &response)
		}
		if _, ok := response["log"].([]interface{}); !ok {
			response["log"] = make([]interface{}, 0)
		}
		if _, ok := response["start-timestamp"].(float64); !ok {
			response["start-timestamp"] = time.Now().Unix()
		}
	}
	return
}

func (instance *TickStatus) writeInfoLive(i interface{}) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		// log.Println("START WRITING...")

		d := gg_utils.Dates.FormatDate(time.Now(), "yyyy-MM-dd HH:mm")
		data := instance.loadInfoLiveFile()

		if s, ok := i.(string); ok {
			if arr, ok := data["log"].([]interface{}); ok {
				arr = append(arr, fmt.Sprintf("[%s]: %s", d, s))
				data["log"] = arr
			}
		} else if m, ok := i.(map[string]interface{}); ok {
			gg_utils.Maps.Merge(true, data, m)
		}

		if start, ok := data["start-timestamp"].(float64); ok {
			diff := time.Since(time.Unix(int64(start), 0))
			data["alive-from"] = fmt.Sprintf("%s", diff)
		}
		data["timestamp"] = time.Now()
		data["date"] = d

		filename := instance.options.FileName
		// write
		_, err := gg_utils.IO.WriteTextToFile(gg_json.JSON.Stringify(data), filename)
		if nil != err {
			instance.logger.Error(gg_errors.Errors.Prefix(err, "Error writing Info Live: "))
		}

		// log.Println("STOP WRITING...")
	}
	return
}
