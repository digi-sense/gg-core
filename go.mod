module bitbucket.org/digi-sense/gg-core

go 1.23.6

require (
	github.com/google/uuid v1.6.0
	golang.org/x/crypto v0.36.0
	golang.org/x/net v0.37.0
	golang.org/x/sys v0.31.0
	golang.org/x/text v0.23.0
)
