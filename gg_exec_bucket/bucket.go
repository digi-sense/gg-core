package gg_exec_bucket

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
)

var Bucket *BucketHelper

const (
	wpName = "bucket"
)

func init() {
	Bucket = NewBucketHelper()
}

// BucketHelper
// main executable container
type BucketHelper struct {
	root string // all buckets are created under this path
}

func NewBucketHelper() (instance *BucketHelper) {
	instance = new(BucketHelper)
	instance.SetRoot(".")

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *BucketHelper) SetRoot(root string) {
	gg_utils.Paths.GetWorkspace(wpName).SetPath(root)
	instance.root = gg_utils.Paths.GetWorkspace(wpName).GetPath()
}

//----------------------------------------------------------------------------------------------------------------------
//	b u i l d e r
//----------------------------------------------------------------------------------------------------------------------

// NewBucket creates and returns a new BucketExec instance using the provided execution path and global scope settings.
// The "global" param indicates if the bucket has a unique UID or if the ID il blank (global=true)
func (instance *BucketHelper) NewBucket(execPath string, global bool) *BucketExec {
	bucket := NewBucketExec(instance.root, execPath, global)
	return bucket
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
