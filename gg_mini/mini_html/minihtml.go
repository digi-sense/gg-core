package mini_html

import (
	"bitbucket.org/digi-sense/gg-core/gg_mini/mini_css"
	"bitbucket.org/digi-sense/gg-core/gg_mini/mini_js"
	"bytes"
	"golang.org/x/net/html"
	"io"
)

//----------------------------------------------------------------------------------------------------------------------
//	Minify
//----------------------------------------------------------------------------------------------------------------------

// Minify returns minified version of the given HTML data.
func Minify(data []byte, miniScripts, miniStyles, unquoteAttrs bool) (out []byte, err error) {
	var b bytes.Buffer
	z := html.NewTokenizer(bytes.NewReader(data))
	raw := 0
	javascript := false
	style := false
	svg := false
	for {
		tt := z.Next()
		switch tt {
		case html.ErrorToken:
			err := z.Err()
			if err == io.EOF {
				return b.Bytes(), nil
			}
			return nil, err
		case html.StartTagToken, html.SelfClosingTagToken:
			tagName, hasAttr := z.TagName()
			switch string(tagName) {
			case "script":
				javascript = true
				raw++
			case "style":
				style = true
				raw++
			case "svg":
				svg = true
				raw++
			case "pre", "code", "textarea":
				raw++
			}
			if svg {
				b.Write(z.Raw())
				continue
			}
			b.WriteByte('<')
			b.Write(tagName)
			var k, v []byte
			isFirst := true
			for hasAttr {
				k, v, hasAttr = z.TagAttr()
				if javascript && string(k) == "type" && string(v) != "text/javascript" {
					javascript = false
				}
				if string(k) == "style" && miniStyles {
					v = []byte("a{" + string(v) + "}") // simulate "full" CSS
					v, _ = mini_css.Minify(v)
					v = v[2 : len(v)-1] // strip simulation
				}
				if isFirst {
					b.WriteByte(' ')
					isFirst = false
				}
				b.Write(k)
				if len(v) > 0 || isAlt(k) {
					b.WriteByte('=')
					qv := html.EscapeString(string(v))
					if !unquoteAttrs || shouldQuote(v) {
						// Quoted value.
						b.WriteByte('"')
						b.WriteString(qv)
						b.WriteByte('"')
					} else {
						// Unquoted value.
						b.WriteString(qv)
					}
				}
				if hasAttr {
					b.WriteByte(' ')
				}
			}
			b.WriteByte('>')
		case html.EndTagToken:
			tagName, _ := z.TagName()
			switch string(tagName) {
			case "script":
				javascript = false
				raw--
			case "style":
				style = false
				raw--
			case "svg":
				svg = false
				raw--
			case "pre", "code", "textarea":
				raw--
			}
			b.Write([]byte("</"))
			b.Write(tagName)
			b.WriteByte('>')
		case html.CommentToken:
			if bytes.HasPrefix(z.Raw(), []byte("<!--[if")) ||
				bytes.HasPrefix(z.Raw(), []byte("<!--//")) {
				// Preserve IE conditional and special style comments.
				b.Write(z.Raw())
			}
			// ... otherwise, skip.
		case html.TextToken:
			if javascript && miniScripts {
				var mini []byte
				mini, err = mini_js.Minify(z.Raw())
				if err != nil {
					// Just write it as is.
					b.Write(z.Raw())
				} else {
					b.Write(mini)
				}
			} else if style && miniStyles {
				data, _ = mini_css.Minify(z.Raw())
				b.Write(data)
			} else if raw > 0 {
				b.Write(z.Raw())
			} else {
				b.Write(trimTextToken(z.Raw()))
			}
		default:
			b.Write(z.Raw())
		}

	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func isAlt(v []byte) bool {
	return len(v) == 3 && v[0] == 'a' && v[1] == 'l' && v[2] == 't'
}

func trimTextToken(b []byte) (out []byte) {
	out = make([]byte, 0)
	seenSpace := false
	for _, c := range b {
		switch c {
		case ' ', '\n', '\r', '\t':
			if !seenSpace {
				out = append(out, c)
				seenSpace = true
			}
		default:
			out = append(out, c)
			seenSpace = false
		}
	}
	return out
}

func shouldQuote(b []byte) bool {
	if len(b) == 0 || bytes.IndexAny(b, "\"'`=<> \n\r\t\b") != -1 {
		return true
	}
	return false
}
