package mini_json

import (
	"bytes"
	"encoding/json"
)

func Minify(src []byte) (minified []byte, err error) {
	dst := &bytes.Buffer{}
	if err = json.Compact(dst, []byte(src)); err != nil {
		return
	}
	minified = dst.Bytes()
	return
}
