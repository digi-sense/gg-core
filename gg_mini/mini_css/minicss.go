package mini_css

import (
	"bytes"
	"fmt"
	"regexp"
	"strconv"
)

const PLACEHOLDER_BOX_MODEL = "___BMH___"
const PLACEHOLDER_COL = "___PSEUDOCLASSCOLON___"

var (
	rcomments      = regexp.MustCompile(`/\*[\s\S]*?\*/`)
	rwhitespace    = regexp.MustCompile(`\s+`)
	rbmh           = regexp.MustCompile(`"\\"\}\\""`)
	runspace1      = regexp.MustCompile(`(?:^|\})[^\{:]+\s+:+[^\{]*\{`)
	runspace2      = regexp.MustCompile(`\s+([!\{\};:>+\(\)\],])`)
	runspace3      = regexp.MustCompile(`([!\{\}:;>+\(\[,])\s+`)
	rsemicolons    = regexp.MustCompile(`([^;\}])\}`)
	runits         = regexp.MustCompile(`(?i)([\s:])([+-]?0)(?:%|em|ex|px|in|cm|mm|pt|pc)`)
	rfourzero      = regexp.MustCompile(`:(?:0 )+0;`)
	rleadzero      = regexp.MustCompile(`(:|\s)0+\.(\d+)`)
	rrgb           = regexp.MustCompile(`rgb\s*\(\s*([0-9,\s]+)\s*\)`)
	rdigits        = regexp.MustCompile(`\d+`)
	rcompresshex   = regexp.MustCompile(`(?i)([^"'=\s])(\s?)\s*#([0-9a-f]){6}`)
	rhexval        = regexp.MustCompile(`[0-9a-f]{2}`)
	remptyrules    = regexp.MustCompile(`[^\}]+\{;\}\n`)
	rmediaspace    = regexp.MustCompile(`\band\(`)
	rredsemicolons = regexp.MustCompile(`;+\}`)
)

//----------------------------------------------------------------------------------------------------------------------
//	Minify
//----------------------------------------------------------------------------------------------------------------------

func Minify(css []byte) (minified []byte, err error) {
	// Remove comments.
	minified = rcomments.ReplaceAll(css, []byte{})

	// Compress all runs of whitespace to a single space to make things easier
	// to work with.
	minified = rwhitespace.ReplaceAll(minified, []byte(" "))

	// Replace box model hacks with placeholders.
	minified = rbmh.ReplaceAll(minified, []byte(PLACEHOLDER_BOX_MODEL))

	// Remove unnecessary spaces, but be careful not to turn "p :link {...}"
	// into "p:link{...}".
	minified = runspace1.ReplaceAllFunc(minified, func(match []byte) []byte {
		return bytes.Replace(match, []byte(":"), []byte(PLACEHOLDER_COL), -1)
	})
	minified = runspace2.ReplaceAll(minified, []byte("$1"))
	minified = bytes.Replace(minified, []byte(PLACEHOLDER_COL), []byte(":"), -1)
	minified = runspace3.ReplaceAll(minified, []byte("$1"))

	// Add missing semicolons.
	minified = rsemicolons.ReplaceAll(minified, []byte("$1;}"))

	// Replace 0(%, em, ex, px, in, cm, mm, pt, pc) with just 0.
	minified = runits.ReplaceAll(minified, []byte("$1$2"))

	// Replace 0 0 0 0; with 0.
	minified = rfourzero.ReplaceAll(minified, []byte(":0;"))

	// Replace background-position:0; with background-position:0 0;
	minified = bytes.Replace(minified, []byte("background-position:0;"), []byte("background-position:0 0;"), -1)

	// Replace 0.6 with .6, but only when preceded by : or a space.
	minified = rleadzero.ReplaceAll(minified, []byte("$1.$2"))

	// Convert rgb color values to hex values.
	minified = rrgb.ReplaceAllFunc(minified, func(match []byte) (out []byte) {
		out = []byte{'#'}
		for _, v := range rdigits.FindAll(match, -1) {
			d, e := strconv.Atoi(string(v))
			if e != nil {
				return match
			}
			out = append(out, []byte(fmt.Sprintf("%02x", d))...)
		}
		return out
	})

	// Compress color hex values, making sure not to touch values used in IE
	// filters, since they would break.
	minified = rcompresshex.ReplaceAllFunc(minified, func(match []byte) (out []byte) {
		vals := rhexval.FindAll(match, -1)
		if len(vals) != 3 {
			return match
		}
		compressible := true
		for _, v := range vals {
			if v[0] != v[1] {
				compressible = false
			}
		}
		if !compressible {
			return match
		}
		out = append(out, match[:bytes.IndexByte(match, '#')+1]...)
		return append(out, vals[0][0], vals[1][0], vals[2][0])
	})

	// Remove empty rules.
	minified = remptyrules.ReplaceAll(minified, []byte{})

	// Re-insert box model hacks.
	minified = bytes.Replace(minified, []byte(PLACEHOLDER_BOX_MODEL), []byte(`"\"}\""`), -1)

	// Put the space back in for media queries
	minified = rmediaspace.ReplaceAll(minified, []byte("and ("))

	// Prevent redundant semicolons.
	minified = rredsemicolons.ReplaceAll(minified, []byte("}"))

	minified = bytes.TrimSpace(minified)

	return
}
