package gg_mini

import (
	"bitbucket.org/digi-sense/gg-core/gg_mini/mini_css"
	"bitbucket.org/digi-sense/gg-core/gg_mini/mini_html"
	"bitbucket.org/digi-sense/gg-core/gg_mini/mini_js"
	"bitbucket.org/digi-sense/gg-core/gg_mini/mini_json"
)

var Minify *MinifyHelper

func init() {
	Minify = new(MinifyHelper)
}

type MinifyHelper struct {
}

func (instance *MinifyHelper) HTML(data []byte) ([]byte, error) {
	return mini_html.Minify(data, true, true, true)
}

func (instance *MinifyHelper) CSS(data []byte) ([]byte, error) {
	return mini_css.Minify(data)
}

func (instance *MinifyHelper) JS(data []byte) ([]byte, error) {
	return mini_js.Minify(data)
}

func (instance *MinifyHelper) JSON(data []byte) ([]byte, error) {
	return mini_json.Minify(data)
}
