package gg_fnvars

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"strings"
	"time"
)

const ToolDate = "date"

// FnVarToolDate date|yyyy-MM-dd|upper
type FnVarToolDate struct{}

func (instance *FnVarToolDate) Name() string {
	return ToolDate
}

func (instance *FnVarToolDate) Solve(token string, context ...interface{}) (interface{}, error) {
	tags := SplitToken(token)
	if len(tags) > 1 {
		return instance.solve(tags[1:])
	}
	return nil, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

// date|yyyy-MM-dd|upper
// date|timestamp|add|day|30
func (instance *FnVarToolDate) solve(args []string) (interface{}, error) {
	pattern := gg_utils.Arrays.GetAt(args, 0, "yyyyMMdd").(string)
	lpattern := strings.ToLower(pattern)
	now := time.Now()
	var output string
	switch lpattern {
	case "iso", "rfc3339":
		output = now.Format(time.RFC3339)
	case "unix":
		output = now.Format(time.UnixDate)
	case "ruby":
		output = now.Format(time.RubyDate)
	case "timestamp", "timestamp_s":
		output = fmt.Sprintf("%d", now.Unix())
	case "timestamp_m":
		output = fmt.Sprintf("%d", now.Unix()*1000)
	default:
		output = gg_utils.Formatter.FormatDate(now, pattern)
	}

	transform := gg_utils.Arrays.GetAt(args, 1, "").(string)
	if len(transform) > 0 {
		switch transform {
		case "add", "sub":
			output = calculate(transform, args, output)
		default:
			output = Transform(output, transform)
		}
	}

	return output, nil
}

func calculate(op string, args []string, output string) string {
	timestamp := gg_utils.Convert.ToInt64Def(output, 0) // seconds
	val := gg_utils.Convert.ToInt(gg_utils.Arrays.GetAt(args, 2, 0))
	um := gg_utils.Arrays.GetAt(args, 3, "second").(string)
	if timestamp > 0 && len(um) > 0 && val > 0 {
		if op == "sub" {
			val = val * -1
		}
		switch um {
		case "millisecond":
			timestamp = timestamp + int64(val/1000)
		case "second":
			timestamp = timestamp + int64(val)
		case "minute":
			timestamp = timestamp + int64(60*val)
		case "hour":
			timestamp = timestamp + int64(60*60*val)
		case "day":
			timestamp = gg_utils.Dates.AddDays(timestamp, val).Unix()
		case "week":
			timestamp = gg_utils.Dates.AddWeeks(timestamp, val).Unix()
		case "month":
			timestamp = gg_utils.Dates.AddMonths(timestamp, val).Unix()
		case "year":
			timestamp = gg_utils.Dates.AddYears(timestamp, val).Unix()
		default:
			// seconds
		}
		output = fmt.Sprintf("%d", timestamp)
	}
	return output
}
