package gg_fnvars

import (
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
)

const (
	ToolRnd = "rnd"
)

// FnVarToolRnd rnd|chars|4|upper
type FnVarToolRnd struct{}

func (instance *FnVarToolRnd) Name() string {
	return ToolRnd
}

func (instance *FnVarToolRnd) Solve(token string, context ...interface{}) (interface{}, error) {
	tags := SplitToken(token)
	if len(tags) > 1 {
		return instance.solve(tags[1:])
	}
	return nil, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *FnVarToolRnd) solve(args []string) (interface{}, error) {
	mode := gg_utils.Arrays.GetAt(args, 0, "numeric")
	var output string
	switch mode {
	case "numeric":
		length := gg_utils.Convert.ToInt(gg_utils.Arrays.GetAt(args, 1, "6"))
		output = gg_rnd.Rnd.RndDigits(length)
	case "range", "between":
		param := gg_utils.Convert.ToString(gg_utils.Arrays.GetAt(args, 1, "0-10"))
		tokens := gg_utils.Strings.Split(param, "-.,:;")
		from := gg_utils.Convert.ToInt64(tokens[0])
		to := from + 5
		if len(tokens) > 1 {
			to = gg_utils.Convert.ToInt64(tokens[1])
		}
		output = gg_utils.Convert.ToString(gg_rnd.Rnd.Between(from, to))
	case "alphanumeric", "chars":
		length := gg_utils.Convert.ToInt(gg_utils.Arrays.GetAt(args, 1, "6"))
		transform := gg_utils.Arrays.GetAt(args, 2, "")
		switch transform {
		case "upper":
			output = gg_rnd.Rnd.RndCharsUpper(length)
		case "lower":
			output = gg_rnd.Rnd.RndCharsLower(length)
		default:
			output = gg_rnd.Rnd.RndChars(length)
		}
	case "guid":
		transform := gg_utils.Arrays.GetAt(args, 2, "").(string)
		output = gg_rnd.Rnd.Uuid()
		output = Transform(output, transform)
	case "id":
		transform := gg_utils.Arrays.GetAt(args, 2, "").(string)
		output = gg_rnd.Rnd.RndId()
		output = Transform(output, transform)
	default:
		length := gg_utils.Convert.ToInt(gg_utils.Arrays.GetAt(args, 1, "6"))
		output = gg_rnd.Rnd.RndDigits(length)
	}
	return output, nil
}
