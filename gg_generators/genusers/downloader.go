package genusers

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
)

/**
Download templates from GitHub or Bitbucket
*/

const (
	repoRoot = "https://bitbucket.org/digi-sense/gg-core/raw/master/gg_generators/genusers/data/"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func DownloadTemplates(dirTarget string) ([]string, []error) {
	// download
	session := gg_utils.IO.NewDownloadSession(templateActions(dirTarget))
	return session.DownloadAll(false)
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func templateActions(dirTarget string) []*gg_utils.DownloaderAction {
	response := make([]*gg_utils.DownloaderAction, 0)

	// data
	response = append(response, gg_utils.IO.NewDownloaderAction("", repoRoot+"names.csv", "", dirTarget))
	response = append(response, gg_utils.IO.NewDownloaderAction("", repoRoot+"surnames.csv", "", dirTarget))
	response = append(response, gg_utils.IO.NewDownloaderAction("", repoRoot+"country_codes.csv", "", dirTarget))

	return response
}
