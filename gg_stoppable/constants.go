//go:build darwin
//+build darwin

package gg_stoppable

import (
	"os"
	"syscall"
)

var SIGNALS = []os.Signal{
	syscall.SIGINT,
	syscall.SIGTERM,
	syscall.SIGHUP,
	syscall.SIGKILL,
	syscall.SIGQUIT,
	syscall.SIGSTOP,
}
