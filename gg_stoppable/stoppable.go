package gg_stoppable

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_sys"
	"fmt"
	"log"
	"os"
	"sync"
	"syscall"
	"time"
)

type ShutdownCallback func() error

var stoppableInstances = make([]*Stoppable, 0)
var mux sync.Mutex

// Stoppable object
type Stoppable struct {
	uid                  string
	name                 string
	index                int
	stopChan             chan bool
	mux                  sync.Mutex
	waiting              bool
	stopped              bool
	onStartHandlers      []func()
	onStopHandlers       []func()
	loggerFunc           func() gg_.ILogger
	shutdownOperations   map[string]ShutdownCallback
	shutdownTimeout      time.Duration
	beforeExitCloseSleep time.Duration

	__logger gg_.ILogger
}

func NewStoppable() *Stoppable {
	instance := new(Stoppable)
	instance.uid = gg_rnd.Rnd.Uuid()
	instance.shutdownOperations = make(map[string]ShutdownCallback)
	instance.shutdownTimeout = 10 * time.Second
	instance.beforeExitCloseSleep = 0 * time.Second
	instance.onStartHandlers = make([]func(), 0)
	instance.onStopHandlers = make([]func(), 0)
	instance.stopped = false
	_ = gg_sys.Sys.OnSignal(instance.onSignal, SIGNALS...)

	// add to an internal list
	mux.Lock()
	defer mux.Unlock()
	stoppableInstances = append(stoppableInstances, instance)

	instance.index = len(stoppableInstances)
	instance.loggerFunc = func() gg_.ILogger {
		return nil
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Stoppable) String() string {
	if nil != instance {
		return gg_json.JSON.Stringify(map[string]interface{}{
			"id":      instance.ItemId(),
			"uid":     instance.uid,
			"name":    instance.name,
			"actions": instance.OperationsName(),
		})
	}
	return ""
}

func (instance *Stoppable) ItemId() string {
	if nil != instance {
		return fmt.Sprintf("item#%v", instance.index)
	}
	return ""
}

func (instance *Stoppable) SetLogger(logger func() gg_.ILogger) *Stoppable {
	if nil != instance {
		instance.loggerFunc = logger
	}
	return instance
}

func (instance *Stoppable) SetName(name string) *Stoppable {
	if nil != instance {
		instance.name = name
	}
	return instance
}

func (instance *Stoppable) GetName() string {
	if nil != instance {
		if len(instance.name) > 0 {
			return instance.name
		}
		return instance.ItemId()
	}
	return ""
}

func (instance *Stoppable) SetTimeout(value time.Duration) *Stoppable {
	if nil != instance {
		instance.shutdownTimeout = value
	}
	return instance
}

func (instance *Stoppable) GetTimeout() time.Duration {
	if nil != instance {
		return instance.shutdownTimeout
	}
	return 0
}

func (instance *Stoppable) SetSleep(value time.Duration) *Stoppable {
	if nil != instance {
		instance.beforeExitCloseSleep = value
	}
	return instance
}

func (instance *Stoppable) GetSleep() time.Duration {
	if nil != instance {
		return instance.beforeExitCloseSleep
	}
	return 0
}

func (instance *Stoppable) AddStopOperation(name string, callback ShutdownCallback) *Stoppable {
	if nil != instance && nil != instance.shutdownOperations {
		instance.shutdownOperations[name] = callback
	}
	return instance
}

func (instance *Stoppable) OperationsName() []string {
	response := make([]string, 0)
	if nil != instance && nil != instance.shutdownOperations {
		for name, _ := range instance.shutdownOperations {
			response = append(response, name)
		}
	}
	return response
}

func (instance *Stoppable) Start() bool {
	if nil != instance && nil == instance.stopChan {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.stopped = false
		instance.stopChan = make(chan bool, 1)
		instance.doStart()

		return true
	}
	return false // not executed
}

func (instance *Stoppable) Stop() bool {
	if nil != instance && nil != instance.stopChan && !instance.stopped {
		instance.onSignal(syscall.SIGQUIT)
		return true
	}
	return false // not executed
}

func (instance *Stoppable) IsStopped() bool {
	if nil != instance && nil != instance.stopChan {
		return false
	}
	return true
}

func (instance *Stoppable) IsJoined() bool {
	return nil != instance && instance.waiting
}

func (instance *Stoppable) Join() {
	if nil != instance && nil != instance.stopChan && !instance.waiting {

		instance.waiting = true

		// wait exit
		<-instance.stopChan
		// reset channel
		instance.stopChan = nil
		instance.waiting = false
	}
}

func (instance *Stoppable) JoinTimeout(d time.Duration) {
	go func() {
		time.Sleep(d)
		instance.Stop()
	}()
	instance.Join()
}

// ---------------------------------------------------------------------------------------------------------------------
//	e v e n t s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Stoppable) OnStart(f func()) {
	instance.onStartHandlers = append(instance.onStartHandlers, f)
}

func (instance *Stoppable) OnStop(f func()) {
	instance.onStopHandlers = append(instance.onStopHandlers, f)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Stoppable) doStart() {
	if nil != instance && nil != instance.onStartHandlers {
		invokeAll(instance.onStartHandlers)
	}
}

func (instance *Stoppable) doStop() {
	if nil != instance && nil != instance.onStopHandlers {
		invokeAll(instance.onStopHandlers)

		if instance.beforeExitCloseSleep > 0 {
			time.Sleep(instance.beforeExitCloseSleep)
		}
	}
}

func (instance *Stoppable) logger() gg_.ILogger {
	if nil != instance && nil != instance.loggerFunc {
		if nil == instance.__logger {
			instance.__logger = instance.loggerFunc()
		}
		return instance.__logger
	}
	return nil
}

func (instance *Stoppable) logInfo(msg string) gg_.ILogger {
	if nil != instance {
		logger := instance.logger()
		if nil != logger {
			logger.Info(msg)
			return logger
		} else {
			log.Println(msg)
		}
	}
	return nil
}

func (instance *Stoppable) onSignal(s os.Signal) {
	if nil != instance && !instance.stopped {
		instance.stopped = true

		// wait := make(chan struct{})

		msg := fmt.Sprintf("STARTING SHUTDOWN for '%s' ('%s') from signal '%v'...", instance.GetName(), instance.ItemId(), s)
		instance.logInfo(msg)

		// intercepted close signal
		if len(instance.shutdownOperations) > 0 {
			// set timeout for the ops to be done to prevent system hang
			timeout := instance.shutdownTimeout // max 10 seconds to shutdown
			timeoutFunc := time.AfterFunc(timeout, func() {
				msg = fmt.Sprintf("[%s] timeout %d ms has been elapsed, force exit",
					instance.ItemId(), timeout.Milliseconds())
				instance.logInfo(msg)
				// kill if timeout exceeds
				os.Exit(0)
			})
			defer timeoutFunc.Stop()

			var wg sync.WaitGroup
			// Do the operations asynchronously to save time
			for key, op := range instance.shutdownOperations {
				wg.Add(1)
				innerOp := op
				innerKey := key
				go func() {
					defer wg.Done()

					opMsg := fmt.Sprintf("\t[%s] cleaning up: %s", instance.ItemId(), innerKey)
					instance.logInfo(opMsg)
					if err := innerOp(); err != nil {
						opMsg = fmt.Sprintf("\t[%s] %s: clean up failed: %s", instance.ItemId(), innerKey, err.Error())
						instance.logInfo(opMsg)
						return
					}

					opMsg = fmt.Sprintf("\t[%s] %s was shutdown gracefully", instance.ItemId(), innerKey)
					instance.logInfo(opMsg)
				}()
			}
			wg.Wait()

			// close wait channel
			// close(wait)
		} else {
			// log.Println("NO OPERATIONS....")
		}

		msg = fmt.Sprintf("TERMINATED SHUTDOWN for '%s' ('%s').",
			instance.GetName(), instance.ItemId())
		if nil != instance.logInfo(msg) {
			// wait a little to allow file writing
			time.Sleep(1 * time.Second)
		}
		_ = instance.free()
	}
}

func (instance *Stoppable) free() bool {
	if nil != instance && nil != instance.stopChan {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		instance.doStop()

		instance.stopChan <- true
		instance.stopChan = nil

		return true
	}
	return false // not executed
}

func invokeAll(handlers []func()) {
	if len(handlers) > 0 {
		var wg sync.WaitGroup
		for i, handler := range handlers {
			if nil != handler {
				wg.Add(1)
				go func(h func(), i int) {
					defer wg.Done()
					h()
					// log.Println("handler", i)
				}(handler, i)
			}
		}
		wg.Wait()
		// log.Println("invokeAll END")
	}
}
