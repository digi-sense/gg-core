package gg_json

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"encoding/json"
	"fmt"
	"strings"
)

type JSONHelper struct {
}

var JSON *JSONHelper

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

func init() {
	JSON = new(JSONHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *JSONHelper) StringToMap(text string) (map[string]interface{}, bool) {
	return instance.BytesToMap([]byte(text))
}

func (instance *JSONHelper) StringToArray(text string) ([]interface{}, bool) {
	return instance.BytesToArray([]byte(text))
}

func (instance *JSONHelper) BytesToMap(data []byte) (map[string]interface{}, bool) {
	var js map[string]interface{}
	err := json.Unmarshal(data, &js)
	if nil != err {
		return nil, false
	}
	return js, true
}

func (instance *JSONHelper) BytesToArray(data []byte) ([]interface{}, bool) {
	var js []interface{}
	err := json.Unmarshal(data, &js)
	if nil != err {
		return nil, false
	}
	return js, true
}

func (instance *JSONHelper) Bytes(entity interface{}) []byte {
	return gg_.Marshal(entity)
}

func (instance *JSONHelper) IsDelimitedJson(s string) bool {
	if strings.Index(s, "{") == 0 {
		return fmt.Sprintf("%c", s[len(s)-1]) == "}"
	}
	if strings.Index(s, "[") == 0 {
		return fmt.Sprintf("%c", s[len(s)-1]) == "]"
	}
	return false
}

func (instance *JSONHelper) IsValidJson(text string) bool {
	return gg_.IsValidJson(text)
}

func (instance *JSONHelper) IsValidJsonObject(text string) bool {
	return gg_.IsValidJsonObject(text)
}

func (instance *JSONHelper) IsValidJsonArray(text string) bool {
	return gg_.IsValidJsonArray(text)
}

func (instance *JSONHelper) Stringify(entity interface{}) string {
	return gg_.Stringify(entity)
}

func (instance *JSONHelper) Decode(filename string, callback func(index int, item map[string]interface{})) error {
	return gg_.Decode(filename, callback)
}

func (instance *JSONHelper) Parse(input interface{}) interface{} {
	if v, b := input.(string); b {
		if o, b := instance.StringToMap(v); b {
			return o // map
		} else if a, b := instance.StringToArray(v); b {
			return a // array
		}
		return v // simple string
	} else if v, b := input.([]byte); b {
		if o, b := instance.BytesToMap(v); b {
			return o // map
		} else if a, b := instance.BytesToArray(v); b {
			return a // array
		}
		return v // simple string
	}
	return input
}

func (instance *JSONHelper) TextExtractor(input interface{}) *JsonExtractor {
	return NewJsonExtractor().Write(input)
}

func (instance *JSONHelper) Wrap(input interface{}) *JSONWrap {
	return NewJSONWrap(input)
}

// Read Support inputs like maps, strings and byte arrays.
func (instance *JSONHelper) Read(input interface{}, entity interface{}) (err error) {
	return gg_.Unmarshal(input, entity)
}

func (instance *JSONHelper) ReadFromFile(fileName string, entity interface{}) error {
	return gg_.UnmarshalFromFile(fileName, entity)
}

func (instance *JSONHelper) ReadMapFromFile(fileName string) (map[string]interface{}, error) {
	b, err := gg_.IOReadBytesFromFile(fileName)
	if nil != err {
		return nil, err
	}
	var response map[string]interface{}
	err = json.Unmarshal(b, &response)
	if nil != err {
		return nil, err
	}
	return response, nil
}

func (instance *JSONHelper) ReadArrayFromFile(fileName string) ([]map[string]interface{}, error) {
	b, err := gg_.IOReadBytesFromFile(fileName)
	if nil != err {
		return nil, err
	}
	var response []map[string]interface{}
	err = json.Unmarshal(b, &response)
	if nil != err {
		return nil, err
	}
	return response, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
