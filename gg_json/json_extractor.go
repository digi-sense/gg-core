package gg_json

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"fmt"
	"reflect"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

var (
	placeholders = map[string]string{
		" null,": "\"_null_\",",
		":null,": "\"_null_\",",
		"null\n": "\"_null_\"\n",
		"\\\"":   "'",
	}
)

const (
	open_graph   = "{"
	close_graph  = "}"
	open_square  = "["
	close_square = "]"
	braket       = "\""
	json_chars   = "{[]}:, \n" + braket
)

//----------------------------------------------------------------------------------------------------------------------
//	JSONWrap
//----------------------------------------------------------------------------------------------------------------------

// JSONWrap simple wrapper for JSON objects
type JSONWrap struct {
	value interface{}
	err   error
	m     map[string]interface{}
	a     []map[string]interface{}
}

func NewJSONWrap(value interface{}) (instance *JSONWrap) {
	instance = new(JSONWrap)
	instance.init(value)
	return
}

func (instance *JSONWrap) String() string {
	if nil != instance {
		if nil != instance.err {
			return fmt.Sprint(instance.err)
		}
		return gg_.Stringify(instance.Value())
	}
	return ""
}

func (instance *JSONWrap) KindRt() reflect.Kind {
	if nil != instance {
		val := instance.Value()
		rt := gg_.ValueOf(val)
		k := rt.Kind()
		switch k {
		case reflect.Slice, reflect.Array:
			return reflect.Array
		default:
			return k
		}
	}
	return reflect.Invalid
}

func (instance *JSONWrap) Kind() string {
	if nil != instance {
		k := instance.KindRt()
		return fmt.Sprintf("%s", k)
	}
	return ""
}

func (instance *JSONWrap) HasError() bool {
	if nil != instance {
		return instance.err != nil
	}
	return true
}

func (instance *JSONWrap) Error() error {
	if nil != instance {
		return instance.err
	}
	return gg_errors.Errors.Prefix(gg_.PanicSystemError, "System error, invalid instance: ")
}

func (instance *JSONWrap) IsMap() bool {
	if nil != instance {
		return nil != instance.m
	}
	return false
}

func (instance *JSONWrap) IsArray() bool {
	if nil != instance {
		return nil != instance.a
	}
	return false
}

func (instance *JSONWrap) IsUnknown() bool {
	if nil != instance {
		return nil == instance.a && nil == instance.m
	}
	return false
}

func (instance *JSONWrap) Value() interface{} {
	if nil != instance {
		if nil != instance.m {
			return instance.m
		}
		if nil != instance.a {
			return instance.a
		}
		return instance.value
	}
	return false
}

func (instance *JSONWrap) Map() map[string]interface{} {
	if nil != instance {
		if nil != instance.m {
			return instance.m
		}
	}
	return nil
}

func (instance *JSONWrap) Array() []map[string]interface{} {
	if nil != instance {
		if nil != instance.a {
			return instance.a
		}
	}
	return nil
}

func (instance *JSONWrap) AsMap() (response map[string]interface{}, err error) {
	if nil != instance {
		err = instance.err
		if nil == err {
			if nil != instance.m {
				response = instance.m
			} else {
				err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Not a JSON object: ")
			}
		}
	}
	return
}

func (instance *JSONWrap) AsArray() (response []map[string]interface{}, err error) {
	if nil != instance {
		err = instance.err
		if nil == err {
			if nil != instance.a {
				response = instance.a
			} else {
				err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Not a JSON array: ")
			}
		}
	}
	return
}

func (instance *JSONWrap) init(value interface{}) {
	if nil != instance {
		instance.value = value
		if s, ok := value.(string); ok {
			if gg_.IsValidJsonObject(s) {
				_ = gg_.Unmarshal(s, &instance.m)
			} else if gg_.IsValidJsonArray(s) {
				_ = gg_.Unmarshal(s, &instance.a)
			} else {
				instance.err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Invalid JSON: ")
			}
			return
		}
		if m, ok := value.(map[string]interface{}); ok {
			instance.m = m
			return
		}
		if a, ok := value.([]map[string]interface{}); ok {
			instance.a = a
			return
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	JsonExtractor
//----------------------------------------------------------------------------------------------------------------------

// JsonExtractor extract json objects from a text file or from long text
type JsonExtractor struct {
	buff *strings.Builder
}

func NewJsonExtractor() (instance *JsonExtractor) {
	instance = new(JsonExtractor)
	instance.buff = new(strings.Builder)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsonExtractor) String() string {
	if nil != instance {
		return instance.buff.String()
	}
	return ""
}

func (instance *JsonExtractor) LoadFromFile(filename string) *JsonExtractor {
	if nil != instance {
		txt, err := gg_.IOReadTextFromFile(filename)
		if nil == err {
			_, _ = instance.buff.WriteString(txt)
		}
	}
	return instance
}

func (instance *JsonExtractor) Write(value interface{}) *JsonExtractor {
	if nil != instance {
		if s, ok := value.(string); ok {
			_, _ = instance.buff.WriteString(s)
			return instance
		}
		if b, ok := value.([]byte); ok {
			_, _ = instance.buff.Write(b)
			return instance
		}
	}
	return instance
}

// Get parse a text to detect and extract all JSON objects or arrays found.
// Returns an object or an array of objects or arrays
func (instance *JsonExtractor) Get() (response interface{}) {
	if nil != instance {
		text := instance.text()
		if len(text) > 0 {
			response = get(text)
		}
	}
	return
}

func (instance *JsonExtractor) GetObjects() (response []*JSONWrap, err error) {
	response = make([]*JSONWrap, 0)
	if nil != instance {
		objects := instance.Get()
		if nil != objects {
			if a, ok := objects.([]string); ok {
				for _, v := range a {
					w := NewJSONWrap(v)
					if w.HasError() {
						err = w.Error()
						return
					}
					response = append(response, w)
				}
			} else if s, ok := objects.(string); ok {
				w := NewJSONWrap(s)
				if w.HasError() {
					err = w.Error()
					return
				}
				response = append(response, w)
			} else {
				rt := gg_.ValueOf(objects)
				err = gg_errors.Errors.Prefix(gg_.PanicSystemError, fmt.Sprintf("Unexpected Kind: %s", rt.Kind()))
			}
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *JsonExtractor) text() string {
	text := strings.TrimSpace(instance.buff.String())
	return text
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func get(text string) (response interface{}) {
	text = placeholder(text)
	var s string
	var cursor int
	collected := make([]string, 0)
	i := strings.Index(text, open_graph)
	ii := strings.LastIndex(text, open_square)
	if i > -1 || ii > -1 {
		// we have JSON
		if i > -1 && ii > -1 {
			// we have both
			if i > ii {
				// before array and then objects, or array of objects
				cursor = ii
				// array
				for {
					s, cursor = array(text, cursor)
					if len(s) > 0 {
						s = normalize(s)
						collected = append(collected, s)
					} else {
						break
					}
				}
				// object
				for {
					s, cursor = object(text, cursor)
					if len(s) > 0 {
						s = normalize(s)
						collected = append(collected, s)
					} else {
						break
					}
				}
			} else {
				// before objects and then array
				cursor = i
				// object
				for {
					s, cursor = object(text, cursor)
					if len(s) > 0 {
						s = normalize(s)
						collected = append(collected, s)
					} else {
						break
					}
				}
				// array
				for {
					s, cursor = array(text, cursor)
					if len(s) > 0 {
						s = normalize(s)
						collected = append(collected, s)
					} else {
						break
					}
				}
			}
		} else {
			if i == -1 {
				// we have array
				cursor = ii
				// array
				for {
					s, cursor = array(text, cursor)
					if len(s) > 0 {
						s = normalize(s)
						collected = append(collected, s)
					} else {
						break
					}
				}
			} else {
				// we have object or objects
				cursor = i
				// object
				for {
					s, cursor = object(text, cursor)
					if len(s) > 0 {
						s = normalize(s)
						collected = append(collected, s)
					} else {
						break
					}
				}
			}
		}
	}
	if len(collected) > 0 {
		if len(collected) > 1 {
			response = collected
		} else {
			response = collected[0]
		}
	}
	return
}

func object(text string, startIdx int) (response string, lastIdx int) {
	buff := new(strings.Builder)
	count := 0
	openBracket := false
	lastIdx = startIdx
	for i, c := range text {
		s := string(c)
		if i < startIdx {
			continue
		}
		if open_square == s && count == 0 {
			// error
			// cannot exists square bracket as starting point
			lastIdx = startIdx
			return
		}
		if open_graph == s {
			count++
		}
		if braket == s {
			openBracket = !openBracket
		}
		if count == 0 || (!openBracket && strings.Index(json_chars, s) == -1) {
			continue
		}

		buff.WriteString(s)
		if close_graph == s {
			if count == 1 {
				// end
				response = buff.String()
				lastIdx = i + 1
				return
			} else {
				count--
			}
		}

	}
	return
}

func array(text string, startIdx int) (response string, lastIdx int) {
	buff := new(strings.Builder)
	count := 0
	openBracket := false
	lastIdx = startIdx
	for i, c := range text {
		s := string(c)
		if i < startIdx {
			continue
		}
		if open_graph == s && count == 0 {
			// error
			// cannot exists square bracket as starting point
			lastIdx = startIdx
			return
		}
		if open_square == s {
			count++
		}
		if braket == s {
			openBracket = !openBracket
		}
		if count == 0 || (!openBracket && strings.Index(json_chars, s) == -1) {
			continue
		}
		buff.WriteString(s)
		if close_square == s {
			if count == 1 {
				// end
				response = buff.String()
				lastIdx = i + 1
				return
			} else {
				count--
			}
		}
	}
	return
}

func placeholder(text string) string {
	if len(text) > 0 {
		for k, v := range placeholders {
			text = strings.ReplaceAll(text, k, v)
		}
		/**
		// null must become ""
		text = strings.ReplaceAll(text, " null,", "\"_null_\",")
		text = strings.ReplaceAll(text, ":null,", "\"_null_\",")
		text = strings.ReplaceAll(text, "null\n", "\"_null_\"\n")
		// \" escape
		text = strings.ReplaceAll(text, "\\\"", "'")*/
	}
	return text
}

func normalize(text string) string {
	for k, v := range placeholders {
		text = strings.ReplaceAll(text, v, k)
	}
	/**
	if strings.Index(text, "\"_null_\"") > -1 {
		text = strings.ReplaceAll(text, "\"_null_\"", "null")
	}*/
	return text
}
