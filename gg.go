package gg

import (
	"bitbucket.org/digi-sense/gg-core/gg_algo/gg_shamir"
	"bitbucket.org/digi-sense/gg-core/gg_algo/gg_stegano"
	"bitbucket.org/digi-sense/gg-core/gg_app"
	"bitbucket.org/digi-sense/gg-core/gg_coding/openssl"
	"bitbucket.org/digi-sense/gg-core/gg_country"
	"bitbucket.org/digi-sense/gg-core/gg_deployer"
	"bitbucket.org/digi-sense/gg-core/gg_email"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_exec"
	"bitbucket.org/digi-sense/gg-core/gg_exec_bucket"
	"bitbucket.org/digi-sense/gg-core/gg_exec_shell"
	"bitbucket.org/digi-sense/gg-core/gg_fnvars"
	"bitbucket.org/digi-sense/gg-core/gg_fs"
	"bitbucket.org/digi-sense/gg-core/gg_generators/genusers"
	"bitbucket.org/digi-sense/gg-core/gg_i18n_bundle"
	"bitbucket.org/digi-sense/gg-core/gg_img"
	"bitbucket.org/digi-sense/gg-core/gg_json"
	"bitbucket.org/digi-sense/gg-core/gg_license"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_math"
	"bitbucket.org/digi-sense/gg-core/gg_mini"
	"bitbucket.org/digi-sense/gg-core/gg_net"
	"bitbucket.org/digi-sense/gg-core/gg_nio"
	"bitbucket.org/digi-sense/gg-core/gg_observable"
	"bitbucket.org/digi-sense/gg-core/gg_rnd"
	"bitbucket.org/digi-sense/gg-core/gg_stopwatch"
	"bitbucket.org/digi-sense/gg-core/gg_structs"
	"bitbucket.org/digi-sense/gg-core/gg_sys"
	"bitbucket.org/digi-sense/gg-core/gg_sys_buildinfo"
	"bitbucket.org/digi-sense/gg-core/gg_tagcloud"
	"bitbucket.org/digi-sense/gg-core/gg_templating"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-core/gg_utils/gg_shellquote"
	"bitbucket.org/digi-sense/gg-core/gg_utils/phonenumbers"
	"bitbucket.org/digi-sense/gg-core/gg_vcal"
	"bitbucket.org/digi-sense/gg-core/gg_vfs"
	"bitbucket.org/digi-sense/gg-core/gg_watchdog"
	"bitbucket.org/digi-sense/gg-core/gg_xtend"
	"bitbucket.org/digi-sense/gg-core/gg_zip"
)

var Strings *gg_utils.StringsHelper
var Arrays *gg_utils.ArraysHelper
var Convert *gg_utils.ConversionHelper
var Compare *gg_utils.CompareHelper
var Dates *gg_utils.DatesHelper
var Errors *gg_errors.ErrorsHelper
var Regex *gg_utils.RegexHelper
var Rnd *gg_rnd.RndHelper
var Paths *gg_utils.PathsHelper
var IO *gg_utils.IoHelper
var Zip *gg_zip.ZipHelper
var Maps *gg_utils.MapsHelper
var JSON *gg_json.JSONHelper
var XML *gg_utils.XMLHelper
var Reflect *gg_utils.ReflectHelper
var CSV *gg_utils.CsvHelper
var Async *gg_utils.AsyncHelper
var Coding *gg_utils.CodingHelper
var BOM *gg_utils.BOMHelper
var Sys *gg_sys.SysHelper
var SysBuildInfo *gg_sys_buildinfo.SysBuildInfoHelper
var Formatter *gg_utils.FormatterHelper
var Math *gg_math.MathHelper
var MIME *gg_utils.MIMEHelper
var Exec *gg_exec.ExecHelper
var Shell *gg_exec_shell.ShellHelper
var StopWatch *gg_stopwatch.StopWatchHelper
var FnVars *gg_fnvars.FnVarsHelper
var GenUsers *genusers.GenUsersHelper
var PhoneNumber *phonenumbers.PhoneNumberHelper
var VCal *gg_vcal.VCalHelper
var Structs *gg_structs.StructHelper
var Stegano *gg_stegano.SteganoHelper
var Shamir *gg_shamir.ShamirHelper
var OpenSSL *openssl.OpenSSLHelper
var Log *gg_log.LogHelper
var Bucket *gg_exec_bucket.BucketHelper
var Dir *gg_utils.DirHelper
var License *gg_license.LicenseHelper
var Net *gg_net.NetHelper
var Xtend *gg_xtend.XtendHelper
var Observable *gg_observable.ObservableHelper
var Images *gg_img.ImageHelper
var ShellQuote *gg_shellquote.ShellQuoteHelper
var TagCloud *gg_tagcloud.TagCloudHelper
var VFS *gg_vfs.VFSHelper
var Deployer *gg_deployer.DeployerHelper
var Templating *gg_templating.TemplatingHelper
var Minify *gg_mini.MinifyHelper
var Fs *gg_fs.FsHelper
var Country *gg_country.CountryHelper

//-- advanced --//

var App *gg_app.AppCtrlHelper
var I18N *gg_i18n_bundle.I18NHelper
var Events *gg_events.EventsHelper
var NIO *gg_nio.NIOHelper
var Email *gg_email.EmailHelper
var Watchdog *gg_watchdog.WatchdogHelper

func init() {
	Strings = gg_utils.Strings
	Arrays = gg_utils.Arrays
	Convert = gg_utils.Convert
	Compare = gg_utils.Compare
	Dates = gg_utils.Dates
	Errors = gg_errors.Errors
	Regex = gg_utils.Regex
	Rnd = gg_rnd.Rnd
	Paths = gg_utils.Paths
	IO = gg_utils.IO
	Zip = gg_zip.Zip
	Maps = gg_utils.Maps
	JSON = gg_json.JSON
	XML = gg_utils.XML
	Reflect = gg_utils.Reflect
	CSV = gg_utils.CSV
	Async = gg_utils.Async
	Coding = gg_utils.Coding
	BOM = gg_utils.BOM
	Sys = gg_sys.Sys
	SysBuildInfo = gg_sys_buildinfo.SysBuildInfo
	Formatter = gg_utils.Formatter
	Math = gg_math.Math
	MIME = gg_utils.MIME
	Exec = gg_exec.Exec
	Shell = gg_exec_shell.Shell
	StopWatch = gg_stopwatch.Watch
	FnVars = gg_fnvars.FnVars
	GenUsers = genusers.GenUsers
	VCal = gg_vcal.VCal
	Structs = gg_structs.Structs
	Stegano = gg_stegano.Stegano
	Shamir = gg_shamir.Shamir
	OpenSSL = openssl.OpenSSLUtil
	Log = gg_log.Log
	Bucket = gg_exec_bucket.Bucket
	Dir = gg_utils.Dir
	PhoneNumber = phonenumbers.PhoneNumber
	License = gg_license.License
	Net = gg_net.Net
	Xtend = gg_xtend.Xtend
	Observable = gg_observable.Observable
	Images = gg_img.Images
	ShellQuote = gg_shellquote.ShellQuote
	TagCloud = gg_tagcloud.TagCloud
	VFS = gg_vfs.VFS
	Deployer = gg_deployer.Deployer
	Templating = gg_templating.Templating
	Minify = gg_mini.Minify
	Fs = gg_fs.Fs
	Country = gg_country.Country

	// advanced
	App = gg_app.App
	I18N = gg_i18n_bundle.I18N
	Events = gg_events.Events
	NIO = gg_nio.NIO
	Email = gg_email.Email
	Watchdog = gg_watchdog.Watchdog

}
