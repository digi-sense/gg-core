package gg_utils

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"
)

type FileWriterOptions struct {
	Filename       string `json:"filename"`
	Append         bool   `json:"append"`
	RotateDir      string `json:"rotate-dir"`       // if assigned, write old files into this dir
	RotateDirLimit int    `json:"rotate-dir-limit"` // max number of files in dir
	RotateMode     string `json:"rotate-mode"`      // size:BYTES, time:SECONDS
}

//----------------------------------------------------------------------------------------------------------------------
//	FileRotator
//----------------------------------------------------------------------------------------------------------------------

type FileWriter struct {
	mux         *sync.Mutex
	options     *FileWriterOptions
	initialized bool // late initialized

	fileDir                string
	fileName               string
	rotatedAt              time.Time
	rotatedBeforeLastWrite bool
}

func NewFileWriter(args ...interface{}) (instance *FileWriter, err error) {
	instance = new(FileWriter)
	instance.mux = new(sync.Mutex)
	err = instance.init(args...)
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	o p t i o n s
//----------------------------------------------------------------------------------------------------------------------

func (instance *FileWriter) Options() (response *FileWriterOptions) {
	if nil != instance {
		if nil == instance.options {
			instance.options = new(FileWriterOptions)
			instance.options.Append = true
			instance.options.RotateDirLimit = 100
		}
		response = instance.options
	}
	return
}
func (instance *FileWriter) SetFilename(value string) *FileWriter {
	if nil != instance {
		instance.Options().Filename = value
	}
	return instance
}
func (instance *FileWriter) SetRotateDir(value string) *FileWriter {
	if nil != instance {
		instance.Options().RotateDir = value
	}
	return instance
}
func (instance *FileWriter) SetRotateDirLimit(value int) *FileWriter {
	if nil != instance {
		instance.Options().RotateDirLimit = value
	}
	return instance
}
func (instance *FileWriter) SetRotateMode(value string) *FileWriter {
	if nil != instance {
		instance.Options().RotateMode = value
	}
	return instance
}
func (instance *FileWriter) SetAppend(value bool) *FileWriter {
	if nil != instance {
		instance.Options().Append = value
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *FileWriter) String() string {
	if nil != instance {
		return gg_.Stringify(instance.Options())
	}
	return ""
}

func (instance *FileWriter) HasRotatedBeforeLastWrite() bool {
	if nil != instance {
		return instance.rotatedBeforeLastWrite
	}
	return false
}

func (instance *FileWriter) LastRotation() time.Time {
	if nil != instance {
		return instance.rotatedAt
	}
	return time.Time{}
}

func (instance *FileWriter) Write(data []byte) (n int, err error) {
	if nil != instance && instance.initialize() {
		_, n, err = instance.write(data)
	}
	return
}

func (instance *FileWriter) Rotate(force bool) (err error) {
	if nil != instance && instance.initialize() {
		_, _, err = instance.rotate(force)
	}
	return
}

func (instance *FileWriter) RotateNow() (err error) {
	if nil != instance && instance.initialize() {
		_, _, err = instance.rotate(true)
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *FileWriter) init(args ...interface{}) (err error) {
	if nil != instance {
		if len(args) == 1 {
			arg := args[0]
			// string
			if s, ok := arg.(string); ok {
				if Regex.IsValidJsonObject(s) {
					err = gg_.Unmarshal(s, &instance.options)
				} else {
					if Paths.ExtensionName(s) == "json" {
						err = gg_.UnmarshalFromFile(s, &instance.options)
					} else {
						instance.Options().Filename = s
					}
				}
				return
			}
			// map
			if m, ok := arg.(map[string]interface{}); ok {
				err = gg_.Unmarshal(m, &instance.options)
				return
			}
			// options
			if options, ok := arg.(*FileWriterOptions); ok {
				instance.options = options
				return
			}
			if options, ok := arg.(FileWriterOptions); ok {
				instance.options = &options
				return
			}
		}
	}
	return
}

func (instance *FileWriter) initialize() bool {
	if nil != instance {
		var err error
		if !instance.initialized {
			instance.initialized = true
			// initialize object reading options
			options := instance.Options()
			if len(options.Filename) == 0 {
				panic(errors.New("missing_filename"))
			}

			options.Filename = Paths.Absolute(options.Filename)
			err = Paths.Mkdir(options.Filename)
			if nil == err {
				instance.fileDir = Paths.Dir(options.Filename)
				instance.fileName = Paths.FileName(options.Filename, true)

				if len(options.RotateDir) > 0 {
					options.RotateDir = Paths.Absolute(options.RotateDir)
				} else {
					options.RotateDir = instance.fileDir
				}
				if exists, _ := Paths.Exists(options.RotateDir + OS_PATH_SEPARATOR); !exists {
					_ = Paths.Mkdir(instance.options.RotateDir + OS_PATH_SEPARATOR)
				}
			}
		}
		return nil == err
	}
	return false
}

func (instance *FileWriter) filename() (response string) {
	if nil != instance {
		return Paths.Concat(instance.fileDir, instance.fileName)
	}
	return ""
}

func (instance *FileWriter) rotate(force bool) (rotated bool, filename string, err error) {
	if nil != instance {
		filename = instance.filename()
		options := instance.options
		if len(options.RotateDir) > 0 || len(options.RotateMode) > 0 {
			if force {
				rotated = true
				instance.rotatedAt = time.Now()
				instance.rotatedBeforeLastWrite = true
				err = rotateDir(filename, options.RotateDir, options.RotateDirLimit)
			} else {
				rotateMode, rotateParam := splitRotateMode(options.RotateMode)
				// MODE
				{
					switch rotateMode {
					case "size":
						maxSize := Convert.ToInt64(rotateParam)
						if filesize(filename) >= maxSize {
							rotated = true
							instance.rotatedAt = time.Now()
							instance.rotatedBeforeLastWrite = true
							err = rotateDir(filename, options.RotateDir, options.RotateDirLimit)
						}
					case "time":
						maxSeconds := Convert.ToInt(rotateParam)
						timeDiff := time.Now().Sub(filetime(filename))
						if timeDiff > time.Duration(maxSeconds)*time.Second {
							rotated = true
							instance.rotatedAt = time.Now()
							instance.rotatedBeforeLastWrite = true
							err = rotateDir(filename, options.RotateDir, options.RotateDirLimit)
						}
					default:
						// not supported
					}
				}
			}
		} else {
			// no rotation
			rotated = false
			filename = instance.filename()
		}
	}
	return
}

func (instance *FileWriter) write(data []byte) (rotated bool, n int, err error) {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		// reset flags
		instance.rotatedBeforeLastWrite = false

		var filename string
		rotated, filename, err = instance.rotate(false)
		if len(filename) > 0 {
			if instance.options.Append {
				n, err = IO.AppendBytesToFile(data, filename)
			} else {
				n, err = IO.WriteBytesToFile(data, filename)
			}
		} else {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, "Missing filename: ")
		}
	}
	return
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func splitRotateMode(mode string) (rotateMode, rotateParam string) {
	tokens := strings.Split(mode, ":")
	if len(tokens) == 2 {
		rotateMode = tokens[0]
		rotateParam = tokens[1]
	}
	return
}

func filesize(filename string) (response int64) {
	response, _ = IO.FileSize(filename)
	return
}

func filetime(filename string) (response time.Time) {
	response, _ = IO.FileModTime(filename)
	return
}

func rotateDir(filename, dir string, limit int) (err error) {
	newFilename := Paths.ChangeFileNameWithSuffix(filename, Dates.FormatDate(time.Now(), "-yyyy-MM-dd-HH-mm-ss"))
	newFilename = Paths.Concat(dir, Paths.FileName(newFilename, true))
	_, err = IO.CopyFile(filename, newFilename)
	if nil != err {
		return
	}
	// remove existing
	err = IO.Remove(filename)
	if nil != err {
		return
	}

	// list files
	if limit > 0 {
		fname := Paths.FileName(filename, false)
		ext := Paths.ExtensionName(filename)
		if len(ext) == 0 {
			ext = "*"
		}
		files, _ := Paths.ListFiles(dir, fmt.Sprintf("%s*.%s", fname, ext))
		diff := len(files) - limit
		if diff > 0 {
			count := 0
			for i := 0; i < len(files); i++ {
				count++
				_ = IO.Remove(files[i])
				if count >= diff {
					break
				}
			}
		}
	}
	return
}
