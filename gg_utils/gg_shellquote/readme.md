# ShellQuote

ShellQuote provides utilities for joining/splitting strings using sh's
word-splitting rules.

Functions:
- Join: quote and join with spaces
- Split: splits a string according to /bin/sh's word-splitting rules.

This package has been forked from the original work of Kevin Ballard:
https://github.com/kballard/go-shellquote
