package gg_utils

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"reflect"
)

type ReflectHelper struct {
}

var Reflect *ReflectHelper

func init() {
	Reflect = new(ReflectHelper)
}

func (instance *ReflectHelper) ValueOf(item interface{}) reflect.Value {
	return gg_.ValueOf(item)
}

func (instance *ReflectHelper) InterfaceOf(item interface{}) interface{} {
	return gg_.ValueOf(item).Interface()
}

func (instance *ReflectHelper) Get(object interface{}, name string) interface{} {
	return gg_.Get(object, name)
}

func (instance *ReflectHelper) GetString(object interface{}, name string) string {
	return gg_.GetString(object, name)
}

func (instance *ReflectHelper) GetBytes(object interface{}, name string) []byte {
	return gg_.GetBytes(object, name)
}

func (instance *ReflectHelper) GetInt(object interface{}, name string) int {
	return gg_.GetInt(object, name)
}

func (instance *ReflectHelper) GetInt8(object interface{}, name string) int8 {
	return gg_.GetInt8(object, name)
}

func (instance *ReflectHelper) GetInt16(object interface{}, name string) int16 {
	return gg_.GetInt16(object, name)
}

func (instance *ReflectHelper) GetInt32(object interface{}, name string) int32 {
	return gg_.GetInt32(object, name)
}

func (instance *ReflectHelper) GetInt64(object interface{}, name string) int64 {
	return gg_.GetInt64(object, name)
}

func (instance *ReflectHelper) GetUint(object interface{}, name string) uint {
	return gg_.GetUint(object, name)
}

func (instance *ReflectHelper) GetUint8(object interface{}, name string) uint8 {
	return gg_.GetUint8(object, name)
}

func (instance *ReflectHelper) GetUint16(object interface{}, name string) uint16 {
	return gg_.GetUint16(object, name)
}

func (instance *ReflectHelper) GetUint32(object interface{}, name string) uint32 {
	return gg_.GetUint32(object, name)
}

func (instance *ReflectHelper) GetUint64(object interface{}, name string) uint64 {
	return gg_.GetUint64(object, name)
}

func (instance *ReflectHelper) GetFloat32(object interface{}, name string) float32 {
	return gg_.GetFloat32(object, name)
}

func (instance *ReflectHelper) GetFloat64(object interface{}, name string) float64 {
	return gg_.GetFloat64(object, name)
}

func (instance *ReflectHelper) GetBool(object interface{}, name string) bool {
	return gg_.GetBool(object, name)
}

func (instance *ReflectHelper) GetArray(object interface{}, name string) []interface{} {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToArray(v)
	}
	return []interface{}{}
}

func (instance *ReflectHelper) GetArrayOfString(object interface{}, name string) []string {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToArrayOfString(v)
	}
	return []string{}
}

func (instance *ReflectHelper) GetArrayOfInt(object interface{}, name string) []int {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToArrayOfInt(v)
	}
	return make([]int, 0)
}

func (instance *ReflectHelper) GetArrayOfInt8(object interface{}, name string) []int8 {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToArrayOfInt8(v)
	}
	return make([]int8, 0)
}

func (instance *ReflectHelper) GetArrayOfInt16(object interface{}, name string) []int16 {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToArrayOfInt16(v)
	}
	return make([]int16, 0)
}

func (instance *ReflectHelper) GetArrayOfInt32(object interface{}, name string) []int32 {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToArrayOfInt32(v)
	}
	return make([]int32, 0)
}

func (instance *ReflectHelper) GetArrayOfInt64(object interface{}, name string) []int64 {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToArrayOfInt64(v)
	}
	return make([]int64, 0)
}

func (instance *ReflectHelper) GetArrayOfByte(object interface{}, name string) []byte {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToArrayOfByte(v)
	}
	return make([]byte, 0)
}

func (instance *ReflectHelper) GetMap(object interface{}, name string) map[string]interface{} {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToMap(v)
	}
	return make(map[string]interface{})
}

func (instance *ReflectHelper) GetMapOfString(object interface{}, name string) map[string]string {
	v := instance.Get(object, name)
	if nil != v {
		return Convert.ToMapOfString(v)
	}
	return make(map[string]string)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
