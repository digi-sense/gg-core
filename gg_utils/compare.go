package gg_utils

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"reflect"
	"strconv"
	"unicode"
)

type CompareHelper struct {
}

var Compare *CompareHelper

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

func init() {
	Compare = new(CompareHelper)
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *CompareHelper) Compare(item1, item2 interface{}) int {
	return gg_.Compare(item1, item2)
}

func (instance *CompareHelper) Equals(item1, item2 interface{}) bool {
	return gg_.Compare(item1, item2) == 0
}

func (instance *CompareHelper) NotEquals(val1, val2 interface{}) bool {
	return !instance.Equals(val1, val2)
}

func (instance *CompareHelper) IsZero(item interface{}) bool {
	if nil != item {
		i := Convert.ToIntDef(item, -1)
		if i == 0 {
			return true
		} else {
			return len(Convert.ToString(item)) == 0
		}
	}
	return true
}

func (instance *CompareHelper) IsGreater(item1, item2 interface{}) bool {
	return gg_.Compare(item1, item2) > 0
}

func (instance *CompareHelper) IsLower(item1, item2 interface{}) bool {
	return gg_.Compare(item1, item2) < 0
}

func (instance *CompareHelper) IsString(val interface{}) (bool, string) {
	s, b := gg_.IsString(val)
	return b, s
}

func (instance *CompareHelper) IsStringASCII(val string) bool {
	for i := 0; i < len(val); i++ {
		if val[i] > unicode.MaxASCII {
			return false
		}
	}
	return true
}

func (instance *CompareHelper) IsInt(val interface{}) (bool, int) {
	v, vv := val.(int)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsIn8(val interface{}) (bool, int8) {
	v, vv := val.(int8)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsIn16(val interface{}) (bool, int16) {
	v, vv := val.(int16)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsIn32(val interface{}) (bool, int32) {
	v, vv := val.(int32)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsIn64(val interface{}) (bool, int64) {
	v, vv := val.(int64)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsBool(val interface{}) (bool, bool) {
	v, vv := val.(bool)
	if vv {
		return true, v
	}
	return false, false
}

func (instance *CompareHelper) IsFloat32(val interface{}) (bool, float32) {
	v, vv := val.(float32)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsFloat64(val interface{}) (bool, float64) {
	v, vv := val.(float64)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsUint(val interface{}) (bool, uint) {
	v, vv := val.(uint)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsUint8(val interface{}) (bool, uint8) {
	v, vv := val.(uint8)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsUint16(val interface{}) (bool, uint16) {
	v, vv := val.(uint16)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsUint32(val interface{}) (bool, uint32) {
	v, vv := val.(uint32)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsUint64(val interface{}) (bool, uint64) {
	v, vv := val.(uint64)
	if vv {
		return true, v
	}
	return false, 0
}

func (instance *CompareHelper) IsNumeric(val interface{}) (bool, float64) {
	switch i := val.(type) {
	case float32:
		return true, float64(i)
	case float64:
		return true, i
	case int:
		return true, float64(i)
	case int8:
		return true, float64(i)
	case int16:
		return true, float64(i)
	case int32:
		return true, float64(i)
	case int64:
		return true, float64(i)
	case uint:
		return true, float64(i)
	case uint8:
		return true, float64(i)
	case uint16:
		return true, float64(i)
	case uint32:
		return true, float64(i)
	case uint64:
		return true, float64(i)
	case string:
		v, err := strconv.ParseFloat(i, 64)
		if nil == err {
			return true, v
		}
	}
	return false, 0
}

func (instance *CompareHelper) IsArray(val interface{}) (bool, reflect.Value) {
	return gg_.IsArrayValue(val)
}

func (instance *CompareHelper) IsArrayNotEmpty(array interface{}) (bool, reflect.Value) {
	return gg_.IsArrayValueNotEmpty(array)
}

func (instance *CompareHelper) IsMap(val interface{}) (bool, reflect.Value) {
	return gg_.IsMapValue(val)
}

func (instance *CompareHelper) IsStruct(val interface{}) (bool, reflect.Value) {
	return gg_.IsStructValue(val)
}

func (instance *CompareHelper) IsPtrValue(val interface{}) bool {
	v := reflect.ValueOf(val)
	return v.Kind() == reflect.Ptr
}

func (instance *CompareHelper) IsUintPtrValue(val interface{}) bool {
	v := reflect.ValueOf(val)
	return v.Kind() == reflect.Uintptr
}

func (instance *CompareHelper) IsIntValue(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Int
}

func (instance *CompareHelper) IsInt8Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Int8
}

func (instance *CompareHelper) IsInt16Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Int16
}

func (instance *CompareHelper) IsInt32Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Int32
}

func (instance *CompareHelper) IsInt64Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Int64
}

func (instance *CompareHelper) IsUintValue(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Uint
}

func (instance *CompareHelper) IsUint8Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Uint8
}

func (instance *CompareHelper) IsUint16Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Uint16
}

func (instance *CompareHelper) IsUint32Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Uint32
}

func (instance *CompareHelper) IsFloat32Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Float32
}

func (instance *CompareHelper) IsFloat64Value(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Float64
}

func (instance *CompareHelper) IsBoolValue(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Bool
}

func (instance *CompareHelper) IsStringValue(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.String
}

func (instance *CompareHelper) IsStructValue(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Struct
}

func (instance *CompareHelper) IsMapValue(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Map
}

func (instance *CompareHelper) IsArrayValue(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Array
}

func (instance *CompareHelper) IsSliceValue(val interface{}) bool {
	v := Reflect.ValueOf(val)
	return v.Kind() == reflect.Slice
}
