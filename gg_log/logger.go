package gg_log

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"
)

const (
	outConsole = "console"
	outFile    = "file"
)

type logMessage struct {
	level Level
	args  []interface{}
}

type Logger struct {
	filename       string
	level          Level
	mux            sync.Mutex
	channel        chan *logMessage
	initialized    bool
	startReceiving bool
	datePattern    string
	messagePattern string
	rotateEnable   bool
	rotateSizeMb   float64
	rotateDir      string
	rotateMaxFiles int
	outputs        []string
	writer         *gg_utils.FileWriter

	rotatedAt            time.Time
	rotatedBeforeLastLog bool
}

func NewLogger() *Logger {
	instance := new(Logger)
	instance.datePattern = DateFormatStandard
	instance.messagePattern = pattern
	instance.rotateMaxFiles = 10
	instance.RotateMaxSizeMb(1)
	instance.outputs = make([]string, 0)

	// init the buffered channel
	instance.channel = make(chan *logMessage, 10)
	go instance.receive(instance.channel)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Logger) String() string {
	m := map[string]interface{}{
		"level":        instance.level.String(),
		"outputs":      instance.outputs,
		"filename":     instance.filename,
		"rotate":       instance.rotateEnable,
		"rotate_limit": instance.rotateSizeMb,
	}
	return gg_.Stringify(m)
}

func (instance *Logger) GetLevel() Level {
	if nil != instance {
		return instance.level
	}
	return InfoLevel
}

func (instance *Logger) SetLevel(v interface{}) *Logger {
	if nil != instance {
		if level, ok := v.(Level); ok {
			instance.level = level
		} else if level, ok := v.(*Level); ok {
			instance.level = *level
		} else if level, ok := v.(string); ok {
			instance.level, _ = ParseLevel(level)
		}
	}
	return instance
}

func (instance *Logger) GetFilename() string {
	if nil != instance {
		return instance.filename
	}
	return ""
}

func (instance *Logger) SetFilename(filename string) *Logger {
	if nil != instance {
		if len(filename) == 0 {
			filename = "logging.log"
		}
		root := gg_utils.Paths.WorkspacePath("./logging")
		instance.filename = gg_utils.Paths.Absolutize(filename, root)

		instance.rotateDir = gg_utils.Paths.Concat(gg_utils.Paths.Dir(instance.filename), "log-rotate")
	}
	return instance
}

func (instance *Logger) OutConsole(value bool) *Logger {
	if nil != instance {
		if value {
			instance.outputs = gg_utils.Arrays.AppendUnique(instance.outputs, outConsole).([]string)
		} else {
			instance.outputs = gg_utils.Arrays.Remove(outConsole, instance.outputs).([]string)
		}
	}
	return instance
}

func (instance *Logger) OutFile(value bool) *Logger {
	if nil != instance {
		if value {
			instance.outputs = gg_utils.Arrays.AppendUnique(instance.outputs, outFile).([]string)
		} else {
			instance.outputs = gg_utils.Arrays.Remove(outFile, instance.outputs).([]string)
		}
	}
	return instance
}

func (instance *Logger) RotateEnable(value bool) *Logger {
	if nil != instance {
		instance.rotateEnable = value
	}
	return instance
}

func (instance *Logger) RotateMaxSizeMb(value float64) *Logger {
	if nil != instance {
		instance.rotateSizeMb = value
		instance.rotateEnable = value > 0
	}
	return instance
}

// Rotate force file rotation
func (instance *Logger) Rotate() (err error) {
	if nil != instance {
		instance.initialize()
		instance.Info("* Force rotating log file ...")
		if nil != instance.writer {
			err = instance.writer.Rotate(true)
		}
	}
	return
}

func (instance *Logger) LastRotationTime() time.Time {
	if nil != instance {
		return instance.rotatedAt
	}
	return time.Time{}
}

func (instance *Logger) HasRotatedBeforeLastWrite() bool {
	if nil != instance {
		return instance.rotatedBeforeLastLog
	}
	return false
}

func (instance *Logger) SetDateFormat(format string) *Logger {
	if nil != instance {
		instance.datePattern = format
	}
	return instance
}

func (instance *Logger) GetDateFormat() (response string) {
	if nil != instance {
		response = instance.datePattern
	}
	return
}

func (instance *Logger) SetMessageFormat(format string) *Logger {
	if nil != instance {
		instance.messagePattern = format
	}
	return instance
}

func (instance *Logger) GetMessageFormat() (response string) {
	if nil != instance {
		response = instance.messagePattern
	}
	return
}

func (instance *Logger) Close() {
	if nil != instance.channel {
		close(instance.channel)
	}
}

func (instance *Logger) Flush() {
	if nil != instance {
		// TODO: add a flush function
		time.Sleep(1 * time.Second)
	}
}

func (instance *Logger) Panic(args ...interface{}) {
	message := new(logMessage)
	message.level = PanicLevel
	message.args = args
	if nil != instance.channel {
		instance.channel <- message
	}
}

func (instance *Logger) Panicf(message string, args ...interface{}) {
	if nil != instance {
		instance.Panic(fmt.Sprintf(message, args...))
	}
}

func (instance *Logger) Error(args ...interface{}) {
	message := new(logMessage)
	message.level = ErrorLevel
	message.args = args
	if nil != instance.channel {
		instance.channel <- message
	}
}

func (instance *Logger) Errorf(message string, args ...interface{}) {
	if nil != instance {
		instance.Error(fmt.Sprintf(message, args...))
	}
}

func (instance *Logger) Warn(args ...interface{}) {
	message := new(logMessage)
	message.level = WarnLevel
	message.args = args
	if nil != instance.channel {
		instance.channel <- message
	}
}

func (instance *Logger) Warnf(message string, args ...interface{}) {
	if nil != instance {
		instance.Warn(fmt.Sprintf(message, args...))
	}
}

func (instance *Logger) Info(args ...interface{}) {
	message := new(logMessage)
	message.level = InfoLevel
	message.args = args
	if nil != instance.channel {
		instance.channel <- message
	}
}

func (instance *Logger) Infof(message string, args ...interface{}) {
	if nil != instance {
		instance.Info(fmt.Sprintf(message, args...))
	}
}

func (instance *Logger) Debug(args ...interface{}) {
	message := new(logMessage)
	message.level = DebugLevel
	message.args = args
	if nil != instance.channel {
		instance.channel <- message
	}
}

func (instance *Logger) Debugf(message string, args ...interface{}) {
	if nil != instance {
		instance.Debug(fmt.Sprintf(message, args...))
	}
}

func (instance *Logger) Trace(args ...interface{}) {
	message := new(logMessage)
	message.level = TraceLevel
	message.args = args
	if nil != instance.channel {
		instance.channel <- message
	}
}

func (instance *Logger) Tracef(message string, args ...interface{}) {
	if nil != instance {
		instance.Trace(fmt.Sprintf(message, args...))
	}
}

func (instance *Logger) Join() {
	if nil != instance {
		for {
			instance.initialize()
			if instance.startReceiving && instance.initialized {
				time.Sleep(500 * time.Millisecond)
				return
			} else {
				time.Sleep(100 * time.Millisecond)
			}
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Logger) initialize() {
	if !instance.initialized {
		instance.initialized = true

		if len(instance.outputs) == 0 {
			instance.outputs = append(instance.outputs, outFile) // default is on file
		}

		if instance.canLogToFile() {
			// set logging file
			if len(instance.filename) == 0 {
				instance.SetFilename("") // default filename
			}

			// create a file writer
			writer, _ := gg_utils.NewFileWriter()
			writer.
				SetFilename(instance.filename).
				SetRotateDirLimit(-1)
			if instance.rotateEnable {
				writer.
					SetRotateDir(instance.rotateDir).
					SetRotateDirLimit(instance.rotateMaxFiles).
					SetRotateMode(fmt.Sprintf("size:%v", instance.rotateSizeMb*1024*1024))
				_ = writer.Rotate(false)
			}
			instance.writer = writer
		}
	}
}

func (instance *Logger) receive(ch <-chan *logMessage) {
	if nil != instance {
		instance.startReceiving = true
		// loop until channel is open
		for message := range ch {
			if nil != instance {
				instance.doLog(message.level, message.args...)
			}
		}
	}
}

func (instance *Logger) doLog(level Level, args ...interface{}) {
	instance.initialize()

	if b := instance.level < level; b {
		return
	}
	var buf strings.Builder
	for i, arg := range args {
		if i > 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(fmt.Sprintf("%v", arg))
	}
	buf.WriteString("\n")

	m := map[string]interface{}{
		"level":   strings.ToUpper(level.String()),
		"message": buf.String(),
	}
	if len(instance.datePattern) > 0 {
		m["date"] = gg_utils.Dates.FormatDate(time.Now(), instance.datePattern)
	}
	message, err := gg_utils.Formatter.MergeText(instance.messagePattern, m)
	if nil != err {
		log.Panicln(fmt.Sprintf("Panic error formatting log message: '%s'", err))
	}

	instance.writeOutput(message)
}

func (instance *Logger) canLogToFile() bool {
	if nil != instance {
		return len(instance.filename) > 0 && gg_utils.Arrays.IndexOf(outFile, instance.outputs) > -1
	}
	return false
}

func (instance *Logger) canLogToConsole() bool {
	if nil != instance {
		return len(instance.filename) == 0 || gg_utils.Arrays.IndexOf(outConsole, instance.outputs) > -1
	}
	return false
}

func (instance *Logger) writeOutput(text string) {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg_utils.Strings.Format("[panic] logger.writeOutput('%s'): '%s'", text, r)
			log.Println(message)
		}
	}()

	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		if instance.canLogToFile() && nil != instance.writer {
			_, err := instance.writer.Write([]byte(text))
			if nil != err {
				log.Panicln(fmt.Sprintf("Panic error writing log file '%s': '%s'", instance.filename, err))
			} else {
				instance.rotatedBeforeLastLog = instance.writer.HasRotatedBeforeLastWrite()
				instance.rotatedAt = instance.writer.LastRotation()
			}
		} else {
			log.Println("[UNABLE TO LOG ON FILE] ", text)
		}

		if instance.canLogToConsole() {
			log.Print(text)
		}
	}
}
