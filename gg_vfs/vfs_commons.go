package gg_vfs

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"time"
)

type IVFSConstructor func(args ...interface{}) (controller IVFSController, err error)

//----------------------------------------------------------------------------------------------------------------------
//	IVFSController
//----------------------------------------------------------------------------------------------------------------------

type IVFSController interface {
	Stat(path string) (*VfsFile, error)
	ListAll(dir string) ([]*VfsFile, error)
	Read(source string) ([]byte, error)
	Write(data []byte, target string) (int, error)
	Remove(source string) error
	MkDir(path string) error
	Exists(path string) (bool, error)
}

//----------------------------------------------------------------------------------------------------------------------
//	VfsFile
//----------------------------------------------------------------------------------------------------------------------

type VfsFile struct {
	AbsolutePath string    `json:"absolute-path"`
	RelativePath string    `json:"relative-path"`
	Root         string    `json:"root"`
	Name         string    `json:"name"`
	Size         int64     `json:"size"`
	ModTime      time.Time `json:"mod_time"`
	IsDir        bool      `json:"is_dir"`
	Mode         string    `json:"mode"`
}

func (instance *VfsFile) String() string {
	return gg_.Stringify(instance)
}

func (instance *VfsFile) SetRoot(path string) *VfsFile {
	if nil != instance && len(path) > 0 {
		instance.Root = path
		return instance.refreshRelativePath()
	}
	return instance
}

func (instance *VfsFile) SetAbsolutePath(path string) *VfsFile {
	if nil != instance && len(path) > 0 {
		instance.AbsolutePath = path
		return instance.refreshRelativePath()
	}
	return instance
}

func (instance *VfsFile) MatchOne(filters ...string) bool {
	if nil != instance {
		for _, filter := range filters {
			if len(gg_utils.Regex.WildcardMatch(instance.Name, filter)) > 0 {
				return true
			}
		}
	}
	return false
}

func (instance *VfsFile) MatchAll(filters ...string) bool {
	if nil != instance {
		for _, filter := range filters {
			if len(gg_utils.Regex.WildcardMatch(instance.Name, filter)) == 0 {
				return false
			}
		}
		return true
	}
	return false
}

func (instance *VfsFile) refreshRelativePath() *VfsFile {
	if nil != instance {
		if len(instance.Root) > 0 && len(instance.AbsolutePath) > 0 {
			instance.RelativePath = gg_utils.Paths.Relativize(instance.AbsolutePath, instance.Root)
		}
	}
	return instance
}
