package gg_vfs

import (
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"os"
	"path/filepath"
)

// VfsDriverOS ( Operating System )
type VfsDriverOS struct {
}

func NewVfsDriverOS(args ...interface{}) (controller IVFSController, err error) {
	controller = new(VfsDriverOS)
	return
}

func (instance *VfsDriverOS) Stat(path string) (response *VfsFile, err error) {
	if nil != instance {
		response, err = instance.NewFile(path)
	}
	return
}

func (instance *VfsDriverOS) ListAll(dir string) (response []*VfsFile, err error) {
	if nil != instance {
		response = make([]*VfsFile, 0)
		dir = gg_utils.Paths.Absolute(dir)

		var items []string
		items, err = gg_utils.Paths.ListAll(dir)
		if nil != err {
			return
		}

		for _, item := range items {
			var vfsFile *VfsFile
			vfsFile, err = instance.NewFile(item)
			if nil != err {
				return
			}
			response = append(response, vfsFile)
		}
	}
	return
}

func (instance *VfsDriverOS) Read(source string) (response []byte, err error) {
	if nil != instance {
		source = gg_utils.Paths.Absolute(source)

		response, err = gg_utils.IO.ReadBytesFromFile(source)
	}
	return
}

func (instance *VfsDriverOS) Write(data []byte, target string) (response int, err error) {
	if nil != instance {
		target = gg_utils.Paths.Absolute(target)
		response, err = gg_utils.IO.WriteBytesToFile(data, target)
	}
	return
}

func (instance *VfsDriverOS) Remove(source string) (err error) {
	if nil != instance {
		source = gg_utils.Paths.Absolute(source)
		err = gg_utils.IO.Remove(source)
	}
	return
}

func (instance *VfsDriverOS) MkDir(path string) (err error) {
	if nil != instance {
		path = gg_utils.Paths.Absolute(path)
		if gg_utils.Paths.IsDirPath(path) {
			err = gg_utils.Paths.Mkdir(path + gg_utils.OS_PATH_SEPARATOR)
		} else {
			err = gg_utils.Paths.Mkdir(path)
		}
	}
	return
}

func (instance *VfsDriverOS) Exists(path string) (response bool, err error) {
	if nil != instance {
		path = gg_utils.Paths.Absolute(path)
		response, err = gg_utils.Paths.Exists(path)
	}
	return
}

func (instance *VfsDriverOS) NewFile(path string) (response *VfsFile, err error) {
	path = gg_utils.Paths.Absolute(path)

	response = new(VfsFile)
	var fileInfo os.FileInfo
	fileInfo, err = os.Lstat(path)
	if nil != err {
		return
	}

	response.SetAbsolutePath(path)
	response.SetRoot(filepath.Dir(path))
	response.Name = fileInfo.Name()
	response.Size = fileInfo.Size()
	response.ModTime = fileInfo.ModTime()
	response.IsDir = fileInfo.IsDir()
	response.Mode = fileInfo.Mode().String()

	return
}
