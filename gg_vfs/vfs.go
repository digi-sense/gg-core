package gg_vfs

import (
	"bitbucket.org/digi-sense/gg-core/gg_"
	"bitbucket.org/digi-sense/gg-core/gg_errors"
	"fmt"
)

type VFSHelper struct {
	ctr map[string]IVFSConstructor
}

var VFS *VFSHelper

func init() {
	VFS = new(VFSHelper)
	VFS.ctr = make(map[string]IVFSConstructor)
	VFS.AddConstructor(DriverOS, NewVfsDriverOS)
}

const (
	DriverOS  = "os"
	DriverFTP = "ftp"
)

func (instance *VFSHelper) AddConstructor(driverName string, ctr func(args ...interface{}) (controller IVFSController, err error)) {
	if nil != instance {
		instance.ctr[driverName] = ctr
	}
}

func (instance *VFSHelper) New(driverName string, args ...interface{}) (response IVFSController, err error) {
	if nil != instance {
		if ctr, ok := instance.ctr[driverName]; ok {
			response, err = ctr(args...)
		} else {
			err = gg_errors.Errors.Prefix(gg_.PanicSystemError, fmt.Sprintf("Driver '%s' not supported: ", driverName))
		}
	}
	return
}

func (instance *VFSHelper) NewOS() (response IVFSController) {
	if nil != instance {
		response, _ = instance.New(DriverOS)
	}
	return
}
